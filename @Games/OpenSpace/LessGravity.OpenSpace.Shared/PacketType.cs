﻿namespace LessGravity.OpenSpace.Shared
{
    public enum PacketType : byte
    {
        Relay = 0,

        LoginRequest,
        LoginResponse,
        LogoutRequest,
        LogoutResponse,
        RegisterRequest,
        RegisterResponse,
        Chat,

        GetCharactersRequest,
        GetCharactersResponse,
        CreateCharacterRequest,
        CreateCharacterResponse,
        SelectCharacterRequest,
        SelectCharacterResponse,
        DeleteCharacterRequest,
        DeleteCharacterResponse
    }
}