﻿using Lidgren.Network;

namespace LessGravity.OpenSpace.Shared
{
    public abstract class Packet : IPacket
    {
        public abstract NetOutgoingMessage GetMessage(NetPeer peer);
    }
}