﻿using Lidgren.Network;

namespace LessGravity.OpenSpace.Shared
{
    public interface IPacket
    {
        NetOutgoingMessage GetMessage(NetPeer peer);
    }
}