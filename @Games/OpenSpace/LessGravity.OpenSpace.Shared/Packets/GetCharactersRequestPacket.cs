﻿using Lidgren.Network;

namespace LessGravity.OpenSpace.Shared.Packets
{
    public sealed class GetCharactersRequestPacket : Packet
    {
        public long UserId { get; set; }

        public override NetOutgoingMessage GetMessage(NetPeer peer)
        {
            var outgoingMessage = peer.CreateMessage();
            outgoingMessage.Write((byte)PacketType.GetCharactersRequest);
            outgoingMessage.Write(UserId);
            return outgoingMessage;
        }

        public GetCharactersRequestPacket() { }

        public GetCharactersRequestPacket(NetBuffer incomingMessage)
        {
            UserId = incomingMessage.ReadInt64();
        }
        
        public override string ToString()
        {
            return $"{GetType().Name} UserId: {UserId}";
        }
    }
}