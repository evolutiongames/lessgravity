﻿using Lidgren.Network;
using System;

namespace LessGravity.OpenSpace.Shared.Packets
{
    public sealed class ChatPacket : Packet
    {
        public string UserName { get; set; }
        public string Channel { get; set; }
        public string Message { get; set; }
        public DateTime MessageTimestamp { get; set; }

        public override NetOutgoingMessage GetMessage(NetPeer peer)
        {
            var outgoingMessage = peer.CreateMessage();
            outgoingMessage.Write((byte)PacketType.Chat);
            outgoingMessage.Write(UserName);
            outgoingMessage.Write(Channel);
            outgoingMessage.Write(Message);
            outgoingMessage.Write(MessageTimestamp.Ticks);
            return outgoingMessage;
        }

        public ChatPacket() { }

        public ChatPacket(NetBuffer incomingMessage)
        {
            UserName = incomingMessage.ReadString();
            Channel = incomingMessage.ReadString();
            Message = incomingMessage.ReadString();
            MessageTimestamp = new DateTime(incomingMessage.ReadInt64());
        }
    }
}