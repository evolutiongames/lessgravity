﻿using Lidgren.Network;
using System;

namespace LessGravity.OpenSpace.Shared.Packets
{
    public sealed class LoginResponsePacket : Packet
    {
        public LoginResult Result { get; set; }
        public DateTime LoginTimeStamp { get; set; }
        public DateTime LastLoginTimeStamp { get; set; }
        public long UserId { get; set; }

        public override NetOutgoingMessage GetMessage(NetPeer peer)
        {
            var outgoingMessage = peer.CreateMessage();
            outgoingMessage.Write((byte)PacketType.LoginResponse);
            outgoingMessage.Write((byte)Result);
            outgoingMessage.Write(UserId);
            outgoingMessage.Write((ulong)LoginTimeStamp.Ticks);
            outgoingMessage.Write((ulong)LastLoginTimeStamp.Ticks);
            return outgoingMessage;
        }

        public LoginResponsePacket() { }

        public LoginResponsePacket(NetBuffer incomingMessage)
        {
            Result = (LoginResult)incomingMessage.ReadByte();
            UserId = incomingMessage.ReadInt64();
            LoginTimeStamp = new DateTime((long)incomingMessage.ReadUInt64());
            LastLoginTimeStamp = new DateTime((long)incomingMessage.ReadUInt64());
        }
        
        public override string ToString()
        {
            return $"{GetType().Name} UserId: {UserId} Result: {Result}";
        }
    }
}