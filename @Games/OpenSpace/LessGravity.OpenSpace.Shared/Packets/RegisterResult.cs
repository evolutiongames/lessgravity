﻿namespace LessGravity.OpenSpace.Shared.Packets
{
    public enum RegisterResult
    {
        Success,
        FailureUserAlreadyExists,
        FailureInvalidPassword, // too long, too short, not complex enough
    }
}