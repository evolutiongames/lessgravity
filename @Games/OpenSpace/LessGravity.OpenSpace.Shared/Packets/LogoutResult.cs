﻿namespace LessGravity.OpenSpace.Shared.Packets
{
    public enum LogoutResult
    {
        Normal,
        Inactivity,
        TemporaryBanned,
        Banned
    }
}