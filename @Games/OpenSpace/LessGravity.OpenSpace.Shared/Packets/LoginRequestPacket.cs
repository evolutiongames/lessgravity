﻿using Lidgren.Network;

namespace LessGravity.OpenSpace.Shared.Packets
{
    public sealed class LoginRequestPacket : Packet
    {
        public string UserName { get; set; }
        public string Password { get; set; }

        public override NetOutgoingMessage GetMessage(NetPeer peer)
        {
            var outgoingMessage = peer.CreateMessage();
            outgoingMessage.Write((byte)PacketType.LoginRequest);
            outgoingMessage.Write(UserName);
            outgoingMessage.Write(Password);

            return outgoingMessage;
        }

        public LoginRequestPacket() { }

        public LoginRequestPacket(NetBuffer incomingMessage)
        {
            UserName = incomingMessage.ReadString();
            Password = incomingMessage.ReadString();
        }

        public override string ToString()
        {
            return $"{GetType().Name} UserName: {UserName}";
        }
    }
}