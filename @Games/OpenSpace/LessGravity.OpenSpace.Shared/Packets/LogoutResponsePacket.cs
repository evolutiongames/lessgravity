﻿using Lidgren.Network;

namespace LessGravity.OpenSpace.Shared.Packets
{
    public sealed class LogoutResponsePacket : Packet
    {
        public long UserId { get; set; }

        public LogoutResult Result {get;set;}

        public override NetOutgoingMessage GetMessage(NetPeer peer)
        {
            var outgoingMessage = peer.CreateMessage();
            outgoingMessage.Write((byte)PacketType.LogoutResponse);
            outgoingMessage.Write(UserId);
            outgoingMessage.Write((byte)Result);

            return outgoingMessage;
        }

        public LogoutResponsePacket() { }

        public LogoutResponsePacket(NetBuffer incomingMessage)
        {
            UserId = incomingMessage.ReadInt64();
            Result = (LogoutResult)incomingMessage.ReadByte();
        }

        public override string ToString()
        {
            return $"{GetType().Name} UserId: {UserId} Result: {Result}";
        }
    }
}