﻿namespace LessGravity.OpenSpace.Shared.Packets
{
    public enum LoginResult : byte
    {
        Success,
        FailureWrongUserName,
        FailureWrongPassword,
        FailureUserTrialExpired,
        FailureUserBanned
    }
}