﻿using Lidgren.Network;
using System.Collections.Generic;

namespace LessGravity.OpenSpace.Shared.Packets
{
    public sealed class GetCharactersResponsePacket : Packet
    {
        public long UserId { get; set; }
        public List<CharacterInfo> Characters { get; private set; }

        public override NetOutgoingMessage GetMessage(NetPeer peer)
        {
            var outgoingMessage = peer.CreateMessage();
            outgoingMessage.Write((byte)PacketType.GetCharactersResponse);
            outgoingMessage.Write(UserId);

            var characterCount = Characters.Count;
            outgoingMessage.Write(characterCount);
            for (var i = 0; i < characterCount; ++i)
            {
                var characterInfo = Characters[i];
                outgoingMessage.Write(characterInfo.CharacterId);
                outgoingMessage.Write(characterInfo.CharacterName);
                outgoingMessage.Write(characterInfo.CharacterAppearance);
            }
            return outgoingMessage;
        }

        public GetCharactersResponsePacket()
        {
            Characters = new List<CharacterInfo>();
        }

        public GetCharactersResponsePacket(NetBuffer incomingMessage)
            : this()
        {
            UserId = incomingMessage.ReadInt32();
            var characterCount = incomingMessage.ReadInt32();
            for (var i = 0; i < characterCount; i++)
            {
                var characterInfo = new CharacterInfo
                {
                    CharacterId = incomingMessage.ReadInt64(),
                    CharacterName = incomingMessage.ReadString(),
                    CharacterAppearance = incomingMessage.ReadUInt64(),
                };
                Characters.Add(characterInfo);
            }
        }

        public override string ToString()
        {
            return $"{GetType().Name} UserId: {UserId}";
        }
    }
}