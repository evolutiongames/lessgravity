﻿namespace LessGravity.OpenSpace.Shared.Packets
{
    public sealed class CharacterInfo
    {
        public long CharacterId { get; set; }
        public string CharacterName { get; set; }
        public ulong CharacterAppearance { get; set; }

        public override string ToString()
        {
            return CharacterName;
        }
    }
}