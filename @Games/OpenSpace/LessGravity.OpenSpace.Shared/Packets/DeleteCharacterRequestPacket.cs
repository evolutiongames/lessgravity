﻿using Lidgren.Network;

namespace LessGravity.OpenSpace.Shared.Packets
{
    public sealed class DeleteCharacterRequestPacket : Packet
    {
        public long UserId { get; set; }
        public long CharacterId { get; set; }

        public override NetOutgoingMessage GetMessage(NetPeer peer)
        {
            var outgoingMessage = peer.CreateMessage();
            outgoingMessage.Write((byte)PacketType.DeleteCharacterRequest);
            outgoingMessage.Write(UserId);
            outgoingMessage.Write(CharacterId);
            return outgoingMessage;
        }

        public DeleteCharacterRequestPacket() { }

        public DeleteCharacterRequestPacket(NetBuffer incomingMessage)
        {
            UserId = incomingMessage.ReadInt64();
            CharacterId = incomingMessage.ReadInt64();
        }

        public override string ToString()
        {
            return $"{GetType().Name} UserId: {UserId}, CharacterId: {CharacterId}";
        }
    }
}