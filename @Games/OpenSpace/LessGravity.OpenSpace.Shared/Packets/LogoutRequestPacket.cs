﻿using Lidgren.Network;

namespace LessGravity.OpenSpace.Shared.Packets
{
    public sealed class LogoutRequestPacket : Packet
    {
        public long UserId { get; set; }

        public override NetOutgoingMessage GetMessage(NetPeer peer)
        {
            var outgoingMessage = peer.CreateMessage();
            outgoingMessage.Write((byte)PacketType.LogoutRequest);
            outgoingMessage.Write(UserId);

            return outgoingMessage;
        }

        public LogoutRequestPacket() { }

        public LogoutRequestPacket(NetBuffer incomingMessage)
        {
            UserId = incomingMessage.ReadInt64();
        }
        
        public override string ToString()
        {
            return $"{GetType().Name} UserId: {UserId}";
        }
    }
}