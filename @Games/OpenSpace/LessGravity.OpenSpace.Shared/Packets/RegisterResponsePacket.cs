﻿using Lidgren.Network;
using System;

namespace LessGravity.OpenSpace.Shared.Packets
{
    public sealed class RegisterResponsePacket : Packet
    {
        public RegisterResult Result { get; set; }
        public long UserId { get; set; }
        public DateTime RegisteredDate { get; set; }

        public override NetOutgoingMessage GetMessage(NetPeer peer)
        {
            var outgoingMessage = peer.CreateMessage();
            outgoingMessage.Write((byte)PacketType.RegisterResponse);
            outgoingMessage.Write((byte)Result);
            outgoingMessage.Write(UserId);
            outgoingMessage.Write(RegisteredDate.Ticks);

            return outgoingMessage;
        }

        public RegisterResponsePacket() { }

        public RegisterResponsePacket(NetBuffer incomingMessage)
        {
            Result = (RegisterResult)incomingMessage.ReadByte();
            UserId = incomingMessage.ReadInt64();
            RegisteredDate = new DateTime(incomingMessage.ReadInt64());
        }
        
        public override string ToString()
        {
            return $"{GetType().Name} UserId: {UserId} Result: {Result}";
        }
    }
}