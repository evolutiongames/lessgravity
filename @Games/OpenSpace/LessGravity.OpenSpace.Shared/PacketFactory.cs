﻿using LessGravity.OpenSpace.Shared.Packets;
using Lidgren.Network;

namespace LessGravity.OpenSpace.Shared
{
    public static class PacketFactory
    {
        //TODO(deccer) replace PacketFactory.CreatePacket with some Reflection Magic (either via classnames or attributes)
        public static IPacket CreatePacket(PacketType packetType, NetIncomingMessage incomingMessage)
        {
            switch (packetType)
            {
                case PacketType.Chat:
                    return new ChatPacket(incomingMessage);
                case PacketType.LoginRequest:
                    return new LoginRequestPacket(incomingMessage);
                case PacketType.LoginResponse:
                    return new LoginResponsePacket(incomingMessage);
                case PacketType.LogoutRequest:
                    return new LogoutRequestPacket(incomingMessage);
                case PacketType.LogoutResponse:
                    return new LogoutResponsePacket(incomingMessage);
                case PacketType.RegisterRequest:
                    return new RegisterRequestPacket(incomingMessage);
                case PacketType.RegisterResponse:
                    return new RegisterResponsePacket(incomingMessage);
                case PacketType.GetCharactersRequest:
                    return new GetCharactersRequestPacket(incomingMessage);
                case PacketType.GetCharactersResponse:
                    return new GetCharactersResponsePacket(incomingMessage);
                case PacketType.DeleteCharacterRequest:
                    return new DeleteCharacterRequestPacket(incomingMessage);
                case PacketType.DeleteCharacterResponse:
                    return new DeleteCharacterResponsePacket(incomingMessage);
                default:
                    return null;
            }
        }

    }
}