﻿namespace LessGravity.OpenSpace.Shared
{
    public enum PacketDirection
    {
        Incoming,
        Outgoing
    }
}