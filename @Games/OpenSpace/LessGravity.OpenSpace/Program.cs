﻿using LessGravity.Quasar.Game;
using System;
using System.Windows.Forms;
using LessGravity.Logging;

namespace LessGravity.OpenSpace
{
    internal static class Program
    {
        [STAThread]
        private static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            using (var game = new MainGame(new Logger(), new DefaultFormFactory()))
            {
                var settings = new GameSettings
                {
                    FullScreen = true,
                    Height = 1080,
                    Width = 1920,
                    Title = "OpenSpace",
                    VSync = false,
                };
                game.Run(settings);
            }
        }
    }
}