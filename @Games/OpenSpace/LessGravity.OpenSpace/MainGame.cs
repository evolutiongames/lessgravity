﻿using LessGravity.Logging;
using LessGravity.Quasar.Game;

namespace LessGravity.OpenSpace
{
    class MainGame : QuasarGameBase
    {
        public MainGame(ILogger logger, IFormFactory formFactory)
            : base(logger, formFactory)
        { }
    }
}