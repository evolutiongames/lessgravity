﻿using LessGravity.OpenSpace.Models.Celestials;
using System;

namespace LessGravity.OpenSpace.Models.StationModules
{
    public class StationModule : ObjectBase, IEquatable<StationModule>
    {
        public Station Station { get; set; }

        public bool Equals(StationModule other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return base.Equals(other) && Equals(Station, other.Station);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((StationModule) obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return (base.GetHashCode() * 397) ^ (Station?.GetHashCode() ?? 0);
            }
        }
    }
}