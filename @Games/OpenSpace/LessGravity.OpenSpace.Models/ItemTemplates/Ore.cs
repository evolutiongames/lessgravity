﻿using System;

namespace LessGravity.OpenSpace.Models.ItemTemplates
{
    public class Ore : ItemTemplate, IEquatable<Ore>
    {
        public double BaseRefineryYied { get; set; }

        public bool Equals(Ore other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return base.Equals(other) && BaseRefineryYied.Equals(other.BaseRefineryYied);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((Ore) obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return (base.GetHashCode() * 397) ^ BaseRefineryYied.GetHashCode();
            }
        }
    }
}