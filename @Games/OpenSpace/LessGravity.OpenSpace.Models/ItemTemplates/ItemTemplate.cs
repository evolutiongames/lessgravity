﻿using System;
using System.Collections.Generic;

namespace LessGravity.OpenSpace.Models.ItemTemplates
{
    public abstract class ItemTemplate : ObjectBase, IEquatable<ItemTemplate>
    {
        public virtual AttributeSet Attributes { get; set; }

        public string Category { get; set; }

        public string SubCategory { get; set; }

        public string Description { get; set; }

        public double Mass { get; set; }

        public double Volume { get; set; }

        public string Resource2d { get; set; }

        public string Resource3d { get; set; }

        public bool IsStackable { get; set; }

        public decimal BasePrice { get; set; }

        public List<ItemResourceIn> ResourcesIn { get; set; } = new List<ItemResourceIn>();

        public List<ItemResourceOut> ResourcesOut { get; set; } = new List<ItemResourceOut>();

        public bool Equals(ItemTemplate other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return base.Equals(other) 
                && Equals(Attributes, other.Attributes) 
                && string.Equals(Category, other.Category) 
                && string.Equals(SubCategory, other.SubCategory) 
                && string.Equals(Description, other.Description) 
                && Mass.Equals(other.Mass) && Volume.Equals(other.Volume) 
                && string.Equals(Resource2d, other.Resource2d) 
                && string.Equals(Resource3d, other.Resource3d) 
                && IsStackable == other.IsStackable 
                && BasePrice == other.BasePrice 
                && Equals(ResourcesIn, other.ResourcesIn) 
                && Equals(ResourcesOut, other.ResourcesOut);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((ItemTemplate) obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                int hashCode = base.GetHashCode();
                hashCode = (hashCode * 397) ^ (Attributes?.GetHashCode() ?? 0);
                hashCode = (hashCode * 397) ^ (Category?.GetHashCode() ?? 0);
                hashCode = (hashCode * 397) ^ (SubCategory?.GetHashCode() ?? 0);
                hashCode = (hashCode * 397) ^ (Description?.GetHashCode() ?? 0);
                hashCode = (hashCode * 397) ^ Mass.GetHashCode();
                hashCode = (hashCode * 397) ^ Volume.GetHashCode();
                hashCode = (hashCode * 397) ^ (Resource2d?.GetHashCode() ?? 0);
                hashCode = (hashCode * 397) ^ (Resource3d?.GetHashCode() ?? 0);
                hashCode = (hashCode * 397) ^ IsStackable.GetHashCode();
                hashCode = (hashCode * 397) ^ BasePrice.GetHashCode();
                hashCode = (hashCode * 397) ^ (ResourcesIn?.GetHashCode() ?? 0);
                hashCode = (hashCode * 397) ^ (ResourcesOut?.GetHashCode() ?? 0);
                return hashCode;
            }
        }
    }
}