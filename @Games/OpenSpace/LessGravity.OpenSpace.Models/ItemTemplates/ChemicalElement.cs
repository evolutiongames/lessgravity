﻿using System;

namespace LessGravity.OpenSpace.Models.ItemTemplates
{
    public class ChemicalElement : ItemTemplate, IEquatable<ChemicalElement>
    {
        public int AtomicNumber { get; set; }

        public double MeltingPoint { get; set; }

        public double BoilingPoint { get; set; }

        public double MolarMass { get; set; }

        public string Symbol { get; set; }

        public int Group { get; set; }

        public bool Equals(ChemicalElement other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return base.Equals(other) 
                && AtomicNumber == other.AtomicNumber
                && MeltingPoint.Equals(other.MeltingPoint) 
                && BoilingPoint.Equals(other.BoilingPoint) 
                && MolarMass.Equals(other.MolarMass) 
                && string.Equals(Symbol, other.Symbol) 
                && Group == other.Group;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((ChemicalElement) obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                int hashCode = base.GetHashCode();
                hashCode = (hashCode * 397) ^ AtomicNumber;
                hashCode = (hashCode * 397) ^ MeltingPoint.GetHashCode();
                hashCode = (hashCode * 397) ^ BoilingPoint.GetHashCode();
                hashCode = (hashCode * 397) ^ MolarMass.GetHashCode();
                hashCode = (hashCode * 397) ^ (Symbol?.GetHashCode() ?? 0);
                hashCode = (hashCode * 397) ^ Group;
                return hashCode;
            }
        }
    }
}