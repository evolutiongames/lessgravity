﻿using System;

namespace LessGravity.OpenSpace.Models.ItemTemplates
{
    public class ItemResourceIn : IEquatable<ItemResourceIn>
    {
        public long Id { get; set; }

        public int Quantity { get; set; }

        public ItemTemplate Template { get; set; }

        public override string ToString()
        {
            return $"{Quantity}x {Template?.Name}";
        }

        public bool Equals(ItemResourceIn other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return Id == other.Id && Quantity == other.Quantity 
                && Equals(Template, other.Template);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((ItemResourceIn) obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = Id.GetHashCode();
                hashCode = (hashCode * 397) ^ Quantity;
                hashCode = (hashCode * 397) ^ (Template?.GetHashCode() ?? 0);
                return hashCode;
            }
        }
    }
}