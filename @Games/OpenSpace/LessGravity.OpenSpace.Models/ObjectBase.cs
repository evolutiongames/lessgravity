﻿using System;

namespace LessGravity.OpenSpace.Models
{
    public abstract class ObjectBase : IEquatable<ObjectBase>
    {
        public long Id { get; set; }

        public virtual string Name { get; set; }

        public override string ToString()
        {
            return Name;
        }

        public bool Equals(ObjectBase other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return Id == other.Id && string.Equals(Name, other.Name);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((ObjectBase) obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return (Id.GetHashCode() * 397) ^ (Name?.GetHashCode() ?? 0);
            }
        }
    }
}