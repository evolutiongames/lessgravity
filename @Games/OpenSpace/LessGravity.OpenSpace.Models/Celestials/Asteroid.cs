﻿using System;
using LessGravity.OpenSpace.Models.ItemTemplates;

namespace LessGravity.OpenSpace.Models.Celestials
{
    public class Asteroid : CelestialObject, IEquatable<Asteroid>
    {
        public virtual Ore Ore { get; set; }

        public virtual SolarSystem SolarSystem { get; set; }

        public double BaseYield { get; set; }

        public bool Equals(Asteroid other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return base.Equals(other) && Equals(Ore, other.Ore) 
                && Equals(SolarSystem, other.SolarSystem) 
                && BaseYield.Equals(other.BaseYield);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((Asteroid) obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                int hashCode = base.GetHashCode();
                hashCode = (hashCode * 397) ^ (Ore?.GetHashCode() ?? 0);
                hashCode = (hashCode * 397) ^ (SolarSystem?.GetHashCode() ?? 0);
                hashCode = (hashCode * 397) ^ BaseYield.GetHashCode();
                return hashCode;
            }
        }
    }
}