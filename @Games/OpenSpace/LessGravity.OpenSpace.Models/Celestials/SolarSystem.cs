﻿using System;

namespace LessGravity.OpenSpace.Models.Celestials
{
    public class SolarSystem : CelestialObject, IEquatable<SolarSystem>
    {
        public Constellation Constellation { get; set; }

        public bool Equals(SolarSystem other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return base.Equals(other) && Equals(Constellation, other.Constellation);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != GetType()) return false;
            return Equals((SolarSystem) obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return (base.GetHashCode() * 397) ^ (Constellation?.GetHashCode() ?? 0);
            }
        }
    }
}