﻿using System;
using System.Collections.Generic;
using LessGravity.OpenSpace.Models.StationModules;

namespace LessGravity.OpenSpace.Models.Celestials
{
    public class Station : ObjectBase, IEquatable<Station>
    {
        public SolarSystem SolarSystem { get; set; }

        public List<StationModule> Modules { get; set; } = new List<StationModule>();

        public decimal OfficeRentalFees { get; set; }

        public bool Equals(Station other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return base.Equals(other) 
                && Equals(SolarSystem, other.SolarSystem) 
                && Equals(Modules, other.Modules) 
                && OfficeRentalFees == other.OfficeRentalFees;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((Station) obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                int hashCode = base.GetHashCode();
                hashCode = (hashCode * 397) ^ (SolarSystem?.GetHashCode() ?? 0);
                hashCode = (hashCode * 397) ^ (Modules?.GetHashCode() ?? 0);
                hashCode = (hashCode * 397) ^ OfficeRentalFees.GetHashCode();
                return hashCode;
            }
        }
    }
}