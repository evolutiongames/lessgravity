﻿using System;

namespace LessGravity.OpenSpace.Models.Celestials
{
    public class Star : CelestialObject, IEquatable<Star>
    {
        public SolarSystem SolarSystem { get; set; }

        public bool Equals(Star other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return base.Equals(other) && Equals(SolarSystem, other.SolarSystem);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((Star)obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return (base.GetHashCode() * 397) ^ (SolarSystem?.GetHashCode() ?? 0);
            }
        }
    }
}