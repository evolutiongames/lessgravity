﻿using System;

namespace LessGravity.OpenSpace.Models.Celestials
{
    public class Region : CelestialObject, IEquatable<Region>
    {
        public Universe Universe { get; set; }

        public bool Equals(Region other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return base.Equals(other) && Equals(Universe, other.Universe);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((Region) obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return (base.GetHashCode() * 397) ^ (Universe?.GetHashCode() ?? 0);
            }
        }
    }
}