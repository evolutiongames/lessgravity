﻿using System;

namespace LessGravity.OpenSpace.Models.Celestials
{
    public class Constellation: CelestialObject, IEquatable<Constellation>
    {
        public Region Region { get; set; }

        public bool Equals(Constellation other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return base.Equals(other) && Equals(Region, other.Region);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((Constellation) obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return (base.GetHashCode() * 397) ^ (Region?.GetHashCode() ?? 0);
            }
        }
    }
}