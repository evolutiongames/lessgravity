﻿using System;
using SharpDX;

namespace LessGravity.OpenSpace.Models.Celestials
{
    public class CelestialObject : ObjectBase, IEquatable<CelestialObject>
    {
        public Vector3 Position { get; set; }

        public BoundingSphere BoundingSphere { get; set; }

        public bool Equals(CelestialObject other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return base.Equals(other) && Position.Equals(other.Position) && BoundingSphere.Equals(other.BoundingSphere);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((CelestialObject) obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                int hashCode = base.GetHashCode();
                hashCode = (hashCode * 397) ^ Position.GetHashCode();
                hashCode = (hashCode * 397) ^ BoundingSphere.GetHashCode();
                return hashCode;
            }
        }
    }
}