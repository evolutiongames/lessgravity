﻿using System;
using System.Globalization;
using System.Windows.Data;
using System.Windows.Media.Imaging;

namespace LessGravity.OpenSpace.Data.Editor.Converters
{
    public class ObjectTypeToBitmapSourceConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var valueAsString = value as string;
            switch (valueAsString)
            {
                case "Region":
                case "RegionViewModel":
                    return new BitmapImage(new Uri("/LessGravity.OpenSpace.Data.Editor;component/Resources/region.png", UriKind.Relative));
                case "Constellation":
                case "ConstellationViewModel":
                    return new BitmapImage(new Uri("/LessGravity.OpenSpace.Data.Editor;component/Resources/constellation.png", UriKind.Relative));
                case "Star":
                case "StarViewModel":
                    return new BitmapImage(new Uri("/LessGravity.OpenSpace.Data.Editor;component/Resources/star.png", UriKind.Relative));
                case "Planet":
                case "PlanetViewModel":
                    return new BitmapImage(new Uri("/LessGravity.OpenSpace.Data.Editor;component/Resources/planet.png", UriKind.Relative));
                case "Asteroid":
                case "AsteroidViewModel":
                    return new BitmapImage(new Uri("/LessGravity.OpenSpace.Data.Editor;component/Resources/asteroid.png", UriKind.Relative));
                case "Station":
                case "StationViewModel":
                    return new BitmapImage(new Uri("/LessGravity.OpenSpace.Data.Editor;component/Resources/station.png", UriKind.Relative));
                case "Universe":
                case "UniverseViewModel":
                    return new BitmapImage(new Uri("/LessGravity.OpenSpace.Data.Editor;component/Resources/universe.png", UriKind.Relative));
                default:
                    return new BitmapImage(new Uri("/LessGravity.OpenSpace.Data.Editor;component/Resources/universe.png", UriKind.Relative));
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}