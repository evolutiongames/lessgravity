﻿using AutoMapper;
using LessGravity.OpenSpace.Data.Context;
using LessGravity.OpenSpace.Data.Editor.ViewModels.Ships;
using LessGravity.Pluto.Observables;
using LessGravity.Pluto.ViewModels;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace LessGravity.OpenSpace.Data.Editor.ViewModels
{
    public class CharacterListViewModel : ViewModel<CharacterViewModel>
    {
        private RangeObservableCollection<UserViewModel> _users;
        public RangeObservableCollection<UserViewModel> Users
        {
            get => _users;
            set => SetValue(ref _users, value);
        }

        private RangeObservableCollection<RaceViewModel> _races;
        public RangeObservableCollection<RaceViewModel> Races
        {
            get => _races;
            set => SetValue(ref _races, value);
        }

        private RangeObservableCollection<FactionViewModel> _factions;
        public RangeObservableCollection<FactionViewModel> Factions
        {
            get => _factions;
            set => SetValue(ref _factions, value);
        }

        private RangeObservableCollection<ShipViewModel> _ships;
        public RangeObservableCollection<ShipViewModel> Ships
        {
            get => _ships;
            set => SetValue(ref _ships, value);
        }

        private RangeObservableCollection<WalletViewModel> _wallets;
        public RangeObservableCollection<WalletViewModel> Wallets
        {
            get => _wallets;
            set => SetValue(ref _wallets, value);
        }

        public async override Task LoadAsync()
        {
            //using (BusyStack.GetToken())
            {
                var userViewModels = Task.Run(() =>
                {
                    using (var db = new GameDbContext())
                    {
                        var users = db.Users.Distinct().ToList();
                        return users.Select(user => Mapper.Map<UserViewModel>(user));
                    }
                });

                var raceViewModels = Task.Run(() =>
                {
                    using (var db = new GameDbContext())
                    {
                        var races = db.Races.Distinct().ToList();
                        return races.Select(race => Mapper.Map<RaceViewModel>(race));
                    }
                });

                var factionViewModels = Task.Run(() =>
                {
                    using (var db = new GameDbContext())
                    {
                        var factions = db.Factions.Distinct().ToList();
                        return factions.Select(faction => Mapper.Map<FactionViewModel>(faction));
                    }
                });

                var shipViewModels = Task.Run(() =>
                {
                    using (var db = new GameDbContext())
                    {
                        var ships = db.Ships.Distinct().ToList();
                        return ships.Select(ship => Mapper.Map<ShipViewModel>(ship));
                    }
                });

                var walletViewModels = Task.Run(() =>
                {
                    using (var db = new GameDbContext())
                    {
                        var wallets = db.Wallets.Distinct().ToList();
                        return wallets.Select(wallet => Mapper.Map<WalletViewModel>(wallet));
                    }
                });

                var characterViewModels = Task.Run(() =>
                {
                    using (var db = new GameDbContext())
                    {
                        var characters = db.Characters.Include(character => character.User)
                                                      .Include(character => character.Race)
                                                      .Include(character => character.Ship)
                                                      .Include(character => character.Wallet)
                                                      .ToList();
                        return characters.Select(character => Mapper.Map<CharacterViewModel>(character)).ToList();
                    }
                });

                await Task.WhenAll(userViewModels, raceViewModels, factionViewModels, shipViewModels, walletViewModels);

                Users.AddRange(userViewModels.Result);
                Races.AddRange(raceViewModels.Result);
                Factions.AddRange(factionViewModels.Result);
                Ships.AddRange(shipViewModels.Result);
                Wallets.AddRange(walletViewModels.Result);
                Items.AddRange(characterViewModels.Result);
            }
        }

        public CharacterListViewModel()
        {
            Users = new RangeObservableCollection<UserViewModel>();
            Races = new RangeObservableCollection<RaceViewModel>();
            Factions = new RangeObservableCollection<FactionViewModel>();
            Ships = new RangeObservableCollection<ShipViewModel>();
            Wallets = new RangeObservableCollection<WalletViewModel>();
        }
    }
}