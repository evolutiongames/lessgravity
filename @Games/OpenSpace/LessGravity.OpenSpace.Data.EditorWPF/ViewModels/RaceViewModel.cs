﻿namespace LessGravity.OpenSpace.Data.Editor.ViewModels
{
    public class RaceViewModel : ObjectBaseViewModel
    {
        public Race Race { get; private set; }

        public RaceViewModel(Race race)
        {
            Race = race;
        }
    }
}