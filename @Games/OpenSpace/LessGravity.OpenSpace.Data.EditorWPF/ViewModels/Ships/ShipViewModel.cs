﻿using LessGravity.OpenSpace.Data.Editor.ViewModels.Celestials;
using LessGravity.OpenSpace.Data.Ships;

namespace LessGravity.OpenSpace.Data.Editor.ViewModels.Ships
{
    public class ShipViewModel : CelestialObjectViewModel
    {
        public Ship Ship { get; private set; }

        public ShipViewModel(Ship ship)
        {
            Ship = ship;
        }
    }
}