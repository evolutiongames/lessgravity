﻿using AutoMapper;
using LessGravity.OpenSpace.Data.Context;
using LessGravity.OpenSpace.Data.Editor.ViewModels.Celestials;
using LessGravity.Pluto.Observables;
using LessGravity.Pluto.ViewModels;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace LessGravity.OpenSpace.Data.Editor.ViewModels.Ships
{
    public class ShipListViewModel : ViewModel<ShipViewModel>
    {
        private RangeObservableCollection<SolarSystemViewModel> _solarSystems;
        public RangeObservableCollection<SolarSystemViewModel> SolarSystems
        {
            get => _solarSystems;
            set => SetValue(ref _solarSystems, value);
        }

        public async override Task LoadAsync()
        {
            //using (BusyStack.GetToken())
            {
                var shipViewModels = Task.Run(() =>
                {
                    using (var db = new GameDbContext())
                    {
                        var ships = db.Ships.Include(ship => ship.SolarSystem).ToList();
                        return ships.Select(ship => Mapper.Map<ShipViewModel>(ship)).ToList();
                    }
                });

                var solarSystemViewModels = Task.Run(() =>
                {
                    using (var db = new GameDbContext())
                    {
                        var solarSystems = db.SolarSystems.Include(solarSystem => solarSystem.Constellation).Distinct().ToList();
                        return solarSystems.Select(solarSystem => Mapper.Map<SolarSystemViewModel>(solarSystem)).ToList();
                    }
                });

                await Task.WhenAll(shipViewModels, solarSystemViewModels);

                Items.AddRange(shipViewModels.Result);
                SolarSystems.AddRange(solarSystemViewModels.Result);
            }
        }

        public ShipListViewModel()
        {
            SolarSystems = new RangeObservableCollection<SolarSystemViewModel>();
        }
    }
}