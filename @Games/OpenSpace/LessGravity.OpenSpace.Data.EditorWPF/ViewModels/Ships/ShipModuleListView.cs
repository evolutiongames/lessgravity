﻿using AutoMapper;
using LessGravity.OpenSpace.Data.Context;
using LessGravity.Pluto.Observables;
using LessGravity.Pluto.ViewModels;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace LessGravity.OpenSpace.Data.Editor.ViewModels.Ships
{
    public class ShipModuleListViewModel : ViewModel<ShipModuleViewModel>
    {
        private RangeObservableCollection<ShipViewModel> _ships;
        public RangeObservableCollection<ShipViewModel> Ships
        {
            get => _ships;
            set => SetValue(ref _ships, value);
        }

        public async override Task LoadAsync()
        {
            //using (BusyStack.GetToken())
            {
                var shipModuleViewModels = Task.Run(() =>
                {
                    using (var db = new GameDbContext())
                    {
                        var shipModules = db.ShipModules.Include(shipModule => shipModule.Ship).ToList();
                        return shipModules.Select(shipModule => Mapper.Map<ShipModuleViewModel>(shipModule));
                    }
                });

                var shipViewModels = Task.Run(() =>
                {
                    using (var db = new GameDbContext())
                    {
                        var ships = db.Ships.Distinct().ToList();
                        return ships.Select(ship => Mapper.Map<ShipViewModel>(ship));
                    }
                });

                await Task.WhenAll(shipModuleViewModels, shipViewModels);

                Items.AddRange(shipModuleViewModels.Result);
                Ships.AddRange(shipViewModels.Result);
            }
        }

        public ShipModuleListViewModel()
        {
            Ships = new RangeObservableCollection<ShipViewModel>();
        }
    }
}