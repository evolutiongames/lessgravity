﻿using LessGravity.OpenSpace.Data.ShipModules;

namespace LessGravity.OpenSpace.Data.Editor.ViewModels.Ships
{
    public class ShipModuleViewModel : ObjectBaseViewModel
    {
        public ShipModule ShipModule { get; private set; }

        private ShipViewModel _ship;
        public ShipViewModel Ship
        {
            get => _ship;
            set => SetValue(ref _ship, value);
        }

        public override bool Equals(object obj)
        {
            var other = obj as ShipModuleViewModel;
            if (other == null)
            {
                return false;
            }
            if (Ship == null)
            {
                return base.Equals(other);
            }
            return base.Equals(other) && Ship.Equals(other?.Ship);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hash = base.GetHashCode();
                if (Ship == null)
                {
                    return hash;
                }
                hash = (hash * 16777619) ^ Ship.GetHashCode();
                return hash;
            }
        }

        public ShipModuleViewModel(ShipModule shipModule)
        {
            ShipModule = shipModule;
        }
    }
}