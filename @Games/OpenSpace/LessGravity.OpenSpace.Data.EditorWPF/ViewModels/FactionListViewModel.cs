﻿using AutoMapper;
using LessGravity.OpenSpace.Data.Context;
using LessGravity.Pluto.ViewModels;
using System.Linq;

namespace LessGravity.OpenSpace.Data.Editor.ViewModels
{
    public class FactionListViewModel : ViewModel<FactionViewModel>
    {
        public FactionListViewModel()
        {
            using (var db = new GameDbContext())
            {
                var factions = db.Factions.Distinct().ToList();
                Items.AddRange(factions.Select(Mapper.Map<FactionViewModel>));
            }
        }
    }
}