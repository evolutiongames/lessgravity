﻿using AutoMapper;
using LessGravity.OpenSpace.Data.Editor.ViewModels.Celestials;
using LessGravity.Pluto.Observables;
using LessGravity.Pluto.ViewModels;
using System.Linq;
using System.Threading.Tasks;
using LessGravity.OpenSpace.Services;

namespace LessGravity.OpenSpace.Data.Editor.ViewModels.Stations
{
    public class StationListViewModel : ViewModel<StationViewModel>
    {
        private readonly ICelestialsService _celestialsService;
        private readonly IStationService _stationService;

        private RangeObservableCollection<SolarSystemViewModel> _solarSystems;
        public RangeObservableCollection<SolarSystemViewModel> SolarSystems
        {
            get => _solarSystems;
            set => SetValue(ref _solarSystems, value);
        }

        public override async Task LoadAsync()
        {
            var stationViewModels = await Task.Run(() => _stationService.GetAllStations().Select(Mapper.Map<StationViewModel>));
            var solarSystemViewModels = await Task.Run(() => _celestialsService.GetAllSolarSystems().Select(Mapper.Map<SolarSystemViewModel>));

            //await Task.WhenAll(stationViewModels, solarSystemViewModels);

            Items.AddRange(stationViewModels);
            SolarSystems.AddRange(solarSystemViewModels);
        }

        public StationListViewModel(ICelestialsService celestialsService, IStationService stationService)
        {
            _celestialsService = celestialsService;
            _stationService = stationService;
            SolarSystems = new RangeObservableCollection<SolarSystemViewModel>();
        }
    }
}