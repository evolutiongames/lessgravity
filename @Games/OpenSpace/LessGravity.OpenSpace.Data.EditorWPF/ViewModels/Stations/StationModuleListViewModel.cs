﻿using AutoMapper;
using LessGravity.OpenSpace.Data.Context;
using LessGravity.Pluto.Observables;
using LessGravity.Pluto.ViewModels;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace LessGravity.OpenSpace.Data.Editor.ViewModels.Stations
{
    public class StationModuleListViewModel : ViewModel<StationModuleViewModel>
    {
        private RangeObservableCollection<StationViewModel> _stations;
        public RangeObservableCollection<StationViewModel> Stations
        {
            get => _stations;
            set => SetValue(ref _stations, value);
        }

        public async override Task LoadAsync()
        {
            //using (BusyStack.GetToken())
            {
                var stationModuleViewModels = Task.Run(() =>
                {
                    using (var db = new GameDbContext())
                    {
                        var stationModules = db.StationModules.Include(stationModule => stationModule.Station).ToList();
                        return stationModules.Select(Mapper.Map<StationModuleViewModel>).ToList();
                    }
                });

                var stationViewModels = Task.Run(() =>
                {
                    using (var db = new GameDbContext())
                    {
                        var stations = db.Stations.Distinct().ToList();
                        return stations.Select(Mapper.Map<StationViewModel>).ToList();
                    }
                });

                await Task.WhenAll(stationModuleViewModels, stationViewModels);

                Items.AddRange(stationModuleViewModels.Result);
                Stations.AddRange(stationViewModels.Result);
            }
        }

        public StationModuleListViewModel()
        {
            Stations = new RangeObservableCollection<StationViewModel>();
        }
    }
}