﻿using LessGravity.OpenSpace.Data.StationModules;

namespace LessGravity.OpenSpace.Data.Editor.ViewModels.Stations
{
    public class StationModuleViewModel : ObjectBaseViewModel
    {
        public StationModule StationModule { get; private set; }

        private StationViewModel _station;
        public StationViewModel Station
        {
            get => _station;
            set => SetValue(ref _station, value);
        }

        public override bool Equals(object obj)
        {
            var other = obj as StationModuleViewModel;
            if (other == null)
            {
                return false;
            }
            if (Station == null)
            {
                return base.Equals(other);
            }
            return base.Equals(other) && Station.Equals(other?.Station);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hash = base.GetHashCode();
                if (Station != null)
                {
                    hash = (hash * 16777619) ^ Station.GetHashCode();
                }
                return hash;
            }
        }

        public StationModuleViewModel(StationModule stationModule)
        {
            StationModule = stationModule;
        }
    }
}