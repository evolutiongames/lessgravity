﻿using LessGravity.OpenSpace.Data.Celestials;
using LessGravity.OpenSpace.Data.Editor.ViewModels.Celestials;

namespace LessGravity.OpenSpace.Data.Editor.ViewModels.Stations
{
    public class StationViewModel : CelestialObjectViewModel
    {
        public Station Station { get; private set; }

        public StationViewModel(Station station)
        {
            Station = station;
        }
    }
}