﻿using AutoMapper;
using LessGravity.OpenSpace.Data.Context;
using LessGravity.Pluto.ViewModels;
using System.Linq;

namespace LessGravity.OpenSpace.Data.Editor.ViewModels
{
    public class UserListViewModel : ViewModel<UserViewModel>
    {
        public UserListViewModel()
        {
            using (var db = new GameDbContext())
            {
                var users = db.Users.ToList();
                Items.AddRange(users.Select(user => Mapper.Map<UserViewModel>(user)));
            }
        }
    }
}