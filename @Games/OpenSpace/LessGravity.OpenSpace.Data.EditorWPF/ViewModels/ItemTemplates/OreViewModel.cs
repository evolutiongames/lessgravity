﻿using LessGravity.OpenSpace.Data.ItemTemplates;

namespace LessGravity.OpenSpace.Data.Editor.ViewModels.ItemTemplates
{
    public class OreViewModel : ObjectBaseViewModel
    {
        public Ore Ore { get; private set; }

        public double BaseRefineryYied { get; set; }

        public OreViewModel(Ore ore)
        {
            Ore = ore;
            BaseRefineryYied = ore.BaseRefineryYied;
        }
    }
}