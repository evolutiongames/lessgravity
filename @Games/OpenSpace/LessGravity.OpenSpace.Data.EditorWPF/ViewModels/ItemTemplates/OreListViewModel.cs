﻿using AutoMapper;
using LessGravity.OpenSpace.Data.Context;
using LessGravity.OpenSpace.Data.ItemTemplates;
using LessGravity.Pluto.ViewModels;
using System.Linq;
using System.Threading.Tasks;

namespace LessGravity.OpenSpace.Data.Editor.ViewModels.ItemTemplates
{
    public class OreListViewModel : ViewModel<OreViewModel>
    {
        public async override Task LoadAsync()
        {
            //using (BusyStack.GetToken())
            {
                var oreViewModels = Task.Run(() =>
                {
                    using (var db = new GameDbContext())
                    {
                        var ores = db.ItemTemplates.OfType<Ore>().Distinct().ToList();
                        return ores.Select(ore => Mapper.Map<OreViewModel>(ore)).ToList();
                    }
                });
                Items.AddRange(await oreViewModels);
            }
        }

        public OreListViewModel()
        {
        }
    }
}