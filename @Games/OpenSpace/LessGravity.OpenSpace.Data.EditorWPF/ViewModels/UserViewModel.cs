﻿using System;

namespace LessGravity.OpenSpace.Data.Editor.ViewModels
{
    public class UserViewModel : ObjectBaseViewModel
    {
        public User User { get; private set; }

        private DateTime _dateCreated;
        public DateTime DateCreated
        {
            get => _dateCreated;
            set => SetValue(ref _dateCreated, value);
        }

        private bool _isBanned;
        public bool IsBanned
        {
            get => _isBanned;
            set => SetValue(ref _isBanned, value);
        }

        private bool _isLoggedIn;
        public bool IsLoggedIn
        {
            get => _isLoggedIn;
            set => SetValue(ref _isLoggedIn, value);
        }

        private bool _isTrialAccount;
        public bool IsTrialAccount
        {
            get => _isTrialAccount;
            set => SetValue(ref _isTrialAccount, value);
        }

        private DateTime? _dateLogin;
        public DateTime? DateLogin
        {
            get => _dateLogin;
            set => SetValue(ref _dateLogin, value);
        }

        private DateTime? _dateLastLogin;
        public DateTime? DateLastLogin
        {
            get => _dateLastLogin;
            set => SetValue(ref _dateLastLogin, value);
        }

        private DateTime? _dateBanned;
        public DateTime? DateBanned
        {
            get => _dateBanned;
            set => SetValue(ref _dateBanned, value);
        }

        private UserRole _userRole;
        public UserRole UserRole
        {
            get => _userRole;
            set => SetValue(ref _userRole, value);
        }

        public override bool Equals(object obj)
        {
            var other = obj as UserViewModel;
            if (other == null)
            {
                return false;
            }
            var result = base.Equals(obj)
                && DateCreated.Equals(other?.DateCreated)
                && IsBanned.Equals(other?.IsBanned)
                && IsLoggedIn.Equals(other?.IsLoggedIn)
                && IsTrialAccount.Equals(other?.IsTrialAccount)
                && DateLogin.Equals(other?.DateLogin)
                && DateLastLogin.Equals(other?.DateLastLogin)
                && DateBanned.Equals(other?.DateBanned)
                && UserRole.Equals(other?.UserRole);
            return result;
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hash = base.GetHashCode();
                hash = (hash * 16777619) ^ DateCreated.GetHashCode();
                hash = (hash * 16777619) ^ IsBanned.GetHashCode();
                hash = (hash * 16777619) ^ IsLoggedIn.GetHashCode();
                hash = (hash * 16777619) ^ IsTrialAccount.GetHashCode();
                hash = (hash * 16777619) ^ DateLogin.GetHashCode();
                hash = (hash * 16777619) ^ DateLastLogin.GetHashCode();
                hash = (hash * 16777619) ^ DateBanned.GetHashCode();
                hash = (hash * 16777619) ^ UserRole.GetHashCode();
                return hash;
            }
        }

        public UserViewModel(User user)
        {
            User = user;
        }
    }
}