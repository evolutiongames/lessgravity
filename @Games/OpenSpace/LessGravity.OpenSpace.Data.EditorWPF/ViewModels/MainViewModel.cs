﻿using AutoMapper;
using LessGravity.OpenSpace.Data.Celestials;
using LessGravity.OpenSpace.Data.Context;
using LessGravity.OpenSpace.Data.Editor.Controls;
using LessGravity.OpenSpace.Data.Editor.Controls.Celestials;
using LessGravity.OpenSpace.Data.Editor.Controls.ItemTemplates;
using LessGravity.OpenSpace.Data.Editor.Controls.Ships;
using LessGravity.OpenSpace.Data.Editor.Controls.Stations;
using LessGravity.OpenSpace.Data.Editor.ViewModels.Celestials;
using LessGravity.OpenSpace.Data.Editor.ViewModels.ItemTemplates;
using LessGravity.OpenSpace.Data.Editor.ViewModels.Ships;
using LessGravity.OpenSpace.Data.Editor.ViewModels.Stations;
using LessGravity.OpenSpace.Data.ItemTemplates;
using LessGravity.OpenSpace.Data.ShipModules;
using LessGravity.OpenSpace.Data.Ships;
using LessGravity.OpenSpace.Data.StationModules;
using LessGravity.Pluto.ViewModels;
using SimpleInjector;
using System.Linq;
using LessGravity.OpenSpace.Services;
using SimpleInjector.Extensions.LifetimeScoping;

namespace LessGravity.OpenSpace.Data.Editor.ViewModels
{
    public sealed class MainViewModel : ViewModel<CategoryViewModel>
    {
        static MainViewModel()
        {
            Mapper.Initialize(config =>
            {
                config.CreateMap<Universe, UniverseViewModel>().ConstructUsing(universe => new UniverseViewModel(universe));
                config.CreateMap<Region, RegionViewModel>().ConstructUsing(region => new RegionViewModel(region));
                config.CreateMap<Constellation, ConstellationViewModel>().ConstructUsing(constellation => new ConstellationViewModel(constellation));
                config.CreateMap<SolarSystem, SolarSystemViewModel>().ConstructUsing(solarSystem => new SolarSystemViewModel(solarSystem));
                config.CreateMap<Star, StarViewModel>().ConstructUsing(star => new StarViewModel(star));
                config.CreateMap<Planet, PlanetViewModel>().ConstructUsing(planet => new PlanetViewModel(planet));
                config.CreateMap<Asteroid, AsteroidViewModel>().ConstructUsing(asteroid => new AsteroidViewModel(asteroid));
                config.CreateMap<Station, StationViewModel>().ConstructUsing(station => new StationViewModel(station));
                config.CreateMap<StationModule, StationModuleViewModel>().ConstructUsing(stationModule => new StationModuleViewModel(stationModule));
                config.CreateMap<Ship, ShipViewModel>().ConstructUsing(ship => new ShipViewModel(ship));
                config.CreateMap<ShipModule, ShipModuleViewModel>().ConstructUsing(shipModule => new ShipModuleViewModel(shipModule));
                config.CreateMap<User, UserViewModel>().ConstructUsing(user => new UserViewModel(user));
                config.CreateMap<Wallet, WalletViewModel>().ConstructUsing(wallet => new WalletViewModel(wallet));
                config.CreateMap<Race, RaceViewModel>().ConstructUsing(race => new RaceViewModel(race));
                config.CreateMap<Faction, FactionViewModel>().ConstructUsing(faction => new FactionViewModel(faction));
                config.CreateMap<Character, CharacterViewModel>().ConstructUsing(character => new CharacterViewModel(character));

                config.CreateMap<Ore, OreViewModel>().ConstructUsing(ore => new OreViewModel(ore));
            });
        }

        public MainViewModel()
        {
            var container = new Container();
            container.Options.DefaultScopedLifestyle = new LifetimeScopeLifestyle();
            container.Register<GameDbContext, GameDbContext>(Lifestyle.Singleton);

            container.Register<ICelestialsService, CelestialsService>();
            container.Register<IStationService, StationService>();

            container.Register<UniverseListViewModel, UniverseListViewModel>();
            container.Register<RegionListViewModel, RegionListViewModel>();
            container.Register<ConstellationListViewModel, ConstellationListViewModel>();
            container.Register<SolarSystemListViewModel, SolarSystemListViewModel>();
            container.Register<StarListViewModel, StarListViewModel>();
            container.Register<PlanetListViewModel, PlanetListViewModel>();
            container.Register<AsteroidListViewModel, AsteroidListViewModel>();

            container.Verify(VerificationOption.VerifyAndDiagnose);

            Items.AddRange(new[]
            {
                //
                // Celestials
                //
                new CategoryViewModel { Caption = "Universe", Content = new UniverseListControl(() => container.GetInstance<UniverseListViewModel>()) },
                new CategoryViewModel { Caption = "Region", Content = new RegionListControl(() => container.GetInstance<RegionListViewModel>()) },
                new CategoryViewModel { Caption = "Constellation", Content = new ConstellationListControl(() => container.GetInstance<ConstellationListViewModel>()) },
                new CategoryViewModel { Caption = "SolarSystem", Content = new SolarSystemListControl(() => container.GetInstance<SolarSystemListViewModel>()) },
                new CategoryViewModel { Caption = "Star", Content = new StarListControl(() => container.GetInstance<StarListViewModel>()) },
                new CategoryViewModel { Caption = "Planet", Content = new PlanetListControl(() => container.GetInstance<PlanetListViewModel>()) },
                new CategoryViewModel { Caption = "Asteroid", Content = new AsteroidListControl(() => container.GetInstance<AsteroidListViewModel>()) },
                new CategoryViewModel { Caption = "Station", Content = new StationListControl(() => container.GetInstance<StationListViewModel>()) },

                new CategoryViewModel { Caption = "StationModule", Content = new StationModuleListControl(() => new StationModuleListViewModel()) },
                new CategoryViewModel { Caption = "Ship", Content = new ShipListControl(() => new ShipListViewModel()) },
                new CategoryViewModel { Caption = "ShipModule", Content = new ShipModuleListControl(() => new ShipModuleListViewModel()) },

                new CategoryViewModel { Caption = "User", Content = new UserListControl(() => new UserListViewModel()) },
                new CategoryViewModel { Caption = "Character", Content = new CharacterListControl(() => new CharacterListViewModel()) },
                new CategoryViewModel { Caption = "Factions", Content = new FactionListControl(() => new FactionListViewModel()) },
                new CategoryViewModel { Caption = "Races", Content = new RaceListControl(() => new RaceListViewModel()) },
                new CategoryViewModel { Caption = "Ores", Content = new OreListControl(() => new OreListViewModel()) }
            });

            SelectedItem = Items.FirstOrDefault();
        }
    }
}