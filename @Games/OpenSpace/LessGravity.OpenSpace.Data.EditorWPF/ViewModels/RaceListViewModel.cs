﻿using AutoMapper;
using LessGravity.OpenSpace.Data.Context;
using LessGravity.Pluto.ViewModels;
using System.Linq;

namespace LessGravity.OpenSpace.Data.Editor.ViewModels
{
    public class RaceListViewModel : ViewModel<RaceViewModel>
    {
        public RaceListViewModel()
        {
            using (var db = new GameDbContext())
            {
                var races = db.Races.Distinct().ToList();
                Items.AddRange(races.Select(race => Mapper.Map<RaceViewModel>(race)));
            }
        }
    }
}