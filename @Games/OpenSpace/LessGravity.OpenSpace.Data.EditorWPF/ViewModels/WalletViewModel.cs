﻿namespace LessGravity.OpenSpace.Data.Editor.ViewModels
{
    public class WalletViewModel : ObjectBaseViewModel
    {
        public Wallet Wallet { get; private set; }

        public WalletViewModel(Wallet wallet)
        {
            Wallet = wallet;
        }
    }
}