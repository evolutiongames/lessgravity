﻿using LessGravity.OpenSpace.Data.Editor.ViewModels.Ships;
using System;

namespace LessGravity.OpenSpace.Data.Editor.ViewModels
{
    public class CharacterViewModel : ObjectBaseViewModel
    {
        public Character Character { get; private set; }

        private ulong _appearance;
        public ulong Appearance
        {
            get => _appearance;
            set => SetValue(ref _appearance, value);
        }

        private DateTime _dateCreated;
        public DateTime DateCreated
        {
            get => _dateCreated;
            set => SetValue(ref _dateCreated, value);
        }

        private UserViewModel _user;
        public UserViewModel User
        {
            get => _user;
            set => SetValue(ref _user, value);
        }

        private RaceViewModel _race;
        public RaceViewModel Race
        {
            get => _race;
            set => SetValue(ref _race, value);
        }

        private CharacterRole _role;
        public CharacterRole Role
        {
            get => _role;
            set => SetValue(ref _role, value);
        }

        private ShipViewModel _ship;
        public ShipViewModel Ship
        {
            get => _ship;
            set => SetValue(ref _ship, value);
        }

        private WalletViewModel _wallet;
        public WalletViewModel Wallet
        {
            get => _wallet;
            set => SetValue(ref _wallet, value);
        }

        public override bool Equals(object obj)
        {
            var other = obj as CharacterViewModel;
            if (other == null)
            {
                return false;
            }
            var result = base.Equals(other)
                && DateCreated.Equals(other?.DateCreated)
                && Appearance.Equals(other?.Appearance)
                && Role.Equals(other?.Role);
            if (User != null)
            {
                result = result && User.Equals(other?.User);
            }
            if (Race != null)
            {
                result = result && Race.Equals(other?.Race);
            }
            if (Wallet != null)
            {
                result = result && Wallet.Equals(other?.Wallet);
            }
            if (Ship != null)
            {
                result = result && Ship.Equals(other?.Ship);
            }
            return result;
        }

        public override int GetHashCode()
        {
            unchecked
            {
                int hash = base.GetHashCode();
                hash = (hash * 16777619) ^ Appearance.GetHashCode();
                hash = (hash * 16777619) ^ DateCreated.GetHashCode();
                hash = (hash * 16777619) ^ Role.GetHashCode();
                if (User != null)
                {
                    hash = (hash * 16777619) ^ User.GetHashCode();
                }
                if (Race != null)
                {
                    hash = (hash * 16777619) ^ Race.GetHashCode();
                }
                if (Wallet != null)
                {
                    hash = (hash * 16777619) ^ Wallet.GetHashCode();
                }
                if (Ship != null)
                {
                    hash = (hash * 16777619) ^ Ship.GetHashCode();
                }
                return hash;
            }
        }

        public CharacterViewModel(Character character)
        {
            Character = character;
        }
    }
}