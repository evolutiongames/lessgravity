﻿using LessGravity.OpenSpace.Data.Celestials;

namespace LessGravity.OpenSpace.Data.Editor.ViewModels.Celestials
{
    public class PlanetViewModel : CelestialObjectViewModel
    {
        public Planet Planet { get; private set; }

        public PlanetViewModel(Planet planet)
        {
            Planet = planet;
        }
    }
}