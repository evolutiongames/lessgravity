﻿using AutoMapper;
using LessGravity.OpenSpace.Data.Celestials;
using LessGravity.OpenSpace.Data.Editor.Controls.Celestials;
using LessGravity.OpenSpace.Services;
using LessGravity.Pluto.Commands;
using LessGravity.Pluto.Observables;
using LessGravity.Pluto.ViewModels;
using MaterialDesignThemes.Wpf;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LessGravity.OpenSpace.Data.Editor.ViewModels.Celestials
{
    public class RegionListViewModel : ViewModel<RegionViewModel>
    {
        private readonly ICelestialsService _celestialsService;

        public IAsyncCommand AddCommand => new AsyncCommand<RegionViewModel>(RunAddDialog);

        private RangeObservableCollection<UniverseViewModel> _universes;
        public RangeObservableCollection<UniverseViewModel> Universes
        {
            get => _universes;
            set => SetValue(ref _universes, value);
        }

        public override async Task LoadAsync()
        {
            var regionViewModels = Task.Run(() => _celestialsService.GetAllRegions().Select(Mapper.Map<RegionViewModel>));
            var universeViewModels = Task.Run(() => _celestialsService.GetAllUniverses().Select(Mapper.Map<UniverseViewModel>));

            await Task.WhenAll(regionViewModels, universeViewModels);

            Items.AddRange(regionViewModels.Result);
            Universes.AddRange(universeViewModels.Result);
        }

        public RegionListViewModel(ICelestialsService celestialsService)
        {
            _celestialsService = celestialsService;
            Universes = new RangeObservableCollection<UniverseViewModel>();
        }

        private async Task RunAddDialog(RegionViewModel o)
        {
            var regionViewModel = new RegionViewModel(new Region());
            RegionViewModel.Universes = new List<UniverseViewModel>(Universes);
            var regionControl = new RegionControl
            {
                DataContext = regionViewModel
            };
            await DialogHost.Show(regionControl, "RootDialog");
            Items.Add(regionViewModel);
        }
    }
}