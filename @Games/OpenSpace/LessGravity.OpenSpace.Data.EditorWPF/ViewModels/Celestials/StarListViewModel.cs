﻿using AutoMapper;
using LessGravity.Pluto.Observables;
using LessGravity.Pluto.ViewModels;
using System.Linq;
using System.Threading.Tasks;
using LessGravity.OpenSpace.Services;

namespace LessGravity.OpenSpace.Data.Editor.ViewModels.Celestials
{
    public class StarListViewModel : ViewModel<StarViewModel>
    {
        private readonly ICelestialsService _celestialsService;

        private RangeObservableCollection<SolarSystemViewModel> _solarSystems;
        public RangeObservableCollection<SolarSystemViewModel> SolarSystems
        {
            get => _solarSystems;
            set => SetValue(ref _solarSystems, value);
        }

        public override async Task LoadAsync()
        {
            var starViewModels = Task.Run(() => _celestialsService.GetAllStars().Select(Mapper.Map<StarViewModel>));
            var solarSystemViewModels = Task.Run(() => _celestialsService.GetAllSolarSystems().Select(Mapper.Map<SolarSystemViewModel>));

            await Task.WhenAll(starViewModels, solarSystemViewModels);

            Items.AddRange(starViewModels.Result);
            SolarSystems.AddRange(solarSystemViewModels.Result);
        }

        public StarListViewModel(ICelestialsService celestialsService)
        {
            _celestialsService = celestialsService;
            SolarSystems = new RangeObservableCollection<SolarSystemViewModel>();
        }
    }
}