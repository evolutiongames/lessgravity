﻿using AutoMapper;
using LessGravity.OpenSpace.Services;
using LessGravity.Pluto.Observables;
using LessGravity.Pluto.ViewModels;
using System.Linq;
using System.Threading.Tasks;

namespace LessGravity.OpenSpace.Data.Editor.ViewModels.Celestials
{
    public class SolarSystemListViewModel : ViewModel<SolarSystemViewModel>
    {
        private readonly ICelestialsService _celestialsService;

        private RangeObservableCollection<ConstellationViewModel> _constellations;
        public RangeObservableCollection<ConstellationViewModel> Constellations
        {
            get => _constellations;
            set => SetValue(ref _constellations, value);
        }

        public override async Task LoadAsync()
        {
            var constellationViewModels = Task.Run(() => _celestialsService.GetAllConstellations().Select(Mapper.Map<ConstellationViewModel>));
            var solarSystemViewModels = Task.Run(() => _celestialsService.GetAllSolarSystems().Select(Mapper.Map<SolarSystemViewModel>));

            await Task.WhenAll(constellationViewModels, solarSystemViewModels);

            Items.AddRange(solarSystemViewModels.Result);
            Constellations.AddRange(constellationViewModels.Result);
        }

        public SolarSystemListViewModel(ICelestialsService celestialsService)
        {
            _celestialsService = celestialsService;
            Constellations = new RangeObservableCollection<ConstellationViewModel>();
        }
    }
}