﻿using LessGravity.OpenSpace.Data.Celestials;

namespace LessGravity.OpenSpace.Data.Editor.ViewModels.Celestials
{
    public class ConstellationViewModel : ObjectBaseViewModel
    {
        public Constellation Constellation { get; private set; }

        private RegionViewModel _region;
        public RegionViewModel Region
        {
            get => _region;
            set => SetValue(ref _region, value);
        }

        public override bool Equals(object obj)
        {
            var other = obj as ConstellationViewModel;
            if (other == null)
            {
                return false;
            }
            if (Region == null)
            {
                return base.Equals(other);
            }
            return base.Equals(other) && Region.Equals(other?.Region);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                int hash = base.GetHashCode();
                if (Region == null)
                {
                    return hash;
                }
                hash = (hash * 16777619) ^ Region.GetHashCode();
                return hash;
            }
        }

        public ConstellationViewModel(Constellation constellation)
        {
            Constellation = constellation;
        }
    }
}