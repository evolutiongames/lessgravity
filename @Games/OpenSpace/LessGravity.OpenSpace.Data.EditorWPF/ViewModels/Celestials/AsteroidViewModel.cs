﻿using LessGravity.OpenSpace.Data.Celestials;

namespace LessGravity.OpenSpace.Data.Editor.ViewModels.Celestials
{
    public class AsteroidViewModel : CelestialObjectViewModel
    {
        public Asteroid Asteroid { get; private set; }

        public AsteroidViewModel(Asteroid asteroid)
        {
            Asteroid = asteroid;
        }
    }
}