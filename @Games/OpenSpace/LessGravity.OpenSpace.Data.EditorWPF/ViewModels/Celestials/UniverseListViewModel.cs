﻿using AutoMapper;
using LessGravity.OpenSpace.Data.Celestials;
using LessGravity.OpenSpace.Data.Editor.Controls.Celestials;
using LessGravity.OpenSpace.Services;
using LessGravity.Pluto.Commands;
using LessGravity.Pluto.ViewModels;
using MaterialDesignThemes.Wpf;
using System.Linq;
using System.Threading.Tasks;

namespace LessGravity.OpenSpace.Data.Editor.ViewModels.Celestials
{
    public class UniverseListViewModel : ViewModel<UniverseViewModel>
    {
        private readonly ICelestialsService _celestialsService;

        public IAsyncCommand AddCommand => new AsyncCommand<UniverseViewModel>(RunAddDialog);

        public override async Task LoadAsync()
        {
            var universeViewModels = Task.Run(() => _celestialsService.GetAllUniverses().Select(Mapper.Map<UniverseViewModel>));
            Items.AddRange(await universeViewModels);
        }

        private async Task RunAddDialog(UniverseViewModel universeViewModel)
        {
            var universeControl = new UniverseControl
            {
                DataContext = universeViewModel
            };
            await DialogHost.Show(universeControl, "RootDialog");
            Items.Add(universeViewModel);

            _celestialsService.AddUniverse(Mapper.Map<Universe>(universeViewModel));
        }

        public UniverseListViewModel(ICelestialsService celestialsService)
        {
            _celestialsService = celestialsService;
        }
    }
}