﻿using LessGravity.OpenSpace.Data.Celestials;

namespace LessGravity.OpenSpace.Data.Editor.ViewModels.Celestials
{
    public class UniverseViewModel : ObjectBaseViewModel
    {
        public Universe Universe { get; private set; }

        public override bool Equals(object obj)
        {
            var other = obj as UniverseViewModel;
            if (string.IsNullOrEmpty(Name))
            {
                return Id.Equals(other?.Id);
            }
            return Id.Equals(other?.Id) && Name.Equals(other?.Name);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                int hash = (int)2166136261;
                hash = (hash * 16777619) ^ Id.GetHashCode();
                if (string.IsNullOrEmpty(Name))
                {
                    return hash;
                }
                hash = (hash * 16777619) ^ Name.GetHashCode();
                return hash;
            }
        }

        public UniverseViewModel(Universe universe)
        {
            Universe = universe;
        }
    }
}