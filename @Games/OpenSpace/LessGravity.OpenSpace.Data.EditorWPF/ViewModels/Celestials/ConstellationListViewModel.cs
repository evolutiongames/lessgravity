﻿using AutoMapper;
using LessGravity.OpenSpace.Services;
using LessGravity.Pluto.Observables;
using LessGravity.Pluto.ViewModels;
using System.Linq;
using System.Threading.Tasks;

namespace LessGravity.OpenSpace.Data.Editor.ViewModels.Celestials
{
    public class ConstellationListViewModel : ViewModel<ConstellationViewModel>
    {
        private readonly ICelestialsService _celestialsService;

        private RangeObservableCollection<RegionViewModel> _regions;
        public RangeObservableCollection<RegionViewModel> Regions
        {
            get => _regions;
            set => SetValue(ref _regions, value);
        }

        public ConstellationListViewModel(ICelestialsService celestialsService)
        {
            _celestialsService = celestialsService;
            Regions = new RangeObservableCollection<RegionViewModel>();
        }

        public override async Task LoadAsync()
        {
            var constellationViewModels = Task.Run(() => _celestialsService.GetAllConstellations().Select(Mapper.Map<ConstellationViewModel>));

            var regionViewModels = Task.Run(() => _celestialsService.GetAllRegions().Select(Mapper.Map<RegionViewModel>));

            await Task.WhenAll(constellationViewModels, regionViewModels);

            Items.AddRange(constellationViewModels.Result);
            Regions.AddRange(regionViewModels.Result);
        }
    }
}