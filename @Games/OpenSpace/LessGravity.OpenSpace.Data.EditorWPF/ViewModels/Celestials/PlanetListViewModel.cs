﻿using AutoMapper;
using LessGravity.Pluto.Observables;
using LessGravity.Pluto.ViewModels;
using System.Linq;
using System.Threading.Tasks;
using LessGravity.OpenSpace.Services;

namespace LessGravity.OpenSpace.Data.Editor.ViewModels.Celestials
{
    public class PlanetListViewModel : ViewModel<PlanetViewModel>
    {
        private readonly ICelestialsService _celestialsService;

        private RangeObservableCollection<SolarSystemViewModel> _solarSystems;
        public RangeObservableCollection<SolarSystemViewModel> SolarSystems
        {
            get => _solarSystems;
            set => SetValue(ref _solarSystems, value);
        }

        public override async Task LoadAsync()
        {
            var planetViewModels = Task.Run(() => _celestialsService.GetAllPlanets().Select(Mapper.Map<PlanetViewModel>));
            var solarSystemViewModels = Task.Run(() => _celestialsService.GetAllSolarSystems().Select(Mapper.Map<SolarSystemViewModel>));

            await Task.WhenAll(planetViewModels, solarSystemViewModels);

            Items.AddRange(planetViewModels.Result);
            SolarSystems.AddRange(solarSystemViewModels.Result);
        }

        public PlanetListViewModel(ICelestialsService celestialsService)
        {
            _celestialsService = celestialsService;
            SolarSystems = new RangeObservableCollection<SolarSystemViewModel>();
        }
    }
}