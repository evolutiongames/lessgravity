﻿using LessGravity.OpenSpace.Data.Celestials;

namespace LessGravity.OpenSpace.Data.Editor.ViewModels.Celestials
{
    public class SolarSystemViewModel : ObjectBaseViewModel
    {
        public SolarSystem SolarSystem { get; private set; }

        private ConstellationViewModel _constellation;
        public ConstellationViewModel Constellation
        {
            get => _constellation;
            set => SetValue(ref _constellation, value);
        }

        public override bool Equals(object obj)
        {
            var other = obj as SolarSystemViewModel;
            if (other == null)
            {
                return false;
            }
            if (Constellation == null)
            {
                return base.Equals(other);
            }
            return base.Equals(other) && Constellation.Equals(other?.Constellation);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hash = base.GetHashCode();
                if (Constellation == null)
                {
                    return hash;
                }
                hash = (hash * 16777619) ^ Constellation.GetHashCode();
                return hash;
            }
        }

        public SolarSystemViewModel(SolarSystem solarSystem)
        {
            SolarSystem = solarSystem;
        }
    }
}