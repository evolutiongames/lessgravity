﻿using LessGravity.OpenSpace.Data.Celestials;

namespace LessGravity.OpenSpace.Data.Editor.ViewModels.Celestials
{
    public class StarViewModel : CelestialObjectViewModel
    {
        public Star Star { get; private set; }

        public StarViewModel(Star star)
        {
            Star = star;
        }
    }
}