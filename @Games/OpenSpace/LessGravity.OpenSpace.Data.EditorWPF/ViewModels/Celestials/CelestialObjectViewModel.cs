﻿namespace LessGravity.OpenSpace.Data.Editor.ViewModels.Celestials
{
    public class CelestialObjectViewModel : ObjectBaseViewModel
    {
        private SolarSystemViewModel _solarSystem;
        public SolarSystemViewModel SolarSystem
        {
            get => _solarSystem;
            set => SetValue(ref _solarSystem, value);
        }

        public override bool Equals(object obj)
        {
            var other = obj as CelestialObjectViewModel;
            if (other == null)
            {
                return false;
            }
            if (SolarSystem == null)
            {
                return base.Equals(other);
            }
            return base.Equals(other) && SolarSystem.Equals(other?.SolarSystem);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hash = base.GetHashCode();
                if (SolarSystem == null)
                {
                    return hash;
                }
                hash = (hash * 16777619) ^ SolarSystem.GetHashCode();
                return hash;
            }
        }
    }
}