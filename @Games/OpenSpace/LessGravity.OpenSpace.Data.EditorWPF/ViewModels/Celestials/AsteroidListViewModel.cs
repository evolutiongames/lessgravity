﻿using AutoMapper;
using LessGravity.OpenSpace.Services;
using LessGravity.Pluto.Observables;
using LessGravity.Pluto.ViewModels;
using System.Linq;
using System.Threading.Tasks;

namespace LessGravity.OpenSpace.Data.Editor.ViewModels.Celestials
{
    public class AsteroidListViewModel : ViewModel<AsteroidViewModel>
    {
        private readonly ICelestialsService _celestialsService;

        private RangeObservableCollection<SolarSystemViewModel> _solarSystems;
        public RangeObservableCollection<SolarSystemViewModel> SolarSystems
        {
            get => _solarSystems;
            set => SetValue(ref _solarSystems, value);
        }

        public override async Task LoadAsync()
        {
            var asteroidViewModels = Task.Run(() => _celestialsService.GetAllAsteroids().Select(Mapper.Map<AsteroidViewModel>));
            var solarSystemViewModels = Task.Run(() => _celestialsService.GetAllSolarSystems().Select(Mapper.Map<SolarSystemViewModel>));

            await Task.WhenAll(asteroidViewModels, solarSystemViewModels);

            Items.AddRange(asteroidViewModels.Result);
            SolarSystems.AddRange(solarSystemViewModels.Result);
        }

        public AsteroidListViewModel(ICelestialsService celestialsService)
        {
            _celestialsService = celestialsService;
            SolarSystems = new RangeObservableCollection<SolarSystemViewModel>();
        }
    }
}