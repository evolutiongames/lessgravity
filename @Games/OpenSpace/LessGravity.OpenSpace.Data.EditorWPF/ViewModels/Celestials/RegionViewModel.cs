﻿using LessGravity.OpenSpace.Data.Celestials;
using System.Collections.Generic;

namespace LessGravity.OpenSpace.Data.Editor.ViewModels.Celestials
{
    public class RegionViewModel : ObjectBaseViewModel
    {
        public static IList<UniverseViewModel> Universes { get; set; }

        public Region Region { get; private set; }

        private UniverseViewModel _universe;
        public UniverseViewModel Universe
        {
            get => _universe;
            set => SetValue(ref _universe, value);
        }

        public override bool Equals(object obj)
        {
            var other = obj as RegionViewModel;
            if (other == null)
            {
                return false;
            }
            if (Universe == null)
            {
                return base.Equals(other);
            }
            return base.Equals(other) && Universe.Equals(other?.Universe);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hash = base.GetHashCode();
                if (Universe == null)
                {
                    return hash;
                }
                hash = (hash * 16777619) ^ Universe.GetHashCode();
                return hash;
            }
        }

        public RegionViewModel(Region region)
        {
            Region = region;
        }
    }
}