﻿namespace LessGravity.OpenSpace.Data.Editor.ViewModels
{
    public class FactionViewModel : ObjectBaseViewModel
    {
        public Faction Faction { get; private set; }

        public FactionViewModel(Faction faction)
        {
            Faction = faction;
        }
    }
}