﻿using LessGravity.Pluto.Observables;

namespace LessGravity.OpenSpace.Data.Editor.ViewModels
{
    public class CategoryViewModel : ObservableObject
    {
        private bool _isLoaded;
        public bool IsLoaded
        {
            get => _isLoaded;
            set => SetValue(ref _isLoaded, value, SetIsLoaded);
        }

        public string _caption;
        public string Caption
        {
            get => _caption;
            set => SetValue(ref _caption, value);
        }

        private object _content;
        public object Content
        {
            get => _content;
            set => SetValue(ref _content, value, NotifyPropertyChanged);
        }

        private void SetIsLoaded()
        {

        }

        private void NotifyPropertyChanged()
        {

        }
    }
}