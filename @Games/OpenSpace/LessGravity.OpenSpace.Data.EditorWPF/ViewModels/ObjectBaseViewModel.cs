﻿using LessGravity.Pluto.Observables;
using System.Diagnostics;

namespace LessGravity.OpenSpace.Data.Editor.ViewModels
{
    [DebuggerDisplay("Type: {GetType().Name} Id: {Id} Name: {Name}")]
    public abstract class ObjectBaseViewModel : ObservableObject
    {
        private long _id;
        public long Id
        {
            get => _id;
            set => SetValue(ref _id, value);
        }

        private string _name;
        public string Name
        {
            get => _name;
            set => SetValue(ref _name, value);
        }

        public override bool Equals(object obj)
        {
            var other = obj as ObjectBaseViewModel;
            if (other == null)
            {
                return false;
            }
            if (string.IsNullOrEmpty(Name))
            {
                return Id.Equals(other?.Id);
            }
            return Id.Equals(other?.Id) && Name.Equals(other?.Name);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                int hash = (int)2166136261;
                hash = (hash * 16777619) ^ Id.GetHashCode();
                if (string.IsNullOrEmpty(Name))
                {
                    return hash;
                }
                hash = (hash * 16777619) ^ Name.GetHashCode();
                return hash;
            }
        }
    }
}