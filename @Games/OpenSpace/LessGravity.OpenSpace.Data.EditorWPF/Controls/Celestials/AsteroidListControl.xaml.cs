﻿using LessGravity.Pluto.Observables;
using System;

namespace LessGravity.OpenSpace.Data.Editor.Controls.Celestials
{
    public partial class AsteroidListControl : ViewModelControl
    {
        public AsteroidListControl()
        {
            InitializeComponent();
        }

        public AsteroidListControl(Func<ObservableObject> viewModelCreator)
            : base(viewModelCreator)
        {
            InitializeComponent();
        }
    }
}