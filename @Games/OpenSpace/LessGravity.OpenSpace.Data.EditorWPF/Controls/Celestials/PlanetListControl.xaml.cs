﻿using LessGravity.Pluto.Observables;
using System;

namespace LessGravity.OpenSpace.Data.Editor.Controls.Celestials
{
    public partial class PlanetListControl : ViewModelControl
    {
        public PlanetListControl()
        {
            InitializeComponent();
        }

        public PlanetListControl(Func<ObservableObject> viewModelCreator)
            : base(viewModelCreator)
        {
            InitializeComponent();
        }
    }
}