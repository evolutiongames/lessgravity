﻿using LessGravity.Pluto.Observables;
using System;

namespace LessGravity.OpenSpace.Data.Editor.Controls.Celestials
{
    public partial class SolarSystemListControl : ViewModelControl
    {
        public SolarSystemListControl()
        {
            InitializeComponent();
        }

        public SolarSystemListControl(Func<ObservableObject> viewModelCreator)
            : base(viewModelCreator)
        {
            InitializeComponent();
        }
    }
}