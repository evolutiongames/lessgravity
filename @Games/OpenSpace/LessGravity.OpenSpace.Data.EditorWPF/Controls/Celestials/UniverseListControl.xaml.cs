﻿using LessGravity.Pluto.Observables;
using System;

namespace LessGravity.OpenSpace.Data.Editor.Controls.Celestials
{
    public partial class UniverseListControl : ViewModelControl
    {
        public UniverseListControl()
        {
            InitializeComponent();
        }

        public UniverseListControl(Func<ObservableObject> viewModelCreator)
            : base(viewModelCreator)
        {
            InitializeComponent();
        }
    }
}