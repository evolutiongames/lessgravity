﻿using LessGravity.Pluto.Observables;
using System;

namespace LessGravity.OpenSpace.Data.Editor.Controls.Celestials
{
    public partial class StarListControl : ViewModelControl
    {
        public StarListControl()
        {
            InitializeComponent();
        }

        public StarListControl(Func<ObservableObject> viewModelCreator)
            : base(viewModelCreator)
        {
            InitializeComponent();
        }
    }
}