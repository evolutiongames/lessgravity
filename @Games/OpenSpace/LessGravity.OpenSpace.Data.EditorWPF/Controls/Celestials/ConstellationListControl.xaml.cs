﻿using LessGravity.Pluto.Observables;
using System;

namespace LessGravity.OpenSpace.Data.Editor.Controls.Celestials
{
    public partial class ConstellationListControl : ViewModelControl
    {
        public ConstellationListControl()
        {
            InitializeComponent();
        }

        public ConstellationListControl(Func<ObservableObject> viewModelCreator)
            : base(viewModelCreator)
        {
            InitializeComponent();
        }
    }
}