﻿using LessGravity.Pluto.Observables;
using System;

namespace LessGravity.OpenSpace.Data.Editor.Controls.Ships
{
    public partial class ShipListControl : ViewModelControl
    {
        public ShipListControl()
        {
            InitializeComponent();
        }

        public ShipListControl(Func<ObservableObject> viewModelCreator)
            : base(viewModelCreator)
        {
            InitializeComponent();
        }
    }
}