﻿using LessGravity.Pluto.Observables;
using System;

namespace LessGravity.OpenSpace.Data.Editor.Controls.Ships
{
    public partial class ShipModuleListControl : ViewModelControl
    {
        public ShipModuleListControl()
        {
            InitializeComponent();
        }

        public ShipModuleListControl(Func<ObservableObject> viewModelCreator)
            : base(viewModelCreator)
        {
            InitializeComponent();
        }
    }
}