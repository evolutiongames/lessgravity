﻿using LessGravity.Pluto.Observables;
using System;

namespace LessGravity.OpenSpace.Data.Editor.Controls
{
    public partial class CharacterListControl : ViewModelControl
    {
        public CharacterListControl()
        {
            InitializeComponent();
        }

        public CharacterListControl(Func<ObservableObject> viewModelCreator)
            : base(viewModelCreator)
        {
            InitializeComponent();
        }
    }
}