﻿using LessGravity.Pluto.Observables;
using System;

namespace LessGravity.OpenSpace.Data.Editor.Controls.ItemTemplates
{
    public partial class OreListControl : ViewModelControl
    {
        public OreListControl()
        {
            InitializeComponent();
        }

        public OreListControl(Func<ObservableObject> viewModelCreator)
            : base(viewModelCreator)
        {
            InitializeComponent();
        }
    }
}