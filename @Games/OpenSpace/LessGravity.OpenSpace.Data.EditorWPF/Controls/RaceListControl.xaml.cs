﻿using LessGravity.Pluto.Observables;
using System;

namespace LessGravity.OpenSpace.Data.Editor.Controls
{
    public partial class RaceListControl : ViewModelControl
    {
        public RaceListControl()
        {
            InitializeComponent();
        }

        public RaceListControl(Func<ObservableObject> viewModelCreator)
            : base(viewModelCreator)
        {
            InitializeComponent();
        }
    }
}