﻿using LessGravity.Pluto.Observables;
using MaterialDesignThemes.Wpf;
using System;
using System.Windows;
using System.Windows.Controls;

namespace LessGravity.OpenSpace.Data.Editor.Controls
{
    public class ViewModelControl : UserControl
    {
        private readonly Func<ObservableObject> _viewModelCreator;

        //
        // default ctor for wpf
        //
        public ViewModelControl()
        {
        }

        public ViewModelControl(Func<ObservableObject> viewModelCreator)
            : this()
        {
            DataContext = null;
            _viewModelCreator = viewModelCreator;
            Loaded += ViewModelControl_Loaded;
        }

        private async void ViewModelControl_Loaded(object sender, RoutedEventArgs e)
        {
            var viewModel = _viewModelCreator();
            DataContext = viewModel;

            await DialogHost.Show(new SampleProgressDialog(), "ContentDialog", async (s, openEventArgs) => 
            {
                await viewModel.LoadAsync();
                openEventArgs.Session.Close();
            }, null);
        }
    }
}