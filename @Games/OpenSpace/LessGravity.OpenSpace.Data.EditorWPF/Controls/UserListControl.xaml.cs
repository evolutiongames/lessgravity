﻿using LessGravity.Pluto.Observables;
using System;

namespace LessGravity.OpenSpace.Data.Editor.Controls
{
    public partial class UserListControl : ViewModelControl
    {
        public UserListControl()
        {
            InitializeComponent();
        }

        public UserListControl(Func<ObservableObject> viewModelCreator)
            : base(viewModelCreator)
        {
            InitializeComponent();
        }
    }
}