﻿using LessGravity.Pluto.Observables;
using System;

namespace LessGravity.OpenSpace.Data.Editor.Controls
{
    public partial class FactionListControl : ViewModelControl
    {
        public FactionListControl()
        {
            InitializeComponent();
        }

        public FactionListControl(Func<ObservableObject> viewModelCreator)
            : base(viewModelCreator)
        {
            InitializeComponent();
        }
    }
}