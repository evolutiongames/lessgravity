﻿using LessGravity.Pluto.Observables;
using System;

namespace LessGravity.OpenSpace.Data.Editor.Controls.Stations
{
    public partial class StationModuleListControl : ViewModelControl
    {
        public StationModuleListControl()
        {
            InitializeComponent();
        }

        public StationModuleListControl(Func<ObservableObject> viewModelCreator)
            : base(viewModelCreator)
        {
            InitializeComponent();
        }
    }
}