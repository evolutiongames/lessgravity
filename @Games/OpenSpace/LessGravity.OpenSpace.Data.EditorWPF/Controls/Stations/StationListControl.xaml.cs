﻿using LessGravity.Pluto.Observables;
using System;

namespace LessGravity.OpenSpace.Data.Editor.Controls.Stations
{
    public partial class StationListControl : ViewModelControl
    {
        public StationListControl()
        {
            InitializeComponent();
        }

        public StationListControl(Func<ObservableObject> viewModelCreator)
            : base(viewModelCreator)
        {
            InitializeComponent();
        }
    }
}