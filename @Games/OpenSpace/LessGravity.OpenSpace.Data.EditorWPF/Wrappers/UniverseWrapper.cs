﻿using LessGravity.OpenSpace.Data.Celestials;
using LessGravity.Pluto.Wrappers;

namespace LessGravity.OpenSpace.Data.Editor.Wrappers
{
    public class UniverseWrapper : ModelWrapper<Universe>
    {
        public UniverseWrapper(Universe universe) : base(universe)
        {

        }

        public long Id
        {
            get => GetValue<long>();
            set => SetValue(ref value);
        }

        public string Name
        {
            get => GetValue<string>();
            set => SetValue(ref value);
        }
    }
}