﻿using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations.Schema;

namespace LessGravity.OpenSpace.Data
{
    [Table("tbl_itemstorage")]
    public class ItemStorage : ObjectBase
    {
        public virtual Collection<Item> Items { get; set; }

        public double? MaxVolume { get; set; }
    }
}