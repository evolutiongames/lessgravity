﻿using LessGravity.OpenSpace.Data.Celestials;
using LessGravity.Pluto.Data;
using System.Collections.Generic;

namespace LessGravity.OpenSpace.Data.Repositories
{
    public interface IConstellationRepository<in TId> : IRepository<Constellation, TId>
    {
        IEnumerable<Constellation> GetAllConstellationsInRegion(Region region);
    }
}