﻿using LessGravity.OpenSpace.Data.Celestials;
using LessGravity.OpenSpace.Data.Context;
using System.Collections.Generic;

namespace LessGravity.OpenSpace.Data.Repositories
{
    public class StarRepository<TId> : GenericRepository<Star, TId>, IStarRepository<TId>
    {
        public StarRepository(GameDbContext context)
            : base(context) { }

        public IEnumerable<Star> GetAllStarsInSolarSystem(SolarSystem solarSystem)
        {
            return Find(star => star.SolarSystem == solarSystem);
        }

        public IEnumerable<Star> GetAllStarsInConstellation(Constellation constellation)
        {
            return Find(star => star.SolarSystem.Constellation == constellation);
        }

        public IEnumerable<Star> GetAllStarsInRegion(Region region)
        {
            return Find(star => star.SolarSystem.Constellation.Region == region);
        }
    }
}