﻿using LessGravity.OpenSpace.Data.Celestials;
using LessGravity.OpenSpace.Data.Context;
using System.Collections.Generic;

namespace LessGravity.OpenSpace.Data.Repositories
{
    public class PlanetRepository<TId> : GenericRepository<Planet, TId>, IPlanetRepository<TId>
    {
        public PlanetRepository(GameDbContext context) 
            : base(context) { }

        public IEnumerable<Planet> GetAllPlanetsInSolarSystem(SolarSystem solarSystem)
        {
            return Find(planet => planet.SolarSystem == solarSystem);
        }

        public IEnumerable<Planet> GetAllPlanetsInConstellation(Constellation constellation)
        {
            return Find(planet => planet.SolarSystem.Constellation == constellation);
        }

        public IEnumerable<Planet> GetAllPlanetsInRegion(Region region)
        {
            return Find(planet => planet.SolarSystem.Constellation.Region == region);
        }
    }
}