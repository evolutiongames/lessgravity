﻿using LessGravity.OpenSpace.Data.Celestials;
using LessGravity.Pluto.Data;
using System.Collections.Generic;

namespace LessGravity.OpenSpace.Data.Repositories
{
    public interface IRegionRepository<in TId> : IRepository<Region, TId>
    {
        IEnumerable<Region> GetAllRegionsInUniverse(Universe universe);
    }
}