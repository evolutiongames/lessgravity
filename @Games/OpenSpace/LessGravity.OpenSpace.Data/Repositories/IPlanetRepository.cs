﻿using LessGravity.OpenSpace.Data.Celestials;
using LessGravity.Pluto.Data;
using System.Collections.Generic;

namespace LessGravity.OpenSpace.Data.Repositories
{
    public interface IPlanetRepository<in TId> : IRepository<Planet, TId>
    {
        IEnumerable<Planet> GetAllPlanetsInSolarSystem(SolarSystem solarSystem);
        IEnumerable<Planet> GetAllPlanetsInConstellation(Constellation constellation);
        IEnumerable<Planet> GetAllPlanetsInRegion(Region region);
    }
}