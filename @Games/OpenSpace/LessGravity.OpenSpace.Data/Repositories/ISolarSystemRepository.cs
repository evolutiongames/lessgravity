﻿using LessGravity.OpenSpace.Data.Celestials;
using LessGravity.Pluto.Data;
using System.Collections.Generic;

namespace LessGravity.OpenSpace.Data.Repositories
{
    public interface ISolarSystemRepository<in TId> : IRepository<SolarSystem, TId>
    {
        IEnumerable<SolarSystem> GetAllSolarSystemsInConstellation(Constellation constellation);
        IEnumerable<SolarSystem> GetAllSolarSystemsInRegion(Region region);
    }
}