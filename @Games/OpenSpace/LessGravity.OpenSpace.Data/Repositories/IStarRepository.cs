﻿using LessGravity.OpenSpace.Data.Celestials;
using LessGravity.Pluto.Data;
using System.Collections.Generic;

namespace LessGravity.OpenSpace.Data.Repositories
{
    public interface IStarRepository<in TId> : IRepository<Star, TId>
    {
        IEnumerable<Star> GetAllStarsInSolarSystem(SolarSystem solarSystem);
        IEnumerable<Star> GetAllStarsInConstellation(Constellation constellation);
        IEnumerable<Star> GetAllStarsInRegion(Region region);
    }
}