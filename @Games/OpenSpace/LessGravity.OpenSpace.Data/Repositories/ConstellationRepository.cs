﻿using LessGravity.OpenSpace.Data.Celestials;
using LessGravity.OpenSpace.Data.Context;
using System;
using System.Collections.Generic;

namespace LessGravity.OpenSpace.Data.Repositories
{
    public class ConstellationRepository<TId> : GenericRepository<Constellation, TId>, IConstellationRepository<TId>
    {
        public ConstellationRepository(GameDbContext context)
            : base(context) { }

        public IEnumerable<Constellation> GetAllConstellationsInRegion(Region region)
        {
            if (region == null) throw new ArgumentNullException(nameof(region));

            return Find(constellation => constellation.Region == region);
        }
    }
}