﻿using LessGravity.OpenSpace.Data.Celestials;
using LessGravity.Pluto.Data;
using System.Collections.Generic;

namespace LessGravity.OpenSpace.Data.Repositories
{
    public interface IStationRepository<in TId> : IRepository<Station, TId>
    {
        IEnumerable<Station> GetAllStationsInSolarSystem(SolarSystem solarSystem);
        IEnumerable<Station> GetAllStationsInConstellation(Constellation constellation);
        IEnumerable<Station> GetAllStationsInRegion(Region region);
    }
}