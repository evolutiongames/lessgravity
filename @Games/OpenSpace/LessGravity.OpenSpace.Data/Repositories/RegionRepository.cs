﻿using LessGravity.OpenSpace.Data.Celestials;
using LessGravity.OpenSpace.Data.Context;
using System;
using System.Collections.Generic;

namespace LessGravity.OpenSpace.Data.Repositories
{
    public class RegionRepository<TId> : GenericRepository<Region, TId>, IRegionRepository<TId>
    {
        public RegionRepository(GameDbContext context)
            : base(context) { }

        public IEnumerable<Region> GetAllRegionsInUniverse(Universe universe)
        {
            if (universe == null) throw new ArgumentNullException(nameof(universe));

            return Find(region => region.Universe == universe);
        }
    }
}