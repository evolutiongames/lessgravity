﻿using LessGravity.OpenSpace.Data.Celestials;
using LessGravity.Pluto.Data;
using System.Collections.Generic;

namespace LessGravity.OpenSpace.Data.Repositories
{
    public interface IAsteroidRepository<in TId> : IRepository<Asteroid, TId>
    {
        IEnumerable<Asteroid> GetAllAsteroidsInSolarSystem(SolarSystem solarSystem);
        IEnumerable<Asteroid> GetAllAsteroidsInConstellation(Constellation constellation);
        IEnumerable<Asteroid> GetAllAsteroidsInRegion(Region region);
    }
}