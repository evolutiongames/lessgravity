﻿using LessGravity.OpenSpace.Data.Celestials;
using LessGravity.OpenSpace.Data.Context;
using System.Collections.Generic;

namespace LessGravity.OpenSpace.Data.Repositories
{
    public class StationRepository<TId> : GenericRepository<Station, TId>, IStationRepository<TId>
    {
        public StationRepository(GameDbContext context) 
            : base(context) { }

        public IEnumerable<Station> GetAllStationsInSolarSystem(SolarSystem solarSystem)
        {
            return Find(station => station.SolarSystem == solarSystem);
        }

        public IEnumerable<Station> GetAllStationsInConstellation(Constellation constellation)
        {
            return Find(station => station.SolarSystem.Constellation == constellation);
        }

        public IEnumerable<Station> GetAllStationsInRegion(Region region)
        {
            return Find(station => station.SolarSystem.Constellation.Region == region);
        }
    }
}