﻿using LessGravity.OpenSpace.Data.Celestials;
using LessGravity.OpenSpace.Data.Context;
using System.Collections.Generic;

namespace LessGravity.OpenSpace.Data.Repositories
{
    public class SolarSystemRepository<TId> : GenericRepository<SolarSystem, TId>, ISolarSystemRepository<TId>
    {
        public SolarSystemRepository(GameDbContext context)
            : base(context) { }

        public IEnumerable<SolarSystem> GetAllSolarSystemsInConstellation(Constellation constellation)
        {
            return Find(solarSystem => solarSystem.Constellation == constellation);
        }

        public IEnumerable<SolarSystem> GetAllSolarSystemsInRegion(Region region)
        {
            return Find(solarSystem => solarSystem.Constellation.Region == region);
        }
    }
}