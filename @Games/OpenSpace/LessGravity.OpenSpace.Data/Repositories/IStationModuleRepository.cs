﻿using LessGravity.OpenSpace.Data.Celestials;
using LessGravity.OpenSpace.Data.StationModules;
using LessGravity.Pluto.Data;
using System.Collections.Generic;

namespace LessGravity.OpenSpace.Data.Repositories
{
    public interface IStationModuleRepository<in TId> : IRepository<StationModule, TId>
    {
        IEnumerable<StationModule> GetAllStationModulesInStation(Station station);
    }
}