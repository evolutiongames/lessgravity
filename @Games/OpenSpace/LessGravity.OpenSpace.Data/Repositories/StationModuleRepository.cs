﻿using LessGravity.OpenSpace.Data.Celestials;
using LessGravity.OpenSpace.Data.Context;
using LessGravity.OpenSpace.Data.StationModules;
using System.Collections.Generic;
using System.Data.Entity;

namespace LessGravity.OpenSpace.Data.Repositories
{
    public class StationModuleRepository<TId> : GenericRepository<StationModule, TId>, IStationModuleRepository<TId>
    {
        public StationModuleRepository(DbContext context)
            : base(context) { }

        public IEnumerable<StationModule> GetAllStationModulesInStation(Station station)
        {
            return Find(stationModule => stationModule.Station == station);
        }
    }
}