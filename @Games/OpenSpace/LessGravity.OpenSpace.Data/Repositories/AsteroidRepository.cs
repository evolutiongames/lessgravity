﻿using LessGravity.OpenSpace.Data.Celestials;
using LessGravity.OpenSpace.Data.Context;
using System.Collections.Generic;

namespace LessGravity.OpenSpace.Data.Repositories
{
    public class AsteroidRepository<TId> : GenericRepository<Asteroid, TId>, IAsteroidRepository<TId>
    {
        public AsteroidRepository(GameDbContext context) 
            : base(context) { }

        public IEnumerable<Asteroid> GetAllAsteroidsInSolarSystem(SolarSystem solarSystem)
        {
            return Find(asteroid => asteroid.SolarSystem == solarSystem);
        }

        public IEnumerable<Asteroid> GetAllAsteroidsInConstellation(Constellation constellation)
        {
            return Find(asteroid => asteroid.SolarSystem.Constellation == constellation);
        }

        public IEnumerable<Asteroid> GetAllAsteroidsInRegion(Region region)
        {
            return Find(asteroid => asteroid.SolarSystem.Constellation.Region == region);
        }
    }
}