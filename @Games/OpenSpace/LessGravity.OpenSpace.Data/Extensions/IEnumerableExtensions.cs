﻿using System;
using System.Collections.Generic;

namespace LessGravity.OpenSpace.Data.Extensions
{
    public static class IEnumerableExtensions
    {
        public static T RandomElement<T>(this IEnumerable<T> source, Random rng = null)
        {
            if (rng == null)
            {
                rng = new Random();
            }
            var current = default(T);
            var count = 0;
            foreach (var element in source)
            {
                count++;
                if (rng.Next(count) == 0)
                {
                    current = element;
                }
            }
            if (count == 0)
            {
                throw new InvalidOperationException("Sequence was empty");
            }
            return current;
        }
    }
}