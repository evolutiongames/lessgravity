﻿using LessGravity.OpenSpace.Data.ItemTemplates;
using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Diagnostics;

/*
Rank	Level 1	Level 2	Level 3	Level 4	Level 5
1	    250	    1.414	8.000	45.254	256.000
2	    500	    2.828	16.000	90.509	512.000
3	    750	    4.242	24.000	135.764	768.000
4	    1.000	5.656	32.000	181.019	1.024.000
5	    1.250	7.071	40.000	226.274	1.280.000
6	    1.500	8.485	48.000	271.529	1.536.000
7	    1.750	9.899	56.000	316.783	1.792.000
8	    2.000	11.313	64.000	362.038	2.048.000
9	    2.250	12.727	72.000	407.293	2.304.000
10	    2.500	14.142	80.000	452.548	2.560.000
11	    2.750	15.556	88.000	497.803	2.816.000
12	    3.000	16.970	96.000	543.058	3.072.000
13	    3.250	18.384	104.000	588.312	3.328.000
14	    3.500	19.798	112.000	633.567	3.584.000
15	    3.750	21.213	120.000	678.822	3.840.000
16	    4.000	22.627	128.000	724.077	4.096.000
 */

namespace LessGravity.OpenSpace.Data
{
    [DebuggerDisplay("Name = {Name}, SP {SkillPoints}/{MaxSkillPointsForCurrentLevel}")]
    [Table("tbl_character_skill")]
    public class CharacterSkill  : ObjectBase
    {
        public Character Character { get; set; }
        public Skill Skill { get; set; }

        private int _level;

        public int Level
        {
            get => _level;
            set
            {
                if (_level == value)
                {
                    return;
                }
                _level = value;
                if (_level == 1)
                {
                    SkillPoints = 0.0;
                }
                else
                {
                    //
                    // sets the skillpoints to the lower end of the new level
                    SkillPoints = Math.Floor(Math.Pow(2, 2.5 * (_level - 2)) * 250 * Skill.Rank);
                }
            }
        }

        public double SkillPoints { get; set; }

        public bool IsLearned { get; set; }

        [NotMapped]
        public double MaxSkillPointsForCurrentLevel => Math.Floor(Math.Pow(2, 2.5 * (Level - 1)) * 250 * Skill.Rank);

        public override string Name
        {
            get => $"{Skill.Name} {Level}";
            set => base.Name = value;
        }

        [NotMapped]
        public TimeSpan TimeUntilSkillLearned
        {
            get
            {
                var skillPointsPerMinute = GetSkillpointsPerMinute();
                return TimeSpan.FromSeconds((MaxSkillPointsForCurrentLevel - SkillPoints) / skillPointsPerMinute);
            }
        }

        private double GetSkillpointsPerMinute()
        {
            var primaryAttribute = Character.SkillAttributeDictionary[Skill.PrimaryAttribute];
            var secondaryAttribute = Character.SkillAttributeDictionary[Skill.SecondaryAttribute];
            return (primaryAttribute + (secondaryAttribute / 2));
        }

        public void Tick()
        {
            SkillPoints += GetSkillpointsPerMinute();
            if (SkillPoints >= MaxSkillPointsForCurrentLevel)
            {
                Level++;
            }
            if (Level > 5)
            {
                Level = 5;
                IsLearned = true;
            }
        }
    }
}