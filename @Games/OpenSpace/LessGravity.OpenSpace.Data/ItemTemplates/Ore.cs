﻿using System.ComponentModel.DataAnnotations.Schema;

namespace LessGravity.OpenSpace.Data.ItemTemplates
{
    [Table("tbl_itemtemplate_ore")]
    public class Ore : ItemTemplate
    {
        public double BaseRefineryYied { get; set; }
    }
}