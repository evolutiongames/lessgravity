﻿using System.ComponentModel.DataAnnotations.Schema;

namespace LessGravity.OpenSpace.Data.ItemTemplates
{
    [Table("tbl_itemtemplate_chemicalelement")]
    public class ChemicalElement : ItemTemplate
    {
        public int AtomicNumber { get; set; }

        public double MeltingPoint { get; set; }

        public double BoilingPoint { get; set; }

        public double MolarMass { get; set; }

        public string Symbol { get; set; }

        public int Group { get; set; }
    }
}