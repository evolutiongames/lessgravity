﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LessGravity.OpenSpace.Data.ItemTemplates
{
    [Table("tbl_itemtemplateresource_in")]
    public class ItemResourceIn : IEquatable<ItemResourceIn>
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }

        public int Quantity { get; set; }

        public virtual ItemTemplate Template { get; set; }

        public bool Equals(ItemResourceIn other)
        {
            return (Template == other.Template && Quantity == other.Quantity);
        }

        public int GetHashCode(ItemResourceIn itemQuantity)
        {
            var hashCode = 71;
            hashCode = hashCode * 37 ^ itemQuantity.Template.GetHashCode();
            hashCode = hashCode * 37 ^ itemQuantity.Quantity.GetHashCode();
            return hashCode;
        }

        public override string ToString()
        {
            return $"{Quantity}x {Template?.Name}";
        }
    }
}