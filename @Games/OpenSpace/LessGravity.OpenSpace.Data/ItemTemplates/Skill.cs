﻿using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations.Schema;

namespace LessGravity.OpenSpace.Data.ItemTemplates
{
    [Table("tbl_itemtemplate_skill")]
    public class Skill : ItemTemplate
    {
        public int Rank { get; set; }

        public string PrimaryAttribute { get; set; }

        public string SecondaryAttribute { get; set; }

        public virtual Collection<Skill> RequiredSkills { get; set; }
    }
}