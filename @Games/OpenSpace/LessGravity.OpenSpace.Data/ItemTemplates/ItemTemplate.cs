﻿using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LessGravity.OpenSpace.Data.ItemTemplates
{
    [Table("tbl_itemtemplate")]
    public abstract class ItemTemplate : ObjectBase
    {
        public virtual AttributeSet Attributes { get; set; }

        [MaxLength(64)]
        public string Category { get; set; }

        [MaxLength(64)]
        public string SubCategory { get; set; }

        [MaxLength(256)]
        public string Description { get; set; }

        public double Mass { get; set; }

        public double Volume { get; set; }

        public string Resource2d { get; set; }

        public string Resource3d { get; set; }

        public bool IsStackable { get; set; }

        public decimal BasePrice { get; set; }

        public virtual Collection<ItemResourceIn> ResourcesIn { get; set; }

        public virtual Collection<ItemResourceOut> ResourcesOut { get; set; }
    }
}