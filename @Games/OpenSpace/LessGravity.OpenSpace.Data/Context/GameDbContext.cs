﻿using System;
using LessGravity.OpenSpace.Data.Celestials;
using LessGravity.OpenSpace.Data.ItemTemplates;
using LessGravity.OpenSpace.Data.ShipModules;
using LessGravity.OpenSpace.Data.Ships;
using LessGravity.OpenSpace.Data.StationModules;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace LessGravity.OpenSpace.Data.Context
{
    public class GameDbContext : DbContext
    {
        public virtual DbSet<ItemTemplate> ItemTemplates { get; set; }

        public virtual DbSet<ShipModule> ShipModules { get; set; }
        public virtual DbSet<Ship> Ships { get; set; }

        public virtual DbSet<CelestialObject> Celestials { get; set; }
        public virtual DbSet<Universe> Universes { get; set; }
        public virtual DbSet<Region> Regions { get; set; }
        public virtual DbSet<Constellation> Constellations { get; set; }
        public virtual DbSet<SolarSystem> SolarSystems { get; set; }
        public virtual DbSet<Star> Stars { get; set; }
        public virtual DbSet<Planet> Planets { get; set; }
        public virtual DbSet<Asteroid> Asteroids { get; set; }
        public virtual DbSet<Station> Stations { get; set; }
        public virtual DbSet<StationModule> StationModules { get; set; }

        public virtual DbSet<User> Users { get; set; }
        public virtual DbSet<Alliance> Alliances { get; set; }
        public virtual DbSet<Corporation> Corporations { get; set; }
        public virtual DbSet<Race> Races { get; set; }
        public virtual DbSet<Faction> Factions { get; set; }
        public virtual DbSet<Character> Characters { get; set; }

        public virtual DbSet<Bank> Banks { get; set; }
        public virtual DbSet<BankAccount> BankAccounts { get; set; }
        public virtual DbSet<Wallet> Wallets { get; set; }

        public GameDbContext()
            : base("GameDb")
        {
            Configuration.LazyLoadingEnabled = false;
            //Configuration.ProxyCreationEnabled = false;

            Database.SetInitializer(new GameDbInitializer());

            Console.WriteLine("Context Created");
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();

            modelBuilder.Entity<Region>().HasRequired(region => region.Universe);

            modelBuilder.Entity<Constellation>().HasRequired(constellation => constellation.Region);

            modelBuilder.Entity<SolarSystem>().HasRequired(solarSystem => solarSystem.Constellation);

            modelBuilder.Entity<Star>().HasRequired(star => star.SolarSystem);

            modelBuilder.Entity<Planet>().HasRequired(planet => planet.SolarSystem);

            modelBuilder.Entity<Asteroid>().HasRequired(asteroid => asteroid.SolarSystem);

            modelBuilder.Entity<Station>().HasRequired(station => station.SolarSystem);
            modelBuilder.Entity<Station>().HasMany(station => station.Modules);

            modelBuilder.Entity<Ship>().HasMany(ship => ship.Modules)
                /*.WithRequired(shipModule => shipModule.Ship).WillCascadeOnDelete(false)*/;

            modelBuilder.Entity<ItemTemplate>().HasMany(itemTemplate => itemTemplate.ResourcesIn);
            modelBuilder.Entity<ItemTemplate>().HasMany(itemTemplate => itemTemplate.ResourcesOut);

            modelBuilder.Entity<Skill>().HasMany(skill => skill.RequiredSkills);

            modelBuilder.Entity<Wallet>().HasMany(wallet => wallet.BankAccounts);

            modelBuilder.Entity<BankAccount>().HasMany(bankAccount => bankAccount.Transactions);

            modelBuilder.Entity<Character>().HasMany(character => character.SkillAttributes);
            modelBuilder.Entity<Character>().HasMany(character => character.Skills);
        }
    }
}