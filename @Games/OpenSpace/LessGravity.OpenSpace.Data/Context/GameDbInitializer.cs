﻿using LessGravity.OpenSpace.Data.Data;
using System.Data.Entity;

namespace LessGravity.OpenSpace.Data.Context
{
    public class GameDbInitializer : DropCreateDatabaseAlways<GameDbContext>
    {
        //private readonly GameDbStaticTestData _testData;
        private readonly GameDbGeneratedTestData _testData;

        public GameDbInitializer()
        {
            //_testData = new GameDbStaticTestData();
            _testData = new GameDbGeneratedTestData();
        }

        protected override void Seed(GameDbContext context)
        {
            base.Seed(context);

            _testData.PopulateTestData(context);
        }
    }
}