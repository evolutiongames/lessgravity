﻿using System.ComponentModel.DataAnnotations.Schema;

namespace LessGravity.OpenSpace.Data
{
    [Table("tbl_faction")]
    public class Faction : ObjectBase
    {
        public virtual AttributeSet Attributes { get; set; }
    }
}