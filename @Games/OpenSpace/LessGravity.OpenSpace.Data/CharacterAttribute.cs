﻿using System.ComponentModel.DataAnnotations.Schema;

namespace LessGravity.OpenSpace.Data
{
    [Table("tbl_character_attribute")]
    public class CharacterAttribute : ObjectBase
    {
        public int Value { get; set; }
    }
}