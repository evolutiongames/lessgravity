﻿namespace LessGravity.OpenSpace.Data
{
    public static class Strings
    {
        //
        // ObjectBase
        //
        public const string Id = "Id";
        public const string Name = "Name";

        //
        // Character
        //
        public const string UserId = "UserId";
        public const string RaceId = "RaceId";
        public const string CorporationId = "CorporationId";
        public const string WalletId = "WalletId";

        //
        // ItemTemplate
        //
        public const string BasePrice = "BasePrice";
        public const string Category = "Category";
        public const string SubCategory = "SubCategory";
        public const string Description = "Description";
        public const string IsStackable = "IsStackable";
        public const string Mass = "Mass";
        public const string Volume = "Volume";

        //
        // Position
        //
        public const string Position = "Position";

        //
        // User
        //
        public const string Password = "Password";
        public const string Role = "Role";
        public const string IsTrialAccount = "IsTrialAccount";
        public const string CreatedTimeStamp = "CreatedTimeStamp";
        public const string LoginTimeStamp = "LoginTimeStamp";
        public const string LastLoginTimeStamp = "LastLoginTimeStamp";
        public const string IsBanned = "IsBanned";
        public const string BannedTimeStamp = "BannedTimeStamp";
        public const string IsLocked = "IsLocked";
        public const string LockedTimeStamp = "LockedTimeStamp";

        public static class Client
        {

        }

        public static class Server
        {
            public const string Host = "Server.Host";
            public const string Port = "Server.Port";
        }

        public static class Session
        {
            public const string ApplicationName = "TestNetwork";
        }

        public static class Tables
        {
            
        }
    }
}