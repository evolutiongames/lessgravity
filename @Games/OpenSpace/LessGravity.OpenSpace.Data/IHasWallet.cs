﻿namespace LessGravity.OpenSpace.Data
{
    public interface IHasWallet
    {
        Wallet Wallet { get; set; }
    }
}