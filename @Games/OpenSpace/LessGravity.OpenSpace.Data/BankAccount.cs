﻿using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations.Schema;

namespace LessGravity.OpenSpace.Data
{
    [Table("tbl_bankaccount")]
    public class BankAccount : ObjectBase
    {
        public virtual Bank Bank { get; set; }

        public decimal Balance { get; set; }

        public virtual Collection<BankAccountTransaction> Transactions { get; set; }
    }
}