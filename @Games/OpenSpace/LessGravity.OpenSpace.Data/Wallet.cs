﻿using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;

namespace LessGravity.OpenSpace.Data
{
    [Table("tbl_wallet")]
    public class Wallet : ObjectBase
    {
        public virtual Collection<BankAccount> BankAccounts { get; set; }

        public virtual BankAccount DefaultBankAccount { get; set; }

        public decimal Balance
        {
            get
            {
                return BankAccounts.Sum(bankAccount => bankAccount.Balance);
            }
        }
    }
}