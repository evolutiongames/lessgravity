﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Diagnostics;

namespace LessGravity.OpenSpace.Data
{
    [DebuggerDisplay("Type: {GetType().Name} Name: {Name} Id: {Id}")]
    public abstract class ObjectBase
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }

        [MaxLength(128)]
        public virtual string Name { get; set; }

        public override string ToString()
        {
            return Name;
        }
    }
}