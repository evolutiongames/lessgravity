﻿namespace LessGravity.OpenSpace.Data
{
    public enum CharacterRole
    {
        Normal,
        GameMaster,
        Admin
    }
}