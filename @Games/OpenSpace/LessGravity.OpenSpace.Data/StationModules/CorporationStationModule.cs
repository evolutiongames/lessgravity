﻿using System.ComponentModel.DataAnnotations.Schema;

namespace LessGravity.OpenSpace.Data.StationModules
{
    [Table("tbl_stationmodule_corporation")]
    public class CorporationStationModule : StationModule
    {
        public virtual Corporation Corporation { get; set; }

        public virtual ItemStorage Storage { get; set; }
    }
}