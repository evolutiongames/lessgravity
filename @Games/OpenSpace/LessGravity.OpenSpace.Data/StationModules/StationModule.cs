﻿using LessGravity.OpenSpace.Data.Celestials;
using System.ComponentModel.DataAnnotations.Schema;

namespace LessGravity.OpenSpace.Data.StationModules
{
    [Table("tbl_stationmodule")]
    public abstract class StationModule : ObjectBase
    {
        public virtual Station Station { get; set; }
    }
}