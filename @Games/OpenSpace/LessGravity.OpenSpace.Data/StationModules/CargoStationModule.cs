﻿using System.ComponentModel.DataAnnotations.Schema;

namespace LessGravity.OpenSpace.Data.StationModules
{
    [Table("tbl_stationmodule_cargo")]
    public class CargoStationModule : StationModule
    {
        public virtual ItemStorage Storage { get; set; }
    }
}