﻿using System.ComponentModel.DataAnnotations.Schema;

namespace LessGravity.OpenSpace.Data.StationModules
{
    [Table("tbl_stationmodule_refinery")]
    public class RefineryStationModule : StationModule
    {
        public double Yield { get; set; }

        public double Fee { get; set; }
    }
}