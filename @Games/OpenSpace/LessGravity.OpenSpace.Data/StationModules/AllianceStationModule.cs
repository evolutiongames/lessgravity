﻿using System.ComponentModel.DataAnnotations.Schema;

namespace LessGravity.OpenSpace.Data.StationModules
{
    [Table("tbl_stationmodule_alliance")]
    public class AllianceStationModule
    {
        public virtual Alliance Alliance { get; set; }

        public virtual ItemStorage Storage { get; set; }
    }
}