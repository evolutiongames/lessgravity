﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace LessGravity.OpenSpace.Data
{
    [Table("tbl_user")]
    public class User : ObjectBase
    {
        public string PasswordHash { get; set; }

        public DateTime DateCreated { get; set; }

        public bool IsBanned { get; set; }
        public bool IsLoggedIn { get; set; }
        public bool IsTrialAccount { get; set; }
        public DateTime? DateLogin { get; set; }
        public DateTime? DateLastLogin { get; set; }
        public DateTime? DateBanned { get; set; }

        public UserRole Role { get; set; }
    }
}