﻿using LessGravity.OpenSpace.Data.Celestials;
using LessGravity.OpenSpace.Data.Repositories;
using LessGravity.Pluto.Data;

namespace LessGravity.OpenSpace.Data.UnitsOfWork
{
    public interface ICelestialsUnitOfWork<in TId> : IUnitOfWork
    {
        IRepository<Universe, TId> UniverseRepository { get; }
        IRegionRepository<TId> RegionRepository { get; }
        IConstellationRepository<TId> ConstellationRepository { get; }
        ISolarSystemRepository<TId> SolarSystemRepository { get; }
        IStarRepository<TId> StarRepository { get; }
        IPlanetRepository<TId> PlanetRepository { get; }
        IAsteroidRepository<TId> AsteroidRepository { get; }
    }
}