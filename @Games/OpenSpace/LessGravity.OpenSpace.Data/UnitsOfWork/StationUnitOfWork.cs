﻿using LessGravity.OpenSpace.Data.Context;
using LessGravity.OpenSpace.Data.Repositories;

namespace LessGravity.OpenSpace.Data.UnitsOfWork
{
    public class StationUnitOfWork<TId> : IStationUnitOfWork<TId>
    {
        private readonly GameDbContext _context;

        public IStationRepository<TId> StationRepository { get; private set; }
        public IStationModuleRepository<TId> StationModuleRepository { get; private set; }

        public StationUnitOfWork(GameDbContext context)
        {
            _context = context;
            StationRepository = new StationRepository<TId>(_context);
            StationModuleRepository = new StationModuleRepository<TId>(_context);
        }

        public void Dispose()
        {
            //_context.Dispose();
        }

        public int SaveChanges()
        {
            return _context.SaveChanges();
        }
    }
}