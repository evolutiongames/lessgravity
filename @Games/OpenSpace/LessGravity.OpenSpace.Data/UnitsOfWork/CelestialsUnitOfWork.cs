﻿using LessGravity.OpenSpace.Data.Celestials;
using LessGravity.OpenSpace.Data.Context;
using LessGravity.OpenSpace.Data.Repositories;
using LessGravity.Pluto.Data;

namespace LessGravity.OpenSpace.Data.UnitsOfWork
{
    public class CelestialsUnitOfWork<TId> : ICelestialsUnitOfWork<TId>
    {
        public IRepository<Universe, TId> UniverseRepository { get; }
        public IRegionRepository<TId> RegionRepository { get; }
        public IConstellationRepository<TId> ConstellationRepository { get; }
        public ISolarSystemRepository<TId> SolarSystemRepository { get; }
        public IStarRepository<TId> StarRepository { get; }
        public IPlanetRepository<TId> PlanetRepository { get; }
        public IAsteroidRepository<TId> AsteroidRepository { get; }
        public IStationRepository<TId> StationRepository { get; }

        private readonly GameDbContext _dbContext;

        public CelestialsUnitOfWork(GameDbContext context)
        {
            _dbContext = context;

            UniverseRepository = new GenericRepository<Universe, TId>(_dbContext);
            RegionRepository = new RegionRepository<TId>(_dbContext);
            ConstellationRepository = new ConstellationRepository<TId>(_dbContext);
            SolarSystemRepository = new SolarSystemRepository<TId>(_dbContext);
            StarRepository = new StarRepository<TId>(_dbContext);
            PlanetRepository = new PlanetRepository<TId>(_dbContext);
            AsteroidRepository = new AsteroidRepository<TId>(_dbContext);
            StationRepository = new StationRepository<TId>(_dbContext);
        }

        public int SaveChanges()
        {
            return _dbContext.SaveChanges();
        }

        public void Dispose()
        {
            //_dbContext.Dispose();
        }
    }
}