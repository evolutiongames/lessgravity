﻿using LessGravity.OpenSpace.Data.Repositories;
using LessGravity.Pluto.Data;

namespace LessGravity.OpenSpace.Data.UnitsOfWork
{
    public interface IStationUnitOfWork<in TId> : IUnitOfWork
    {
        IStationRepository<TId> StationRepository { get; }
        IStationModuleRepository<TId> StationModuleRepository { get; }
    }
}