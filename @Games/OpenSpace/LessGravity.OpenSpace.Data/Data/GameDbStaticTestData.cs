﻿using LessGravity.OpenSpace.Data.Celestials;
using LessGravity.OpenSpace.Data.Context;
using LessGravity.OpenSpace.Data.ItemTemplates;
using LessGravity.OpenSpace.Data.ShipModules;
using LessGravity.OpenSpace.Data.Ships;
using LessGravity.OpenSpace.Data.StationModules;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.Entity.Infrastructure;
using System.Linq;

namespace LessGravity.OpenSpace.Data.Data
{
    internal class GameDbStaticTestData
    {
        private readonly Lazy<Dictionary<string, Universe>> _universes;
        private readonly Lazy<Dictionary<string, Region>> _regions;
        private readonly Lazy<Dictionary<string, Constellation>> _constellations;
        private readonly Lazy<Dictionary<string, SolarSystem>> _solarsystems;
        private readonly Lazy<Dictionary<string, Star>> _stars;
        private readonly Lazy<Dictionary<string, Planet>> _planets;
        private readonly Lazy<Dictionary<string, Asteroid>> _asteroids;
        private readonly Lazy<Dictionary<string, Station>> _stations;

        private readonly Lazy<Dictionary<string, StationModule>> _stationModules;
        private readonly Lazy<Dictionary<string, Ship>> _ships;
        private readonly Lazy<Dictionary<string, ShipModule>> _shipModules;

        private readonly Lazy<Dictionary<string, User>> _users;
        private readonly Lazy<Dictionary<string, Alliance>> _alliances;
        private readonly Lazy<Dictionary<string, Corporation>> _corporations;
        private readonly Lazy<Dictionary<string, Race>> _races;
        private readonly Lazy<Dictionary<string, Character>> _characters;
        private readonly Lazy<Dictionary<string, BankAccount>> _bankAccounts;
        private readonly Lazy<Dictionary<string, Bank>> _banks;

        private readonly Lazy<Dictionary<string, ItemStorage>> _itemStorages;

        private readonly Lazy<Dictionary<string, Skill>> _skillsLevel1;
        private readonly Lazy<Dictionary<string, Skill>> _skillsLevel2;
        private readonly Lazy<Dictionary<string, Skill>> _skillsLevel3;
        private readonly Lazy<Dictionary<string, Skill>> _skillsLevel4;
        private readonly Lazy<Dictionary<string, Skill>> _skillsLevel5;
        private readonly Lazy<Dictionary<string, Ore>> _ores;
        private readonly Lazy<Dictionary<string, ChemicalElement>> _chemicalElements;

        #region Celestials

        private IEnumerable<Universe> GetUniverses()
        {
            return new[]
            {
                new Universe { Name = "Universe 1" }
            };
        }

        private IEnumerable<Region> GetRegions()
        {
            return new[]
            {
                new Region { Id = 1000000, Name = "The Bleak Lands", Universe = _universes.Value["Universe 1"] },
                new Region { Id = 1000001, Name = "Kor-Azor", Universe = _universes.Value["Universe 1"] },
                new Region { Id = 1000002, Name = "Deceit", Universe = _universes.Value["Universe 1"] }
            };
        }

        private IEnumerable<Constellation> GetConstellations()
        {
            return new[]
            {
                new Constellation { Id = 2000000, Name = "Ichida", Region = _regions.Value["The Bleak Lands"] },
                new Constellation { Id = 2000001, Name = "Sasen", Region = _regions.Value["The Bleak Lands"] },
                new Constellation { Id = 2000002, Name = "Vaarma", Region = _regions.Value["The Bleak Lands"] },
                new Constellation { Id = 2000003, Name = "Comadu", Region = _regions.Value["Kor-Azor"] },
                new Constellation { Id = 2000004, Name = "Finena", Region = _regions.Value["Kor-Azor"] },
                new Constellation { Id = 2000005, Name = "Jatari", Region = _regions.Value["Kor-Azor"] },
                new Constellation { Id = 2000006, Name = "Orus", Region = _regions.Value["Kor-Azor"] },
            };
        }

        private IEnumerable<SolarSystem> GetSolarsystems()
        {
            return new[]
            {
                new SolarSystem { Id = 3000000, Name = "Hakodan", Constellation = _constellations.Value["Ichida"] },
                new SolarSystem { Id = 3000001, Name = "Junsen", Constellation = _constellations.Value["Ichida"] },
                new SolarSystem { Id = 3000002, Name = "Ronne", Constellation = _constellations.Value["Ichida"] },
                new SolarSystem { Id = 3000003, Name = "Huola", Constellation = _constellations.Value["Sasen"] },
                new SolarSystem { Id = 3000004, Name = "Kamela", Constellation = _constellations.Value["Sasen"] },
                new SolarSystem { Id = 3000005, Name = "Otelen", Constellation = _constellations.Value["Sasen"] },
                new SolarSystem { Id = 3000006, Name = "Anka", Constellation = _constellations.Value["Vaarma"] },
                new SolarSystem { Id = 3000007, Name = "Saikamon", Constellation = _constellations.Value["Vaarma"] },
                new SolarSystem { Id = 3000008, Name = "Sosala", Constellation = _constellations.Value["Vaarma"] },
                new SolarSystem { Id = 3000009, Name = "Piri", Constellation = _constellations.Value["Comadu"] },
                new SolarSystem { Id = 3000010, Name = "Rannoze", Constellation = _constellations.Value["Comadu"] },
                new SolarSystem { Id = 3000011, Name = "Shokal", Constellation = _constellations.Value["Comadu"] },
                new SolarSystem { Id = 3000012, Name = "Misha", Constellation = _constellations.Value["Finena"] },
                new SolarSystem { Id = 3000013, Name = "Ordion", Constellation = _constellations.Value["Finena"] },
                new SolarSystem { Id = 3000014, Name = "Ami", Constellation = _constellations.Value["Jatari"] },
                new SolarSystem { Id = 3000015, Name = "Bridi", Constellation = _constellations.Value["Jatari"] },
                new SolarSystem { Id = 3000016, Name = "Jeni", Constellation = _constellations.Value["Jatari"] },
                new SolarSystem { Id = 3000017, Name = "Daran", Constellation = _constellations.Value["Orus"] },
            };
        }

        private IEnumerable<Star> GetStars()
        {
            return new[]
            {
                new Star { Id = 4000000, Name = "Hakodan - Star", SolarSystem = _solarsystems.Value["Hakodan"] },
                new Star { Id = 4000001, Name = "Huola - Star", SolarSystem = _solarsystems.Value["Huola"] },
                new Star { Id = 4000002, Name = "Anka - Star", SolarSystem = _solarsystems.Value["Anka"] },
                new Star { Id = 4000003, Name = "Misha - Star", SolarSystem = _solarsystems.Value["Misha"] },
                new Star { Id = 4000004, Name = "Bridi - Star", SolarSystem = _solarsystems.Value["Bridi"] },
                new Star { Id = 4000005, Name = "Daran - Star", SolarSystem = _solarsystems.Value["Daran"] },
            };
        }

        private IEnumerable<Planet> GetPlanets()
        {
            return new[]
            {
                new Planet { Id = 5000000, Name = "Hakodan - I", SolarSystem = _solarsystems.Value["Hakodan"] },
                new Planet { Id = 5000001, Name = "Hakodan - II", SolarSystem = _solarsystems.Value["Hakodan"] },
                new Planet { Id = 5000002, Name = "Hakodan - III", SolarSystem = _solarsystems.Value["Hakodan"] },
                new Planet { Id = 5000003, Name = "Huola - I", SolarSystem = _solarsystems.Value["Huola"] },
                new Planet { Id = 5000004, Name = "Huola - II", SolarSystem = _solarsystems.Value["Huola"] },
                new Planet { Id = 5000005, Name = "Huola - III", SolarSystem = _solarsystems.Value["Huola"] },
                new Planet { Id = 5000006, Name = "Huola - IV", SolarSystem = _solarsystems.Value["Huola"] },
                new Planet { Id = 5000007, Name = "Huola - V", SolarSystem = _solarsystems.Value["Huola"] },
                new Planet { Id = 5000008, Name = "Huola - VI", SolarSystem = _solarsystems.Value["Huola"] },
                new Planet { Id = 5000009, Name = "Anka - I", SolarSystem = _solarsystems.Value["Anka"] },
                new Planet { Id = 5000010, Name = "Misha - I", SolarSystem = _solarsystems.Value["Misha"] },
                new Planet { Id = 5000011, Name = "Bridi - I", SolarSystem = _solarsystems.Value["Bridi"] },
                new Planet { Id = 5000012, Name = "Bridi - II", SolarSystem = _solarsystems.Value["Bridi"] },
                new Planet { Id = 5000013, Name = "Bridi - III", SolarSystem = _solarsystems.Value["Bridi"] },
                new Planet { Id = 5000014, Name = "Daran - I", SolarSystem = _solarsystems.Value["Daran"] },
            };
        }

        private IEnumerable<Asteroid> GetAsteroids()
        {
            return new[]
            {
                new Asteroid { Id = 6000000, Name = "Misha - Asteroid - I", SolarSystem = _solarsystems.Value["Misha"], BaseYield = 300, Ore = _ores.Value["Ore 1"] },
                new Asteroid { Id = 6000001, Name = "Misha - Asteroid - II", SolarSystem = _solarsystems.Value["Misha"], BaseYield = 300, Ore = _ores.Value["Ore 2"] },
                new Asteroid { Id = 6000002, Name = "Misha - Asteroid - III", SolarSystem = _solarsystems.Value["Misha"], BaseYield = 300, Ore = _ores.Value["Ore 3"] },
                new Asteroid { Id = 6000003, Name = "Misha - Asteroid - IV", SolarSystem = _solarsystems.Value["Misha"], BaseYield = 300, Ore = _ores.Value["Ore 4"] },
                new Asteroid { Id = 6000004, Name = "Misha - Asteroid - V", SolarSystem = _solarsystems.Value["Misha"], BaseYield = 300, Ore = _ores.Value["Ore 5"] },
                new Asteroid { Id = 6000005, Name = "Misha - Asteroid - VI", SolarSystem = _solarsystems.Value["Misha"], BaseYield = 300, Ore = _ores.Value["Ore 6"] },
                new Asteroid { Id = 6000006, Name = "Misha - Asteroid - VII", SolarSystem = _solarsystems.Value["Misha"], BaseYield = 300, Ore = _ores.Value["Ore 7"] },
            };
        }

        private IEnumerable<Station> GetStations()
        {
            var refineryHuola1 = _stationModules.Value["Refinery - Huola - I - Ishukone Corporation Factory"];

            return new[]
            {
                new Station { Id = 7000000, Name = "Huola - I - Ishukone Corporation Factory", SolarSystem = _solarsystems.Value["Huola"], Modules = new Collection<StationModule> { refineryHuola1 } },
            };
        }

        private IEnumerable<StationModule> GetStationModules()
        {
            return new[]
            {
                // Huola - I - Ishukone Corporation Factory
                new RefineryStationModule { Id = 8000000, Name = "Refinery - Huola - I - Ishukone Corporation Factory", Fee = 200, },
            };
        }

        private IEnumerable<Ship> GetShips()
        {
            return new[]
            {
                new Ship { Id = 9000000, Name = "Ship - Chun-Li", Modules = new Collection<ShipModule> { _shipModules.Value["Cargo - Ship - Chun-Li"], _shipModules.Value["Power - Ship - Chun-Li"] } },
                new Ship { Id = 9000001, Name = "Ship - Faith Connors", Modules = new Collection<ShipModule> { _shipModules.Value["Power - Ship - Faith Connors"], _shipModules.Value["Shield - Ship - Faith Connors"] } },
                new Ship { Id = 9000002, Name = "Ship - Aya Brea", Modules = new Collection<ShipModule> { _shipModules.Value["Power - Ship - Aya Brea"], _shipModules.Value["Shield - Ship - Aya Brea"] } },
                new Ship { Id = 9000003, Name = "Ship - Tifa Lockheart", Modules = new Collection<ShipModule> { _shipModules.Value["Power - Ship - Tifa Lockheart"], _shipModules.Value["Shield - Ship - Tifa Lockheart"] } },
                new Ship { Id = 9000004, Name = "Ship - Sophitia Alexandra", Modules = new Collection<ShipModule> { _shipModules.Value["Power - Ship - Sophitia Alexandra"], _shipModules.Value["Shield - Ship - Sophitia Alexandra"] } },
            };
        }

        private IEnumerable<ShipModule> GetShipModules()
        {
            return new ShipModule[]
            {
                new CargoShipModule { Name = "Cargo - Ship - Chun-Li", /*Ship = Ships.Value["Ship - Chun-Li"],*/ Storage = _itemStorages.Value["Storage - CHAR - Huola - I - Ishukone Corporation Factory - Chun-Li"] },
                new PowerShipModule { Name = "Power - Ship - Chun-Li", /*Ship = Ships.Value["Ship - Chun-Li"],*/ },

                new PowerShipModule { Name = "Power - Ship - Faith Connors", /*Ship = Ships.Value["Ship - Faith Connors"]*/ },
                new ShieldShipModule { Name = "Shield - Ship - Faith Connors", /*Ship = Ships.Value["Ship - Faith Connors"]*/ },

                new PowerShipModule { Name = "Power - Ship - Aya Brea", /*Ship = Ships.Value["Ship - Aya Brea"]*/ },
                new ShieldShipModule { Name = "Shield - Ship - Aya Brea", /*Ship = Ships.Value["Ship - Aya Brea"]*/ },

                new PowerShipModule { Name = "Power - Ship - Tifa Lockheart", /*Ship = Ships.Value["Ship - Tifa Lockheart"]*/ },
                new ShieldShipModule { Name = "Shield - Ship - Tifa Lockheart", /*Ship = Ships.Value["Ship - Tifa Lockheart"]*/ },

                new PowerShipModule { Name = "Power - Ship - Sophitia Alexandra", /*Ship = Ships.Value["Ship - Sophitia Alexandra"]*/ },
                new ShieldShipModule { Name = "Shield - Ship - Sophitia Alexandra", /*Ship = Ships.Value["Ship - Sophitia Alexandra"]*/ }

            };
        }

        #endregion Celestials

        #region ItemTemplates

        private IEnumerable<Skill> GetSkillsLevel1()
        {
            return new[]
            {
                new Skill { Name = "Industry 1", Rank = 1, Category = "Skills", PrimaryAttribute = "Intelligence", SecondaryAttribute = "Charisma" },
                new Skill { Name = "Mechanics 1", Rank = 1, Category = "Skills", PrimaryAttribute = "Wisdom", SecondaryAttribute = "Willpower" },
                new Skill { Name = "Science 1", Rank = 1, Category = "Skills", PrimaryAttribute = "Wisdom", SecondaryAttribute = "Willpower" },
                new Skill { Name = "Survey 1", Rank = 1, Category = "Skills", PrimaryAttribute = "Wisdom", SecondaryAttribute = "Intelligence" },
            };
        }

        private IEnumerable<Skill> GetSkillsLevel2()
        {
            return new[]
            {
                new Skill { Name = "Mechanics 2", Rank = 1, Category = "Skills", PrimaryAttribute = "Wisdom", SecondaryAttribute = "Willpower", RequiredSkills = new Collection<Skill> { _skillsLevel1.Value["Mechanics 1"] } },
            };
        }

        private IEnumerable<Skill> GetSkillsLevel3()
        {
            return new[]
            {
                new Skill { Name = "Mechanics 3", Rank = 1, Category = "Skills", PrimaryAttribute = "Wisdom", SecondaryAttribute = "Willpower", RequiredSkills = new Collection<Skill> { _skillsLevel2.Value["Mechanics 2"] } },
            };
        }

        private IEnumerable<Skill> GetSkillsLevel4()
        {
            return new[]
            {
                new Skill { Name = "Mechanics 4", Rank = 1, Category = "Skills", PrimaryAttribute = "Wisdom", SecondaryAttribute = "Willpower", RequiredSkills = new Collection<Skill> { _skillsLevel3.Value["Mechanics 3"] } },
            };
        }

        private IEnumerable<Skill> GetSkillsLevel5()
        {
            return new[]
            {
                new Skill { Name = "Mechanics 5", Rank = 1, Category = "Skills", PrimaryAttribute = "Wisdom", SecondaryAttribute = "Willpower", RequiredSkills = new Collection<Skill> { _skillsLevel4.Value["Mechanics 4"] } },
            };
        }

        private IEnumerable<Ore> GetOres()
        {
            return new[]
            {
                new Ore { Name = "Ore 1", Category = "Ore", Mass = 10, IsStackable = true, Volume = 2.0, BasePrice = 16.0m },
                new Ore { Name = "Ore 2", Category = "Ore", Mass = 10, IsStackable = true, Volume = 2.0, BasePrice = 32.0m },
                new Ore { Name = "Ore 3", Category = "Ore", Mass = 10, IsStackable = true, Volume = 2.0, BasePrice = 64.0m },
                new Ore { Name = "Ore 4", Category = "Ore", Mass = 10, IsStackable = true, Volume = 2.0, BasePrice = 128.0m },
                new Ore { Name = "Ore 5", Category = "Ore", Mass = 10, IsStackable = true, Volume = 2.0, BasePrice = 256.0m },
                new Ore { Name = "Ore 6", Category = "Ore", Mass = 10, IsStackable = true, Volume = 2.0, BasePrice = 512.0m },
                new Ore { Name = "Ore 7", Category = "Ore", Mass = 10, IsStackable = true, Volume = 2.0, BasePrice = 1024.0m },
            };
        }

        private IEnumerable<ChemicalElement> GetChemicalElements()
        {
            return new[]
            {
                new ChemicalElement { AtomicNumber = 1, BasePrice = 1, BoilingPoint = -252.879, Category = "Elements", Group = 1, MolarMass = 1.008, MeltingPoint = -259.16, Name = "Hydrogen", Symbol = "H" },
                new ChemicalElement { AtomicNumber = 2, BasePrice = 1, BoilingPoint = -269.000, Category = "Elements", Group = 18, MolarMass = 4.002, MeltingPoint = -272.2, Name = "Helium", Symbol = "He" },
                new ChemicalElement { AtomicNumber = 3, BasePrice = 1, BoilingPoint = 1330.000, Category = "Elements", Group = 1, MolarMass = 6.94, MeltingPoint = 180.54, Name = "Lithium", Symbol = "Li" },
                new ChemicalElement { AtomicNumber = 4, BasePrice = 1, BoilingPoint = 2969.000, Category = "Elements", Group = 2, MolarMass = 9.01, MeltingPoint = 1287, Name = "Beryllium", Symbol = "Be" },
            };
        }

        #endregion ItemTemplates

        #region Items

        #endregion Items

        #region ItemStorages

        private IEnumerable<ItemStorage> GetItemStorages()
        {
            var hydrogen100 = new Item { Quantity = 100, Template = _chemicalElements.Value["Hydrogen"] };
            var hydrogen200 = new Item { Quantity = 200, Template = _chemicalElements.Value["Hydrogen"] };
            var hydrogen300 = new Item { Quantity = 100, Template = _chemicalElements.Value["Hydrogen"] };
            var hydrogen400 = new Item { Quantity = 200, Template = _chemicalElements.Value["Hydrogen"] };

            var helium333 = new Item { Quantity = 333, Template = _chemicalElements.Value["Helium"] };
            var helium450 = new Item { Quantity = 333, Template = _chemicalElements.Value["Helium"] };
            var helium520 = new Item { Quantity = 333, Template = _chemicalElements.Value["Helium"] };
            var helium100 = new Item { Quantity = 333, Template = _chemicalElements.Value["Helium"] };

            return new[]
            {
                new ItemStorage { Name = "Storage - ALLI - Triumvirate", Items = new Collection<Item> { hydrogen100, helium333 } },
                new ItemStorage { Name = "Storage - CORP - Huola - I - Ishukone Corporation Factory - Ishukone Corporation", Items = new Collection<Item> { hydrogen200 } },
                new ItemStorage { Name = "Storage - CHAR - Huola - I - Ishukone Corporation Factory - Chun-Li", Items = new Collection<Item> { hydrogen400, helium100 } },
                new ItemStorage { Name = "Storage - CHAR - Huola - I - Ishukone Corporation Factory - Tifa Lockheart", Items = new Collection<Item> { hydrogen300, helium450, helium520 } },
            };
        }

        #endregion

        #region Users

        private IEnumerable<User> GetUsers()
        {
            return new[]
            {
                new User { Name = "User 1", DateCreated = DateTime.Now, PasswordHash = "123", Role = UserRole.Administrator },
                new User { Name = "User 2", DateCreated = DateTime.Now, PasswordHash = "123", Role = UserRole.Normal },
            };
        }

        private IEnumerable<Bank> GetBanks()
        {
            return new[]
            {
                new Bank { Name = "Garoun Investment Bank", },
                new Bank { Name = "Bank of Luminaire", },
            };
        }

        private IEnumerable<BankAccount> GetBankAccounts()
        {
            return new[]
            {
                new BankAccount { Name = "Bank Account - CHAR - Chun-Li 1", Balance = 10000m, },
                new BankAccount { Name = "Bank Account - CHAR - Chun-Li 2", Balance = 20000m, },
                new BankAccount { Name = "Bank Account - CHAR - Faith Connors", Balance = 200000m, },
                new BankAccount { Name = "Bank Account - CHAR - Aya Brea", Balance = 2000000m, },
                new BankAccount { Name = "Bank Account - CHAR - Tifa Lockheart", Balance = 20000000m, },
                new BankAccount { Name = "Bank Account - CHAR - Sophitia Alexandra", Balance = 200000000m, },
                new BankAccount { Name = "Bank Account - ALLI - Curatores Veritatis Alliance", Balance = 655540000m, },
                new BankAccount { Name = "Bank Account - ALLI - Triumvirate", Balance = 90000000m, },
                new BankAccount { Name = "Bank Account - CORP - Ametat Security", Balance = 16000m, },
                new BankAccount { Name = "Bank Account - CORP - Ishukone Corporation", Balance = 18000m, },
                new BankAccount { Name = "Bank Account - CORP - Villore Sec Ops", Balance = 20000m, },
             };
        }

        private IEnumerable<Alliance> GetAlliances()
        {

            var allianceWallet1 = new Wallet { Name = "Wallet - ALLI - Curatores Veritatis Alliance" };
            allianceWallet1.BankAccounts = new Collection<BankAccount> { _bankAccounts.Value["Bank Account - ALLI - Curatores Veritatis Alliance"] };

            var allianceWallet2 = new Wallet { Name = "Wallet - ALLI - Triumvirate" };
            allianceWallet2.BankAccounts = new Collection<BankAccount> { _bankAccounts.Value["Bank Account - ALLI - Triumvirate"] };

            return new[]
            {
                new Alliance { Name = "Curatores Veritatis Alliance", Wallet = allianceWallet1 },
                new Alliance { Name = "Triumvirate", Wallet = allianceWallet2 },
            };
        }

        private IEnumerable<Corporation> GetCorporations()
        {
            var corporation1Wallet = new Wallet { Name = "Wallet - CORP - Ametat Security" };
            corporation1Wallet.BankAccounts = new Collection<BankAccount> { _bankAccounts.Value["Bank Account - CORP - Ametat Security"] };
            corporation1Wallet.DefaultBankAccount = corporation1Wallet.BankAccounts[0];

            var corporation2Wallet = new Wallet { Name = "Wallet - CORP - Ishukone Corporation" };
            corporation2Wallet.BankAccounts = new Collection<BankAccount> { _bankAccounts.Value["Bank Account - CORP - Ishukone Corporation"] };
            corporation2Wallet.DefaultBankAccount = corporation2Wallet.BankAccounts[0];

            var corporation3Wallet = new Wallet { Name = "Wallet - CORP - Villore Sec Ops" };
            corporation3Wallet.BankAccounts = new Collection<BankAccount> { _bankAccounts.Value["Bank Account - CORP - Villore Sec Ops"] };
            corporation3Wallet.DefaultBankAccount = corporation3Wallet.BankAccounts[0];

            return new[]
            {
                new Corporation { Name = "Ametat Security", Wallet = corporation1Wallet, Alliance = _alliances.Value["Triumvirate"], HomeSystem = _solarsystems.Value["Huola"] },
                new Corporation { Name = "Ishukone Corporation", Wallet = corporation2Wallet, HomeSystem = _solarsystems.Value["Huola"] },
                new Corporation { Name = "Villore Sec Ops", Wallet = corporation3Wallet, HomeSystem = _solarsystems.Value["Huola"] },
            };
        }

        private IEnumerable<Race> GetRaces()
        {
            return new[]
            {
                new Race { Name = "Race 1", Attributes = new AttributeSet() },
                new Race { Name = "Race 2", Attributes = new AttributeSet() },
                new Race { Name = "Race 3", Attributes = new AttributeSet() },
            };
        }

        private IEnumerable<Character> GetCharacters()
        {

            var character1Wallet = new Wallet { Name = "Wallet - CHAR - Chun-Li" };
            character1Wallet.BankAccounts = new Collection<BankAccount> { _bankAccounts.Value["Bank Account - CHAR - Chun-Li 1"], _bankAccounts.Value["Bank Account - CHAR - Chun-Li 2"] };
            character1Wallet.DefaultBankAccount = character1Wallet.BankAccounts[0];

            var character2Wallet = new Wallet { Name = "Wallet - CHAR - Faith Connors" };
            character2Wallet.BankAccounts = new Collection<BankAccount> { _bankAccounts.Value["Bank Account - CHAR - Faith Connors"] };
            character2Wallet.DefaultBankAccount = character2Wallet.BankAccounts[0];

            var character3Wallet = new Wallet { Name = "Wallet - CHAR - Aya Brea" };
            character3Wallet.BankAccounts = new Collection<BankAccount> { _bankAccounts.Value["Bank Account - CHAR - Aya Brea"] };
            character3Wallet.DefaultBankAccount = character3Wallet.BankAccounts[0];

            var character4Wallet = new Wallet { Name = "Wallet - CHAR - Tifa Lockheart" };
            character4Wallet.BankAccounts = new Collection<BankAccount> { _bankAccounts.Value["Bank Account - CHAR - Tifa Lockheart"] };
            character4Wallet.DefaultBankAccount = character4Wallet.BankAccounts[0];

            var character5Wallet = new Wallet { Name = "Wallet - CHAR - Sophitia Alexandra" };
            character5Wallet.BankAccounts = new Collection<BankAccount>(new[] { _bankAccounts.Value["Bank Account - CHAR - Sophitia Alexandra"] });
            character5Wallet.DefaultBankAccount = character5Wallet.BankAccounts[0];

            return new[]
            {
                new Character { Name = "Chun-Li", Race = _races.Value["Race 1"], Role = CharacterRole.Normal, DateCreated = DateTime.Now, User = _users.Value["User 1"], Wallet = character1Wallet, Attributes = new AttributeSet(), Ship = _ships.Value["Ship - Chun-Li"] },
                new Character { Name = "Faith Connors", Race = _races.Value["Race 1"], Role = CharacterRole.Normal, DateCreated = DateTime.Now, User = _users.Value["User 1"], Wallet = character2Wallet, Attributes = new AttributeSet(), Ship = _ships.Value["Ship - Faith Connors"] },
                new Character { Name = "Aya Brea", Race = _races.Value["Race 1"], Role = CharacterRole.Normal, DateCreated = DateTime.Now, User = _users.Value["User 1"], Wallet = character3Wallet, Attributes = new AttributeSet(), Ship = _ships.Value["Ship - Aya Brea"] },
                new Character { Name = "Tifa Lockheart", Race = _races.Value["Race 1"], Role = CharacterRole.Normal, DateCreated = DateTime.Now, User = _users.Value["User 2"], Wallet = character4Wallet, Attributes = new AttributeSet(), Ship = _ships.Value["Ship - Tifa Lockheart"] },
                new Character { Name = "Sophitia Alexandra", Race = _races.Value["Race 2"], Role = CharacterRole.Normal, DateCreated = DateTime.Now, User = _users.Value["User 2"], Wallet = character5Wallet, Attributes = new AttributeSet(), Ship = _ships.Value["Ship - Sophitia Alexandra"] },
            };
        }

        #endregion Users

        public GameDbStaticTestData()
        {
            _universes = new Lazy<Dictionary<string, Universe>>(() => GetUniverses().ToDictionary(universe => universe.Name));
            _regions = new Lazy<Dictionary<string, Region>>(() => GetRegions().ToDictionary(region => region.Name));
            _constellations = new Lazy<Dictionary<string, Constellation>>(() => GetConstellations().ToDictionary(constellation => constellation.Name));
            _solarsystems = new Lazy<Dictionary<string, SolarSystem>>(() => GetSolarsystems().ToDictionary(solarsystem => solarsystem.Name));
            _stars = new Lazy<Dictionary<string, Star>>(() => GetStars().ToDictionary(star => star.Name));
            _planets = new Lazy<Dictionary<string, Planet>>(() => GetPlanets().ToDictionary(planet => planet.Name));
            _asteroids = new Lazy<Dictionary<string, Asteroid>>(() => GetAsteroids().ToDictionary(asteroid => asteroid.Name));
            _stations = new Lazy<Dictionary<string, Station>>(() => GetStations().ToDictionary(station => station.Name));
            _stationModules = new Lazy<Dictionary<string, StationModule>>(() => GetStationModules().ToDictionary(stationModule => stationModule.Name));
            _ships = new Lazy<Dictionary<string, Ship>>(() => GetShips().ToDictionary(ship => ship.Name));
            _shipModules = new Lazy<Dictionary<string, ShipModule>>(() => GetShipModules().ToDictionary(shipModule => shipModule.Name));

            _users = new Lazy<Dictionary<string, User>>(() => GetUsers().ToDictionary(user => user.Name));
            _corporations = new Lazy<Dictionary<string, Corporation>>(() => GetCorporations().ToDictionary(corporation => corporation.Name));
            _banks = new Lazy<Dictionary<string, Bank>>(() => GetBanks().ToDictionary(bank => bank.Name));
            _bankAccounts = new Lazy<Dictionary<string, BankAccount>>(() => GetBankAccounts().ToDictionary(bankAccount => bankAccount.Name));

            _alliances = new Lazy<Dictionary<string, Alliance>>(() => GetAlliances().ToDictionary(alliance => alliance.Name));
            _races = new Lazy<Dictionary<string, Race>>(() => GetRaces().ToDictionary(race => race.Name));
            _characters = new Lazy<Dictionary<string, Character>>(() => GetCharacters().ToDictionary(character => character.Name));

            _skillsLevel1 = new Lazy<Dictionary<string, Skill>>(() => GetSkillsLevel1().ToDictionary(skill => skill.Name));
            _skillsLevel2 = new Lazy<Dictionary<string, Skill>>(() => GetSkillsLevel2().ToDictionary(skill => skill.Name));
            _skillsLevel3 = new Lazy<Dictionary<string, Skill>>(() => GetSkillsLevel3().ToDictionary(skill => skill.Name));
            _skillsLevel4 = new Lazy<Dictionary<string, Skill>>(() => GetSkillsLevel4().ToDictionary(skill => skill.Name));
            _skillsLevel5 = new Lazy<Dictionary<string, Skill>>(() => GetSkillsLevel5().ToDictionary(skill => skill.Name));
            _ores = new Lazy<Dictionary<string, Ore>>(() => GetOres().ToDictionary(ore => ore.Name));
            _chemicalElements = new Lazy<Dictionary<string, ChemicalElement>>(() => GetChemicalElements().ToDictionary(element => element.Name));

            _itemStorages = new Lazy<Dictionary<string, ItemStorage>>(() => GetItemStorages().ToDictionary(storage => storage.Name));
        }

        public void PopulateTestData(GameDbContext context)
        {
            try
            {
                context.ItemTemplates.AddRange(_ores.Value.Values);
                context.ItemTemplates.AddRange(_chemicalElements.Value.Values);
                context.ItemTemplates.AddRange(_skillsLevel1.Value.Values);
                context.ItemTemplates.AddRange(_skillsLevel2.Value.Values);
                context.ItemTemplates.AddRange(_skillsLevel3.Value.Values);
                context.ItemTemplates.AddRange(_skillsLevel4.Value.Values);
                context.ItemTemplates.AddRange(_skillsLevel5.Value.Values);

                context.Universes.AddRange(_universes.Value.Values);
                context.Regions.AddRange(_regions.Value.Values);
                context.Constellations.AddRange(_constellations.Value.Values);
                context.SolarSystems.AddRange(_solarsystems.Value.Values);
                context.Stars.AddRange(_stars.Value.Values);
                context.Planets.AddRange(_planets.Value.Values);
                context.Asteroids.AddRange(_asteroids.Value.Values);
                context.Stations.AddRange(_stations.Value.Values);

                context.Ships.AddRange(_ships.Value.Values);

                context.Users.AddRange(_users.Value.Values);
                context.Races.AddRange(_races.Value.Values);
                context.SaveChanges();

                AssignBankToBankAccounts(context);

                context.Alliances.AddRange(_alliances.Value.Values);
                context.SaveChanges();

                context.Corporations.AddRange(_corporations.Value.Values);
                context.SaveChanges();

                context.Characters.AddRange(_characters.Value.Values);
                context.SaveChanges();

                //context.ShipModules.AddRange(ShipModules.Value.Values);
                //context.SaveChanges();

                //context.Ships.AddRange(Ships.Value.Values);
                //context.SaveChanges();
            }
            catch (DbUpdateException ex)
            {
                Console.WriteLine(ex.ToString());
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        private void AssignBankToBankAccounts(GameDbContext context)
        {
            var bankAccounts = _bankAccounts.Value.Values;
            var random = new Random();
            foreach (var bankAccount in bankAccounts)
            {
                var bank = _banks.Value[_banks.Value.Keys.ElementAt(random.Next(0, _banks.Value.Keys.Count))];
                bankAccount.Bank = bank;
            }
            context.BankAccounts.AddRange(bankAccounts);
        }
    }
}