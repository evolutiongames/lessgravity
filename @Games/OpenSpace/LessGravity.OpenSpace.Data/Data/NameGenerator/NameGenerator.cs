﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace LessGravity.OpenSpace.Data.Data.NameGenerator
{
    public class NameGenerator : Generator
    {
        private IList<string> _forDuplicates;

        public NameGenerator(IEnumerable<string> trainingData, int order, double smoothing) 
            : base(trainingData, order, smoothing)
        {
            _forDuplicates = new List<string>();
        }

        private string GenerateName(int minLength, int maxLength, int maxDistance, string similarTo, Random rnd)
        {
            var name = Generate(rnd).Replace("#", "");
            if (name.Length < minLength || name.Length > maxLength)
            {
                return null;
            }

            if (similarTo == null || LevenshteinDistance.Compute(similarTo, name) <= maxDistance)
            {
                return name;
            }
            return null; 
        }

        public Task<List<string>> GenerateNames(int count, int length, Random rnd)
        {
            return GenerateNames(count, length, length, 0, null, rnd);
        }

        public async Task<List<string>> GenerateNames(int count, int minLength, int maxLength, int maxDistance, string similarTo, Random rnd)
        {
            return await Task.Run(() =>
            {
                var names = new List<string>();

                while (names.Count < count)
                {
                    var name = GenerateName(minLength, maxLength, maxDistance, similarTo, rnd);
                    if (name != null && !names.Contains(name))
                    {
                        names.Add(name);
                    }
                }
                return names;
            });
        }
    }
}