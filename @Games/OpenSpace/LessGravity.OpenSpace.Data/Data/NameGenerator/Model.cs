﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace LessGravity.OpenSpace.Data.Data.NameGenerator
{
    /// <summary>
    /// <seealso cref="https://github.com/Tw1ddle/MarkovNameGenerator/blob/master/src/markov/namegen/Model.hx"/>
    /// </summary>
    class Model
    {
        private readonly int _order;
        private readonly double _smoothing;
        private readonly List<char> _alphabet;
        private readonly Dictionary<string, List<char>> _observations;
        private Dictionary<string, List<double>> _chains;

        public Model(IEnumerable<string> trainingData, int order, double smoothing, List<char> alphabet)
        {
            _order = order;
            _smoothing = smoothing;
            _alphabet = alphabet;

            _observations = new Dictionary<string, List<char>>();
            Retrain(trainingData);
        }

        public char Generate(string context, Random rnd)
        {
            List<double> chain;
            return _chains.TryGetValue(context, out chain) ? _alphabet[SelectIndex(chain, rnd)] : '\0';
        }

        public void Retrain(IEnumerable<string> trainingData)
        {
            // mustn't we clear _observations here? Not in original source
            Train(trainingData);
            BuildChains();
        }

        private void Train(IEnumerable<string> trainingData)
        {
            foreach (var d in trainingData)
            {
                var data = new string('#', _order) + d + '#';
                for (int i = 0; i < data.Length - _order; i++)
                {
                    var key = data.Substring(i, _order);
                    List<char> value;
                    if (!_observations.TryGetValue(key, out value))
                    {
                        value = new List<char>();
                        _observations[key] = value;
                    }
                    value.Add(data[i + _order]);
                }
            }
        }

        private void BuildChains()
        {
            _chains = new Dictionary<string, List<double>>();

            foreach (var context in _observations.Keys)
            {
                foreach (var prediction in _alphabet)
                {
                    List<double> chain;
                    if (!_chains.TryGetValue(context, out chain))
                    {
                        chain = new List<double>();
                        _chains[context] = chain;
                    }
                    var count = 0;
                    List<char> observation;
                    if (_observations.TryGetValue(context, out observation))
                        count = observation.Count(c => c == prediction);
                    chain.Add(_smoothing + count);
                }
            }
        }

        private static int SelectIndex(IEnumerable<double> chain, Random rnd)
        {
            var totals = new List<double>();
            double accumulator = 0f;

            foreach (var weight in chain)
            {
                accumulator += weight;
                totals.Add(accumulator);
            }

            var rand = rnd.NextDouble() * accumulator;

            for (var i = 0; i < totals.Count; i++)
            {
                if (rand < totals[i])
                    return i;
            }

            return 0;
        }
    }
}