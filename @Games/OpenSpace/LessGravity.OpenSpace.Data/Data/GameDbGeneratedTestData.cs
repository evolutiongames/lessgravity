﻿using LessGravity.OpenSpace.Data.Celestials;
using LessGravity.OpenSpace.Data.Context;
using LessGravity.OpenSpace.Data.Extensions;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;

namespace LessGravity.OpenSpace.Data.Data
{
    internal class GameDbGeneratedTestData
    {
        private IEnumerable<string> GetNames(int nameCount)
        {
            var sourceNames = File.ReadAllLines("Names.txt");
            var generator = new NameGenerator.NameGenerator(sourceNames, 3, 0.01);

            var task = generator.GenerateNames(nameCount, 3, 13, 6, null, new Random());
            //task.Wait();
            var names = task.Result;

            var ci = CultureInfo.InvariantCulture;
            var duplicates = names.Distinct().Select(name => ci.TextInfo.ToTitleCase(name)).OrderBy(name => name);
            return duplicates.ToList();
        }

        public void PopulateTestData(GameDbContext context)
        {
            const int universeCount = 1;
            const int regionCount = 20;
            const int constellationCount = 30;
            const int solarSystemCount = 900;

            const int starCount = 900;
            const int planetCount = 1200;
            const int asteroidCount = 400;
            const int stationCount = 100;

            var random = new Random();
            Debug.WriteLine("Generating Universes...");
            var universeNames = GetNames(universeCount).OrderBy(name => Guid.NewGuid()).ToList();
            for (var i = 0; i < universeCount; ++i)
            {
                var universe = new Universe
                {
                    Name = universeNames[i]
                };
                context.Universes.Add(universe);
            }
            context.SaveChanges();
            Debug.WriteLine("Generating Universes...Done.");

            Debug.WriteLine("Generating Regions...");
            var regionNames = GetNames(regionCount).OrderBy(name => Guid.NewGuid()).ToList();
            var universes = context.Universes.ToList();
            for (var i = 0; i < regionCount; ++i)
            {
                var region = new Region
                {
                    Name = regionNames[i],
                    Universe = universes.RandomElement(random)
                };
                context.Regions.Add(region);
            }
            context.SaveChanges();
            Debug.WriteLine("Generating Regions...Done.");

            Debug.WriteLine("Generating Constellations...");
            var constellationNames = GetNames(constellationCount).OrderBy(name => Guid.NewGuid()).ToList();
            var regions = context.Regions.ToList();
            for (var i = 0; i < constellationCount; ++i)
            {
                var constellation = new Constellation
                {
                    Name = constellationNames[i],
                    Region = regions.RandomElement(random)
                };
                context.Constellations.Add(constellation);
            }
            context.SaveChanges();
            Debug.WriteLine("Generating Constellation...Done.");

            Debug.WriteLine("Generating SolarSystems...");
            var solarSystemNames = GetNames(solarSystemCount).OrderBy(name => Guid.NewGuid()).ToList();
            var constellations = context.Constellations.ToList();
            for (var i = 0; i < solarSystemCount; ++i)
            {
                var solarSystem = new SolarSystem
                {
                    Name = solarSystemNames[i],
                    Constellation = constellations.RandomElement(random)
                };
                context.SolarSystems.Add(solarSystem);
            }
            context.SaveChanges();
            Debug.WriteLine("Generating SolarSystems...Done.");

            Debug.WriteLine("Generating Stars...");
            var starNames = GetNames(starCount).OrderBy(name => Guid.NewGuid()).ToList();
            var solarSystems = context.SolarSystems.ToList();
            for (var i = 0; i < starCount; ++i)
            {
                var star = new Star
                {
                    Name = starNames[i],
                    SolarSystem = solarSystems.RandomElement(random)
                };
                context.Stars.Add(star);
            }
            context.SaveChanges();
            Debug.WriteLine("Generating Stars...Done.");

            Debug.WriteLine("Generating Planets...");
            var planetNames = GetNames(planetCount).OrderBy(name => Guid.NewGuid()).ToList();
            for (var i = 0; i < planetCount; ++i)
            {
                var planet = new Planet
                {
                    Name = planetNames[i],
                    SolarSystem = solarSystems.RandomElement(random)
                };
                context.Planets.Add(planet);
            }
            context.SaveChanges();
            Debug.WriteLine("Generating Planets...Done.");

            Debug.WriteLine("Generating Asteroids...");
            var asteroidNames = GetNames(asteroidCount).OrderBy(name => Guid.NewGuid()).ToList();
            for (var i = 0; i < planetCount; ++i)
            {
                var asteroid = new Asteroid
                {
                    Name = asteroidNames[i],
                    SolarSystem = solarSystems.RandomElement(random)
                };
                context.Asteroids.Add(asteroid);
            }
            context.SaveChanges();
            Debug.WriteLine("Generating Asteroids...Done.");

            Debug.WriteLine("Generating Stations...");
            var stationNames = GetNames(stationCount).OrderBy(name => Guid.NewGuid()).ToList();
            for (var i = 0; i < stationCount; ++i)
            {
                var station = new Station
                {
                    Name = stationNames[i],
                    SolarSystem = solarSystems.RandomElement(random)
                };
                context.Stations.Add(station);
            }
            context.SaveChanges();
            Debug.WriteLine("Generating Stations...Done.");
        }
    }
}