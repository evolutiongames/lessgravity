﻿using System.ComponentModel.DataAnnotations.Schema;

namespace LessGravity.OpenSpace.Data.Celestials
{
    [Table("tbl_celestial_solarsystem")]
    public class SolarSystem : CelestialObject
    {
        public virtual Constellation Constellation { get; set; }

        public virtual Server Server { get; set; }

        public float SecurityLevel { get; set; }
    }
}