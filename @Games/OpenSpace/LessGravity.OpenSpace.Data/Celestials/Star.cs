﻿using System.ComponentModel.DataAnnotations.Schema;

namespace LessGravity.OpenSpace.Data.Celestials
{
    [Table("tbl_celestial_star")]
    public class Star : CelestialObject
    {
        public virtual SolarSystem SolarSystem { get; set; }

        public string SpectralClass { get; set; }
    }
}