﻿using System.ComponentModel.DataAnnotations.Schema;

namespace LessGravity.OpenSpace.Data.Celestials
{
    [Table("tbl_celestial_region")]
    public class Region : CelestialObject
    {
        public virtual Universe Universe { get; set; }
    }
}