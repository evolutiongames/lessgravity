﻿using LessGravity.OpenSpace.Data.StationModules;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations.Schema;

namespace LessGravity.OpenSpace.Data.Celestials
{
    [Table("tbl_station")]
    public class Station : ObjectBase
    {
        public virtual SolarSystem SolarSystem { get; set; }

        public virtual Collection<StationModule> Modules { get; set; }

        public decimal OfficeRentalFees { get; set; }
    }
}