﻿using System.ComponentModel.DataAnnotations.Schema;

namespace LessGravity.OpenSpace.Data.Celestials
{
    [Table("tbl_celestial_planet")]
    public class Planet : CelestialObject
    {
        public virtual SolarSystem SolarSystem { get; set; }

        public string PlanetClass { get; set; }
    }
}