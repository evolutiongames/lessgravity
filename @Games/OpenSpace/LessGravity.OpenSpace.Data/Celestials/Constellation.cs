﻿using System.ComponentModel.DataAnnotations.Schema;

namespace LessGravity.OpenSpace.Data.Celestials
{
    [Table("tbl_celestial_constellation")]
    public class Constellation : CelestialObject
    {
        public virtual Region Region { get; set; }
    }
}