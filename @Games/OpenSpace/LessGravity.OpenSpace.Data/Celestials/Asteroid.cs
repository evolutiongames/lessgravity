﻿using LessGravity.OpenSpace.Data.ItemTemplates;
using System.ComponentModel.DataAnnotations.Schema;

namespace LessGravity.OpenSpace.Data.Celestials
{
    [Table("tbl_celestial_asteroid")]
    public class Asteroid : CelestialObject
    {
        public virtual Ore Ore { get; set; }

        public virtual SolarSystem SolarSystem { get; set; }

        public double BaseYield { get; set; }
    }
}