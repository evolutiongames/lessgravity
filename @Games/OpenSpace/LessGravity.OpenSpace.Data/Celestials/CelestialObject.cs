﻿using SharpDX;
using System.ComponentModel.DataAnnotations.Schema;

namespace LessGravity.OpenSpace.Data.Celestials
{
    [Table("tbl_celestial")]
    public class CelestialObject : ObjectBase
    {
        public Vector3 Position { get; set; }

        public BoundingSphere BoundingSphere { get; set; }
    }
}