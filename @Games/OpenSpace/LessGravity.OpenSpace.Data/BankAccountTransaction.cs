﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LessGravity.OpenSpace.Data
{
    [Table("tbl_bankaccount_transaction")]
    public class BankAccountTransaction
    {
        [Key]
        public Guid Id { get; set; }

        public DateTime Timestamp { get; set; }

        public virtual BankAccount BankAccount1 { get; set; }

        public virtual BankAccount BankAccount2 { get; set; }

        public decimal Value { get; set; }

        public BankTransactionType Type { get; set; }
    }
}