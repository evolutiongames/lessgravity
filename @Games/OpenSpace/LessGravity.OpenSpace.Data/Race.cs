﻿using System.ComponentModel.DataAnnotations.Schema;

namespace LessGravity.OpenSpace.Data
{
    [Table("tbl_race")]
    public class Race : ObjectBase
    {
        public virtual AttributeSet Attributes { get; set; }
    }
}