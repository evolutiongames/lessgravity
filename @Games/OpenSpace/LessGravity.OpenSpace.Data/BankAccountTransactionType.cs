﻿namespace LessGravity.OpenSpace.Data
{
    public enum BankTransactionType
    {
        Deposit,
        Withdrawal
    }
}