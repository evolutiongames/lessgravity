﻿using LessGravity.OpenSpace.Data.Ships;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;

namespace LessGravity.OpenSpace.Data
{
    [Table("tbl_character")]
    public class Character : ObjectBase, IHasWallet
    {
        public virtual AttributeSet Attributes { get; set; }

        public ulong Appearance { get; set; }

        public DateTime DateCreated { get; set; }

        public virtual User User { get; set; }

        public virtual Race Race { get; set; }

        public CharacterRole Role { get; set; }

        public virtual Wallet Wallet { get; set; }

        public virtual Ship Ship { get; set; }

        public virtual Faction Faction { get; set; }

        [NotMapped]
        public double Skillpoints => Skills.Sum(skill => skill.SkillPoints);

        public virtual Collection<CharacterAttribute> SkillAttributes { get; set; }

        [NotMapped]
        public Dictionary<string, int> SkillAttributeDictionary => SkillAttributes.ToDictionary(attribute => attribute.Name, attribute => attribute.Value);

        public virtual Collection<CharacterSkill> Skills { get; set; }
    }
}