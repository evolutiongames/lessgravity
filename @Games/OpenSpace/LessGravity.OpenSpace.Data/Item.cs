﻿using LessGravity.OpenSpace.Data.ItemTemplates;
using System.ComponentModel.DataAnnotations.Schema;

namespace LessGravity.OpenSpace.Data
{
    [Table("tbl_item")]
    public class Item : ObjectBase
    {
        public override string Name
        {
            get => $"{Quantity}x {Template?.Name ?? base.Name}";
            set => base.Name = value;
        }

        public uint Quantity { get; set; }

        public virtual ItemTemplate Template { get; set; }

        public double Volume => Quantity * (Template?.Volume ?? 0);
    }
}