﻿using System.ComponentModel.DataAnnotations.Schema;

namespace LessGravity.OpenSpace.Data
{
    [Table("tbl_server")]
    public class Server : ObjectBase
    {
        public string HostName { get; set; }

        public int Port { get; set; }
    }
}