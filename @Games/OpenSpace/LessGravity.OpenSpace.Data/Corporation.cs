﻿using LessGravity.OpenSpace.Data.Celestials;
using System.ComponentModel.DataAnnotations.Schema;

namespace LessGravity.OpenSpace.Data
{
    [Table("tbl_corporation")]
    public class Corporation : ObjectBase, IHasWallet
    {
        public virtual Alliance Alliance { get; set; }

        public virtual Faction Faction { get; set; }

        public virtual SolarSystem HomeSystem { get; set; }

        public virtual Wallet Wallet { get; set; }

        public string Ticker { get; set; }

        public float TaxRate { get; set; }
    }
}