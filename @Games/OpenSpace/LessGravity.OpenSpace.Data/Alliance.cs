﻿using System.ComponentModel.DataAnnotations.Schema;

namespace LessGravity.OpenSpace.Data
{
    [Table("tbl_alliance")]
    public class Alliance : ObjectBase, IHasWallet // maybe Corporation?
    {
        public virtual Wallet Wallet { get; set; }
    }
}