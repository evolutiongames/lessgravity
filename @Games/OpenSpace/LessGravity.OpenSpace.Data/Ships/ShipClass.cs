﻿namespace LessGravity.OpenSpace.Data.Ships
{
    public enum ShipClass
    {
        Small,
        Medium,
        Large,
        ExtraLarge,
        Capital
    }
}