﻿using LessGravity.OpenSpace.Data.Celestials;
using LessGravity.OpenSpace.Data.ShipModules;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations.Schema;

namespace LessGravity.OpenSpace.Data.Ships
{
    [Table("tbl_ship")]
    public class Ship : ObjectBase
    {
        public virtual SolarSystem SolarSystem { get; set; }

        public ShipClass ShipClass { get; set; }

        public virtual Collection<ShipModule> Modules { get; set; }
    }
}