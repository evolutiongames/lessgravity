﻿using System.ComponentModel.DataAnnotations.Schema;

namespace LessGravity.OpenSpace.Data.ShipModules
{
    [Table("tbl_shipmodule_cargo")]
    public class CargoShipModule : ShipModule
    {
        public virtual ItemStorage Storage { get; set; }
    }
}