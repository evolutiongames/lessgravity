﻿using System.ComponentModel.DataAnnotations.Schema;

namespace LessGravity.OpenSpace.Data.ShipModules
{
    [Table("tbl_shipmodule_shield")]
    public class ShieldShipModule : ShipModule
    {
        public double ShieldFrequency { get; set; }
    }
}