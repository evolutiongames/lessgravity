﻿using LessGravity.OpenSpace.Data.Ships;
using System.ComponentModel.DataAnnotations.Schema;

namespace LessGravity.OpenSpace.Data.ShipModules
{
    [Table("tbl_shipmodule")]
    public abstract class ShipModule : ObjectBase
    {
        public virtual Ship Ship { get; set; }
    }
}