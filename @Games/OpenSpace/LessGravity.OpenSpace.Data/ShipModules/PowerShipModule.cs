﻿using System.ComponentModel.DataAnnotations.Schema;

namespace LessGravity.OpenSpace.Data.ShipModules
{
    [Table("tbl_shipmodule_power")]
    public class PowerShipModule : ShipModule
    {
        public double MaxOutput { get; set; }
        public double CurrentOutput { get; set; }
    }
}