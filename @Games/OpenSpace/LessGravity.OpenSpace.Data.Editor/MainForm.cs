﻿using LessGravity.OpenSpace.Data.Celestials;
using LessGravity.OpenSpace.Data.Context;
using LessGravity.OpenSpace.Data.StationModules;
using System;
using System.Linq;
using System.Windows.Forms;

namespace LessGravity.OpenSpace.Data.Editor
{
    public partial class MainForm : Form
    {
        private readonly GameDbContext _data;

        public MainForm()
        {
            InitializeComponent();

            _data = new GameDbContext();

            lstUniverses.ItemType = typeof(Universe);
            lstRegions.ItemType = typeof(Region);
            lstConstellations.ItemType = typeof(Constellation);
            lstSolarSystems.ItemType = typeof(SolarSystem);
            lstStars.ItemType = typeof(Star);
            lstPlanets.ItemType = typeof(Planet);
            lstAsteroids.ItemType = typeof(Asteroid);
            lstStations.ItemType = typeof(Station);

            LoadData();
        }

        private void LoadData()
        {
            lstUniverses.LoadItems(_data.Universes);
            lstRegions.LoadItems(_data.Regions);
            lstConstellations.LoadItems(_data.Constellations);
            lstSolarSystems.LoadItems(_data.SolarSystems);
            lstStars.LoadItems(_data.Stars);
            lstPlanets.LoadItems(_data.Planets);
            lstAsteroids.LoadItems(_data.Asteroids);
            //lstJumpGates.LoadItems(_data.JumpGates);

            lstStations.LoadItems(_data.Stations);
            LoadUniverseTree();

            var availableStationModules = typeof(StationModule).Assembly.GetTypes().Where(type => !type.IsAbstract && typeof(StationModule).IsAssignableFrom(type));
            lbAvailableStationModules.Items.AddRange(availableStationModules.Select(m => m.Name).ToArray());

            var users = _data.Users.ToList();
            lstUsers.LoadItems(users);
            lstUsers.ItemType = typeof(User);

            lstCharacters.ItemType = typeof(Character);
        }

        private void LoadUniverseTree()
        {
            foreach (var universe in _data.Universes)
            {
                var universeNode = tvUniverse.Nodes.Find(universe.Name, false).FirstOrDefault();
                if (universeNode == null)
                {
                    universeNode = tvUniverse.Nodes.Add(universe.Name, universe.Name);
                    universeNode.Tag = universe;
                    universeNode.ImageIndex = 0;
                    universeNode.SelectedImageIndex = 0;
                }
            }

            foreach (var region in _data.Regions)
            {
                var universeNode = tvUniverse.Nodes.Find(region.Universe.Name, true).FirstOrDefault();
                var regionNode = universeNode.Nodes.Find(region.Name, false).FirstOrDefault();
                if (regionNode == null)
                {
                    regionNode = universeNode.Nodes.Add(region.Name, region.Name);
                    regionNode.Tag = region;
                    regionNode.ImageIndex = 0;
                    regionNode.SelectedImageIndex = 0;
                }
            }

            foreach (var constellation in _data.Constellations)
            {
                var regionNode = tvUniverse.Nodes.Find(constellation.Region.Name, true).FirstOrDefault();
                if (regionNode == null)
                {
                    continue;
                }
                var constellationNode = regionNode.Nodes.Find(constellation.Name, false).FirstOrDefault();
                if (constellationNode == null)
                {
                    constellationNode = regionNode.Nodes.Add(constellation.Name, constellation.Name);
                    constellationNode.Tag = constellation;
                    constellationNode.ImageIndex = 0;
                    constellationNode.SelectedImageIndex = 0;
                }
            }

            foreach (var solarSystem in _data.SolarSystems)
            {
                var constellationNode = tvUniverse.Nodes.Find(solarSystem.Constellation.Name, true).FirstOrDefault();
                if (constellationNode == null)
                {
                    continue;
                }

                var solarSystemNode = constellationNode.Nodes.Find(solarSystem.Name, false).FirstOrDefault();
                if (solarSystemNode == null)
                {
                    solarSystemNode = constellationNode.Nodes.Add(solarSystem.Name, solarSystem.Name);
                    solarSystemNode.Tag = solarSystem;
                    solarSystemNode.ImageIndex = 3;
                    solarSystemNode.SelectedImageIndex = 3;
                }
            }

            foreach (var star in _data.Stars)
            {
                var solarSystemNode = tvUniverse.Nodes.Find(star.SolarSystem.Name, true).FirstOrDefault();

                var starNode = solarSystemNode.Nodes.Find(star.Name, false).FirstOrDefault();
                if (starNode == null)
                {
                    starNode = solarSystemNode.Nodes.Add(star.Name, star.Name);
                    starNode.Tag = star;
                    starNode.ImageIndex = 1;
                    starNode.SelectedImageIndex = 1;
                }
            }

            foreach (var planet in _data.Planets)
            {
                var solarSystemNode = tvUniverse.Nodes.Find(planet.SolarSystem.Name, true).FirstOrDefault();

                var planetNode = solarSystemNode.Nodes.Find(planet.Name, false).FirstOrDefault();
                if (planetNode == null)
                {
                    planetNode = solarSystemNode.Nodes.Add(planet.Name, planet.Name);
                    planetNode.Tag = planet;
                    planetNode.ImageIndex = 2;
                    planetNode.SelectedImageIndex = 2;
                }
            }

            foreach (var asteroid in _data.Asteroids)
            {
                var solarSystemNode = tvUniverse.Nodes.Find(asteroid.SolarSystem.Name, true).FirstOrDefault();

                var asteroidNode = solarSystemNode.Nodes.Find(asteroid.Name, false).FirstOrDefault();
                if (asteroidNode == null)
                {
                    asteroidNode = solarSystemNode.Nodes.Add(asteroid.Name, asteroid.Name);
                    asteroidNode.Tag = asteroid;
                    asteroidNode.ImageIndex = 0;
                    asteroidNode.SelectedImageIndex = 0;
                }
            }

            foreach (var station in _data.Stations)
            {
                var solarSystemNode = tvUniverse.Nodes.Find(station.SolarSystem.Name, true).FirstOrDefault();

                var stationNode = solarSystemNode.Nodes.Find(station.Name, false).FirstOrDefault();
                if (stationNode == null)
                {
                    stationNode = solarSystemNode.Nodes.Add(station.Name, station.Name);
                    stationNode.Tag = station;
                    stationNode.ImageIndex = 5;
                    stationNode.SelectedImageIndex = 5;
                }
            }

        }

        private void lstList_SelectedItemChanged(Controls.ListControl sender, object obj)
        {
            //pgMain.SetObject(obj);

            var objNode = tvUniverse.Nodes.Find(((ObjectBase)obj).Name, true).FirstOrDefault();
            if (objNode != null)
            {
                var objNodeTag = objNode.Tag;
                if (objNodeTag != null)
                {
                    objNode.EnsureVisible();
                }
            }
            if (sender.Name == "lstStations")
            {
                LoadStationModules((Station)obj);
            }
        }

        private void btnAssignStationModule_Click(object sender, System.EventArgs e)
        {
            var selectedStationModule = lbAvailableStationModules.SelectedItem as string;
            var availableStationModules = typeof(StationModule).Assembly.GetTypes().Where(type => !type.IsAbstract && typeof(StationModule).IsAssignableFrom(type));
            var station = lstStations.SelectedItem as Station;

            if (station == null)
            {
                return;
            }

            if (!station.Modules.Any(m => m.GetType().Name.Equals(selectedStationModule)))
            {
                var stationModuleType = availableStationModules.FirstOrDefault(m => m.Name == selectedStationModule);
                if (stationModuleType == null)
                {
                    return;
                }
                var stationModule = (StationModule)Activator.CreateInstance(stationModuleType);
                station.Modules.Add(stationModule);
                LoadStationModules(station);
            }
        }

        private void LoadStationModules(Station station)
        {
            if (station == null)
            {
                throw new ArgumentNullException(nameof(station));
            }
            lstAssignedStationModules.LoadItems(station.Modules);
        }

        private void lstUsers_SelectedItemChanged(Controls.ListControl sender, object item)
        {
            var selectedUser = item as User;
            var characters = _data.Characters.Where(character => character.User.Id == selectedUser.Id).ToList();

            lstCharacters.LoadItems(characters);
        }
    }
}