﻿using LessGravity.OpenSpace.Data.Celestials;
using LessGravity.OpenSpace.Data.Context;
using LessGravity.OpenSpace.Data.ItemTemplates;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Windows.Forms;
using LessGravity.OpenSpace.Data.Editor.Controls.Primitives;

namespace LessGravity.OpenSpace.Data.Editor.Controls
{
    public partial class PropertyGrid : UserControl
    {
        private object _obj;

        public GameDbContext GameDbContext { get; set; }

        public PropertyGrid()
        {
            InitializeComponent();
        }

        private void OnPropertyValueChanged()
        {
            if (GameDbContext != null)
            {
                SetObject(_obj, GameDbContext);
            }
        }

        public void SetObject(object obj, GameDbContext dbContext)
        {
            _obj = obj;
            if (obj == null)
            {
                return;
            }
            layoutPanel.SuspendLayout();
            layoutPanel.Controls.Clear();

            var objType = obj.GetType();
            var objTypeProperties = objType.GetProperties();
            var objTypePropertiesSorted = objTypeProperties.OrderBy(property => property.Name.Replace("Id", "aaaa"))
                                                           .ThenBy(property => property.Name.Replace("Name", "aaab"));
            foreach (var objTypeProperty in objTypePropertiesSorted)
            {
                if (objTypeProperty.PropertyType == typeof(decimal))
                {
                    var decimalControl = new DecimalPropertyControl(this, obj)
                    {
                        PropertyName = objTypeProperty.Name,
                        PropertyValue = (decimal)objTypeProperty.GetValue(obj)
                    };
                    layoutPanel.Controls.Add(decimalControl);
                }
                else if (objTypeProperty.PropertyType == typeof(double))
                {
                    var doubleControl = new DoublePropertyControl(this, obj)
                    {
                        PropertyName = objTypeProperty.Name,
                        PropertyValue = (double)objTypeProperty.GetValue(obj)
                    };
                    layoutPanel.Controls.Add(doubleControl);
                }
                else if (objTypeProperty.PropertyType == typeof(int))
                {
                    var int32Control = new Int32PropertyControl(this, obj)
                    {
                        PropertyName = objTypeProperty.Name,
                        PropertyValue = (int)objTypeProperty.GetValue(obj)
                    };
                    layoutPanel.Controls.Add(int32Control);
                }
                else if (objTypeProperty.PropertyType == typeof(long))
                {
                    var int64Control = new Int64PropertyControl(this, obj)
                    {
                        PropertyName = objTypeProperty.Name,
                        PropertyValue = (long)objTypeProperty.GetValue(obj)
                    };
                    layoutPanel.Controls.Add(int64Control);
                }
                else if (objTypeProperty.PropertyType == typeof(float))
                {
                    var singleControl = new SinglePropertyControl(this, obj)
                    {
                        PropertyName = objTypeProperty.Name,
                        PropertyValue = (float)objTypeProperty.GetValue(obj)
                    };
                    layoutPanel.Controls.Add(singleControl);
                }
                else if (objTypeProperty.PropertyType == typeof(uint))
                {
                    var uint32Control = new UInt32PropertyControl(this, obj)
                    {
                        PropertyName = objTypeProperty.Name,
                        PropertyValue = (uint)objTypeProperty.GetValue(obj)
                    };
                    layoutPanel.Controls.Add(uint32Control);
                }
                else if (objTypeProperty.PropertyType == typeof(ulong))
                {
                    var uint64Control = new UInt64PropertyControl(this, obj)
                    {
                        PropertyName = objTypeProperty.Name,
                        PropertyValue = (ulong)objTypeProperty.GetValue(obj)
                    };
                    layoutPanel.Controls.Add(uint64Control);
                }
                else if (objTypeProperty.PropertyType == typeof(string))
                {
                    var stringControl = new StringPropertyControl(this, obj)
                    {
                        PropertyName = objTypeProperty.Name,
                        PropertyValue = (string)objTypeProperty.GetValue(obj)
                    };
                    layoutPanel.Controls.Add(stringControl);
                }
                else if (objTypeProperty.PropertyType == typeof(ItemTemplate))
                {
                    var itemTemplateControl = new ItemTemplatePropertyControl(this, obj)
                    {
                        PropertyName = objTypeProperty.Name,
                        PropertyValue = (ItemTemplate)objTypeProperty.GetValue(obj)
                    };
                    itemTemplateControl.OnPropertyValueChanged += OnPropertyValueChanged;
                    layoutPanel.Controls.Add(itemTemplateControl);
                }
                else if (objTypeProperty.PropertyType == typeof(Universe))
                {
                    var universeControl = new UniversePropertyControl(this, obj, dbContext.Universes)
                    {
                        PropertyName = objTypeProperty.Name,
                        PropertyValue = (Universe)objTypeProperty.GetValue(obj)
                    };
                    layoutPanel.Controls.Add(universeControl);
                }
                else if (objTypeProperty.PropertyType == typeof(Region))
                {
                    var regionControl = new RegionPropertyControl(this, obj, dbContext.Regions)
                    {
                        PropertyName = objTypeProperty.Name,
                        PropertyValue = (Region)objTypeProperty.GetValue(obj)
                    };
                    layoutPanel.Controls.Add(regionControl);
                }
                else if (objTypeProperty.PropertyType == typeof(Constellation))
                {
                    var constellationControl = new ConstellationPropertyControl(this, obj, dbContext.Constellations)
                    {
                        PropertyName = objTypeProperty.Name,
                        PropertyValue = (Constellation)objTypeProperty.GetValue(obj)
                    };
                    layoutPanel.Controls.Add(constellationControl);
                }
                else if (objTypeProperty.PropertyType == typeof(SolarSystem))
                {
                    var solarSystemControl = new SolarSystemPropertyControl(this, obj, dbContext.SolarSystems)
                    {
                        PropertyName = objTypeProperty.Name,
                        PropertyValue = (SolarSystem)objTypeProperty.GetValue(obj)
                    };
                    layoutPanel.Controls.Add(solarSystemControl);
                }
                else if (objTypeProperty.PropertyType == typeof(IList<ItemResourceIn>))
                {

                }
                else if (objTypeProperty.PropertyType.IsGenericType && objTypeProperty.PropertyType.GenericTypeArguments[0] == typeof(ItemResourceIn))
                {
                    var foo = objTypeProperty.GetValue(obj);
                }
                else
                {

                }
            }
            layoutPanel.ResumeLayout(true);
        }
    }
}