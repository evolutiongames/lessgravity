﻿namespace LessGravity.OpenSpace.Data.Editor.Controls
{
    partial class NewListItemForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pnlHeader = new System.Windows.Forms.Panel();
            this.lblItemTemplateName = new System.Windows.Forms.Label();
            this.pbItemTemplate = new System.Windows.Forms.PictureBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnOK = new System.Windows.Forms.Button();
            this.pgMain = new LessGravity.OpenSpace.Data.Editor.Controls.PropertyGrid();
            this.pnlHeader.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbItemTemplate)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlHeader
            // 
            this.pnlHeader.Controls.Add(this.lblItemTemplateName);
            this.pnlHeader.Controls.Add(this.pbItemTemplate);
            this.pnlHeader.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlHeader.Location = new System.Drawing.Point(0, 0);
            this.pnlHeader.Name = "pnlHeader";
            this.pnlHeader.Size = new System.Drawing.Size(331, 64);
            this.pnlHeader.TabIndex = 0;
            // 
            // lblItemTemplateName
            // 
            this.lblItemTemplateName.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblItemTemplateName.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblItemTemplateName.Location = new System.Drawing.Point(64, 0);
            this.lblItemTemplateName.Name = "lblItemTemplateName";
            this.lblItemTemplateName.Size = new System.Drawing.Size(267, 64);
            this.lblItemTemplateName.TabIndex = 1;
            this.lblItemTemplateName.Text = "TemplateName";
            this.lblItemTemplateName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // pbItemTemplate
            // 
            this.pbItemTemplate.Dock = System.Windows.Forms.DockStyle.Left;
            this.pbItemTemplate.Image = global::LessGravity.OpenSpace.Data.Editor.Properties.Resources.Structure_64;
            this.pbItemTemplate.Location = new System.Drawing.Point(0, 0);
            this.pbItemTemplate.Name = "pbItemTemplate";
            this.pbItemTemplate.Size = new System.Drawing.Size(64, 64);
            this.pbItemTemplate.TabIndex = 0;
            this.pbItemTemplate.TabStop = false;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.btnCancel);
            this.panel1.Controls.Add(this.btnOK);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 394);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(331, 48);
            this.panel1.TabIndex = 1;
            // 
            // btnCancel
            // 
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(244, 13);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 1;
            this.btnCancel.Text = "&Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            // 
            // btnOK
            // 
            this.btnOK.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnOK.Location = new System.Drawing.Point(163, 13);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(75, 23);
            this.btnOK.TabIndex = 0;
            this.btnOK.Text = "OK";
            this.btnOK.UseVisualStyleBackColor = true;
            // 
            // pgMain
            // 
            this.pgMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pgMain.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pgMain.GameDbContext = null;
            this.pgMain.Location = new System.Drawing.Point(0, 64);
            this.pgMain.Name = "pgMain";
            this.pgMain.Size = new System.Drawing.Size(331, 330);
            this.pgMain.TabIndex = 2;
            // 
            // NewListItemForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(331, 442);
            this.Controls.Add(this.pgMain);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.pnlHeader);
            this.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "NewListItemForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "New Item";
            this.pnlHeader.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pbItemTemplate)).EndInit();
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnlHeader;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnOK;
        private PropertyGrid pgMain;
        private System.Windows.Forms.Label lblItemTemplateName;
        private System.Windows.Forms.PictureBox pbItemTemplate;
    }
}