﻿using LessGravity.OpenSpace.Data.Celestials;
using System;
using System.Collections.Generic;
using System.Linq;

namespace LessGravity.OpenSpace.Data.Editor.Controls
{
    public partial class ConstellationPropertyControl : PropertyControl
    {
        public string PropertyName
        {
            get => lblPropertyName.Text;
            set => lblPropertyName.Text = value;
        }

        public Constellation PropertyValue
        {
            get => (Constellation)cbPropertyValue.SelectedItem;
            set => cbPropertyValue.SelectedItem = value;
        }

        public ConstellationPropertyControl()
        {
            InitializeComponent();
        }

        public ConstellationPropertyControl(PropertyGrid propertyGrid, object @object, IEnumerable<Constellation> constellations)
            : base(propertyGrid, @object)
        {
            InitializeComponent();
            cbPropertyValue.Items.AddRange(constellations.ToArray());
        }

        private void txtPropertyValue_Leave(object sender, EventArgs e)
        {
            var property = Object.GetType().GetProperty(PropertyName);
            property?.SetValue(Object, PropertyValue);
        }
    }
}