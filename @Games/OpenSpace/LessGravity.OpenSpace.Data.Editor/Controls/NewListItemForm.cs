﻿using System;
using System.Windows.Forms;

namespace LessGravity.OpenSpace.Data.Editor.Controls
{
    public partial class NewListItemForm : Form
    {
        public Type ItemType { get; set; }

        public ObjectBase Item { get; set; }

        public NewListItemForm()
        {
            InitializeComponent();
        }

        public NewListItemForm(Type itemType) 
            : this()
        {
            ItemType = itemType ?? throw new ArgumentNullException(nameof(itemType));
            Item = (ObjectBase)Activator.CreateInstance(ItemType);

            lblItemTemplateName.Text = ItemType.Name;
            //pgMain.SetObject(Item);
        }

    }
}