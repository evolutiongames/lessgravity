﻿namespace LessGravity.OpenSpace.Data.Editor.Controls
{
    partial class ItemTemplatesForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pnlButtons = new System.Windows.Forms.Panel();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnOK = new System.Windows.Forms.Button();
            this.pnlItemTemplates = new System.Windows.Forms.Panel();
            this.lbItemTemplates = new System.Windows.Forms.ListBox();
            this.pnlProperties = new System.Windows.Forms.Panel();
            this.propertyGrid1 = new LessGravity.OpenSpace.Data.Editor.Controls.PropertyGrid();
            this.pnlItemTemplateHeader = new System.Windows.Forms.Panel();
            this.lblItemTemplateName = new System.Windows.Forms.Label();
            this.pbItemTemplate = new System.Windows.Forms.PictureBox();
            this.pnlButtons.SuspendLayout();
            this.pnlItemTemplates.SuspendLayout();
            this.pnlProperties.SuspendLayout();
            this.pnlItemTemplateHeader.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbItemTemplate)).BeginInit();
            this.SuspendLayout();
            // 
            // pnlButtons
            // 
            this.pnlButtons.BackColor = System.Drawing.SystemColors.ControlDark;
            this.pnlButtons.Controls.Add(this.btnCancel);
            this.pnlButtons.Controls.Add(this.btnOK);
            this.pnlButtons.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnlButtons.ForeColor = System.Drawing.Color.Black;
            this.pnlButtons.Location = new System.Drawing.Point(0, 573);
            this.pnlButtons.Name = "pnlButtons";
            this.pnlButtons.Size = new System.Drawing.Size(820, 40);
            this.pnlButtons.TabIndex = 0;
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(737, 8);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 1;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            // 
            // btnOK
            // 
            this.btnOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOK.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnOK.Location = new System.Drawing.Point(656, 8);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(75, 23);
            this.btnOK.TabIndex = 0;
            this.btnOK.Text = "OK";
            this.btnOK.UseVisualStyleBackColor = true;
            // 
            // pnlItemTemplates
            // 
            this.pnlItemTemplates.Controls.Add(this.lbItemTemplates);
            this.pnlItemTemplates.Dock = System.Windows.Forms.DockStyle.Left;
            this.pnlItemTemplates.Location = new System.Drawing.Point(0, 0);
            this.pnlItemTemplates.Name = "pnlItemTemplates";
            this.pnlItemTemplates.Padding = new System.Windows.Forms.Padding(8);
            this.pnlItemTemplates.Size = new System.Drawing.Size(265, 573);
            this.pnlItemTemplates.TabIndex = 2;
            // 
            // lbItemTemplates
            // 
            this.lbItemTemplates.BackColor = System.Drawing.SystemColors.Window;
            this.lbItemTemplates.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.lbItemTemplates.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbItemTemplates.ForeColor = System.Drawing.SystemColors.WindowText;
            this.lbItemTemplates.FormattingEnabled = true;
            this.lbItemTemplates.ItemHeight = 15;
            this.lbItemTemplates.Location = new System.Drawing.Point(8, 8);
            this.lbItemTemplates.Name = "lbItemTemplates";
            this.lbItemTemplates.Size = new System.Drawing.Size(249, 557);
            this.lbItemTemplates.TabIndex = 0;
            this.lbItemTemplates.SelectedIndexChanged += new System.EventHandler(this.lbItemTemplates_SelectedIndexChanged);
            // 
            // pnlProperties
            // 
            this.pnlProperties.Controls.Add(this.propertyGrid1);
            this.pnlProperties.Controls.Add(this.pnlItemTemplateHeader);
            this.pnlProperties.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlProperties.Location = new System.Drawing.Point(265, 0);
            this.pnlProperties.Name = "pnlProperties";
            this.pnlProperties.Padding = new System.Windows.Forms.Padding(8);
            this.pnlProperties.Size = new System.Drawing.Size(555, 573);
            this.pnlProperties.TabIndex = 3;
            // 
            // propertyGrid1
            // 
            this.propertyGrid1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.propertyGrid1.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.propertyGrid1.Location = new System.Drawing.Point(8, 72);
            this.propertyGrid1.Name = "propertyGrid1";
            this.propertyGrid1.Size = new System.Drawing.Size(539, 493);
            this.propertyGrid1.TabIndex = 4;
            // 
            // pnlItemTemplateHeader
            // 
            this.pnlItemTemplateHeader.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.pnlItemTemplateHeader.Controls.Add(this.lblItemTemplateName);
            this.pnlItemTemplateHeader.Controls.Add(this.pbItemTemplate);
            this.pnlItemTemplateHeader.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlItemTemplateHeader.Location = new System.Drawing.Point(8, 8);
            this.pnlItemTemplateHeader.Name = "pnlItemTemplateHeader";
            this.pnlItemTemplateHeader.Size = new System.Drawing.Size(539, 64);
            this.pnlItemTemplateHeader.TabIndex = 2;
            // 
            // lblItemTemplateName
            // 
            this.lblItemTemplateName.BackColor = System.Drawing.SystemColors.Control;
            this.lblItemTemplateName.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblItemTemplateName.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblItemTemplateName.Location = new System.Drawing.Point(64, 0);
            this.lblItemTemplateName.Name = "lblItemTemplateName";
            this.lblItemTemplateName.Size = new System.Drawing.Size(475, 64);
            this.lblItemTemplateName.TabIndex = 1;
            this.lblItemTemplateName.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pbItemTemplate
            // 
            this.pbItemTemplate.BackColor = System.Drawing.SystemColors.Control;
            this.pbItemTemplate.Dock = System.Windows.Forms.DockStyle.Left;
            this.pbItemTemplate.Image = global::LessGravity.OpenSpace.Data.Editor.Properties.Resources.Structure_64;
            this.pbItemTemplate.Location = new System.Drawing.Point(0, 0);
            this.pbItemTemplate.Name = "pbItemTemplate";
            this.pbItemTemplate.Size = new System.Drawing.Size(64, 64);
            this.pbItemTemplate.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbItemTemplate.TabIndex = 0;
            this.pbItemTemplate.TabStop = false;
            // 
            // ItemTemplatesForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(820, 613);
            this.Controls.Add(this.pnlProperties);
            this.Controls.Add(this.pnlItemTemplates);
            this.Controls.Add(this.pnlButtons);
            this.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Name = "ItemTemplatesForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ItemTemplates";
            this.Load += new System.EventHandler(this.ItemTemplatesForm_Load);
            this.pnlButtons.ResumeLayout(false);
            this.pnlItemTemplates.ResumeLayout(false);
            this.pnlProperties.ResumeLayout(false);
            this.pnlItemTemplateHeader.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pbItemTemplate)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnlButtons;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.Panel pnlItemTemplates;
        private System.Windows.Forms.ListBox lbItemTemplates;
        private System.Windows.Forms.Panel pnlProperties;
        private System.Windows.Forms.Panel pnlItemTemplateHeader;
        private System.Windows.Forms.PictureBox pbItemTemplate;
        private PropertyGrid propertyGrid1;
        private System.Windows.Forms.Label lblItemTemplateName;

    }
}