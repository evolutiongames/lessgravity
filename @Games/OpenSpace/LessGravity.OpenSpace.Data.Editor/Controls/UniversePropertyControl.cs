﻿using LessGravity.OpenSpace.Data.Celestials;
using System;
using System.Collections.Generic;
using System.Linq;

namespace LessGravity.OpenSpace.Data.Editor.Controls
{
    public partial class UniversePropertyControl : PropertyControl
    {
        public string PropertyName
        {
            get => lblPropertyName.Text;
            set => lblPropertyName.Text = value;
        }

        public Universe PropertyValue
        {
            get => (Universe)cbPropertyValue.SelectedItem;
            set => cbPropertyValue.SelectedItem = value;
        }

        public UniversePropertyControl()
        {
            InitializeComponent();
        }

        public UniversePropertyControl(PropertyGrid propertyGrid, object @object, IEnumerable<Universe> universes)
            : base(propertyGrid, @object)
        {
            InitializeComponent();
            cbPropertyValue.Items.AddRange(universes.ToArray());
        }

        private void txtPropertyValue_Leave(object sender, EventArgs e)
        {
            var property = Object.GetType().GetProperty(PropertyName);
            property?.SetValue(Object, PropertyValue);
        }
    }
}