﻿namespace LessGravity.OpenSpace.Data.Editor.Controls
{
    partial class SolarSystemPropertyControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblPropertyName = new System.Windows.Forms.Label();
            this.cbPropertyValue = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // lblPropertyName
            // 
            this.lblPropertyName.AutoSize = true;
            this.lblPropertyName.Location = new System.Drawing.Point(3, 4);
            this.lblPropertyName.Name = "lblPropertyName";
            this.lblPropertyName.Size = new System.Drawing.Size(92, 15);
            this.lblPropertyName.TabIndex = 0;
            this.lblPropertyName.Text = "[PropertyName]";
            // 
            // cbPropertyValue
            // 
            this.cbPropertyValue.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cbPropertyValue.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbPropertyValue.Location = new System.Drawing.Point(6, 22);
            this.cbPropertyValue.Name = "cbPropertyValue";
            this.cbPropertyValue.Size = new System.Drawing.Size(239, 23);
            this.cbPropertyValue.TabIndex = 1;
            this.cbPropertyValue.Leave += new System.EventHandler(this.txtPropertyValue_Leave);
            // 
            // SolarSystemPropertyControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.cbPropertyValue);
            this.Controls.Add(this.lblPropertyName);
            this.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "SolarSystemPropertyControl";
            this.Size = new System.Drawing.Size(248, 49);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblPropertyName;
        private System.Windows.Forms.ComboBox cbPropertyValue;
    }
}
