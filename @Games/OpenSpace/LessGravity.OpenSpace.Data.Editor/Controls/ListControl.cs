﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace LessGravity.OpenSpace.Data.Editor.Controls
{
    public partial class ListControl : UserControl
    {
        public event Action<ListControl, object> SelectedItemChanged;

        public object SelectedItem
        {
            get => lbItems.SelectedItem;
            set => lbItems.SelectedItem = value;
        }

        public Type ItemType { get; set; }

        public bool AddButtonEnabled
        {
            get => btnAddItem.Enabled;
            set => btnAddItem.Enabled = value;
        }

        public bool AddButtonVisible
        {
            get => btnAddItem.Visible;
            set => btnAddItem.Visible = value;
        }

        public bool RemoveButtonEnabled
        {
            get => btnRemoveItem.Enabled;
            set => btnRemoveItem.Enabled = value;
        }

        public bool RemoveButtonVisible
        {
            get => btnRemoveItem.Visible;
            set => btnRemoveItem.Visible = value;
        }

        public bool EditButtonEnabled
        {
            get => btnEditItem.Enabled;
            set => btnEditItem.Enabled = value;
        }

        public bool EditButtonVisible
        {
            get => btnEditItem.Visible;
            set => btnEditItem.Visible = value;
        }


        public string Caption
        {
            get => lblCaption.Text;
            set => lblCaption.Text = value;
        }

        public string StatusText
        {
            get => lblStatus.Text;
            set => lblStatus.Text = value;
        }

        public bool StatusTextVisible
        {
            get => lblStatus.Visible;
            set => lblStatus.Visible = value;
        }

        public IEnumerable<T> GetItems<T>()
        {
            return lbItems.Items.Cast<T>();
        }

        public ListControl()
        {
            InitializeComponent();

            lbItems.SelectedIndexChanged += lbItemsSelectedIndexChanged;
        }

        private void lbItemsSelectedIndexChanged(object sender, EventArgs e)
        {
            var selectedItemChanged = SelectedItemChanged;
            if (selectedItemChanged != null && lbItems.SelectedIndex != -1)
            {
                selectedItemChanged(this, lbItems.SelectedItem);
            }
        }

        public void LoadItems<T>(IEnumerable<T> items)
        {
            lbItems.BeginUpdate();
            lbItems.Items.Clear();
            foreach (var item in items)
            {
                lbItems.Items.Add(item);
            }
            lbItems.EndUpdate();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            using (var newItemForm = new NewListItemForm(ItemType))
            {
                if (newItemForm.ShowDialog() == DialogResult.OK)
                {
                    lbItems.BeginUpdate();
                    lbItems.Items.Add(newItemForm.Item);
                    lbItems.EndUpdate();
                }
            }
        }
    }
}