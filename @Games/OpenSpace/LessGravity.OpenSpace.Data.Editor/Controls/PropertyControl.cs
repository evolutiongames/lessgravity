﻿using System.Windows.Forms;

namespace LessGravity.OpenSpace.Data.Editor.Controls
{
    public partial class PropertyControl : UserControl
    {
        public object Object { get; private set; }

        public PropertyGrid PropertyGrid { get; private set; }
        
        public PropertyControl()
        {
            InitializeComponent();
        }

        public PropertyControl(PropertyGrid propertyGrid, object @object) : this()
        {
            Object = @object;
            PropertyGrid = propertyGrid;
        }
    }
}