﻿using LessGravity.OpenSpace.Data.ItemTemplates;
using System;
using System.Windows.Forms;

namespace LessGravity.OpenSpace.Data.Editor.Controls
{
    public partial class ItemTemplatesForm : Form
    {
        public ItemTemplate SelectedItemTemplate
        {
            get { return (ItemTemplate)lbItemTemplates.SelectedItem; }
            set { lbItemTemplates.SelectedItem = value; }
        }

        public ItemTemplatesForm()
        {
            InitializeComponent();
        }

        private void ItemTemplatesForm_Load(object sender, EventArgs e)
        {
            /*
            var itemTemplates = ItemTemplateRegistry.GetAll().OrderBy(template => template.GetType().BaseType.Name).ThenBy(template => template.Name);
            lbItemTemplates.BeginUpdate();
            lbItemTemplates.Items.Clear();
            foreach (var itemTemplate in itemTemplates)
            {
                lbItemTemplates.Items.Add(itemTemplate);
            }
            lbItemTemplates.EndUpdate();
            */
        }

        private void lbItemTemplates_SelectedIndexChanged(object sender, EventArgs e)
        {
            /*
            var itemTemplate = (ItemTemplate)lbItemTemplates.SelectedItem;
            if (itemTemplate == null)
            {
                return;
            }
            if (File.Exists(itemTemplate.Resource2d))
            {
                pbItemTemplate.Image = Image.FromFile(itemTemplate.Resource2d);
            }
            else
            {
                pbItemTemplate.Image = Properties.Resources.Structure_64;
            }
            lblItemTemplateName.Text = itemTemplate.Name;
            propertyGrid1.SetObject(itemTemplate);
            */
        }
    }
}