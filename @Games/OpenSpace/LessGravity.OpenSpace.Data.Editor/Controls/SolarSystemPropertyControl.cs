﻿using LessGravity.OpenSpace.Data.Celestials;
using System;
using System.Collections.Generic;
using System.Linq;

namespace LessGravity.OpenSpace.Data.Editor.Controls
{
    public partial class SolarSystemPropertyControl : PropertyControl
    {
        public string PropertyName
        {
            get => lblPropertyName.Text;
            set => lblPropertyName.Text = value;
        }

        public SolarSystem PropertyValue
        {
            get => (SolarSystem)cbPropertyValue.SelectedItem;
            set => cbPropertyValue.SelectedItem = value;
        }

        public SolarSystemPropertyControl()
        {
            InitializeComponent();
        }

        public SolarSystemPropertyControl(PropertyGrid propertyGrid, object @object, IEnumerable<SolarSystem> solarSystems)
            : base(propertyGrid, @object)
        {
            InitializeComponent();
            cbPropertyValue.Items.AddRange(solarSystems.ToArray());
        }

        private void txtPropertyValue_Leave(object sender, EventArgs e)
        {
            var property = Object.GetType().GetProperty(PropertyName);
            property?.SetValue(Object, PropertyValue);
        }
    }
}