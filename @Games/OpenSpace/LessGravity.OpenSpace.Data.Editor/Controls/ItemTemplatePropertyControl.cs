﻿using LessGravity.OpenSpace.Data.ItemTemplates;
using System;
using System.Windows.Forms;

namespace LessGravity.OpenSpace.Data.Editor.Controls
{
    public partial class ItemTemplatePropertyControl : PropertyControl
    {
        private ItemTemplate _itemTemplate;

        public event Action OnPropertyValueChanged;

        public string PropertyName
        {
            get => lblPropertyName.Text;
            set => lblPropertyName.Text = value;
        }

        public ItemTemplate PropertyValue
        {
            get => _itemTemplate;
            set 
            {
                _itemTemplate = value;
                txtPropertyValue.Text = _itemTemplate == null ? string.Empty : _itemTemplate.Name; 
            }
        }

        public bool ReadOnly
        {
            get => txtPropertyValue.ReadOnly;
            set => txtPropertyValue.ReadOnly = value;
        }

        public ItemTemplatePropertyControl()
        {
            InitializeComponent();
            ReadOnly = true;
        }

        public ItemTemplatePropertyControl(PropertyGrid propertyGrid, object @object)
            : base(propertyGrid, @object)
        {
            InitializeComponent();
        }

        private void btnBrowseTemplates_Click(object sender, EventArgs e)
        {
            using (var itemTemplatesForm = new ItemTemplatesForm())
            {
                itemTemplatesForm.SelectedItemTemplate = PropertyValue;
                if (itemTemplatesForm.ShowDialog() == DialogResult.OK)
                {
                    var newPropertyValue = itemTemplatesForm.SelectedItemTemplate;
                    if (PropertyValue != newPropertyValue)
                    {
                        PropertyValue = itemTemplatesForm.SelectedItemTemplate;
                        var onPropertyValueChanged = OnPropertyValueChanged;
                        if (onPropertyValueChanged != null)
                        {
                            OnPropertyValueChanged();
                        }
                    }
                }
            }
        }
    }
}