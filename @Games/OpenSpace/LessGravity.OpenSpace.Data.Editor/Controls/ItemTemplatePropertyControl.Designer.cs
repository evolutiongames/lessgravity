﻿namespace LessGravity.OpenSpace.Data.Editor.Controls
{
    partial class ItemTemplatePropertyControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtPropertyValue = new System.Windows.Forms.TextBox();
            this.lblPropertyName = new System.Windows.Forms.Label();
            this.btnBrowseTemplates = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // txtPropertyValue
            // 
            this.txtPropertyValue.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtPropertyValue.Location = new System.Drawing.Point(6, 23);
            this.txtPropertyValue.Name = "txtPropertyValue";
            this.txtPropertyValue.Size = new System.Drawing.Size(202, 20);
            this.txtPropertyValue.TabIndex = 3;
            // 
            // lblPropertyName
            // 
            this.lblPropertyName.AutoSize = true;
            this.lblPropertyName.Location = new System.Drawing.Point(3, 5);
            this.lblPropertyName.Name = "lblPropertyName";
            this.lblPropertyName.Size = new System.Drawing.Size(80, 13);
            this.lblPropertyName.TabIndex = 2;
            this.lblPropertyName.Text = "[PropertyName]";
            // 
            // btnBrowseTemplates
            // 
            this.btnBrowseTemplates.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnBrowseTemplates.Location = new System.Drawing.Point(214, 21);
            this.btnBrowseTemplates.Name = "btnBrowseTemplates";
            this.btnBrowseTemplates.Size = new System.Drawing.Size(31, 23);
            this.btnBrowseTemplates.TabIndex = 4;
            this.btnBrowseTemplates.Text = "...";
            this.btnBrowseTemplates.UseVisualStyleBackColor = true;
            this.btnBrowseTemplates.Click += new System.EventHandler(this.btnBrowseTemplates_Click);
            // 
            // ItemTemplatePropertyControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.btnBrowseTemplates);
            this.Controls.Add(this.txtPropertyValue);
            this.Controls.Add(this.lblPropertyName);
            this.Name = "ItemTemplatePropertyControl";
            this.Size = new System.Drawing.Size(248, 49);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtPropertyValue;
        private System.Windows.Forms.Label lblPropertyName;
        private System.Windows.Forms.Button btnBrowseTemplates;
    }
}
