﻿using LessGravity.OpenSpace.Data.Celestials;
using System;
using System.Collections.Generic;
using System.Linq;

namespace LessGravity.OpenSpace.Data.Editor.Controls
{
    public partial class RegionPropertyControl : PropertyControl
    {
        public string PropertyName
        {
            get => lblPropertyName.Text;
            set => lblPropertyName.Text = value;
        }

        public Region PropertyValue
        {
            get => (Region)cbPropertyValue.SelectedItem;
            set => cbPropertyValue.SelectedItem = value;
        }

        public RegionPropertyControl()
        {
            InitializeComponent();
        }

        public RegionPropertyControl(PropertyGrid propertyGrid, object @object, IEnumerable<Region> regions)
            : base(propertyGrid, @object)
        {
            InitializeComponent();
            cbPropertyValue.Items.AddRange(regions.ToArray());
        }

        private void txtPropertyValue_Leave(object sender, EventArgs e)
        {
            var property = Object.GetType().GetProperty(PropertyName);
            property?.SetValue(Object, PropertyValue);
        }
    }
}