﻿using System;

namespace LessGravity.OpenSpace.Data.Editor.Controls.Primitives
{
    public partial class SinglePropertyControl : PropertyControl
    {
        public string PropertyName
        {
            get => lblPropertyName.Text;
            set => lblPropertyName.Text = value;
        }

        public float PropertyValue
        {
            get => float.TryParse(txtPropertyValue.Text, out float value) ? value : Single.NaN;
            set => txtPropertyValue.Text = $"{value:F4}";
        }

        public bool ReadOnly
        {
            get => txtPropertyValue.ReadOnly;
            set => txtPropertyValue.ReadOnly = value;
        }

        public SinglePropertyControl()
        {
            InitializeComponent();
        }

        public SinglePropertyControl(PropertyGrid propertyGrid, object @object)
            : base(propertyGrid, @object)
        {
            InitializeComponent();
        }

        private void txtPropertyValue_Leave(object sender, EventArgs e)
        {
            var property = Object.GetType().GetProperty(PropertyName);
            property?.SetValue(Object, PropertyValue);
        }
    }
}