﻿using System;

namespace LessGravity.OpenSpace.Data.Editor.Controls.Primitives
{
    public partial class Int64PropertyControl : PropertyControl
    {
        public string PropertyName
        {
            get => lblPropertyName.Text;
            set => lblPropertyName.Text = value;
        }

        public long PropertyValue
        {
            get => Convert.ToInt64(txtPropertyValue.Text);
            set => txtPropertyValue.Text = value.ToString();
        }

        public bool ReadOnly
        {
            get => txtPropertyValue.ReadOnly;
            set => txtPropertyValue.ReadOnly = value;
        }

        public Int64PropertyControl()
        {
            InitializeComponent();
        }

        public Int64PropertyControl(PropertyGrid propertyGrid, object @object)
            : base(propertyGrid, @object)
        {
            InitializeComponent();
        }

        private void txtPropertyValue_Leave(object sender, EventArgs e)
        {
            var property = Object.GetType().GetProperty(PropertyName);
            property?.SetValue(Object, PropertyValue);
        }
    }
}