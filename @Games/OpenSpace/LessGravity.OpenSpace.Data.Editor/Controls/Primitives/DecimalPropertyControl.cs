﻿using System;

namespace LessGravity.OpenSpace.Data.Editor.Controls.Primitives
{
    public partial class DecimalPropertyControl : PropertyControl
    {
        public string PropertyName
        {
            get => lblPropertyName.Text;
            set => lblPropertyName.Text = value;
        }

        public decimal PropertyValue
        {
            get => decimal.TryParse(txtPropertyValue.Text, out decimal value) ? value : Decimal.Zero;
            set => txtPropertyValue.Text = string.Format("{0:F4}", value);
        }

        public bool ReadOnly
        {
            get => txtPropertyValue.ReadOnly;
            set => txtPropertyValue.ReadOnly = value;
        }

        public DecimalPropertyControl()
        {
            InitializeComponent();
        }

        public DecimalPropertyControl(PropertyGrid propertyGrid, object @object) 
            : base(propertyGrid, @object)
        {
            InitializeComponent();
        }

        private void txtPropertyValue_Leave(object sender, EventArgs e)
        {
            var property = Object.GetType().GetProperty(PropertyName);
            property?.SetValue(Object, PropertyValue);
        }
    }
}