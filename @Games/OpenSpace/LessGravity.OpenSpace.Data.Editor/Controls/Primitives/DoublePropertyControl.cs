﻿using System;

namespace LessGravity.OpenSpace.Data.Editor.Controls.Primitives
{
    public partial class DoublePropertyControl : PropertyControl
    {
        public string PropertyName
        {
            get => lblPropertyName.Text;
            set => lblPropertyName.Text = value;
        }

        public double PropertyValue
        {
            get => double.TryParse(txtPropertyValue.Text, out double value) ? value : Double.NaN;
            set => txtPropertyValue.Text = string.Format("{0:F4}", value);
        }

        public bool ReadOnly
        {
            get => txtPropertyValue.ReadOnly;
            set => txtPropertyValue.ReadOnly = value;
        }

        public DoublePropertyControl()
        {
            InitializeComponent();
        }

        public DoublePropertyControl(PropertyGrid propertyGrid, object @object)
            : base(propertyGrid, @object)
        {
            InitializeComponent();
        }

        private void txtPropertyValue_Leave(object sender, EventArgs e)
        {
            var property = Object.GetType().GetProperty(PropertyName);
            property?.SetValue(Object, PropertyValue);
        }
    }
}