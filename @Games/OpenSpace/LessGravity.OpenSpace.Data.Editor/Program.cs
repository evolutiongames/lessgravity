﻿using LessGravity.OpenSpace.Data.ItemTemplates;
using System;
using System.Collections.ObjectModel;
using System.Windows.Forms;

namespace LessGravity.OpenSpace.Data.Editor
{
    internal static class Program
    {
        [STAThread]
        private static void Main()
        {
            var character = new Character
            {
                SkillAttributes = new Collection<CharacterAttribute>
                {
                    new CharacterAttribute { Name = "Wisdom", Value = 21 },
                    new CharacterAttribute { Name = "Intelligence", Value = 19 }
                },
            };

            var skillMechanics = new Skill
            {
                Rank = 16,
                Name = "Mechanics",
                PrimaryAttribute = "Intelligence",
                SecondaryAttribute = "Wisdom",
            };

            var characterSkill = new CharacterSkill
            {
                Character = character,
                Skill = skillMechanics,
                Level = 5,
            };

            characterSkill.Tick();
            characterSkill.Tick();
            characterSkill.Tick();
            characterSkill.SkillPoints = 2399800;
            characterSkill.Tick();

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new MainForm());
        }
    }
}