﻿using System.Collections.Generic;
using System;

namespace LessGravity.OpenSpace.Data.Editor
{
    public class DataContext
    {
        private static DataContext _instance;

        public static DataContext Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new DataContext();
                }
                return _instance;
            }
        }

        private void CreateListItems<T>(IList<T> forList, string stem, int amount) where T : ObjectBase, new()
        {
            for (var i = 0; i < amount; ++i)
            {
                forList.Add(new T { Name = $"{stem} {i}" });
            }
        }

        private void CreateListItems<T>(IList<T> forList, int amount) where T : ObjectBase, new()
        {
            for (var i = 0; i < amount; ++i)
            {
                forList.Add(new T { Name = CreateName() });
            }
        }

        private static Random _random = new Random();
        private static List<string> _syllables = new List<string>();

        static DataContext()
        {
            _syllables.Add("mon");
            _syllables.Add("fay");
            _syllables.Add("shi");
            _syllables.Add("zag");
            _syllables.Add("blarg");
            _syllables.Add("rash");
            _syllables.Add("izen");
            _syllables.Add("unpr");
            _syllables.Add("hala");
            _syllables.Add("semi");
            _syllables.Add("whit");
            _syllables.Add("appo");
            _syllables.Add("ulul");
            _syllables.Add("demi");
            _syllables.Add("mite");
            _syllables.Add("nons");
            _syllables.Add("sewe");
            _syllables.Add("tama");
            _syllables.Add("afte");
            _syllables.Add("megi");
            _syllables.Add("silv");
            _syllables.Add("unsp");
            _syllables.Add("gute");
            _syllables.Add("affe");
            _syllables.Add("ruff");
            _syllables.Add("bers");
            _syllables.Add("suff");
            _syllables.Add("bras");
            _syllables.Add("unod");
            _syllables.Add("shes");
            _syllables.Add("sils");
            _syllables.Add("requ");
        }

        private string CreateName()
        {
            //Creates a first name with 2-3 syllables
            var firstName = string.Empty;
            var numberOfSyllablesInFirstName = _random.Next(2, 4);
            for (var i = 0; i < numberOfSyllablesInFirstName; i++)
            {
                firstName += _syllables[_random.Next(0, _syllables.Count)];
            }
            var firstNameLetter = string.Empty;
            firstNameLetter = firstName.Substring(0, 1);
            firstName = firstName.Remove(0, 1);
            firstNameLetter = firstNameLetter.ToUpper();
            firstName = firstNameLetter + firstName;
            return firstName;
        }

        public static string ToRoman(int number)
        {
            if ((number < 0) || (number > 3999)) throw new ArgumentOutOfRangeException(nameof(number), "insert value betwheen 1 and 3999");
            if (number < 1) return string.Empty;
            if (number >= 1000) return "M" + ToRoman(number - 1000);
            if (number >= 900) return "CM" + ToRoman(number - 900); //EDIT: i've typed 400 instead 900
            if (number >= 500) return "D" + ToRoman(number - 500);
            if (number >= 400) return "CD" + ToRoman(number - 400);
            if (number >= 100) return "C" + ToRoman(number - 100);
            if (number >= 90) return "XC" + ToRoman(number - 90);
            if (number >= 50) return "L" + ToRoman(number - 50);
            if (number >= 40) return "XL" + ToRoman(number - 40);
            if (number >= 10) return "X" + ToRoman(number - 10);
            if (number >= 9) return "IX" + ToRoman(number - 9);
            if (number >= 5) return "V" + ToRoman(number - 5);
            if (number >= 4) return "IV" + ToRoman(number - 4);
            if (number >= 1) return "I" + ToRoman(number - 1);
            throw new ArgumentOutOfRangeException(nameof(number), "something bad happened");
        }

        private DataContext()
        {
            PopulateData();
        }

        public void PopulateData()
        {
        }
    }
}