﻿using System;
using System.Collections.Generic;

namespace LessGravity.OpenSpace.Data.Editor
{
    public class NameGenerator
    {
        private Dictionary<string, List<char>> _chains = new Dictionary<string, List<char>>();
        private List<string> _samples = new List<string>();
        private readonly List<string> _used = new List<string>();
        private Random _rnd = new Random();
        private int _order;
        private int _minLength;

        private char GetLetter(string token)
        {
            if (!_chains.ContainsKey(token))
            {
                return '?';
            }
            var letters = _chains[token];
            var n = _rnd.Next(letters.Count);
            return letters[n];
        }

        public NameGenerator(IEnumerable<string> sampleNames, int order, int minLength)
        {
            if (order < 1)
            {
                order = 1;
            }
            if (minLength < 1)
            {
                minLength = 1;
            }
            _order = order;
            _minLength = minLength;

            foreach (var sampleName in sampleNames)
            {
                var tokens = sampleName.Split(',');
                foreach (var token in tokens)
                {
                    var upper = token.Trim().ToUpper();
                    if (upper.Length < order + 1)
                    {
                        continue;
                    }
                    _samples.Add(upper);
                }
            }

            foreach (string sample in _samples)
            {
                for (var letter = 0; letter < sample.Length - order; letter++)
                {
                    var token = sample.Substring(letter, order);
                    List<char> entry = null;
                    if (_chains.ContainsKey(token))
                    {
                        entry = _chains[token];
                    }
                    else
                    {
                        entry = new List<char>();
                        _chains[token] = entry;
                    }
                    entry.Add(sample[letter + order]);
                }
            }
        }

        public string GetName()
        {

            var s = string.Empty;
            do
            {
                var n = _rnd.Next(_samples.Count);
                var nameLength = _samples[n].Length;
                s = _samples[n].Substring(_rnd.Next(0, _samples[n].Length - _order), _order);
                while (s.Length < nameLength)
                {
                    var token = s.Substring(s.Length - _order, _order);
                    var c = GetLetter(token);
                    if (c != '?')
                    {
                        s += GetLetter(token);
                    }
                    else
                    {
                        break;
                    }
                }

                if (s.Contains(" "))
                {
                    var tokens = s.Split(' ');
                    s = "";
                    for (int t = 0; t < tokens.Length; t++)
                    {
                        if (tokens[t] == string.Empty)
                        {
                            continue;
                        }
                        if (tokens[t].Length == 1)
                        {
                            tokens[t] = tokens[t].ToUpper();
                        }
                        else
                        {
                            tokens[t] = tokens[t].Substring(0, 1) + tokens[t].Substring(1).ToLower();
                        }
                        if (s != string.Empty)
                        {
                            s += " ";
                        }
                        s += tokens[t];
                    }
                }
                else
                {
                    s = s.Substring(0, 1) + s.Substring(1).ToLower();
                }
            }
            while (_used.Contains(s) || s.Length < _minLength);
            _used.Add(s);
            return s;
        }

        public void Reset()
        {
            _used.Clear();
        }
    }
}