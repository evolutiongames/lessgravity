﻿using LessGravity.OpenSpace.Data.Editor.Controls;

namespace LessGravity.OpenSpace.Data.Editor
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.mnuMain = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.stsMain = new System.Windows.Forms.StatusStrip();
            this.tvUniverse = new System.Windows.Forms.TreeView();
            this.il16_16 = new System.Windows.Forms.ImageList(this.components);
            this.splitter1 = new System.Windows.Forms.Splitter();
            this.splitter2 = new System.Windows.Forms.Splitter();
            this.tcMain = new System.Windows.Forms.TabControl();
            this.tpServers = new System.Windows.Forms.TabPage();
            this.tbUsers = new System.Windows.Forms.TabPage();
            this.lstUsers = new LessGravity.OpenSpace.Data.Editor.Controls.ListControl();
            this.tpCharacters = new System.Windows.Forms.TabPage();
            this.tpUniverse = new System.Windows.Forms.TabPage();
            this.lstJumpGates = new LessGravity.OpenSpace.Data.Editor.Controls.ListControl();
            this.splitter8 = new System.Windows.Forms.Splitter();
            this.panel4 = new System.Windows.Forms.Panel();
            this.lstPlanets = new LessGravity.OpenSpace.Data.Editor.Controls.ListControl();
            this.splitter5 = new System.Windows.Forms.Splitter();
            this.panel3 = new System.Windows.Forms.Panel();
            this.lstAsteroids = new LessGravity.OpenSpace.Data.Editor.Controls.ListControl();
            this.splitter9 = new System.Windows.Forms.Splitter();
            this.lstStars = new LessGravity.OpenSpace.Data.Editor.Controls.ListControl();
            this.splitter4 = new System.Windows.Forms.Splitter();
            this.panel2 = new System.Windows.Forms.Panel();
            this.lstSolarSystems = new LessGravity.OpenSpace.Data.Editor.Controls.ListControl();
            this.splitter7 = new System.Windows.Forms.Splitter();
            this.lstConstellations = new LessGravity.OpenSpace.Data.Editor.Controls.ListControl();
            this.splitter3 = new System.Windows.Forms.Splitter();
            this.pnlUniverseRegion = new System.Windows.Forms.Panel();
            this.lstRegions = new LessGravity.OpenSpace.Data.Editor.Controls.ListControl();
            this.splitter6 = new System.Windows.Forms.Splitter();
            this.lstUniverses = new LessGravity.OpenSpace.Data.Editor.Controls.ListControl();
            this.tpStation = new System.Windows.Forms.TabPage();
            this.pnlStationModules = new System.Windows.Forms.Panel();
            this.pnlButtonsStationModules = new System.Windows.Forms.Panel();
            this.lstAssignedStationModules = new LessGravity.OpenSpace.Data.Editor.Controls.ListControl();
            this.tsButtonsStationModules = new System.Windows.Forms.ToolStrip();
            this.btnAssignStationModule = new System.Windows.Forms.ToolStripButton();
            this.btnRemoveStationModule = new System.Windows.Forms.ToolStripButton();
            this.splitter11 = new System.Windows.Forms.Splitter();
            this.pnlAvailableStationModules = new System.Windows.Forms.Panel();
            this.lbAvailableStationModules = new System.Windows.Forms.ListBox();
            this.tsStationModules = new System.Windows.Forms.ToolStrip();
            this.lblAvailableStationModules = new System.Windows.Forms.ToolStripLabel();
            this.splitter10 = new System.Windows.Forms.Splitter();
            this.lstStations = new LessGravity.OpenSpace.Data.Editor.Controls.ListControl();
            this.tpShips = new System.Windows.Forms.TabPage();
            this.tpItemTemplates = new System.Windows.Forms.TabPage();
            this.tpItems = new System.Windows.Forms.TabPage();
            this.tpMarket = new System.Windows.Forms.TabPage();
            this.tpStockExchange = new System.Windows.Forms.TabPage();
            this.il32_32 = new System.Windows.Forms.ImageList(this.components);
            this.pgMain = new LessGravity.OpenSpace.Data.Editor.Controls.PropertyGrid();
            this.lstCharacters = new LessGravity.OpenSpace.Data.Editor.Controls.ListControl();
            this.mnuMain.SuspendLayout();
            this.tcMain.SuspendLayout();
            this.tbUsers.SuspendLayout();
            this.tpUniverse.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel2.SuspendLayout();
            this.pnlUniverseRegion.SuspendLayout();
            this.tpStation.SuspendLayout();
            this.pnlStationModules.SuspendLayout();
            this.pnlButtonsStationModules.SuspendLayout();
            this.tsButtonsStationModules.SuspendLayout();
            this.pnlAvailableStationModules.SuspendLayout();
            this.tsStationModules.SuspendLayout();
            this.SuspendLayout();
            // 
            // mnuMain
            // 
            this.mnuMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem});
            this.mnuMain.Location = new System.Drawing.Point(0, 0);
            this.mnuMain.Name = "mnuMain";
            this.mnuMain.Size = new System.Drawing.Size(1264, 24);
            this.mnuMain.TabIndex = 0;
            this.mnuMain.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "&File";
            // 
            // stsMain
            // 
            this.stsMain.Location = new System.Drawing.Point(0, 625);
            this.stsMain.Name = "stsMain";
            this.stsMain.Size = new System.Drawing.Size(1264, 22);
            this.stsMain.TabIndex = 1;
            this.stsMain.Text = "statusStrip1";
            // 
            // tvUniverse
            // 
            this.tvUniverse.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tvUniverse.Dock = System.Windows.Forms.DockStyle.Left;
            this.tvUniverse.ImageIndex = 0;
            this.tvUniverse.ImageList = this.il16_16;
            this.tvUniverse.Location = new System.Drawing.Point(0, 24);
            this.tvUniverse.Name = "tvUniverse";
            this.tvUniverse.SelectedImageIndex = 0;
            this.tvUniverse.Size = new System.Drawing.Size(153, 601);
            this.tvUniverse.TabIndex = 9;
            // 
            // il16_16
            // 
            this.il16_16.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("il16_16.ImageStream")));
            this.il16_16.TransparentColor = System.Drawing.Color.Transparent;
            this.il16_16.Images.SetKeyName(0, "Empty_16.png");
            this.il16_16.Images.SetKeyName(1, "Star_16.png");
            this.il16_16.Images.SetKeyName(2, "Planet_16.png");
            this.il16_16.Images.SetKeyName(3, "StationModule_16.png");
            this.il16_16.Images.SetKeyName(4, "Storage_16.png");
            this.il16_16.Images.SetKeyName(5, "Structure_16.png");
            // 
            // splitter1
            // 
            this.splitter1.Location = new System.Drawing.Point(153, 24);
            this.splitter1.Name = "splitter1";
            this.splitter1.Size = new System.Drawing.Size(3, 601);
            this.splitter1.TabIndex = 10;
            this.splitter1.TabStop = false;
            // 
            // splitter2
            // 
            this.splitter2.Dock = System.Windows.Forms.DockStyle.Right;
            this.splitter2.Location = new System.Drawing.Point(949, 24);
            this.splitter2.Name = "splitter2";
            this.splitter2.Size = new System.Drawing.Size(3, 601);
            this.splitter2.TabIndex = 12;
            this.splitter2.TabStop = false;
            // 
            // tcMain
            // 
            this.tcMain.Controls.Add(this.tpServers);
            this.tcMain.Controls.Add(this.tbUsers);
            this.tcMain.Controls.Add(this.tpCharacters);
            this.tcMain.Controls.Add(this.tpUniverse);
            this.tcMain.Controls.Add(this.tpStation);
            this.tcMain.Controls.Add(this.tpShips);
            this.tcMain.Controls.Add(this.tpItemTemplates);
            this.tcMain.Controls.Add(this.tpItems);
            this.tcMain.Controls.Add(this.tpMarket);
            this.tcMain.Controls.Add(this.tpStockExchange);
            this.tcMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tcMain.Location = new System.Drawing.Point(156, 24);
            this.tcMain.Name = "tcMain";
            this.tcMain.SelectedIndex = 0;
            this.tcMain.Size = new System.Drawing.Size(793, 601);
            this.tcMain.TabIndex = 13;
            // 
            // tpServers
            // 
            this.tpServers.Location = new System.Drawing.Point(4, 24);
            this.tpServers.Name = "tpServers";
            this.tpServers.Padding = new System.Windows.Forms.Padding(3);
            this.tpServers.Size = new System.Drawing.Size(785, 573);
            this.tpServers.TabIndex = 9;
            this.tpServers.Text = "Servers";
            this.tpServers.UseVisualStyleBackColor = true;
            // 
            // tbUsers
            // 
            this.tbUsers.Controls.Add(this.lstCharacters);
            this.tbUsers.Controls.Add(this.lstUsers);
            this.tbUsers.Location = new System.Drawing.Point(4, 24);
            this.tbUsers.Name = "tbUsers";
            this.tbUsers.Padding = new System.Windows.Forms.Padding(3);
            this.tbUsers.Size = new System.Drawing.Size(785, 573);
            this.tbUsers.TabIndex = 4;
            this.tbUsers.Text = "Users";
            this.tbUsers.UseVisualStyleBackColor = true;
            // 
            // lstUsers
            // 
            this.lstUsers.AddButtonEnabled = true;
            this.lstUsers.AddButtonVisible = true;
            this.lstUsers.Caption = "Users";
            this.lstUsers.EditButtonEnabled = true;
            this.lstUsers.EditButtonVisible = true;
            this.lstUsers.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lstUsers.ItemType = null;
            this.lstUsers.Location = new System.Drawing.Point(6, 6);
            this.lstUsers.Name = "lstUsers";
            this.lstUsers.RemoveButtonEnabled = true;
            this.lstUsers.RemoveButtonVisible = true;
            this.lstUsers.SelectedItem = null;
            this.lstUsers.Size = new System.Drawing.Size(213, 269);
            this.lstUsers.StatusText = "toolStripStatusLabel1";
            this.lstUsers.StatusTextVisible = false;
            this.lstUsers.TabIndex = 0;
            this.lstUsers.SelectedItemChanged += new System.Action<LessGravity.OpenSpace.Data.Editor.Controls.ListControl, object>(this.lstUsers_SelectedItemChanged);
            // 
            // tpCharacters
            // 
            this.tpCharacters.Location = new System.Drawing.Point(4, 24);
            this.tpCharacters.Name = "tpCharacters";
            this.tpCharacters.Padding = new System.Windows.Forms.Padding(3);
            this.tpCharacters.Size = new System.Drawing.Size(785, 573);
            this.tpCharacters.TabIndex = 3;
            this.tpCharacters.Text = "Characters";
            this.tpCharacters.UseVisualStyleBackColor = true;
            // 
            // tpUniverse
            // 
            this.tpUniverse.Controls.Add(this.lstJumpGates);
            this.tpUniverse.Controls.Add(this.splitter8);
            this.tpUniverse.Controls.Add(this.panel4);
            this.tpUniverse.Controls.Add(this.splitter5);
            this.tpUniverse.Controls.Add(this.panel3);
            this.tpUniverse.Controls.Add(this.splitter4);
            this.tpUniverse.Controls.Add(this.panel2);
            this.tpUniverse.Controls.Add(this.splitter3);
            this.tpUniverse.Controls.Add(this.pnlUniverseRegion);
            this.tpUniverse.Location = new System.Drawing.Point(4, 24);
            this.tpUniverse.Name = "tpUniverse";
            this.tpUniverse.Padding = new System.Windows.Forms.Padding(3);
            this.tpUniverse.Size = new System.Drawing.Size(785, 573);
            this.tpUniverse.TabIndex = 0;
            this.tpUniverse.Text = "Universe";
            this.tpUniverse.UseVisualStyleBackColor = true;
            // 
            // lstJumpGates
            // 
            this.lstJumpGates.AddButtonEnabled = true;
            this.lstJumpGates.AddButtonVisible = false;
            this.lstJumpGates.Caption = "Jump Gates";
            this.lstJumpGates.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lstJumpGates.EditButtonEnabled = true;
            this.lstJumpGates.EditButtonVisible = false;
            this.lstJumpGates.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lstJumpGates.ItemType = null;
            this.lstJumpGates.Location = new System.Drawing.Point(660, 3);
            this.lstJumpGates.Name = "lstJumpGates";
            this.lstJumpGates.RemoveButtonEnabled = true;
            this.lstJumpGates.RemoveButtonVisible = false;
            this.lstJumpGates.SelectedItem = null;
            this.lstJumpGates.Size = new System.Drawing.Size(122, 567);
            this.lstJumpGates.StatusText = "toolStripStatusLabel1";
            this.lstJumpGates.StatusTextVisible = false;
            this.lstJumpGates.TabIndex = 8;
            this.lstJumpGates.SelectedItemChanged += new System.Action<LessGravity.OpenSpace.Data.Editor.Controls.ListControl, object>(this.lstList_SelectedItemChanged);
            // 
            // splitter8
            // 
            this.splitter8.Location = new System.Drawing.Point(657, 3);
            this.splitter8.Name = "splitter8";
            this.splitter8.Size = new System.Drawing.Size(3, 567);
            this.splitter8.TabIndex = 7;
            this.splitter8.TabStop = false;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.lstPlanets);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel4.Location = new System.Drawing.Point(517, 3);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(140, 567);
            this.panel4.TabIndex = 6;
            // 
            // lstPlanets
            // 
            this.lstPlanets.AddButtonEnabled = true;
            this.lstPlanets.AddButtonVisible = false;
            this.lstPlanets.Caption = "Planets";
            this.lstPlanets.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lstPlanets.EditButtonEnabled = true;
            this.lstPlanets.EditButtonVisible = false;
            this.lstPlanets.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lstPlanets.ItemType = null;
            this.lstPlanets.Location = new System.Drawing.Point(0, 0);
            this.lstPlanets.Name = "lstPlanets";
            this.lstPlanets.RemoveButtonEnabled = true;
            this.lstPlanets.RemoveButtonVisible = false;
            this.lstPlanets.SelectedItem = null;
            this.lstPlanets.Size = new System.Drawing.Size(140, 567);
            this.lstPlanets.StatusText = "toolStripStatusLabel1";
            this.lstPlanets.StatusTextVisible = false;
            this.lstPlanets.TabIndex = 3;
            this.lstPlanets.SelectedItemChanged += new System.Action<LessGravity.OpenSpace.Data.Editor.Controls.ListControl, object>(this.lstList_SelectedItemChanged);
            // 
            // splitter5
            // 
            this.splitter5.Location = new System.Drawing.Point(514, 3);
            this.splitter5.Name = "splitter5";
            this.splitter5.Size = new System.Drawing.Size(3, 567);
            this.splitter5.TabIndex = 5;
            this.splitter5.TabStop = false;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.lstAsteroids);
            this.panel3.Controls.Add(this.splitter9);
            this.panel3.Controls.Add(this.lstStars);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel3.Location = new System.Drawing.Point(349, 3);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(165, 567);
            this.panel3.TabIndex = 4;
            // 
            // lstAsteroids
            // 
            this.lstAsteroids.AddButtonEnabled = true;
            this.lstAsteroids.AddButtonVisible = false;
            this.lstAsteroids.Caption = "Asteroids";
            this.lstAsteroids.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lstAsteroids.EditButtonEnabled = true;
            this.lstAsteroids.EditButtonVisible = false;
            this.lstAsteroids.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lstAsteroids.ItemType = null;
            this.lstAsteroids.Location = new System.Drawing.Point(0, 216);
            this.lstAsteroids.Name = "lstAsteroids";
            this.lstAsteroids.RemoveButtonEnabled = true;
            this.lstAsteroids.RemoveButtonVisible = false;
            this.lstAsteroids.SelectedItem = null;
            this.lstAsteroids.Size = new System.Drawing.Size(165, 351);
            this.lstAsteroids.StatusText = "toolStripStatusLabel1";
            this.lstAsteroids.StatusTextVisible = false;
            this.lstAsteroids.TabIndex = 4;
            this.lstAsteroids.SelectedItemChanged += new System.Action<LessGravity.OpenSpace.Data.Editor.Controls.ListControl, object>(this.lstList_SelectedItemChanged);
            // 
            // splitter9
            // 
            this.splitter9.Dock = System.Windows.Forms.DockStyle.Top;
            this.splitter9.Location = new System.Drawing.Point(0, 213);
            this.splitter9.Name = "splitter9";
            this.splitter9.Size = new System.Drawing.Size(165, 3);
            this.splitter9.TabIndex = 3;
            this.splitter9.TabStop = false;
            // 
            // lstStars
            // 
            this.lstStars.AddButtonEnabled = true;
            this.lstStars.AddButtonVisible = false;
            this.lstStars.Caption = "Stars";
            this.lstStars.Dock = System.Windows.Forms.DockStyle.Top;
            this.lstStars.EditButtonEnabled = true;
            this.lstStars.EditButtonVisible = false;
            this.lstStars.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lstStars.ItemType = null;
            this.lstStars.Location = new System.Drawing.Point(0, 0);
            this.lstStars.Name = "lstStars";
            this.lstStars.RemoveButtonEnabled = true;
            this.lstStars.RemoveButtonVisible = false;
            this.lstStars.SelectedItem = null;
            this.lstStars.Size = new System.Drawing.Size(165, 213);
            this.lstStars.StatusText = "toolStripStatusLabel1";
            this.lstStars.StatusTextVisible = false;
            this.lstStars.TabIndex = 2;
            this.lstStars.SelectedItemChanged += new System.Action<LessGravity.OpenSpace.Data.Editor.Controls.ListControl, object>(this.lstList_SelectedItemChanged);
            // 
            // splitter4
            // 
            this.splitter4.Location = new System.Drawing.Point(346, 3);
            this.splitter4.Name = "splitter4";
            this.splitter4.Size = new System.Drawing.Size(3, 567);
            this.splitter4.TabIndex = 3;
            this.splitter4.TabStop = false;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.lstSolarSystems);
            this.panel2.Controls.Add(this.splitter7);
            this.panel2.Controls.Add(this.lstConstellations);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel2.Location = new System.Drawing.Point(172, 3);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(174, 567);
            this.panel2.TabIndex = 2;
            // 
            // lstSolarSystems
            // 
            this.lstSolarSystems.AddButtonEnabled = true;
            this.lstSolarSystems.AddButtonVisible = false;
            this.lstSolarSystems.Caption = "Solar Systems";
            this.lstSolarSystems.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lstSolarSystems.EditButtonEnabled = true;
            this.lstSolarSystems.EditButtonVisible = false;
            this.lstSolarSystems.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lstSolarSystems.ItemType = null;
            this.lstSolarSystems.Location = new System.Drawing.Point(0, 216);
            this.lstSolarSystems.Name = "lstSolarSystems";
            this.lstSolarSystems.RemoveButtonEnabled = true;
            this.lstSolarSystems.RemoveButtonVisible = false;
            this.lstSolarSystems.SelectedItem = null;
            this.lstSolarSystems.Size = new System.Drawing.Size(174, 351);
            this.lstSolarSystems.StatusText = "toolStripStatusLabel1";
            this.lstSolarSystems.StatusTextVisible = false;
            this.lstSolarSystems.TabIndex = 3;
            this.lstSolarSystems.SelectedItemChanged += new System.Action<LessGravity.OpenSpace.Data.Editor.Controls.ListControl, object>(this.lstList_SelectedItemChanged);
            // 
            // splitter7
            // 
            this.splitter7.Dock = System.Windows.Forms.DockStyle.Top;
            this.splitter7.Location = new System.Drawing.Point(0, 213);
            this.splitter7.Name = "splitter7";
            this.splitter7.Size = new System.Drawing.Size(174, 3);
            this.splitter7.TabIndex = 2;
            this.splitter7.TabStop = false;
            // 
            // lstConstellations
            // 
            this.lstConstellations.AddButtonEnabled = true;
            this.lstConstellations.AddButtonVisible = false;
            this.lstConstellations.Caption = "Constellations";
            this.lstConstellations.Dock = System.Windows.Forms.DockStyle.Top;
            this.lstConstellations.EditButtonEnabled = true;
            this.lstConstellations.EditButtonVisible = false;
            this.lstConstellations.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lstConstellations.ItemType = null;
            this.lstConstellations.Location = new System.Drawing.Point(0, 0);
            this.lstConstellations.Name = "lstConstellations";
            this.lstConstellations.RemoveButtonEnabled = true;
            this.lstConstellations.RemoveButtonVisible = false;
            this.lstConstellations.SelectedItem = null;
            this.lstConstellations.Size = new System.Drawing.Size(174, 213);
            this.lstConstellations.StatusText = "toolStripStatusLabel1";
            this.lstConstellations.StatusTextVisible = false;
            this.lstConstellations.TabIndex = 1;
            this.lstConstellations.SelectedItemChanged += new System.Action<LessGravity.OpenSpace.Data.Editor.Controls.ListControl, object>(this.lstList_SelectedItemChanged);
            // 
            // splitter3
            // 
            this.splitter3.Location = new System.Drawing.Point(169, 3);
            this.splitter3.Name = "splitter3";
            this.splitter3.Size = new System.Drawing.Size(3, 567);
            this.splitter3.TabIndex = 1;
            this.splitter3.TabStop = false;
            // 
            // pnlUniverseRegion
            // 
            this.pnlUniverseRegion.Controls.Add(this.lstRegions);
            this.pnlUniverseRegion.Controls.Add(this.splitter6);
            this.pnlUniverseRegion.Controls.Add(this.lstUniverses);
            this.pnlUniverseRegion.Dock = System.Windows.Forms.DockStyle.Left;
            this.pnlUniverseRegion.Location = new System.Drawing.Point(3, 3);
            this.pnlUniverseRegion.Name = "pnlUniverseRegion";
            this.pnlUniverseRegion.Size = new System.Drawing.Size(166, 567);
            this.pnlUniverseRegion.TabIndex = 0;
            // 
            // lstRegions
            // 
            this.lstRegions.AddButtonEnabled = true;
            this.lstRegions.AddButtonVisible = false;
            this.lstRegions.Caption = "Regions";
            this.lstRegions.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lstRegions.EditButtonEnabled = true;
            this.lstRegions.EditButtonVisible = false;
            this.lstRegions.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lstRegions.ItemType = null;
            this.lstRegions.Location = new System.Drawing.Point(0, 216);
            this.lstRegions.Name = "lstRegions";
            this.lstRegions.RemoveButtonEnabled = true;
            this.lstRegions.RemoveButtonVisible = false;
            this.lstRegions.SelectedItem = null;
            this.lstRegions.Size = new System.Drawing.Size(166, 351);
            this.lstRegions.StatusText = "toolStripStatusLabel1";
            this.lstRegions.StatusTextVisible = false;
            this.lstRegions.TabIndex = 2;
            this.lstRegions.SelectedItemChanged += new System.Action<LessGravity.OpenSpace.Data.Editor.Controls.ListControl, object>(this.lstList_SelectedItemChanged);
            // 
            // splitter6
            // 
            this.splitter6.Dock = System.Windows.Forms.DockStyle.Top;
            this.splitter6.Location = new System.Drawing.Point(0, 213);
            this.splitter6.Name = "splitter6";
            this.splitter6.Size = new System.Drawing.Size(166, 3);
            this.splitter6.TabIndex = 1;
            this.splitter6.TabStop = false;
            // 
            // lstUniverses
            // 
            this.lstUniverses.AddButtonEnabled = true;
            this.lstUniverses.AddButtonVisible = false;
            this.lstUniverses.Caption = "Universes";
            this.lstUniverses.Dock = System.Windows.Forms.DockStyle.Top;
            this.lstUniverses.EditButtonEnabled = true;
            this.lstUniverses.EditButtonVisible = false;
            this.lstUniverses.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lstUniverses.ItemType = null;
            this.lstUniverses.Location = new System.Drawing.Point(0, 0);
            this.lstUniverses.Name = "lstUniverses";
            this.lstUniverses.RemoveButtonEnabled = true;
            this.lstUniverses.RemoveButtonVisible = false;
            this.lstUniverses.SelectedItem = null;
            this.lstUniverses.Size = new System.Drawing.Size(166, 213);
            this.lstUniverses.StatusText = "toolStripStatusLabel1";
            this.lstUniverses.StatusTextVisible = false;
            this.lstUniverses.TabIndex = 0;
            this.lstUniverses.SelectedItemChanged += new System.Action<LessGravity.OpenSpace.Data.Editor.Controls.ListControl, object>(this.lstList_SelectedItemChanged);
            // 
            // tpStation
            // 
            this.tpStation.Controls.Add(this.pnlStationModules);
            this.tpStation.Controls.Add(this.splitter10);
            this.tpStation.Controls.Add(this.lstStations);
            this.tpStation.Location = new System.Drawing.Point(4, 24);
            this.tpStation.Name = "tpStation";
            this.tpStation.Padding = new System.Windows.Forms.Padding(3);
            this.tpStation.Size = new System.Drawing.Size(785, 573);
            this.tpStation.TabIndex = 1;
            this.tpStation.Text = "Stations";
            this.tpStation.UseVisualStyleBackColor = true;
            // 
            // pnlStationModules
            // 
            this.pnlStationModules.Controls.Add(this.pnlButtonsStationModules);
            this.pnlStationModules.Controls.Add(this.tsButtonsStationModules);
            this.pnlStationModules.Controls.Add(this.splitter11);
            this.pnlStationModules.Controls.Add(this.pnlAvailableStationModules);
            this.pnlStationModules.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlStationModules.Location = new System.Drawing.Point(207, 3);
            this.pnlStationModules.Name = "pnlStationModules";
            this.pnlStationModules.Size = new System.Drawing.Size(575, 567);
            this.pnlStationModules.TabIndex = 2;
            // 
            // pnlButtonsStationModules
            // 
            this.pnlButtonsStationModules.Controls.Add(this.lstAssignedStationModules);
            this.pnlButtonsStationModules.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlButtonsStationModules.Location = new System.Drawing.Point(0, 327);
            this.pnlButtonsStationModules.Name = "pnlButtonsStationModules";
            this.pnlButtonsStationModules.Size = new System.Drawing.Size(575, 240);
            this.pnlButtonsStationModules.TabIndex = 5;
            // 
            // lstAssignedStationModules
            // 
            this.lstAssignedStationModules.AddButtonEnabled = true;
            this.lstAssignedStationModules.AddButtonVisible = false;
            this.lstAssignedStationModules.Caption = "Assigned Station Modules";
            this.lstAssignedStationModules.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lstAssignedStationModules.EditButtonEnabled = true;
            this.lstAssignedStationModules.EditButtonVisible = false;
            this.lstAssignedStationModules.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lstAssignedStationModules.ItemType = null;
            this.lstAssignedStationModules.Location = new System.Drawing.Point(0, 0);
            this.lstAssignedStationModules.Name = "lstAssignedStationModules";
            this.lstAssignedStationModules.RemoveButtonEnabled = true;
            this.lstAssignedStationModules.RemoveButtonVisible = false;
            this.lstAssignedStationModules.SelectedItem = null;
            this.lstAssignedStationModules.Size = new System.Drawing.Size(575, 240);
            this.lstAssignedStationModules.StatusText = "toolStripStatusLabel1";
            this.lstAssignedStationModules.StatusTextVisible = false;
            this.lstAssignedStationModules.TabIndex = 1;
            this.lstAssignedStationModules.SelectedItemChanged += new System.Action<LessGravity.OpenSpace.Data.Editor.Controls.ListControl, object>(this.lstList_SelectedItemChanged);
            // 
            // tsButtonsStationModules
            // 
            this.tsButtonsStationModules.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.tsButtonsStationModules.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnAssignStationModule,
            this.btnRemoveStationModule});
            this.tsButtonsStationModules.Location = new System.Drawing.Point(0, 302);
            this.tsButtonsStationModules.Name = "tsButtonsStationModules";
            this.tsButtonsStationModules.Size = new System.Drawing.Size(575, 25);
            this.tsButtonsStationModules.TabIndex = 4;
            this.tsButtonsStationModules.Text = "toolStrip1";
            // 
            // btnAssignStationModule
            // 
            this.btnAssignStationModule.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnAssignStationModule.Image = ((System.Drawing.Image)(resources.GetObject("btnAssignStationModule.Image")));
            this.btnAssignStationModule.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnAssignStationModule.Name = "btnAssignStationModule";
            this.btnAssignStationModule.Size = new System.Drawing.Size(23, 22);
            this.btnAssignStationModule.Text = "toolStripButton1";
            this.btnAssignStationModule.Click += new System.EventHandler(this.btnAssignStationModule_Click);
            // 
            // btnRemoveStationModule
            // 
            this.btnRemoveStationModule.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnRemoveStationModule.Image = ((System.Drawing.Image)(resources.GetObject("btnRemoveStationModule.Image")));
            this.btnRemoveStationModule.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnRemoveStationModule.Name = "btnRemoveStationModule";
            this.btnRemoveStationModule.Size = new System.Drawing.Size(23, 22);
            this.btnRemoveStationModule.Text = "toolStripButton2";
            // 
            // splitter11
            // 
            this.splitter11.Dock = System.Windows.Forms.DockStyle.Top;
            this.splitter11.Location = new System.Drawing.Point(0, 299);
            this.splitter11.Name = "splitter11";
            this.splitter11.Size = new System.Drawing.Size(575, 3);
            this.splitter11.TabIndex = 1;
            this.splitter11.TabStop = false;
            // 
            // pnlAvailableStationModules
            // 
            this.pnlAvailableStationModules.Controls.Add(this.lbAvailableStationModules);
            this.pnlAvailableStationModules.Controls.Add(this.tsStationModules);
            this.pnlAvailableStationModules.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlAvailableStationModules.Location = new System.Drawing.Point(0, 0);
            this.pnlAvailableStationModules.Name = "pnlAvailableStationModules";
            this.pnlAvailableStationModules.Size = new System.Drawing.Size(575, 299);
            this.pnlAvailableStationModules.TabIndex = 0;
            // 
            // lbAvailableStationModules
            // 
            this.lbAvailableStationModules.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.lbAvailableStationModules.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbAvailableStationModules.FormattingEnabled = true;
            this.lbAvailableStationModules.IntegralHeight = false;
            this.lbAvailableStationModules.ItemHeight = 15;
            this.lbAvailableStationModules.Location = new System.Drawing.Point(0, 25);
            this.lbAvailableStationModules.Name = "lbAvailableStationModules";
            this.lbAvailableStationModules.Size = new System.Drawing.Size(575, 274);
            this.lbAvailableStationModules.TabIndex = 4;
            // 
            // tsStationModules
            // 
            this.tsStationModules.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.tsStationModules.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.lblAvailableStationModules});
            this.tsStationModules.Location = new System.Drawing.Point(0, 0);
            this.tsStationModules.Name = "tsStationModules";
            this.tsStationModules.Size = new System.Drawing.Size(575, 25);
            this.tsStationModules.TabIndex = 3;
            this.tsStationModules.Text = "toolStrip1";
            // 
            // lblAvailableStationModules
            // 
            this.lblAvailableStationModules.Name = "lblAvailableStationModules";
            this.lblAvailableStationModules.Size = new System.Drawing.Size(166, 22);
            this.lblAvailableStationModules.Text = "Available Station HullModules";
            // 
            // splitter10
            // 
            this.splitter10.Location = new System.Drawing.Point(204, 3);
            this.splitter10.Name = "splitter10";
            this.splitter10.Size = new System.Drawing.Size(3, 567);
            this.splitter10.TabIndex = 1;
            this.splitter10.TabStop = false;
            // 
            // lstStations
            // 
            this.lstStations.AddButtonEnabled = true;
            this.lstStations.AddButtonVisible = false;
            this.lstStations.Caption = "Stations";
            this.lstStations.Dock = System.Windows.Forms.DockStyle.Left;
            this.lstStations.EditButtonEnabled = true;
            this.lstStations.EditButtonVisible = false;
            this.lstStations.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lstStations.ItemType = null;
            this.lstStations.Location = new System.Drawing.Point(3, 3);
            this.lstStations.Name = "lstStations";
            this.lstStations.RemoveButtonEnabled = true;
            this.lstStations.RemoveButtonVisible = false;
            this.lstStations.SelectedItem = null;
            this.lstStations.Size = new System.Drawing.Size(201, 567);
            this.lstStations.StatusText = "toolStripStatusLabel1";
            this.lstStations.StatusTextVisible = false;
            this.lstStations.TabIndex = 0;
            this.lstStations.SelectedItemChanged += new System.Action<LessGravity.OpenSpace.Data.Editor.Controls.ListControl, object>(this.lstList_SelectedItemChanged);
            // 
            // tpShips
            // 
            this.tpShips.Location = new System.Drawing.Point(4, 24);
            this.tpShips.Name = "tpShips";
            this.tpShips.Padding = new System.Windows.Forms.Padding(3);
            this.tpShips.Size = new System.Drawing.Size(785, 573);
            this.tpShips.TabIndex = 2;
            this.tpShips.Text = "Ships";
            this.tpShips.UseVisualStyleBackColor = true;
            // 
            // tpItemTemplates
            // 
            this.tpItemTemplates.Location = new System.Drawing.Point(4, 24);
            this.tpItemTemplates.Name = "tpItemTemplates";
            this.tpItemTemplates.Padding = new System.Windows.Forms.Padding(3);
            this.tpItemTemplates.Size = new System.Drawing.Size(785, 573);
            this.tpItemTemplates.TabIndex = 5;
            this.tpItemTemplates.Text = "ItemTemplates";
            this.tpItemTemplates.UseVisualStyleBackColor = true;
            // 
            // tpItems
            // 
            this.tpItems.Location = new System.Drawing.Point(4, 24);
            this.tpItems.Name = "tpItems";
            this.tpItems.Padding = new System.Windows.Forms.Padding(3);
            this.tpItems.Size = new System.Drawing.Size(785, 573);
            this.tpItems.TabIndex = 6;
            this.tpItems.Text = "Items";
            this.tpItems.UseVisualStyleBackColor = true;
            // 
            // tpMarket
            // 
            this.tpMarket.Location = new System.Drawing.Point(4, 24);
            this.tpMarket.Name = "tpMarket";
            this.tpMarket.Padding = new System.Windows.Forms.Padding(3);
            this.tpMarket.Size = new System.Drawing.Size(785, 573);
            this.tpMarket.TabIndex = 7;
            this.tpMarket.Text = "Market";
            this.tpMarket.UseVisualStyleBackColor = true;
            // 
            // tpStockExchange
            // 
            this.tpStockExchange.Location = new System.Drawing.Point(4, 24);
            this.tpStockExchange.Name = "tpStockExchange";
            this.tpStockExchange.Padding = new System.Windows.Forms.Padding(3);
            this.tpStockExchange.Size = new System.Drawing.Size(785, 573);
            this.tpStockExchange.TabIndex = 8;
            this.tpStockExchange.Text = "Stock Exchange";
            this.tpStockExchange.UseVisualStyleBackColor = true;
            // 
            // il32_32
            // 
            this.il32_32.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("il32_32.ImageStream")));
            this.il32_32.TransparentColor = System.Drawing.Color.Transparent;
            this.il32_32.Images.SetKeyName(0, "Empty_32.png");
            this.il32_32.Images.SetKeyName(1, "Star_32.png");
            this.il32_32.Images.SetKeyName(2, "Planet_32.png");
            this.il32_32.Images.SetKeyName(3, "StationModule_32.png");
            this.il32_32.Images.SetKeyName(4, "Storage_32.png");
            this.il32_32.Images.SetKeyName(5, "Structure_32.png");
            // 
            // pgMain
            // 
            this.pgMain.Dock = System.Windows.Forms.DockStyle.Right;
            this.pgMain.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pgMain.GameDbContext = null;
            this.pgMain.Location = new System.Drawing.Point(952, 24);
            this.pgMain.Name = "pgMain";
            this.pgMain.Size = new System.Drawing.Size(312, 601);
            this.pgMain.TabIndex = 11;
            // 
            // lstCharacters
            // 
            this.lstCharacters.AddButtonEnabled = true;
            this.lstCharacters.AddButtonVisible = true;
            this.lstCharacters.Caption = "Characters";
            this.lstCharacters.EditButtonEnabled = true;
            this.lstCharacters.EditButtonVisible = true;
            this.lstCharacters.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lstCharacters.ItemType = null;
            this.lstCharacters.Location = new System.Drawing.Point(225, 6);
            this.lstCharacters.Name = "lstCharacters";
            this.lstCharacters.RemoveButtonEnabled = true;
            this.lstCharacters.RemoveButtonVisible = true;
            this.lstCharacters.SelectedItem = null;
            this.lstCharacters.Size = new System.Drawing.Size(213, 269);
            this.lstCharacters.StatusText = "toolStripStatusLabel1";
            this.lstCharacters.StatusTextVisible = false;
            this.lstCharacters.TabIndex = 1;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1264, 647);
            this.Controls.Add(this.tcMain);
            this.Controls.Add(this.splitter2);
            this.Controls.Add(this.pgMain);
            this.Controls.Add(this.splitter1);
            this.Controls.Add(this.tvUniverse);
            this.Controls.Add(this.stsMain);
            this.Controls.Add(this.mnuMain);
            this.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MainMenuStrip = this.mnuMain;
            this.Name = "MainForm";
            this.Text = "MainForm";
            this.mnuMain.ResumeLayout(false);
            this.mnuMain.PerformLayout();
            this.tcMain.ResumeLayout(false);
            this.tbUsers.ResumeLayout(false);
            this.tpUniverse.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.pnlUniverseRegion.ResumeLayout(false);
            this.tpStation.ResumeLayout(false);
            this.pnlStationModules.ResumeLayout(false);
            this.pnlStationModules.PerformLayout();
            this.pnlButtonsStationModules.ResumeLayout(false);
            this.tsButtonsStationModules.ResumeLayout(false);
            this.tsButtonsStationModules.PerformLayout();
            this.pnlAvailableStationModules.ResumeLayout(false);
            this.pnlAvailableStationModules.PerformLayout();
            this.tsStationModules.ResumeLayout(false);
            this.tsStationModules.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip mnuMain;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.StatusStrip stsMain;
        private System.Windows.Forms.Splitter splitter1;
        private System.Windows.Forms.TreeView tvUniverse;
        private Controls.PropertyGrid pgMain;
        private System.Windows.Forms.Splitter splitter2;
        private System.Windows.Forms.TabControl tcMain;
        private System.Windows.Forms.TabPage tpUniverse;
        private System.Windows.Forms.TabPage tpStation;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Splitter splitter5;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Splitter splitter4;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Splitter splitter3;
        private System.Windows.Forms.Panel pnlUniverseRegion;
        private Controls.ListControl lstUniverses;
        private Controls.ListControl lstRegions;
        private System.Windows.Forms.Splitter splitter6;
        private Controls.ListControl lstJumpGates;
        private System.Windows.Forms.Splitter splitter8;
        private Controls.ListControl lstPlanets;
        private Controls.ListControl lstAsteroids;
        private System.Windows.Forms.Splitter splitter9;
        private Controls.ListControl lstStars;
        private Controls.ListControl lstSolarSystems;
        private System.Windows.Forms.Splitter splitter7;
        private Controls.ListControl lstConstellations;
        private System.Windows.Forms.TabPage tpShips;
        private System.Windows.Forms.TabPage tpCharacters;
        private System.Windows.Forms.TabPage tbUsers;
        private System.Windows.Forms.TabPage tpItemTemplates;
        private System.Windows.Forms.TabPage tpItems;
        private System.Windows.Forms.TabPage tpMarket;
        private System.Windows.Forms.TabPage tpStockExchange;
        private System.Windows.Forms.TabPage tpServers;
        private System.Windows.Forms.Panel pnlStationModules;
        private System.Windows.Forms.Panel pnlButtonsStationModules;
        private System.Windows.Forms.ToolStrip tsButtonsStationModules;
        private System.Windows.Forms.ToolStripButton btnAssignStationModule;
        private System.Windows.Forms.ToolStripButton btnRemoveStationModule;
        private System.Windows.Forms.Splitter splitter11;
        private System.Windows.Forms.Panel pnlAvailableStationModules;
        private System.Windows.Forms.ListBox lbAvailableStationModules;
        private System.Windows.Forms.ToolStrip tsStationModules;
        private System.Windows.Forms.ToolStripLabel lblAvailableStationModules;
        private System.Windows.Forms.Splitter splitter10;
        private Controls.ListControl lstStations;
        private ListControl lstAssignedStationModules;
        private System.Windows.Forms.ImageList il16_16;
        private System.Windows.Forms.ImageList il32_32;
        private ListControl lstUsers;
        private ListControl lstCharacters;
    }
}