﻿using LessGravity.OpenSpace.Data.Celestials;
using System.Collections.Generic;

namespace LessGravity.OpenSpace.Services
{
    public interface ICelestialsService
    {
        IEnumerable<Universe> GetAllUniverses();
        Universe GetUniverse(long id);
        void AddUniverse(Universe universe);

        IEnumerable<Region> GetAllRegions();
        IEnumerable<Region> GetAllRegionsInUniverse(Universe universe);
        Region GetRegion(long id);
        void AddRegion(Region region);

        IEnumerable<Constellation> GetAllConstellations();
        IEnumerable<Constellation> GetAllConstellationsInRegion(Region region);
        Constellation GetConstellation(long id);
        void AddConstellation(Constellation constellation);

        IEnumerable<SolarSystem> GetAllSolarSystems();
        IEnumerable<SolarSystem> GetAllSolarSystemsInConstellation(Constellation constellation);
        IEnumerable<SolarSystem> GetAllSolarSystemsInRegion(Region region);
        SolarSystem GetSolarSystem(long id);
        void AddSolarSystem(SolarSystem solarSystem);

        IEnumerable<Star> GetAllStars();
        IEnumerable<Star> GetAllStarsInSolarSystem(SolarSystem solarSystem);
        IEnumerable<Star> GetAllStarsInConstellation(Constellation constellation);
        Star GetStar(long id);
        void AddStar(Star star);

        IEnumerable<Planet> GetAllPlanets();
        IEnumerable<Planet> GetAllPlanetsInSolarSystem(SolarSystem solarSystem);
        Planet GetPlanet(long id);
        void AddPlanet(Planet planet);

        IEnumerable<Asteroid> GetAllAsteroids();
        IEnumerable<Asteroid> GetAllAsteroidsInSolarSystem(SolarSystem solarSystem);
        Asteroid GetAsteroid(long id);
        void AddAsteroid(Asteroid asteroid);
    }
}