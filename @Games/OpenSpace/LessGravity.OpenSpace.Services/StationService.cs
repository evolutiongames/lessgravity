﻿using LessGravity.OpenSpace.Data.Celestials;
using LessGravity.OpenSpace.Data.StationModules;
using LessGravity.OpenSpace.Data.UnitsOfWork;
using System.Collections.Generic;

namespace LessGravity.OpenSpace.Services
{
    public class StationService : IStationService
    {
        private readonly IStationUnitOfWork<long> _unitOfWork;

        public IEnumerable<Station> GetAllStations()
        {
            return _unitOfWork.StationRepository.GetAll();
        }

        public IEnumerable<Station> GetAllStationsInSolarSystem(SolarSystem solarSystem)
        {
            return _unitOfWork.StationRepository.GetAllStationsInSolarSystem(solarSystem);
        }

        public Station GetStation(long id)
        {
            return _unitOfWork.StationRepository.Get(id);
        }

        public void AddStation(Station station)
        {
            _unitOfWork.StationRepository.Add(station);
            _unitOfWork.SaveChanges();
        }

        public IEnumerable<StationModule> GetAllStationModules()
        {
            return _unitOfWork.StationModuleRepository.GetAll();
        }

        public IEnumerable<StationModule> GetAllStationModulesInStation(Station station)
        {
            return _unitOfWork.StationModuleRepository.GetAllStationModulesInStation(station);
        }

        public StationModule GetStationModule(long id)
        {
            return _unitOfWork.StationModuleRepository.Get(id);
        }

        public void AddStationModule(StationModule stationModule)
        {
            _unitOfWork.StationModuleRepository.Add(stationModule);
            _unitOfWork.SaveChanges();
        }
    }
}