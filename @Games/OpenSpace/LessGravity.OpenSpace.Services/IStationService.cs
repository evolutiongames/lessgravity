﻿using LessGravity.OpenSpace.Data.Celestials;
using LessGravity.OpenSpace.Data.StationModules;
using System.Collections.Generic;

namespace LessGravity.OpenSpace.Services
{
    public interface IStationService
    {
        IEnumerable<Station> GetAllStations();
        IEnumerable<Station> GetAllStationsInSolarSystem(SolarSystem solarSystem);
        Station GetStation(long id);
        void AddStation(Station station);

        IEnumerable<StationModule> GetAllStationModules();
        IEnumerable<StationModule> GetAllStationModulesInStation(Station station);
        StationModule GetStationModule(long id);
        void AddStationModule(StationModule stationModule);
    }
}