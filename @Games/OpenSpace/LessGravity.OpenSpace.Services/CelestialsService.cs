﻿using LessGravity.OpenSpace.Data.Celestials;
using LessGravity.OpenSpace.Data.Context;
using LessGravity.OpenSpace.Data.UnitsOfWork;
using System.Collections.Generic;
using System.Linq;

namespace LessGravity.OpenSpace.Services
{
    public class CelestialsService : ICelestialsService
    {
        private readonly ICelestialsUnitOfWork<long> _unitOfWork;

        public CelestialsService(GameDbContext context)
        {
            _unitOfWork = new CelestialsUnitOfWork<long>(context);
        }

        public IEnumerable<Universe> GetAllUniverses()
        {
            return _unitOfWork.UniverseRepository.GetAll();
        }

        public Universe GetUniverse(long id)
        {
            return _unitOfWork.UniverseRepository.Find(u => u.Id == id).FirstOrDefault();
        }

        public IEnumerable<Region> GetAllRegions()
        {
            return _unitOfWork.RegionRepository.GetAll();
        }

        public IEnumerable<Region> GetAllRegionsInUniverse(Universe universe)
        {
            return _unitOfWork.RegionRepository.GetAllRegionsInUniverse(universe);
        }

        public Region GetRegion(long id)
        {
            return _unitOfWork.RegionRepository.Get(id);
        }

        public IEnumerable<Constellation> GetAllConstellations()
        {
            return _unitOfWork.ConstellationRepository.GetAll();
        }

        public IEnumerable<Constellation> GetAllConstellationsInRegion(Region region)
        {
            return _unitOfWork.ConstellationRepository.GetAllConstellationsInRegion(region);
        }

        public Constellation GetConstellation(long id)
        {
            return _unitOfWork.ConstellationRepository.Get(id);
        }

        public IEnumerable<SolarSystem> GetAllSolarSystems()
        {
            return _unitOfWork.SolarSystemRepository.GetAll();
        }

        public IEnumerable<SolarSystem> GetAllSolarSystemsInConstellation(Constellation constellation)
        {
            return _unitOfWork.SolarSystemRepository.GetAllSolarSystemsInConstellation(constellation);
        }

        public IEnumerable<SolarSystem> GetAllSolarSystemsInRegion(Region region)
        {
            return _unitOfWork.SolarSystemRepository.GetAllSolarSystemsInRegion(region);
        }

        public SolarSystem GetSolarSystem(long id)
        {
            return _unitOfWork.SolarSystemRepository.Get(id);
        }

        public IEnumerable<Star> GetAllStars()
        {
            return _unitOfWork.StarRepository.GetAll();
        }

        public IEnumerable<Star> GetAllStarsInSolarSystem(SolarSystem solarSystem)
        {
            return _unitOfWork.StarRepository.GetAllStarsInSolarSystem(solarSystem);
        }

        public IEnumerable<Star> GetAllStarsInConstellation(Constellation constellation)
        {
            return _unitOfWork.StarRepository.GetAllStarsInConstellation(constellation);
        }

        public Star GetStar(long id)
        {
            return _unitOfWork.StarRepository.Get(id);
        }

        public IEnumerable<Planet> GetAllPlanets()
        {
            return _unitOfWork.PlanetRepository.GetAll();
        }

        public IEnumerable<Planet> GetAllPlanetsInSolarSystem(SolarSystem solarSystem)
        {
            return _unitOfWork.PlanetRepository.GetAllPlanetsInSolarSystem(solarSystem);
        }

        public Planet GetPlanet(long id)
        {
            return _unitOfWork.PlanetRepository.Get(id);
        }

        public IEnumerable<Asteroid> GetAllAsteroids()
        {
            return _unitOfWork.AsteroidRepository.GetAll();
        }

        public IEnumerable<Asteroid> GetAllAsteroidsInSolarSystem(SolarSystem solarSystem)
        {
            return _unitOfWork.AsteroidRepository.GetAllAsteroidsInSolarSystem(solarSystem);
        }

        public Asteroid GetAsteroid(long id)
        {
            return _unitOfWork.AsteroidRepository.Get(id);
        }

        public void AddUniverse(Universe universe)
        {
            _unitOfWork.UniverseRepository.Add(universe);
            _unitOfWork.SaveChanges();
        }

        public void AddRegion(Region region)
        {
            _unitOfWork.RegionRepository.Add(region);
            _unitOfWork.SaveChanges();
        }

        public void AddConstellation(Constellation constellation)
        {
            _unitOfWork.ConstellationRepository.Add(constellation);
            _unitOfWork.SaveChanges();
        }

        public void AddSolarSystem(SolarSystem solarSystem)
        {
            _unitOfWork.SolarSystemRepository.Add(solarSystem);
            _unitOfWork.SaveChanges();
        }

        public void AddStar(Star star)
        {
            _unitOfWork.StarRepository.Add(star);
            _unitOfWork.SaveChanges();
        }

        public void AddPlanet(Planet planet)
        {
            _unitOfWork.PlanetRepository.Add(planet);
            _unitOfWork.SaveChanges();
        }

        public void AddAsteroid(Asteroid asteroid)
        {
            _unitOfWork.AsteroidRepository.Add(asteroid);
            _unitOfWork.SaveChanges();
        }
    }
}