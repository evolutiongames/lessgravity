struct VS_OUT
{
    float4 Position : SV_POSITION;
    float2 UV : TEXCOORD0;
    float3 FrustumCorner : TEXCOORD1;
};

cbuffer Input
{
    float3 FrustumCorners[4];
}

static const float4 Positions[4] =
{
    float4(-1, -1, +0, +1),
    float4(-1, +1, +0, +1),
    float4(+1, -1, +0, +1),
    float4(+1, +1, +0, +1)
};

VS_OUT Main(uint vertexId : SV_VertexID)
{
    VS_OUT output;
    output.Position = Positions[vertexId];
    output.UV = (Positions[vertexId].xy + 1) * 0.5f;
    output.UV.y = 1.0f - output.UV.y;
    output.FrustumCorner = FrustumCorners[vertexId];
    return output;
}