﻿cbuffer PerFrame
{
    float4x4 M_World;
    float4x4 M_View;
    float4x4 M_Projection;
};

struct VS_IN
{
    float3 Position : POSITION;
    float3 Normal : NORMAL0;
    float3 Tangent : TANGENT;
    float2 UV : TEXCOORD0;
};

struct VS_OUT
{
    float4 Position : SV_POSITION;
    float3 Normal : NORMAL0;
    float3 Tangent : TANGENT;
    float2 UV : TEXCOORD0;
};

float4x4 worldViewProj;

VS_OUT Main(VS_IN input)
{
    VS_OUT output = (VS_OUT)0;

    output.Position = mul(float4(input.Position, 1.0f), M_World);
    output.Position = mul(output.Position, M_View);
    output.Position = mul(output.Position, M_Projection);

    float3x3 viewRotation = (float3x3)M_View;
    float3x3 worldRotation = (float3x3)M_World;

    output.Normal = mul(input.Normal, worldRotation);
    output.Normal = mul(output.Normal, viewRotation);
    output.Normal = normalize(output.Normal);

    output.Tangent = input.Tanget;
    output.UV = input.UV;

    return output;
}