Texture2D<float4> TEX_GBuffer1 : register(t0);
Texture2D<float4> TEX_GBuffer2 : register(t1);
Texture2D<float4> TEX_GBuffer3 : register(t2);
TextureCube TXC_Sky : register(t3);

sampler SMP_Linear : register(s0);

struct PS_IN
{
    float4 Position : SV_Position;
    float2 UV : TEXCOORD0;
    float3 FrustumCorner : TEXCOORD1;
};

cbuffer Input
{
    uint2 ScreenSize;
    float3 FrustumCorners[4];
    float4x4 InvertView;
    float3 CameraPosition;
};

float3 DecodeNormal(float2 enc)
{
    float2 fenc = enc * 4.0f - 2;
    float f = dot(fenc, fenc);
    float g = sqrt(1.0f - f * 0.25f);
    float3 n;
    n.xy = fenc * g;
    n.z = 1.0f - f * 0.5f;
    return n;
}

float3 GetViewSpacePosition(float2 uv, float depth)
{
    float3 farPlanePos = FrustumCorners[0] +
        uv.x * (FrustumCorners[2] - FrustumCorners[0]) +
        uv.y * (FrustumCorners[1] - FrustumCorners[0]);
    return farPlanePos * depth;
}

float3 YCoCg2RGB(float3 colour) 
{
	colour.y -= 0.5;
	colour.z -= 0.5;
	return float3(colour.r + colour.g - colour.b, 
		          colour.r + colour.b, 
		          colour.r - colour.g - colour.b);
}

float4 Main(PS_IN input) : SV_Target0
{
    int3 loadPosition = int3(input.Position.xy, 0);

    float2 uv = input.Position.xy / ScreenSize;
    uv.y = 1.0f - uv.y;

    float4 gBuffer1Data = TEX_GBuffer1.Load(loadPosition);
    float4 gBuffer2Data = TEX_GBuffer2.Load(loadPosition);
	float4 gBuffer3Data = TEX_GBuffer3.Load(loadPosition);

    float3x3 invertViewRotation = (float3x3)InvertView;
	        
    if (gBuffer2Data.w >= 1.0f)
    {
		//return TXC_Sky.Sample(SMP_Linear, input.FrustumCorner);
		return TXC_Sky.Sample(SMP_Linear, mul(normalize(input.FrustumCorner), invertViewRotation));
    }

	//float3 YCoCg = float3(gBuffer1Data.rg, 0f);
	//float3 colorDiffuse = YCoCg2RGB(YCoCg);

	float3 colorDiffuse = gBuffer1Data.rgb;
    float3 position = GetViewSpacePosition(uv, gBuffer2Data.w);
    float3 normal = gBuffer2Data.xyz;

	float3 viewDirection = CameraPosition - position;

    float3 reflectedWorld = mul(reflect(position, -normal), invertViewRotation);
	//float3 reflectedWorld = mul(reflect(position, -normal), invertViewRotation);
	//float3 reflectedWorld = reflect(position, -normal) * sign(CameraPosition.z);
	//float3 reflectedWorld = mul(normalize(input.FrustumCorner), input.FrustumCorner);
    float3 reflectedColor = TXC_Sky.Sample(SMP_Linear, reflectedWorld).rgb;

    //return float4(colorDiffuse, 1.0f);
    //return float4(result * reflectedColor * colorDiffuse + (0.125f * normal), 1.0f);

	//float3 finalColor = reflectedColor;
	float3 colorSpecular = gBuffer3Data.rgb;

	/*
	float3 L = CameraPosition;
	float3 V = CameraPosition;
	float3 N = normal;
	float3 mix = brdf(L, V, N, colorDiffuse * reflectedColor, colorSpecular, 0.5f);
	*/

	float3 finalColor = colorDiffuse * reflectedColor * colorSpecular;
	//float3 finalColor = colorSpecular;
	return float4(finalColor, 1.0f);
}