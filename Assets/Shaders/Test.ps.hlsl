﻿Texture2D TEX_Diffuse : register(t0);
Texture2D TEX_Normal : register(t1);
sampler SMP_Linear : register(s0);

struct PS_IN
{
    float4 Position : SV_POSITION;
    float3 Normal : NORMAL0;
    float3 Tangent : TANGENT;
    float2 UV : TEXCOORD0;
};

float4 Main(PS_IN input) : SV_Target
{
    float3 lightPosition = float3(3, 10, 2);
    lightPosition = normalize(lightPosition);

    float3 normal = TEX_Normal.Sample(SMP_Linear, input.UV).xyz;

    float lightDiffuse = dot(lightPosition, normal);

    return TEX_Diffuse.Sample(SMP_Linear, input.UV) * float4(normal, 1.0f); // lightDiffuse;
}