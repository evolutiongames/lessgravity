﻿Texture2D TEX_Diffuse : register(t0);
Texture2D TEX_Normal : register(t1);
Texture2D TEX_Specular : register(t2);
sampler SMP_Linear : register(s0);

struct PS_IN
{
    float4 Position : SV_POSITION;
    float3 Normal : NORMAL0;
    float3 Tangent : TANGENT;
    float2 UV : TEXCOORD0;
    float Depth : TEXCOORD1;
};

struct PS_OUT
{
    float4 GBuffer1	: SV_TARGET0;
    float4 GBuffer2	: SV_TARGET1;
	float4 GBuffer3 : SV_TARGET2;
};

cbuffer Input
{
    float CameraFarPlane;
};

float2 EncodeNormal(float3 n)
{
    float f = sqrt(8 * n.z + 8);
    return n.xy / f + 0.5;
}

float3 RGB2YCoCg(float3 rgbColor) 
{
	return float3(+0.25f * rgbColor.r + 
		          +0.50f * rgbColor.g + 
		          +0.25f * rgbColor.b, 
		
		          +0.50f * rgbColor.r - 
		          +0.50f * rgbColor.b + 0.5, 
		
		          -0.25f * rgbColor.r + 
		          +0.50f * rgbColor.g - 
		          +0.25f * rgbColor.b + 0.5);
}

PS_OUT Main(PS_IN input)
{
    float3 bumpNormal = 2 * (TEX_Normal.Sample(SMP_Linear, input.UV).xyz - 0.5);
    float3 biNormal = cross(input.Tangent, input.Normal);
    float3x3 tangentToView;
    tangentToView[0] = normalize(input.Tangent);
    tangentToView[1] = normalize(biNormal);
    tangentToView[2] = normalize(input.Normal);

    float3 normal = mul(normalize(bumpNormal), tangentToView);

    PS_OUT output = (PS_OUT)0;
	/*
	float3 color = TEX_Diffuse.Sample(SMP_Linear, input.UV).rgb;
	float3 YCoCg = RGB2YCoCg(color);

	float2 crd = input.UV;
	bool pattern = (fmod(crd.x, 2.0f) == fmod(crd.y, 2.0f));
	YCoCg.g = (pattern) ? YCoCg.b : YCoCg.g;
	YCoCg.b = 0.0;

	output.GBuffer1.xy = YCoCg.rg;
	*/
	output.GBuffer1.xyz = TEX_Diffuse.Sample(SMP_Linear, input.UV).rgb;
    output.GBuffer1.w = input.Depth;

    output.GBuffer2.xyz = normal;
    output.GBuffer2.w = input.Depth / CameraFarPlane;

	output.GBuffer3.xyz = TEX_Specular.Sample(SMP_Linear, input.UV).rgb;
    return output;
}