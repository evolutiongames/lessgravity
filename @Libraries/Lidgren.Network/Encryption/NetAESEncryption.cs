using System;
using System.IO;
using System.Security.Cryptography;

namespace Lidgren.Network
{
    /// <summary>
    /// 
    /// </summary>
	public class NetAESEncryption : NetCryptoProviderBase
	{
        /// <summary>
        /// 
        /// </summary>
        /// <param name="peer"></param>
		public NetAESEncryption(NetPeer peer)
#if UNITY
			: base(peer, new RijndaelManaged())
#else
			: base(peer, new AesCryptoServiceProvider())
#endif
		{
		}

        /// <summary>
        /// 
        /// </summary>
        /// <param name="peer"></param>
        /// <param name="key"></param>
		public NetAESEncryption(NetPeer peer, string key)
#if UNITY
			: base(peer, new RijndaelManaged())
#else
			: base(peer, new AesCryptoServiceProvider())
#endif
		{
			SetKey(key);
		}

        /// <summary>
        /// 
        /// </summary>
        /// <param name="peer"></param>
        /// <param name="data"></param>
        /// <param name="offset"></param>
        /// <param name="count"></param>
		public NetAESEncryption(NetPeer peer, byte[] data, int offset, int count)
#if UNITY
			: base(peer, new RijndaelManaged())
#else
			: base(peer, new AesCryptoServiceProvider())
#endif
		{
			SetKey(data, offset, count);
		}
	}
}