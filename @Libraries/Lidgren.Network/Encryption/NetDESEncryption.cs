using System;
using System.IO;
using System.Security.Cryptography;

namespace Lidgren.Network
{
    /// <summary>
    /// 
    /// </summary>
	public class NetDESEncryption : NetCryptoProviderBase
	{
        /// <summary>
        /// 
        /// </summary>
        /// <param name="peer"></param>
		public NetDESEncryption(NetPeer peer)
			: base(peer, new DESCryptoServiceProvider())
		{
		}

        /// <summary>
        /// 
        /// </summary>
        /// <param name="peer"></param>
        /// <param name="key"></param>
		public NetDESEncryption(NetPeer peer, string key)
			: base(peer, new DESCryptoServiceProvider())
		{
			SetKey(key);
		}

        /// <summary>
        /// 
        /// </summary>
        /// <param name="peer"></param>
        /// <param name="data"></param>
        /// <param name="offset"></param>
        /// <param name="count"></param>
		public NetDESEncryption(NetPeer peer, byte[] data, int offset, int count)
			: base(peer, new DESCryptoServiceProvider())
		{
			SetKey(data, offset, count);
		}
	}
}
