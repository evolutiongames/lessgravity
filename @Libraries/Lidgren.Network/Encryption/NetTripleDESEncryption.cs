using System;
using System.IO;
using System.Security.Cryptography;

namespace Lidgren.Network
{
    /// <summary>
    /// 
    /// </summary>
	public class NetTripleDESEncryption : NetCryptoProviderBase
	{
        /// <summary>
        /// 
        /// </summary>
        /// <param name="peer"></param>
		public NetTripleDESEncryption(NetPeer peer)
			: base(peer, new TripleDESCryptoServiceProvider())
		{
		}

        /// <summary>
        /// 
        /// </summary>
        /// <param name="peer"></param>
        /// <param name="key"></param>
		public NetTripleDESEncryption(NetPeer peer, string key)
			: base(peer, new TripleDESCryptoServiceProvider())
		{
			SetKey(key);
		}

        /// <summary>
        /// 
        /// </summary>
        /// <param name="peer"></param>
        /// <param name="data"></param>
        /// <param name="offset"></param>
        /// <param name="count"></param>
		public NetTripleDESEncryption(NetPeer peer, byte[] data, int offset, int count)
			: base(peer, new TripleDESCryptoServiceProvider())
		{
			SetKey(data, offset, count);
		}
	}
}
