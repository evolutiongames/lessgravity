using System;
using System.IO;
using System.Security.Cryptography;

namespace Lidgren.Network
{
    /// <summary>
    /// 
    /// </summary>
	public class NetRC2Encryption : NetCryptoProviderBase
	{
        /// <summary>
        /// 
        /// </summary>
        /// <param name="peer"></param>
		public NetRC2Encryption(NetPeer peer)
			: base(peer, new RC2CryptoServiceProvider())
		{
		}

        /// <summary>
        /// 
        /// </summary>
        /// <param name="peer"></param>
        /// <param name="key"></param>
		public NetRC2Encryption(NetPeer peer, string key)
			: base(peer, new RC2CryptoServiceProvider())
		{
			SetKey(key);
		}

        /// <summary>
        /// 
        /// </summary>
        /// <param name="peer"></param>
        /// <param name="data"></param>
        /// <param name="offset"></param>
        /// <param name="count"></param>
		public NetRC2Encryption(NetPeer peer, byte[] data, int offset, int count)
			: base(peer, new RC2CryptoServiceProvider())
		{
			SetKey(data, offset, count);
		}
	}
}
