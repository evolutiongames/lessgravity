﻿using LessGravity.Logging;
using LessGravity.Quasar.Assets.Readers;
using LessGravity.Quasar.Graphics;
using SharpDX.Direct3D11;
using SharpDX.WIC;
using System;
using System.Collections.Generic;
using System.IO;

namespace LessGravity.Quasar.Assets
{
    public class AssetManager : IDisposable, IAssetManager
    {
        private readonly Dictionary<string, object> _loadedAssets = new Dictionary<string, object>(StringComparer.OrdinalIgnoreCase);
        private readonly List<IDisposable> _disposableAssets = new List<IDisposable>();
        private readonly ILogger _logger;
        private readonly ImagingFactory2 _imagingFactory;
        private readonly Device _device;
        private bool _disposed;


        public string RootDirectory { get; set; }

        public void Add<T>(T asset, string assetName)
        {
            if (!_loadedAssets.ContainsKey(assetName))
            {
                _loadedAssets.Add(assetName, asset);
                if (asset is IDisposable)
                {
                    _disposableAssets.Add(asset as IDisposable);
                }
            }
        }

        //TODO: deccer - remove IServiceRegistry and inject dependencies directly
        public AssetManager(Device device, ImagingFactory2 imagingFactory, ILogger logger, string rootDirectory)
        {
            _device = device;
            _imagingFactory = imagingFactory;
            _logger = logger;
            RootDirectory = rootDirectory;
        }

        ~AssetManager()
        {
            Dispose(false);
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (_disposed)
            {
                return;
            }
            if (disposing)
            {
                Unload();
            }
            _disposed = true;
        }

        private AssetReader GetAssetReaderFromBinary(string originalAssetName, ref Stream stream, BinaryReader binaryReader, Action<IDisposable> recordDisposableObject)
        {
            var magic1 = binaryReader.ReadByte();
            var magic2 = binaryReader.ReadByte();
            var magic3 = binaryReader.ReadByte();
            var magic4 = binaryReader.ReadByte();
            var versionMajor = binaryReader.ReadByte();
            var versionMinor = binaryReader.ReadByte();
            var compressionLxcFlag = (versionMinor & 128) != 0;
            var compressionLz4Flag = (versionMinor & 64) != 0;
            if (versionMajor != 5 && versionMajor != 4)
            {
                throw new AssetLoadException("Invalid version");
            }
            var num = binaryReader.ReadInt32();
            var assetReader = new AssetReader(this, stream, _device, originalAssetName, versionMajor, recordDisposableObject);
            return assetReader;
        }

        public T Load<T>(string assetName)
        {
            if (string.IsNullOrEmpty(assetName))
            {
                throw new ArgumentNullException(nameof(assetName));
            }
            var assetKey = assetName.Replace('\\', '/');
            if (_loadedAssets.TryGetValue(assetKey, out object obj) && obj is T)
            {
                return (T)obj;
            }
            var asset = ReadAsset<T>(assetName, null);
            _loadedAssets[assetKey] = asset;
            return asset;
        }

        protected virtual string Normalize<T>(string assetName)
        {
            if (typeof(T) == typeof(Material))
            {
                return MaterialReader.Normalize(assetName);
            }
            if (typeof(T) == typeof(Model))
            {
                return ModelReader.Normalize(assetName);
            }
            if (typeof(T) == typeof(Shader))
            {
                return ShaderReader.Normalize(assetName);
            }
            if (typeof(T) == typeof(Texture))
            {
                return TextureReader.Normalize(assetName);
            }
            return null;
        }

        protected virtual Stream OpenStream(string assetName)
        {
            Stream result;
            var assetBinaryFileName = string.Empty;
            try
            {
                assetBinaryFileName = Path.Combine(RootDirectory, assetName) + ".lg";
                result = Path.IsPathRooted(assetBinaryFileName) ? File.OpenRead(assetBinaryFileName) : AssetManagerHelper.OpenStream(assetBinaryFileName);
            }
            catch (FileNotFoundException ex)
            {
                _logger.Debug($"The asset file {assetBinaryFileName} was not found.", Strings.Logging.Categories.Assets);
                throw new AssetLoadException("The asset file was not found.", ex);
            }
            catch (DirectoryNotFoundException ex)
            {
                throw new AssetLoadException("The directory was not found.", ex);
            }
            catch (Exception ex)
            {
                throw new AssetLoadException("Opening stream error.", ex);
            }
            return result;
        }

        protected T ReadAsset<T>(string assetName, Action<IDisposable> recordDisposableObject)
        {
            if (string.IsNullOrEmpty(assetName))
            {
                throw new ArgumentNullException(nameof(assetName));
            }
            string text = assetName;
            object asset;
            if (_device == null)
            {
                throw new InvalidOperationException("No Graphics Device Service");
            }
            try
            {
                var stream = OpenStream(assetName);
                try
                {
                    using (var binaryReader = new BinaryReader(stream))
                    {
                        using (var assetReader = GetAssetReaderFromBinary(assetName, ref stream, binaryReader, recordDisposableObject))
                        {
                            asset = assetReader.ReadAsset<T>();
                        }
                    }
                }
                finally
                {
                    stream?.Dispose();
                }
            }
            catch (AssetLoadException innerException)
            {
                //TODO(deccer) ugly hack - find a better way
                if (typeof(T) == typeof(TextureCube))
                {
                    asset = ReadRawAsset<T>(assetName, text);
                }
                else
                {
                    assetName = AssetManagerHelper.GetFilename(Path.Combine(RootDirectory, assetName));
                    assetName = Normalize<T>(assetName);
                    if (string.IsNullOrEmpty(assetName))
                    {
                        throw new AssetLoadException("Could not load " + text + " asset as a non-content file!", innerException);
                    }
                    asset = ReadRawAsset<T>(assetName, text);
                }
                if (asset is IDisposable)
                {
                    if (recordDisposableObject != null)
                    {
                        recordDisposableObject(asset as IDisposable);
                    }
                    else
                    {
                        _disposableAssets.Add(asset as IDisposable);
                    }
                }
            }
            if (asset == null)
            {
                throw new AssetLoadException("Could not load " + text + " asset!");
            }
            return (T)asset;
        }

        protected virtual object ReadRawAsset<T>(string assetName, string originalAssetName)
        {
            if (typeof(T) == typeof(Material))
            {
                var material = new Material();
                return material;
            }
            if (typeof(T) == typeof(Model))
            {
                var model = new Model(assetName);
                return model;
            }
            if (typeof(T) == typeof(Shader))
            {
                var shader = Shader.FromFile(_device, _device.ImmediateContext, _logger, assetName);
                return shader;
            }
            if (typeof(T) == typeof(Texture))
            {
                var texture = new Texture(_device, _logger, _imagingFactory, assetName);
                return texture;
            }
            if (typeof(T) == typeof(TextureCube))
            {
                var textureCube = new TextureCube(_device, _logger, _imagingFactory, assetName);
                return textureCube;
            }
            return null;
        }

        internal void RecordDisposable(IDisposable disposable)
        {
            if (!_disposableAssets.Contains(disposable))
            {
                _disposableAssets.Add(disposable);
            }
        }

        public virtual void Unload()
        {
            foreach (var current in _disposableAssets)
            {
                current?.Dispose();
            }
            _disposableAssets.Clear();
            _loadedAssets.Clear();
        }
    }
}