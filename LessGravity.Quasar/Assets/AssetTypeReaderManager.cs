﻿using LessGravity.Quasar.Extensions;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Text.RegularExpressions;

namespace LessGravity.Quasar.Assets
{
    public sealed class AssetTypeReaderManager
    {
        private static readonly Dictionary<string, Func<AssetTypeReader>> _typeCreators;
        private static readonly string _assemblyName;
        private Dictionary<Type, AssetTypeReader> _contentReaders;
        private static readonly Dictionary<Type, AssetTypeReader> _contentReadersCache;
        private static readonly object _locker;

        static AssetTypeReaderManager()
        {
            _typeCreators = new Dictionary<string, Func<AssetTypeReader>>();
            _locker = new object();
            _contentReadersCache = new Dictionary<Type, AssetTypeReader>(255);
            _assemblyName = Assembly.GetExecutingAssembly().FullName;
        }

        public static void AddTypeCreator(string typeString, Func<AssetTypeReader> createFunction)
        {
            if (!_typeCreators.ContainsKey(typeString))
            {
                _typeCreators.Add(typeString, createFunction);
            }
        }

        public static void ClearTypeCreators()
        {
            _typeCreators.Clear();
        }

        public AssetTypeReader GetTypeReader(Type targetType)
        {
            AssetTypeReader assetTypeReader;
            return _contentReaders.TryGetValue(targetType, out assetTypeReader) ? assetTypeReader : null;
        }

        internal AssetTypeReader[] LoadAssetReaders(AssetReader reader)
        {
            var typeReaderIndex = reader.Read7BitEncodedInt();
            var assetTypeReaders = new AssetTypeReader[typeReaderIndex];
            var bitArray = new BitArray(typeReaderIndex);
            _contentReaders = new Dictionary<Type, AssetTypeReader>(typeReaderIndex);
            lock (_locker)
            {
                for (var i = 0; i < typeReaderIndex; ++i)
                {
                    var text = reader.ReadString();
                    Func<AssetTypeReader> func;
                    if (_typeCreators.TryGetValue(text, out func))
                    {
                        assetTypeReaders[i] = func();
                        bitArray[i] = true;
                    }
                    else
                    {
                        var text2 = text;
                        text2 = PrepareType(text2);
                        var type = Type.GetType(text2);
                        if (!(type != null))
                        {
                            throw new AssetLoadException(string.Concat(new []
							{
								"Could not find ContentTypeReader Type. Please ensure the name of the Assembly that contains the Type matches the assembly in the full type name: ",
								text,
								" (",
								text2,
								")"
							}));
                        }
                        AssetTypeReader contentTypeReader;
                        if (!_contentReadersCache.TryGetValue(type, out contentTypeReader))
                        {
                            try
                            {
                                contentTypeReader = (type.GetDefaultConstructor().Invoke(null) as AssetTypeReader);
                            }
                            catch (TargetInvocationException innerException)
                            {
                                throw new InvalidOperationException("Failed to get default constructor for AssetTypeReader. To work around, add a creation function to ContentTypeReaderManager.AddTypeCreator() with the following failed type string: " + text, innerException);
                            }
                            bitArray[i] = true;
                            _contentReadersCache.Add(type, contentTypeReader);
                        }
                        assetTypeReaders[i] = contentTypeReader;
                    }
                    var targetType = assetTypeReaders[i].TargetType;
                    if (targetType != null)
                    {
                        _contentReaders.Add(targetType, assetTypeReaders[i]);
                    }
                    reader.ReadInt32();
                }
                for (var j = 0; j < assetTypeReaders.Length; ++j)
                {
                    if (bitArray.Get(j))
                    {
                        assetTypeReaders[j].Initialize(this);
                    }
                }
            }
            return assetTypeReaders;
        }

        public static string PrepareType(string type)
        {
            var num = type.Split(new[]
			{
				"[["
			}, StringSplitOptions.None).Length - 1;
            var text = type;
            for (var i = 0; i < num; i++)
            {
                text = Regex.Replace(text, "\\[(.+?), Version=.+?\\]", "[$1]");
            }
            if (text.Contains("PublicKeyToken"))
            {
                text = Regex.Replace(text, "(.+?), Version=.+?$", "$1");
            }
            return text;
        }

    }
}