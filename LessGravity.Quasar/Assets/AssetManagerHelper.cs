﻿using System;
using System.IO;

namespace LessGravity.Quasar.Assets
{
    public static class AssetManagerHelper
    {
        internal static string Location
        {
            get;
            private set;
        }

        static AssetManagerHelper()
        {
            Location = AppDomain.CurrentDomain.BaseDirectory;
        }

        internal static string GetFilename(string name)
        {
            return FileHelpers.NormalizeFilePathSeparators(new Uri("file:///" + name).LocalPath.Substring(1));
        }

        public static Stream OpenStream(string name)
        {
            string filename = GetFilename(name);
            if (Path.IsPathRooted(filename))
            {
                throw new ArgumentException("Invalid filename. TitleContainer.OpenStream requires a relative path.");
            }
            string path = Path.Combine(Location, filename);
            return File.OpenRead(path);
        }
    }
}