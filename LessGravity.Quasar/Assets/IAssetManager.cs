namespace LessGravity.Quasar.Assets
{
    public interface IAssetManager
    {
        void Add<T>(T asset, string assetName);
        T Load<T>(string assetName);
        void Unload();
    }
}