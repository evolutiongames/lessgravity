﻿using System;
using System.Collections.Generic;
using System.IO;
using SharpDX;
using SharpDX.Direct3D11;

namespace LessGravity.Quasar.Assets
{
    public sealed class AssetReader : BinaryReader
    {
        private readonly AssetManager _assetManager;
        private readonly string _assetName;

        private readonly Device _graphicsDevice;

        private readonly Action<IDisposable> _recordDisposableObject;

        internal int _sharedResourceCount;

        private List<KeyValuePair<int, Action<object>>> _sharedResourceFixups;

        private AssetTypeReaderManager _typeReaderManager;

        internal int _version;

        internal AssetReader(AssetManager manager, Stream stream, Device graphicsDevice, string assetName, int version,
            Action<IDisposable> recordDisposableObject)
            : base(stream)
        {
            _graphicsDevice = graphicsDevice;
            _recordDisposableObject = recordDisposableObject;
            _assetManager = manager;
            _assetName = assetName;
            _version = version;
        }

        public string AssetName => _assetName;

        public AssetManager AssetManager => _assetManager;

        internal Device GraphicsDevice => _graphicsDevice;

        internal AssetTypeReader[] TypeReaders { get; private set; }

        internal void InitializeTypeReaders()
        {
            _typeReaderManager = new AssetTypeReaderManager();
            TypeReaders = _typeReaderManager.LoadAssetReaders(this);
            _sharedResourceCount = Read7BitEncodedInt();
            _sharedResourceFixups = new List<KeyValuePair<int, Action<object>>>();
        }

        private T InnerReadObject<T>(T existingInstance)
        {
            int assetTypeReaderIndex = Read7BitEncodedInt();
            if (assetTypeReaderIndex == 0)
            {
                return existingInstance;
            }
            if (assetTypeReaderIndex > TypeReaders.Length)
            {
                throw new AssetLoadException("Incorrect type reader index found!");
            }
            var assetTypeReader = TypeReaders[assetTypeReaderIndex - 1];
            var asset = (T)assetTypeReader.Read(this, existingInstance);
            RecordDisposable(asset);
            return asset;
        }

        public new int Read7BitEncodedInt()
        {
            return base.Read7BitEncodedInt();
        }

        internal object ReadAsset<T>()
        {
            InitializeTypeReaders();
            object asset = ReadObject<T>();
            ReadSharedResources();
            return asset;
        }

        internal BoundingSphere ReadBoundingSphere()
        {
            var center = ReadVector3();
            var radius = ReadSingle();
            return new BoundingSphere(center, radius);
        }

        public Color ReadColor()
        {
            return new Color
            {
                R = ReadByte(),
                G = ReadByte(),
                B = ReadByte(),
                A = ReadByte()
            };
        }

        public T ReadExternalReference<T>()
        {
            var text = ReadString();
            return !string.IsNullOrEmpty(text) ? _assetManager.Load<T>(FileHelpers.ResolveRelativePath(_assetName, text)) : default(T);
        }

        public Matrix ReadMatrix()
        {
            return new Matrix
            {
                M11 = ReadSingle(),
                M12 = ReadSingle(),
                M13 = ReadSingle(),
                M14 = ReadSingle(),
                M21 = ReadSingle(),
                M22 = ReadSingle(),
                M23 = ReadSingle(),
                M24 = ReadSingle(),
                M31 = ReadSingle(),
                M32 = ReadSingle(),
                M33 = ReadSingle(),
                M34 = ReadSingle(),
                M41 = ReadSingle(),
                M42 = ReadSingle(),
                M43 = ReadSingle(),
                M44 = ReadSingle()
            };
        }

        public T ReadObject<T>(AssetTypeReader typeReader)
        {
            var result = (T)((object)typeReader.Read(this, default(T)));
            RecordDisposable(result);
            return result;
        }

        public T ReadObject<T>(T existingInstance = default(T))
        {
            return InnerReadObject(existingInstance);
        }

        public T ReadObject<T>(AssetTypeReader typeReader, T existingInstance)
        {
            if (!typeReader.TargetType.IsValueType)
            {
                return ReadObject(existingInstance);
            }
            var result = (T)typeReader.Read(this, existingInstance);
            RecordDisposable(result);
            return result;
        }

        public Quaternion ReadQuaternion()
        {
            return new Quaternion
            {
                X = ReadSingle(),
                Y = ReadSingle(),
                Z = ReadSingle(),
                W = ReadSingle()
            };
        }

        public T ReadRawObject<T>(T existingInstance = default(T))
        {
            var typeFromHandle = typeof(T);
            var typeReaders = TypeReaders;
            for (var i = 0; i < typeReaders.Length; i++)
            {
                var assetTypeReader = typeReaders[i];
                if (assetTypeReader.TargetType == typeFromHandle)
                {
                    return ReadRawObject(assetTypeReader, existingInstance);
                }
            }
            throw new NotSupportedException();
        }

        public T ReadRawObject<T>(AssetTypeReader typeReader, T existingInstance = default(T))
        {
            return (T)typeReader.Read(this, existingInstance);
        }

        public void ReadSharedResource<T>(Action<T> fixup)
        {
            var num = Read7BitEncodedInt();
            if (num > 0)
            {
                _sharedResourceFixups.Add(new KeyValuePair<int, Action<object>>(num - 1, delegate(object v)
                {
                    if (!(v is T))
                    {
                        throw new AssetLoadException($"Error loading shared resource. Expected type {typeof(T).Name}, received type {v.GetType().Name}");
                    }
                    fixup((T)v);
                }));
            }
        }

        internal void ReadSharedResources()
        {
            if (_sharedResourceCount <= 0)
            {
                return;
            }
            var array = new object[_sharedResourceCount];
            for (var i = 0; i < _sharedResourceCount; i++)
            {
                array[i] = InnerReadObject<object>(null);
            }
            foreach (var current in _sharedResourceFixups)
            {
                current.Value(array[current.Key]);
            }
        }

        public Vector2 ReadVector2()
        {
            return new Vector2
            {
                X = ReadSingle(),
                Y = ReadSingle()
            };
        }

        public Vector3 ReadVector3()
        {
            return new Vector3
            {
                X = ReadSingle(),
                Y = ReadSingle(),
                Z = ReadSingle()
            };
        }

        public Vector4 ReadVector4()
        {
            return new Vector4
            {
                X = ReadSingle(),
                Y = ReadSingle(),
                Z = ReadSingle(),
                W = ReadSingle()
            };
        }

        private void RecordDisposable<T>(T result)
        {
            var disposable = result as IDisposable;
            if (disposable == null)
            {
                return;
            }
            if (_recordDisposableObject != null)
            {
                _recordDisposableObject(disposable);
                return;
            }
            _assetManager.RecordDisposable(disposable);
        }
    }
}