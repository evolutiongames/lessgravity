﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace LessGravity.Quasar.Assets
{
    public abstract class AssetTypeReader
    {
        public virtual bool CanDeserializeIntoExistingObject { get { return false; } }

        public Type TargetType { get; private set; }
        
        protected AssetTypeReader(Type targetType)
        {
            TargetType = targetType;
        }

        protected internal virtual void Initialize(AssetTypeReaderManager manager)
        {
        }

        public static string Normalize(string fileName, IEnumerable<string> extensions)
        {
            return File.Exists(fileName) ? fileName : extensions.Select(extension => fileName + extension).FirstOrDefault(File.Exists);
        }

        protected internal abstract object Read(AssetReader assetReader, object existingInstance);
    }

    public abstract class AssetTypeReader<T> : AssetTypeReader
    {
        protected AssetTypeReader()
            : base(typeof(T))
        {
        }

        protected internal override object Read(AssetReader assetReader, object existingInstance)
        {
            return existingInstance == null ? Read(assetReader, default(T)) : Read(assetReader, (T)existingInstance);
        }

        protected internal abstract T Read(AssetReader assetReader, T existingInstance);
    }

}