﻿using System;
using System.Runtime.InteropServices;

namespace LessGravity.Quasar.Types
{
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct PackedUInt2
    {
        public uint X;
        public uint Y;
    }
}