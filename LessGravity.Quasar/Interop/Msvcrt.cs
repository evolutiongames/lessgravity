﻿using System;
using System.Runtime.InteropServices;
using System.Security;

namespace LessGravity.Quasar.Interop
{
    public static class Msvcrt
    {
        [SuppressUnmanagedCodeSecurity]
        [DllImport("msvcrt.dll", EntryPoint = "memcpy", CallingConvention = CallingConvention.Cdecl, SetLastError = false)]
        public static extern IntPtr MemCopy(IntPtr destination, IntPtr source, int count);

        [SuppressUnmanagedCodeSecurity]
        [DllImport("msvcrt.dll", EntryPoint = "memcpy", CallingConvention = CallingConvention.Cdecl, SetLastError = false)]
        public static extern unsafe void* MemCopy(void* destination, void* source, int count);
    }
}