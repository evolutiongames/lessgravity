﻿using SharpDX;

namespace LessGravity.Quasar.Graphics.Blocks
{
    public static class BlockPositionExtensions
    {
        public static Vector3 ToVector3(this BlockPosition blockPosition)
        {
            return new Vector3(blockPosition.X, blockPosition.Y, blockPosition.Z);
        }
    }
}