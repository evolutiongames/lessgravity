﻿using SharpDX.WIC;
using System.Collections.Generic;
using System.Linq;

namespace SharpDX.Direct3D11
{
    public static class DeviceExtensions
    {
        //TODO(deccer) add params for miplevels and arraysize
        public static Texture2D CreateTexture2DFromBitmap(this Device device, BitmapSource bitmapSource)
        {
            int stride = bitmapSource.Size.Width * 4;
            using (var buffer = new DataStream(bitmapSource.Size.Height * stride, true, true))
            {
                bitmapSource.CopyPixels(stride, buffer);
                return new Texture2D(device, new Texture2DDescription()
                {
                    Width = bitmapSource.Size.Width,
                    Height = bitmapSource.Size.Height,
                    ArraySize = 1,
                    BindFlags = BindFlags.ShaderResource,
                    Usage = ResourceUsage.Immutable,
                    CpuAccessFlags = CpuAccessFlags.None,
                    Format = DXGI.Format.R8G8B8A8_UNorm,
                    MipLevels = 1,
                    OptionFlags = ResourceOptionFlags.None,
                    SampleDescription = new DXGI.SampleDescription(1, 0),
                }, new DataRectangle(buffer.DataPointer, stride));
            }
        }

        //TODO(deccer) add params for miplevels and arraysize
        public static Texture2D CreateTexture2DCubeFromBitmaps(this Device device, IEnumerable<BitmapSource> bitmapSources)
        {
            IEnumerable<DataStream> dataStreams = null;
            try
            {
                var bitmapSource = bitmapSources.First();
                var stride = bitmapSource.Size.Width * 4;

                dataStreams = bitmapSources.Select(bs =>
                {
                    var buffer = new DataStream(bs.Size.Height * stride, true, true);
                    bs.CopyPixels(stride, buffer);
                    return buffer;
                });
                return new Texture2D(device, new Texture2DDescription()
                {
                    Width = bitmapSource.Size.Width,
                    Height = bitmapSource.Size.Height,
                    ArraySize = 6,
                    BindFlags = BindFlags.ShaderResource,
                    Usage = ResourceUsage.Immutable,
                    CpuAccessFlags = CpuAccessFlags.None,
                    Format = DXGI.Format.R8G8B8A8_UNorm_SRgb,
                    MipLevels = 1,
                    OptionFlags = ResourceOptionFlags.TextureCube,
                    SampleDescription = new DXGI.SampleDescription(1, 0),
                }, dataStreams.Select(ds => new DataRectangle(ds.DataPointer, stride)).ToArray());
            }
            finally
            {
                if (dataStreams != null)
                {
                    foreach (var dataStream in dataStreams)
                    {
                        dataStream.Dispose();
                    }
                }
            }
        }
    }
}