﻿using SharpDX;

namespace LessGravity.Quasar.Extensions
{
    public static class DisposeBaseExtensions
    {
        public static void DisposeIfNotDisposed(this DisposeBase disposeBase)
        {
            if (disposeBase != null && !disposeBase.IsDisposed)
            {
                disposeBase.Dispose();
            }
        }
    }
}