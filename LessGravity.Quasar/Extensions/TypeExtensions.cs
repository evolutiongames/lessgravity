﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace LessGravity.Quasar.Extensions
{
    public static class TypeExtensions
    {
        public static FieldInfo[] GetAllFields(this Type type)
        {
            const BindingFlags bindingAttr =
                BindingFlags.DeclaredOnly | BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic;
            return type.GetFields(bindingAttr);
        }

        public static PropertyInfo[] GetAllProperties(this Type type)
        {
            List<PropertyInfo> list =
                type.GetProperties(BindingFlags.DeclaredOnly | BindingFlags.Instance | BindingFlags.Public |
                                   BindingFlags.NonPublic).ToList();
            return
                list.FindAll(p => p.GetGetMethod(true) != null && p.GetGetMethod(true) == p.GetGetMethod(true).GetBaseDefinition())
                    .ToArray();
        }

        public static ConstructorInfo GetDefaultConstructor(this Type type)
        {
            const BindingFlags bindingAttr = BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic;
            return type.GetConstructor(bindingAttr, null, new Type[0], null);
        }

        public static bool IsClass(this Type type)
        {
            return type.IsClass;
        }
    }
}