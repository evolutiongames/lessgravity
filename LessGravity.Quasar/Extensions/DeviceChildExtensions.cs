﻿using LessGravity.Logging;
using SharpDX.Direct3D11;
using System.Diagnostics;

namespace LessGravity.Quasar.Extensions
{
    public static class DeviceChildExtensions
    {
        public static void DisposeIfNotDisposed(this DeviceChild deviceChild)
        {
            if (deviceChild != null && !deviceChild.IsDisposed)
            {
                //Logger.Debug($"Disposing Resource ({deviceChild.NativePointer.ToInt64():X16}) [{deviceChild.DebugName}:{deviceChild.GetType().Name}]", Strings.Logging.Categories.Resources);
                deviceChild.Dispose();
            }
        }

        [Conditional("DEBUG")]
        public static void SetDebugName(this DeviceChild deviceChild, string debugName, bool showNativePointer = true)
        {
            if (showNativePointer)
            {
                deviceChild.DebugName = $"{debugName} ({deviceChild.NativePointer.ToInt64():X16})";
            }
            else
            {
                deviceChild.DebugName = debugName;
            }
        }

        [Conditional("DEBUG")]
        public static void SetDebugName(this SharpDX.DXGI.DeviceChild deviceChild, string debugName, bool showNativePointer = true)
        {
            if (showNativePointer)
            {
                deviceChild.DebugName = $"{debugName} ({deviceChild.NativePointer.ToInt64():X16})";
            }
            else
            {
                deviceChild.DebugName = debugName;
            }
        }
    }
}