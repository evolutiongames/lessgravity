﻿using LessGravity.Quasar.Graphics.Blocks;
using System;

namespace SharpDX
{
    public static class Vector3Extensions
    {
        public static Vector3 Floor(this Vector3 vector, float gridSize)
        {
            var x = (float)Math.Floor(vector.X / gridSize);
            var y = (float)Math.Floor(vector.Y / gridSize);
            var z = (float)Math.Floor(vector.Z / gridSize);

            return new Vector3(x, y, z);
        }

        public static BlockPosition ToBlockPosition(this Vector3 vector)
        {
            return new BlockPosition((int)Math.Floor(vector.X), (int)Math.Floor(vector.Y), (int)Math.Floor(vector.Z));
        }
    }
}