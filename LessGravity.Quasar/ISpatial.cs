﻿using SharpDX;

namespace LessGravity.Quasar
{
    public interface ISpatial
    {
        Vector3 Position { get; set; }
    }
}