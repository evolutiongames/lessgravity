﻿using System;

namespace LessGravity.Quasar.Graphics
{
    public class TypedBufferException : Exception
    {
        public TypedBufferException(string message) 
            : base(message)
        {
            
        }
    }
}