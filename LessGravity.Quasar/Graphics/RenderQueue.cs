﻿using LessGravity.Logging;
using LessGravity.Quasar.Assets;
using SharpDX.Direct3D11;
using System.Collections;
using System.Collections.Generic;

namespace LessGravity.Quasar.Graphics
{
    public sealed class RenderQueue : IEnumerable<RenderQueue.ModelMeshInstance>
    {
        #region Nested type: ModelMeshInstance

        public struct ModelMeshInstance
        {
            public readonly ModelMesh Mesh;
            public readonly ModelInstanceBase ModelInstance;

            public ModelMeshInstance(ModelInstanceBase modelInstance, ModelMesh mesh)
            {
                ModelInstance = modelInstance;
                Mesh = mesh;
            }

            public override string ToString()
            {
                return Mesh == null ? "//TODO(deccer): fix me and clean me up :D" : Mesh.Model.Name;
            }
        }

        #endregion

        private readonly List<ModelMeshInstance> _meshInstances;
        private readonly ILogger _logger;

        public RenderQueue(ILogger logger)
        {
            _logger = logger;
            _meshInstances = new List<ModelMeshInstance>();
        }

        public IEnumerator<ModelMeshInstance> GetEnumerator()
        {
            return _meshInstances.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public void AddMesh(ModelInstanceBase modelInstance, ModelMesh mesh)
        {
            _meshInstances.Add(new ModelMeshInstance(modelInstance, mesh));
        }

        public void Clear()
        {
            _meshInstances.Clear();
        }

        public void Draw(Device device, Shader pixelShader, Shader vertexShader, AssetManager assetManager)
        {
            Material lastMaterial = null;

            for (var meshInstanceIndex = 0; meshInstanceIndex < _meshInstances.Count; ++meshInstanceIndex)
            {
                var meshInstance = _meshInstances[meshInstanceIndex];
                var mesh = meshInstance.Mesh;
                if (mesh.Material != null)
                {
                    if (mesh.Material != lastMaterial)
                    {
                        mesh.Material.Apply(pixelShader, device, assetManager);
                        lastMaterial = mesh.Material;
                    }
                }
                else if (lastMaterial != null)
                {
                    Material.ApplyDefault(pixelShader, device, assetManager, _logger);
                    lastMaterial = null;
                }
                else
                {
                    Material.ApplyDefault(pixelShader, device, assetManager, _logger);
                }

                var meshModel = mesh.Model;
                var meshIndexBuffer = mesh.IndexBuffer;
                var meshVertexBuffer = meshModel.VertexBuffer;

                var deviceContext = device.ImmediateContext;
                var inputAssembler = deviceContext.InputAssembler;
                inputAssembler.SetVertexBuffers(0, meshModel.VertexBuffer.GetVertexBufferBinding(device));
                inputAssembler.SetIndexBuffer(meshIndexBuffer.GetIndexBuffer(device), meshIndexBuffer.Format, 0);
                inputAssembler.PrimitiveTopology = meshVertexBuffer.Topology;
                inputAssembler.InputLayout = meshVertexBuffer.GetInputLayout(device, vertexShader.Bytecode);

                vertexShader.Parameters["M_World"].SetValue(meshInstance.ModelInstance.WorldMatrix);

                vertexShader.BindBuffers();
                pixelShader.BindBuffers();

                deviceContext.DrawIndexed(meshIndexBuffer.Count, 0, 0);
            }
        }

        public void Sort()
        {
            _meshInstances.Sort((a, b) =>
            {
                if (ReferenceEquals(a.Mesh.Material, b.Mesh.Material))
                {
                    return 0;
                }
                if (a.Mesh.Material == null)
                {
                    return 1;
                }
                if (b.Mesh.Material == null)
                {
                    return -1;
                }
                return a.Mesh.Material.Index - b.Mesh.Material.Index;
            });
        }
    }
}