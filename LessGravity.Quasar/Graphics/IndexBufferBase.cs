﻿using LessGravity.Quasar.Extensions;
using SharpDX.Direct3D11;
using System;
using System.Runtime.InteropServices;
using Buffer = SharpDX.Direct3D11.Buffer;

namespace LessGravity.Quasar.Graphics
{
    public abstract class IndexBufferBase : IDisposable
    {
        protected Buffer Buffer;

        public abstract SharpDX.DXGI.Format Format { get; }

        public int Count { get; protected set; }

        public bool CanGrow { get; set; }

        public abstract int this[int index] { get; }

        public void Dispose()
        {
            Invalidate();
        }

        public Buffer GetIndexBuffer(Device device)
        {
            if (Buffer != null)
            {
                return Buffer;
            }

            var data = GetData();
            if (data.Length == 0)
            {
                //Logger.Warn("Empty IndexBuffer", "IndexBufferBase");
                return null;
            }
            var bd = new BufferDescription
            {
                BindFlags = BindFlags.IndexBuffer,
                CpuAccessFlags = CpuAccessFlags.None,
                OptionFlags = ResourceOptionFlags.None,
                SizeInBytes = data.Length * Marshal.SizeOf(data.GetValue(0)),
                Usage = ResourceUsage.Immutable
            };
            var dataPtr = GCHandle.Alloc(data, GCHandleType.Pinned);
            Buffer = new Buffer(device, dataPtr.AddrOfPinnedObject(), bd);
            dataPtr.Free();
            return Buffer;
        }

        protected abstract Array GetData();

        protected void Invalidate()
        {
            Buffer.DisposeIfNotDisposed();
        }
    }
}