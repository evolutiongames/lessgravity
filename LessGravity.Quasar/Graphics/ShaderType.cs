﻿namespace LessGravity.Quasar.Graphics
{
    public enum ShaderType
    {
        Undefined = 0,
        ComputeShader,
        DomainShader,
        GeometryShader,
        HullShader,
        PixelShader,
        VertexShader
    }
}