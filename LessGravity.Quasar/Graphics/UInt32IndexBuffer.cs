﻿using SharpDX.DXGI;
using System;
using System.Collections.Generic;

namespace LessGravity.Quasar.Graphics
{
    public class UInt32IndexBuffer : IndexBuffer<UInt32>
    {
        public UInt32IndexBuffer(int capacity)
            : base(capacity)
        {

        }

        public UInt32IndexBuffer(IEnumerable<UInt32> data, int count = -1) :
            base(data, count)
        {
        }

        public override Format Format => Format.R32_UInt;

        public override int this[int index] => (int)Data[index];
    }
}