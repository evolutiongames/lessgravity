﻿using System;
using LessGravity.Logging;
using LessGravity.Quasar.Assets;
using SharpDX;
using SharpDX.Direct3D11;

namespace LessGravity.Quasar.Graphics
{
    public class Material : IDisposable
    {
        public Texture AlphaTexture { get; set; }
        private string _alphaTextureName;

        public string AlphaTextureName
        {
            get => _alphaTextureName;
            set
            {
                if (_alphaTextureName == value)
                {
                    return;
                }
                _alphaTextureName = value;
                IsDirty = true;
            }
        }

        public Texture DiffuseTexture { get; set; }
        private string _diffuseTextureName;

        public string DiffuseTextureName
        {
            get => _diffuseTextureName;
            set
            {
                if (_diffuseTextureName == value)
                {
                    return;
                }
                _diffuseTextureName = value;
                IsDirty = true;
            }
        }

        public string Name { get; set; }

        public Texture NormalTexture { get; set; }
        private string _normalTextureName;

        public string NormalTextureName
        {
            get => _normalTextureName;
            set
            {
                if (_normalTextureName == value)
                {
                    return;
                }
                _normalTextureName = value;
                IsDirty = true;
            }
        }
        public Texture SpecularTexture { get; set; }
        private string _specularTextureName;

        public string SpecularTextureName
        {
            get => _specularTextureName;
            set
            {
                if (_specularTextureName == value)
                {
                    return;
                }
                _specularTextureName = value;
                IsDirty = true;
            }
        }

        public bool IsDirty { get; internal set; }

        public Color AmbientColor { get; set; }
        public Color DiffuseColor { get; set; }
        public Color EmissionColor { get; set; }
        public Color SpecularColor { get; set; }
        public float SpecularCoefficient { get; set; }
        public float Metalness { get; set; }
        public float Roughness { get; set; }
        public int Index { get; set; }

        private SamplerState _linearSamplerState;
        private static Texture _defaultDiffuseTexture;
        private static Texture _defaultNormalTexture;
        private static Texture _defaultSpecularTexture;

        public static void ApplyDefault(Shader pixelShader, Device device, AssetManager assetManager, ILogger logger)
        {
            if (_defaultDiffuseTexture == null)
            {
                //_defaultDiffuseTexture = assetManager.Load<Texture>("Textures\\T_Default_D");
                //TODO(deccer) this is probably malarky - simplify texture creation
                _defaultDiffuseTexture = new Texture(device, logger, 4, 4, SharpDX.DXGI.Format.R8G8B8A8_UNorm_SRgb, BindFlags.ShaderResource, 1, ResourceOptionFlags.None, "TEX_Cel");
                _defaultDiffuseTexture.SetData(new[] 
                {
                    new Color4(0x44000000), new Color4(0x88000000), new Color4(0xCC000000), new Color4(0xFF000000),
                    new Color4(0x44000000), new Color4(0x88000000), new Color4(0xCC000000), new Color4(0xFF000000),
                    new Color4(0x44000000), new Color4(0x88000000), new Color4(0xCC000000), new Color4(0xFF000000),
                    new Color4(0x44000000), new Color4(0x88000000), new Color4(0xCC000000), new Color4(0xFF000000),
                });
            }
            if (_defaultNormalTexture == null)
            {
                _defaultNormalTexture = assetManager.Load<Texture>("Textures\\T_Default_N");
            }
            if (_defaultSpecularTexture == null)
            {
                _defaultSpecularTexture = assetManager.Load<Texture>("Textures\\T_Default_S");
            }

            pixelShader.SetResource("TEX_Diffuse", _defaultDiffuseTexture);
            pixelShader.SetResource("TEX_Normal", _defaultDiffuseTexture);
        }

        public static Material FromFile(string fileName)
        {
            return null;
        }

        public void Apply(Shader pixelShader, Device device, AssetManager assetManager)
        {
            if (IsDirty)
            {
                ValidateTextures(device, assetManager);
            }

            if (_linearSamplerState == null)
            {
                _linearSamplerState = SamplerStates.CreateLinearClampSamplerState(device);
            }
            pixelShader.SetSamplerState("SMP_Linear", _linearSamplerState);

            if (DiffuseTexture != null)
            {
                pixelShader.SetResource("TEX_Diffuse", DiffuseTexture);
            }
            if (NormalTexture != null)
            {
                pixelShader.SetResource("TEX_Normal", NormalTexture);
            }
            if (SpecularTexture != null)
            {
                pixelShader.SetResource("TEX_Specular", SpecularTexture);
            }
            if (AlphaTexture != null)
            {
                pixelShader.SetResource("TEX_Alpha", AlphaTexture);
            }

            //pixelShader.Parameters["MaterialID"].SetValue(Index);
            //pixelShader.SetResource("Materials", contentManager.GetMaterialLibrarySrv(device));
        }

        public void Dispose()
        {
            _linearSamplerState?.Dispose();
            AlphaTexture?.Dispose();
            DiffuseTexture?.Dispose();
            NormalTexture?.Dispose();
            SpecularTexture?.Dispose();
        }

        private void ValidateTextures(Device device, AssetManager assetManager)
        {
            AlphaTexture?.Dispose();
            if (!string.IsNullOrEmpty(AlphaTextureName))
            {
                AlphaTexture = assetManager.Load<Texture>(AlphaTextureName);
            }

            DiffuseTexture?.Dispose();
            if (!string.IsNullOrEmpty(DiffuseTextureName))
            {
                DiffuseTexture = assetManager.Load<Texture>(DiffuseTextureName);
            }

            NormalTexture?.Dispose();
            if (!string.IsNullOrEmpty(NormalTextureName))
            {
                NormalTexture = assetManager.Load<Texture>(NormalTextureName);
            }

            SpecularTexture?.Dispose();
            if (!string.IsNullOrEmpty(SpecularTextureName))
            {
                SpecularTexture = assetManager.Load<Texture>(SpecularTextureName);
            }
            IsDirty = false;
        }
    }
}