﻿using LessGravity.Quasar.Extensions;
using SharpDX;
using SharpDX.Direct3D11;

namespace LessGravity.Quasar.Graphics
{
    public static class SamplerStates
    {
        public static SamplerState CreateLinearClampSamplerState(Device device)
        {
            var linear = CreateSamplerState(device, Filter.MinMagMipLinear, TextureAddressMode.Clamp, float.MinValue,
                float.MaxValue, 0.0f, 1, Comparison.Always, new Color4(0, 0, 0, 0));
            linear.SetDebugName("SMP MinMagMipLinear Clamp");
            return linear;
        }

        public static SamplerState CreateLinearMirroredSamplerState(Device device)
        {
            var linear = CreateSamplerState(device, Filter.MinMagMipLinear, TextureAddressMode.Mirror, float.MinValue,
                float.MaxValue, 0.0f, 1, Comparison.Always, new Color4(0, 0, 0, 0));
            linear.SetDebugName("SMP MinMagMipLinear Mirror");
            return linear;
        }

        public static SamplerState CreateLinearWrapSamplerState(Device device)
        {
            var wrapLinear = CreateSamplerState(device, Filter.MinMagMipLinear, TextureAddressMode.Wrap, float.MinValue,
                float.MaxValue, 0.0f, 1, Comparison.Always, new Color4(0, 0, 0, 0));
            wrapLinear.SetDebugName("SMP MinMagMipLinear Wrap");
            return wrapLinear;
        }

        public static SamplerState CreateNearestClampSamplerState(Device device)
        {
            var linear = CreateSamplerState(device, Filter.MinMagMipPoint, TextureAddressMode.Clamp, float.MinValue,
                float.MaxValue, 0.0f, 1, Comparison.Always, new Color4(1, 0, 0, 0));
            linear.SetDebugName("SMP MinMagMipPoint Clamp");
            return linear;
        }

        private static SamplerState CreateSamplerState(Device device, Filter filter,
            TextureAddressMode textureAddressMode,
            float minLod, float maxLod, float mipLodBias,
            int maxAnisotropy, Comparison comparison,
            Color4 borderColor)
        {
            var samplerStateDescriptor = new SamplerStateDescription
            {
                Filter = filter,
                AddressU = textureAddressMode,
                AddressV = textureAddressMode,
                AddressW = textureAddressMode,
                MipLodBias = mipLodBias,
                MaximumAnisotropy = maxAnisotropy,
                ComparisonFunction = comparison,
                BorderColor = borderColor,
                MinimumLod = minLod,
                MaximumLod = maxLod
            };

            var samplerState = new SamplerState(device, samplerStateDescriptor)
            {
                DebugName = "SMP " + filter + textureAddressMode + comparison + "_" + minLod + "_" + maxLod + "_" + mipLodBias
            };
            return samplerState;
        }
    }
}