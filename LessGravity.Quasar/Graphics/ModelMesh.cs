﻿using System;
using SharpDX;

namespace LessGravity.Quasar.Graphics
{
    public class ModelMesh : IDisposable
    {
        public BoundingBox BoundingBox { get; internal set; }
        public BoundingSphere BoundingSphere { get; internal set; }
        public IndexBufferBase IndexBuffer { get; internal set; }
        public Material Material { get; set; }
        public Model Model { get; set; }

        public void Dispose()
        {
            IndexBuffer?.Dispose();
        }

        public ModelMesh(Model model, Material material)
        {
            Model = model;
            Material = material;
        }

        public ModelMesh(Model model, Material material, IndexBufferBase indexBuffer)
        {
            Model = model;
            Material = material;
            IndexBuffer = indexBuffer;
        }
    }
}