﻿using SharpDX;
using System.Runtime.InteropServices;

namespace LessGravity.Quasar.Graphics.VertexTypes
{
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct PositionColorTexture : ISpatial
    {
        [VertexMember("POSITION", 0)]
        private Vector3 _position;

        public Vector3 Position
        {
            get => _position;
            set => _position = value;
        }

        [VertexMember("COLOR", 0)]
        public Vector4 Color;

        [VertexMember("TEXCOORD", 0)]
        public Vector2 TextureCoordinate;

        public PositionColorTexture(Vector3 position, Vector4 color, Vector2 textureCoordinate)
        {
            _position = position;
            Color = color;
            TextureCoordinate = textureCoordinate;
        }
    }
}