﻿using SharpDX.DXGI;
using System;

namespace LessGravity.Quasar.Graphics.VertexTypes
{
    [AttributeUsage(AttributeTargets.Field)]
    public sealed class VertexMemberAttribute : Attribute
    {
        public Format Format { get; set; }

        public int SemanticIndex { get; }

        public string SemanticName { get; }

        public int Slot { get; set; }

        public VertexMemberAttribute(string semanticName, int semanticIndex)
        {
            SemanticName = semanticName;
            SemanticIndex = semanticIndex;
        }
    }
}