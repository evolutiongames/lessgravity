﻿using SharpDX;
using System.Runtime.InteropServices;

namespace LessGravity.Quasar.Graphics.VertexTypes
{
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct PositionNormalTangentTexture : ISpatial
    {
        [VertexMember("POSITION", 0)]
        private Vector3 _position;

        public Vector3 Position
        {
            get => _position;
            set => _position = value;
        }

        [VertexMember("NORMAL", 0)]
        public Vector3 Normal;

        [VertexMember("TANGENT", 0)]
        public Vector3 Tangent;

        [VertexMember("TEXCOORD", 0)]
        public Vector2 TextureCoordinate;

        public PositionNormalTangentTexture(Vector3 position, Vector3 normal, Vector3 tangent, Vector2 textureCoordinate)
        {
            _position = position;
            Normal = normal;
            Tangent = tangent;
            TextureCoordinate = textureCoordinate;
        }
    }
}