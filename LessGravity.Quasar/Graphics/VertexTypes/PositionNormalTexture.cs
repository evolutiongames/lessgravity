﻿using SharpDX;
using System.Runtime.InteropServices;

namespace LessGravity.Quasar.Graphics.VertexTypes
{
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct PositionNormalTexture : ISpatial
    {
        [VertexMember("POSITION", 0)]
        private Vector3 _position;

        public Vector3 Position
        {
            get => _position;
            set => _position = value;
        }

        [VertexMember("NORMAL", 0)]
        public Vector3 Normal;

        [VertexMember("TEXCOORD", 0)]
        public Vector2 TextureCoordinate;

        public PositionNormalTexture(Vector3 position, Vector3 normal, Vector2 textureCoordinate)
        {
            _position = position;
            Normal = normal;
            TextureCoordinate = textureCoordinate;
        }
    }
}