﻿using SharpDX;
using System.Runtime.InteropServices;

namespace LessGravity.Quasar.Graphics.VertexTypes
{
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct PositionColor
    {
        [VertexMember("POSITION", 0)]
        private Vector3 _position;

        public Vector3 Position
        {
            get { return _position; }
            set { _position = value; }
        }

        [VertexMember("COLOR", 0)]
        public Color4 Color;

        public PositionColor(Vector3 position, Color4 color)
        {
            _position = position;
            Color = color;
        }
    }
}