﻿using LessGravity.Quasar.Extensions;
using SharpDX;
using SharpDX.D3DCompiler;
using SharpDX.Direct3D;
using SharpDX.Direct3D11;
using System;
using System.Runtime.InteropServices;
using Buffer = SharpDX.Direct3D11.Buffer;

namespace LessGravity.Quasar.Graphics
{
    public abstract class TypedBufferBase : IDisposable
    {
        private Buffer _buffer;
        private VertexBufferBinding _bufferBinding;
        private ShaderResourceView _srv;

        public BindFlags BindFlags { get; }
        protected abstract int Capacity { get; }
        public bool Changed { get; private set; }
        public int Count { get; protected set; }
        public bool IsStructured { get; }
        public PrimitiveTopology Topology { get; }

        private Buffer CreateBuffer(Device device)
        {
            var bufferDescription = new BufferDescription
            {
                BindFlags = BindFlags,
                SizeInBytes = Capacity * GetVertexStride(),
                CpuAccessFlags = CpuAccessFlags.None,
                OptionFlags = IsStructured ? ResourceOptionFlags.BufferStructured : ResourceOptionFlags.None,
                Usage = ResourceUsage.Default,
                StructureByteStride = IsStructured ? GetVertexStride() : 0
            };

            var elements = GetElements();
            if (elements.Length == 0)
            {
                throw new TypedBufferException("No Elements provided. Empty Buffers are not supported.");
            }
            var elementsPtr = GCHandle.Alloc(elements, GCHandleType.Pinned);
            _buffer = new Buffer(device, elementsPtr.AddrOfPinnedObject(), bufferDescription);
            elementsPtr.Free();

            _bufferBinding = new VertexBufferBinding(_buffer, GetVertexStride(), 0);
            Changed = false;

            return _buffer;
        }

        public virtual void Dispose()
        {
            _buffer.DisposeIfNotDisposed();
        }

        public Buffer GetBuffer(Device device)
        {
            if (_buffer == null)
            {
                _buffer = CreateBuffer(device);
            }
            return _buffer;
        }

        protected abstract Array GetElements();

        public abstract InputLayout GetInputLayout(Device device, ShaderBytecode bytecode);

        public VertexBufferBinding GetVertexBufferBinding(Device device)
        {
            if (_buffer == null)
            {
                _buffer = CreateBuffer(device);
            }
            return _bufferBinding;
        }

        public ShaderResourceView GetSrv(Device device)
        {
            if (!IsStructured)
            {
                throw new TypedBufferException("Buffer needs to be Structured");
            }
            if (_srv == null)
            {
                var srvDescription = new ShaderResourceViewDescription
                {
                    Format = SharpDX.DXGI.Format.Unknown,
                    Dimension = ShaderResourceViewDimension.Buffer,
                    Buffer = new ShaderResourceViewDescription.BufferResource
                    {
                        FirstElement = 0,
                        ElementOffset = 0
                    },
                    BufferEx = new ShaderResourceViewDescription.ExtendedBufferResource
                    {
                        ElementCount = Capacity,
                        FirstElement = 0
                    }
                };
                _srv = new ShaderResourceView(device, _buffer, srvDescription);
            }
            return _srv;
        }

        protected abstract int GetVertexStride();

        public abstract bool RayCastIntersect(Ray ray, IndexBufferBase indexContainer, out Vector3? hitPosition, out Vector3? hitNormal);

        protected TypedBufferBase(PrimitiveTopology topology, BindFlags bindFlags, bool isStructured)
        {
            Topology = topology;
            BindFlags = bindFlags;
            IsStructured = isStructured;
        }

        private void UpdateBuffer(DeviceContext deviceContext)
        {
            if (!Changed)
            {
                return;
            }

            var elementsPtr = GCHandle.Alloc(GetElements(), GCHandleType.Pinned);
            deviceContext.UpdateSubresource(new DataBox(elementsPtr.AddrOfPinnedObject(), Count * GetVertexStride(), 0), _buffer);
            elementsPtr.Free();
            Changed = false;
        }
    }
}