﻿using LessGravity.Quasar.Extensions;
using SharpDX.Direct3D11;

namespace LessGravity.Quasar.Graphics
{
    public static class DepthStencilStates
    {
        public static DepthStencilState CreateDefaultDepthStencilState(Device device)
        {
            var @default = CreateDepthStencilState(device, true, DepthWriteMask.All, Comparison.Less, false,
                0xff,
                0xff,
                Comparison.Always, StencilOperation.Keep, StencilOperation.Keep,
                StencilOperation.Keep, Comparison.Always,
                StencilOperation.Keep, StencilOperation.Keep, StencilOperation.Keep);
            @default.SetDebugName("DST Default");
            return @default;
        }

        public static DepthStencilState CreateGreaterNoWrite(Device device)
        {
            var greaterNoWrite = CreateDepthStencilState(device, true, DepthWriteMask.Zero, Comparison.Greater, false,
                0xff,
                0xff,
                Comparison.Always, StencilOperation.Keep,
                StencilOperation.Keep,
                StencilOperation.Keep, Comparison.Always,
                StencilOperation.Keep,
                StencilOperation.Keep, StencilOperation.Keep);
            greaterNoWrite.SetDebugName("DST GreaterNoWrite");
            return greaterNoWrite;
        }

        private static DepthStencilState CreateDepthStencilState(Device device, bool depthEnable,
            DepthWriteMask depthWriteMask,
            Comparison depthFunc,
            bool stencilEnable,
            byte stencilReadMask,
            byte stencilWriteMask,
            Comparison backFaceStencilFunc,
            StencilOperation backFaceStencilPassOp,
            StencilOperation backFaceStencilFailOp,
            StencilOperation backFaceStencilDepthFailOp,
            Comparison frontFaceStencilFunc,
            StencilOperation frontFaceStencilPassOp,
            StencilOperation frontFaceStencilFailOp,
            StencilOperation frontFaceStencilDepthFailOp)
        {
            var depthStencilDescriptor = new DepthStencilStateDescription
            {
                IsDepthEnabled = depthEnable,
                DepthComparison = depthFunc,
                DepthWriteMask = depthWriteMask,
                StencilReadMask = stencilReadMask,
                StencilWriteMask = stencilWriteMask,
                IsStencilEnabled = stencilEnable,
                BackFace = new DepthStencilOperationDescription
                {
                    Comparison = backFaceStencilFunc,
                    PassOperation = backFaceStencilPassOp,
                    FailOperation = backFaceStencilFailOp,
                    DepthFailOperation = backFaceStencilDepthFailOp
                },
                FrontFace = new DepthStencilOperationDescription
                {
                    Comparison = frontFaceStencilFunc,
                    PassOperation = frontFaceStencilPassOp,
                    FailOperation = frontFaceStencilFailOp,
                    DepthFailOperation = frontFaceStencilDepthFailOp
                }
            };
            var depthStencilState = new DepthStencilState(device, depthStencilDescriptor);
            return depthStencilState;
        }
    }

}