﻿using LessGravity.Quasar.Extensions;
using SharpDX.Direct3D11;

namespace LessGravity.Quasar.Graphics
{
    public static class RasterizerStates
    {
        public static RasterizerState CreateCullCWRasterizerState(Device device)
        {
            var cullClockWise = CreateRasterizerState(device, false, CullMode.Back, 0, 0.0f, true, FillMode.Solid, false, false, false, 0.0f);
            cullClockWise.SetDebugName("RST CullClockWise [CullMode.Back][DepthClip true][FillMode.Wireframe][CCW false][MS false][Scissor false]");
            return cullClockWise;
        }

        public static RasterizerState CreateCullCCWRasterizerState(Device device)
        {
            var cullCounterClockwise = CreateRasterizerState(device, false, CullMode.Back, 0, 0.0f, true, FillMode.Solid, true, false, false, 0.0f);
            cullCounterClockwise.SetDebugName("RST CullClockWise [CullMode.Back][DepthClip true][FillMode.Solid][CCW true][MS false][Scissor false]");
            return cullCounterClockwise;
        }

        public static RasterizerState CreateNoCullWireFrameRasterizerState(Device device)
        {
            var noCullWireFrame = CreateRasterizerState(device, false, CullMode.None, 0, 0.0f, true, FillMode.Wireframe, true, false, false, 0.0f);
            noCullWireFrame.SetDebugName("RST NoCullWireFrame [CullMode.None][DepthClip true][FillMode.Wireframe][CCW true][MS false][Scissor false]");
            return noCullWireFrame;

        }

        public static RasterizerState CreateNoCullRasterizerState(Device device)
        {
            var noCull = CreateRasterizerState(device, false, CullMode.None, 0, 0.0f, true, FillMode.Solid, true, false, false, 0.0f);
            noCull.SetDebugName("RST NoCull [CullMode.None][DepthClip true][FillMode.Solid][CCW true][MS false][Scissor false]");
            return noCull;
        }

        public static RasterizerState CreateLightRasterizerState(Device device)
        {
            var lightRasterizerState = CreateRasterizerState(device, false, CullMode.None, 0, 0.0f, false, FillMode.Solid, true, false, false, 0.0f);
            lightRasterizerState.SetDebugName("RST Light [CullMode.None][DepthClip false][FillMode.Solid][CCW true][MS false][Scissor false]");
            return lightRasterizerState;
        }

        private static RasterizerState CreateRasterizerState(Device device, bool antialiasedLineEnable,
            CullMode cullMode, int depthBias,
            float depthBiasClamp,
            bool depthClipEnable,
            FillMode fillMode,
            bool frontCounterClockWise,
            bool multiSampleEnable,
            bool scissorEnable,
            float slopeScaledDepthBias)
        {
            var rasterizerStateDescriptor = new RasterizerStateDescription
            {
                IsAntialiasedLineEnabled = antialiasedLineEnable,
                CullMode = cullMode,
                DepthBias = depthBias,
                DepthBiasClamp = depthBiasClamp,
                IsDepthClipEnabled = depthClipEnable,
                FillMode = fillMode,
                IsFrontCounterClockwise = frontCounterClockWise,
                IsMultisampleEnabled = multiSampleEnable,
                IsScissorEnabled = scissorEnable,
                SlopeScaledDepthBias = slopeScaledDepthBias
            };
            var rasterizerState = new RasterizerState(device, rasterizerStateDescriptor);
            return rasterizerState;
        }
    }
}