﻿using LessGravity.Quasar.Graphics.VertexTypes;
using SharpDX;
using SharpDX.Direct3D;
using System;
using System.Collections.Generic;

namespace LessGravity.Quasar.Graphics
{
    public class Model : IDisposable
    {
        public static readonly Vector2 VectorU0V0 = new Vector2(0, 0);
        public static readonly Vector2 VectorU1V0 = new Vector2(1, 0);
        public static readonly Vector2 VectorU0V1 = new Vector2(0, 1);
        public static readonly Vector2 VectorU1V1 = new Vector2(1, 1);

        private static readonly Vector3 VectorTFL = new Vector3(-1, +1, +1); // top front left
        private static readonly Vector3 VectorTFR = new Vector3(+1, +1, +1); // top front right
        private static readonly Vector3 VectorTBL = new Vector3(-1, +1, -1); // top back left
        private static readonly Vector3 VectorTBR = new Vector3(+1, +1, -1); // top back right
        private static readonly Vector3 VectorBFL = new Vector3(-1, -1, +1); // bottom front left
        private static readonly Vector3 VectorBFR = new Vector3(+1, -1, +1); // bottom front right
        private static readonly Vector3 VectorBBL = new Vector3(-1, -1, -1); // bottom back left
        private static readonly Vector3 VectorBBR = new Vector3(+1, -1, -1); // bottom back right

        public List<ModelMesh> Meshes { get; }
        public string Name { get; }
        public TypedBufferBase VertexBuffer { get; protected set; }

        public static Model CreateCube(float width, float height, float depth)
        {
            var vertices = new List<PositionNormalTangentTexture>(24);
            var indices = new List<ushort>(36);

            float hw = width / 2, hh = height / 2, hd = depth / 2;

            //
            // front
            //
            var color = Color.Blue.ToColor4();
            var normal = Vector3.UnitZ;
            var tangent = -Vector3.UnitX;

            vertices.Add(new PositionNormalTangentTexture(new Vector3(-hw, +hh, +hd), normal, tangent, VectorU0V0));
            vertices.Add(new PositionNormalTangentTexture(new Vector3(+hw, +hh, +hd), normal, tangent, VectorU1V0));
            vertices.Add(new PositionNormalTangentTexture(new Vector3(-hw, -hh, +hd), normal, tangent, VectorU0V1));
            vertices.Add(new PositionNormalTangentTexture(new Vector3(+hw, -hh, +hd), normal, tangent, VectorU1V1));

            indices.AddRange(new ushort[] { 1, 3, 0, 3, 2, 0 });

            //
            // back
            //
            color = Color.DarkBlue.ToColor4();
            normal = -Vector3.UnitZ;
            tangent = Vector3.UnitX;

            /*
            vertices.Add(new PositionNormalTangentTexture(new Vector3(+hw, +hh, -hd), normal, tangent, VectorU0V0));
            vertices.Add(new PositionNormalTangentTexture(new Vector3(-hw, +hh, -hd), normal, tangent, VectorU1V0));
            vertices.Add(new PositionNormalTangentTexture(new Vector3(+hw, -hh, -hd), normal, tangent, VectorU0V1));
            vertices.Add(new PositionNormalTangentTexture(new Vector3(-hw, -hh, -hd), normal, tangent, VectorU1V1));
             * */
            vertices.Add(new PositionNormalTangentTexture(new Vector3(-hw, +hh, -hd), normal, tangent, VectorU0V0));
            vertices.Add(new PositionNormalTangentTexture(new Vector3(+hw, +hh, -hd), normal, tangent, VectorU1V0));
            vertices.Add(new PositionNormalTangentTexture(new Vector3(-hw, -hh, -hd), normal, tangent, VectorU0V1));
            vertices.Add(new PositionNormalTangentTexture(new Vector3(+hw, -hh, -hd), normal, tangent, VectorU1V1));

            const ushort backOffset = 4;
            //indices.AddRange(new ushort[] { backOffset + 0, backOffset + 1, backOffset + 2, backOffset + 1, backOffset + 3, backOffset + 2 });
            indices.AddRange(new ushort[] { backOffset + 0, backOffset + 2, backOffset + 1, backOffset + 1, backOffset + 2, backOffset + 3 });

            //
            // left
            //
            color = Color.DarkRed.ToColor4();
            normal = -Vector3.UnitX;
            tangent = Vector3.UnitZ;

            vertices.Add(new PositionNormalTangentTexture(new Vector3(-hw, +hh, -hd), normal, tangent, VectorU1V0));
            vertices.Add(new PositionNormalTangentTexture(new Vector3(-hw, +hh, +hd), normal, tangent, VectorU0V0));
            vertices.Add(new PositionNormalTangentTexture(new Vector3(-hw, -hh, -hd), normal, tangent, VectorU1V1));
            vertices.Add(new PositionNormalTangentTexture(new Vector3(-hw, -hh, +hd), normal, tangent, VectorU0V1));
            const ushort leftOffset = 8;
            indices.AddRange(new ushort[] { leftOffset + 0, leftOffset + 1, leftOffset + 3, leftOffset + 0, leftOffset + 3, leftOffset + 2 });

            //
            // right
            //
            color = Color.Red.ToColor4();
            normal = Vector3.UnitX;
            tangent = Vector3.UnitZ;

            vertices.Add(new PositionNormalTangentTexture(new Vector3(+hw, +hh, +hd), normal, tangent, VectorU0V0));
            vertices.Add(new PositionNormalTangentTexture(new Vector3(+hw, +hh, -hd), normal, tangent, VectorU1V0));
            vertices.Add(new PositionNormalTangentTexture(new Vector3(+hw, -hh, +hd), normal, tangent, VectorU0V1));
            vertices.Add(new PositionNormalTangentTexture(new Vector3(+hw, -hh, -hd), normal, tangent, VectorU1V1));
            const ushort rightOffset = 12;
            indices.AddRange(new ushort[] { rightOffset + 0, rightOffset + 1, rightOffset + 2, rightOffset + 2, rightOffset + 1, rightOffset + 3 });

            //
            // top
            //
            color = Color.Green.ToColor4();
            normal = Vector3.UnitY;
            tangent = Vector3.UnitX;

            vertices.Add(new PositionNormalTangentTexture(new Vector3(-hw, +hh, +hd), normal, tangent, VectorU0V1));
            vertices.Add(new PositionNormalTangentTexture(new Vector3(+hw, +hh, +hd), normal, tangent, VectorU1V1));
            vertices.Add(new PositionNormalTangentTexture(new Vector3(-hw, +hh, -hd), normal, tangent, VectorU0V0));
            vertices.Add(new PositionNormalTangentTexture(new Vector3(+hw, +hh, -hd), normal, tangent, VectorU1V0));
            const ushort topOffset = 16;
            indices.AddRange(new ushort[] { topOffset + 2, topOffset + 1, topOffset + 0, topOffset + 2, topOffset + 3, topOffset + 1 });

            //
            // bottom
            //
            color = Color.DarkGreen.ToColor4();
            normal = -Vector3.UnitY;
            tangent = Vector3.UnitX;

            vertices.Add(new PositionNormalTangentTexture(new Vector3(+hw, -hh, +hd), normal, tangent, VectorU1V0));
            vertices.Add(new PositionNormalTangentTexture(new Vector3(-hw, -hh, +hd), normal, tangent, VectorU0V0));
            vertices.Add(new PositionNormalTangentTexture(new Vector3(+hw, -hh, -hd), normal, tangent, VectorU1V1));
            vertices.Add(new PositionNormalTangentTexture(new Vector3(-hw, -hh, -hd), normal, tangent, VectorU0V1));

            const ushort bottomOffset = 20;
            indices.AddRange(new ushort[] { bottomOffset + 0, bottomOffset + 2, bottomOffset + 1, bottomOffset + 1, bottomOffset + 2, bottomOffset + 3 });

            var vertexBuffer = new TypedBuffer<PositionNormalTangentTexture>(vertices, PrimitiveTopology.TriangleList);
            var indexBuffer = new UInt16IndexBuffer(indices);

            var cubeMaterial = new Material
            {
                AlphaTextureName = string.Empty,
                DiffuseTextureName = "Textures\\T_Default_D",
                NormalTextureName = "Textures\\T_Default_N",
                DiffuseColor = Color.Green,
                AmbientColor = Color.DarkGreen,
            };

            return CreateFromTypedBuffer($"Cube_{width}x{height}x{depth}", vertexBuffer, indexBuffer, null);
        }

        public static Model CreateFromTypedBuffer(string name, TypedBufferBase vertexBuffer, IndexBufferBase indexBuffer, Material material)
        {
            var result = new Model(name)
            {
                VertexBuffer = vertexBuffer
            };
            result.Meshes.Add(new ModelMesh(result, material) { IndexBuffer = indexBuffer });
            return result;
        }

        public virtual void Dispose()
        {
            if (VertexBuffer != null)
            {
                VertexBuffer.Dispose();
                VertexBuffer = null;
            }
            if (Meshes == null)
            {
                return;
            }
            Meshes.ForEach(mesh => mesh.Dispose());
            Meshes.Clear();
        }

        public Model(string name)
        {
            Name = name;
            Meshes = new List<ModelMesh>();
        }

        public Model(string name, TypedBufferBase vertexBuffer)
            : this(name)
        {
            VertexBuffer = vertexBuffer;
        }
    }
}