﻿using LessGravity.Quasar.Assets;
using LessGravity.Quasar.Graphics.VertexTypes;
using SharpDX;
using SharpDX.Direct3D;
using SharpDX.Direct3D11;
using System;
using System.Collections.Generic;
using System.Linq;

namespace LessGravity.Quasar.Graphics.Blocks
{
    public class BlockMesh : Model
    {
        public Matrix WorldMatrix { get; set; }

        private Dictionary<string, Dictionary<BlockFace, Point>> _blockTextures;
        private readonly List<BlockInfo> _blocks;

        private List<PositionNormalTangentTexture> _vertices;
        private List<ushort> _indices;
        private IndexBufferBase _indexBuffer;
        private int _tileSize;

        private readonly AssetManager _assetManager;
        private readonly Device _device;
        private string _atlasAssetName;

        private int _textureTotalHeight;
        private int _textureTotalWidth;
        private float _textureHeight;
        private float _textureWidth;
        private float _textureUvHeight;
        private float _textureUvWidth;

        public int BlockCount { get; private set; }

        public BoundingBox BoundingBox { get; private set; }

        public BlockMesh(Device device, AssetManager assetManager, string atlasAssetName, int tileSize)
            : base("BlockMesh")
        {
            _device = device;
            _assetManager = assetManager;
            _atlasAssetName = atlasAssetName;
            _blocks = new List<BlockInfo>(64);
            _blockTextures = new Dictionary<string, Dictionary<BlockFace, Point>>();
            _tileSize = tileSize;

            WorldMatrix = Matrix.Identity;
        }

        public void AddBlock(int x, int y, int z, string texture)
        {
            AddBlock(new BlockPosition(x, y, z), texture);
        }

        public void AddBlock(BlockPosition position, string texture)
        {
            _blocks.Add(new BlockInfo
            {
                Position = position,
                Block = new Block { Type = BlockType.Solid },
                TextureName = texture
            });

            RebuildBoundingBox();

            BlockCount += 1;
        }

        public override void Dispose()
        {
            _indexBuffer.Dispose();
            base.Dispose();
        }

        public BlockInfo? GetBlock(Vector3 position)
        {
            return _blocks.FirstOrDefault(blockInfo => blockInfo.Position.Equals(position));
        }

        private void RebuildBoundingBox()
        {
            BoundingBox = new BoundingBox();
            foreach (var block in _blocks)
            {
                BoundingBox = BoundingBox.Merge(BoundingBox, new BoundingBox(block.Position.ToVector3(), (block.Position + BlockPosition.One).ToVector3()));
            }
        }

        public void RemoveBlock(BlockPosition position)
        {
            var blockToRemove = _blocks.FirstOrDefault(blockInfo => blockInfo.Position.Equals(position));
            {
                if (_blocks.Remove(blockToRemove))
                {
                    RebuildBoundingBox();
                    BlockCount -= 1;
                }
            }
        }

        public void BuildMesh(Dictionary<string, Dictionary<BlockFace, Point>> blockTextures)
        {
            _blockTextures = blockTextures;

            _vertices = new List<PositionNormalTangentTexture>(32);
            _indices = new List<ushort>(64);

            var blockCount = _blocks.Count;
            for (var blockIndex = 0; blockIndex < blockCount; blockIndex++)
            {
                var blockInfo = _blocks[blockIndex];
                BuildMeshVertices(blockInfo.Position, _blockTextures[blockInfo.TextureName]);
            }

            Material blockMeshMaterial = null;
            try
            {
                blockMeshMaterial = _assetManager.Load<Material>("MAT_BlockMesh");
            }
            catch { }

            if (blockMeshMaterial == null)
            {
                blockMeshMaterial = new Material
                {
                    Name = "MAT_BlockMesh",
                    DiffuseTextureName = _atlasAssetName + "_D",
                    NormalTextureName = _atlasAssetName + "_N",
                    SpecularTextureName = _atlasAssetName + "_S",
                    IsDirty = true
                };
                _assetManager.Add(blockMeshMaterial, blockMeshMaterial.Name);
            }

            /*
            _textureTotalWidth = 128; //TODO(deccer) get the width of the atlas somehow
            _textureTotalHeight = 128; //TODO(deccer) get the height of the atlas somehow
            _textureWidth = _tileSize;
            _textureHeight = _tileSize;
            _textureUvHeight = 1.0f / (_textureTotalHeight / _textureHeight);
            _textureUvWidth = 1.0f / (_textureTotalWidth / _textureWidth);
            */

            /*
            int[] vertexRemap;
            var a = _vertices.Select(v => v.Position).ToList();
            var numberOfMergedVertices = GeometryHelper.MergeDuplicatePositions(a, 0.01f, out vertexRemap);
            if (numberOfMergedVertices > 0)
            {
                    int numberOfIndices = _indices.Count;
                    for (int i = 0; i < numberOfIndices; i++)
                        _indices[i] = (ushort)vertexRemap[_indices[i]];
            }
            */
            VertexBuffer = new TypedBuffer<PositionNormalTangentTexture>(_vertices, PrimitiveTopology.TriangleList);
            _indexBuffer = new UInt16IndexBuffer(_indices);

            foreach (var modelMesh in Meshes)
            {
                modelMesh.Dispose();
            }
            Meshes.Clear();
            Meshes.Add(new ModelMesh(this, blockMeshMaterial) { IndexBuffer = _indexBuffer });
        }

        private void BuildMeshFaces(BlockFace blockFace, BlockPosition blockPosition, Point textureIndex)
        {
            const float height = 1.0f;

            Action<ushort[]> addIndex = indexList =>
            {
                var vertexOffset = (ushort)(_vertices.Count - 4);
                _indices.AddRange(indexList.Select(i => (ushort)(i + vertexOffset)));
            };

            Action<Vector3, Vector2, Vector3, Vector3> addVertex = (normal, texCoord, tangent, size) =>
            {
                var vertex = new PositionNormalTangentTexture
                {
                    Position = blockPosition.ToVector3() + size,
                    Normal = normal,
                    TextureCoordinate = texCoord,
                    Tangent = tangent
                };
                _vertices.Add(vertex);
            };

            Vector3 faceNormal;
            Vector3 faceTangent;

            _textureTotalWidth = 512; //TODO(deccer) get the width of the atlas somehow
            _textureTotalHeight = 512; //TODO(deccer) get the height of the atlas somehow
            _textureWidth = _tileSize;
            _textureHeight = _tileSize;
            _textureUvHeight = 1.0f / (_textureTotalHeight / _textureHeight);
            _textureUvWidth = 1.0f / (_textureTotalWidth / _textureWidth);


            var u0v0 = new Vector2(textureIndex.X * _textureUvWidth, textureIndex.Y * _textureUvHeight);
            var u0v1 = new Vector2(textureIndex.X * _textureUvWidth, textureIndex.Y * _textureUvHeight + _textureUvHeight);
            var u1v0 = new Vector2(textureIndex.X * _textureUvWidth + _textureUvWidth, textureIndex.Y * _textureUvHeight);
            var u1v1 = new Vector2(textureIndex.X * _textureUvWidth + _textureUvWidth, textureIndex.Y * _textureUvHeight + _textureUvHeight);

            switch (blockFace)
            {
                case BlockFace.Front:
                    {
                        faceNormal = Vector3.UnitZ;
                        faceTangent = -Vector3.UnitX;
                        addVertex(faceNormal, u1v0, faceTangent, new Vector3(0, height, 1));
                        addVertex(faceNormal, u0v0, faceTangent, new Vector3(1, height, 1));
                        addVertex(faceNormal, u1v1, faceTangent, new Vector3(0, 0, 1));
                        addVertex(faceNormal, u0v1, faceTangent, new Vector3(1, 0, 1));
                        addIndex(new ushort[] { 1, 3, 0, 3, 2, 0 });
                    }
                    break;
                case BlockFace.Back:
                    {
                        faceNormal = -Vector3.UnitZ;
                        faceTangent = Vector3.UnitX;
                        addVertex(faceNormal, u1v0, faceTangent, new Vector3(1, height, 0));
                        addVertex(faceNormal, u0v0, faceTangent, new Vector3(0, height, 0));
                        addVertex(faceNormal, u1v1, faceTangent, new Vector3(1, 0, 0));
                        addVertex(faceNormal, u0v1, faceTangent, new Vector3(0, 0, 0));
                        addIndex(new ushort[] { 0, 1, 2, 1, 3, 2 });
                    }
                    break;
                case BlockFace.Left:
                    {
                        faceNormal = -Vector3.UnitX;
                        faceTangent = Vector3.UnitZ;
                        addVertex(faceNormal, u1v0, faceTangent, new Vector3(0, height, 0));
                        addVertex(faceNormal, u0v0, faceTangent, new Vector3(0, height, 1));
                        addVertex(faceNormal, u1v1, faceTangent, new Vector3(0, 0, 0));
                        addVertex(faceNormal, u0v1, faceTangent, new Vector3(0, 0, 1));
                        addIndex(new ushort[] { 0, 1, 3, 0, 3, 2 });
                    }
                    break;
                case BlockFace.Right:
                    {
                        faceNormal = Vector3.UnitX;
                        faceTangent = Vector3.UnitZ;
                        addVertex(faceNormal, u1v0, faceTangent, new Vector3(1, height, 1));
                        addVertex(faceNormal, u0v0, faceTangent, new Vector3(1, height, 0));
                        addVertex(faceNormal, u1v1, faceTangent, new Vector3(1, 0, 1));
                        addVertex(faceNormal, u0v1, faceTangent, new Vector3(1, 0, 0));
                        addIndex(new ushort[] { 0, 1, 2, 2, 1, 3 });
                    }
                    break;
                case BlockFace.Top:
                    {
                        faceNormal = Vector3.UnitY;
                        faceTangent = Vector3.UnitX;
                        addVertex(faceNormal, u1v1, faceTangent, new Vector3(0, height, 1));
                        addVertex(faceNormal, u0v1, faceTangent, new Vector3(1, height, 1));
                        addVertex(faceNormal, u1v0, faceTangent, new Vector3(0, height, 0));
                        addVertex(faceNormal, u0v0, faceTangent, new Vector3(1, height, 0));
                        addIndex(new ushort[] { 2, 1, 0, 2, 3, 1 });
                    }
                    break;
                case BlockFace.Bottom:
                    {
                        faceNormal = -Vector3.UnitY;
                        faceTangent = Vector3.UnitX;
                        addVertex(faceNormal, u0v0, faceTangent, new Vector3(1, 0, 1));
                        addVertex(faceNormal, u1v0, faceTangent, new Vector3(0, 0, 1));
                        addVertex(faceNormal, u0v1, faceTangent, new Vector3(1, 0, 0));
                        addVertex(faceNormal, u1v1, faceTangent, new Vector3(0, 0, 0));
                        addIndex(new ushort[] { 0, 2, 1, 1, 2, 3 });
                    }
                    break;
            }
        }

        private Block GetBlock(int x, int y, int z)
        {
            var block = _blocks.FirstOrDefault(blockInfo => blockInfo.Position.Equals(new BlockPosition(x, y, z)));
            return block.Block;
        }

        private void BuildMeshVertices(BlockPosition blockPosition, Dictionary<BlockFace, Point> textureIndices)
        {
            var x = blockPosition.X;
            var y = blockPosition.Y;
            var z = blockPosition.Z;

            var blockTop = GetBlock(x, y + 1, z);
            var blockFront = GetBlock(x, y, z + 1);
            var blockLeft = GetBlock(x - 1, y, z);
            var blockRight = GetBlock(x + 1, y, z);
            var blockBack = GetBlock(x, y, z - 1);
            var blockBottom = GetBlock(x, y - 1, z);

            if (Block.IsTransparentBlock(blockTop.Type))
            {
                BuildMeshFaces(BlockFace.Top, blockPosition, textureIndices[BlockFace.Top]);
            }
            if (Block.IsTransparentBlock(blockBottom.Type))
            {
                BuildMeshFaces(BlockFace.Bottom, blockPosition, textureIndices[BlockFace.Bottom]);
            }
            if (Block.IsTransparentBlock(blockLeft.Type))
            {
                BuildMeshFaces(BlockFace.Left, blockPosition, textureIndices[BlockFace.Left]);
            }
            if (Block.IsTransparentBlock(blockRight.Type))
            {
                BuildMeshFaces(BlockFace.Right, blockPosition, textureIndices[BlockFace.Right]);
            }
            if (Block.IsTransparentBlock(blockBack.Type))
            {
                BuildMeshFaces(BlockFace.Back, blockPosition, textureIndices[BlockFace.Back]);
            }
            if (Block.IsTransparentBlock(blockFront.Type))
            {
                BuildMeshFaces(BlockFace.Front, blockPosition, textureIndices[BlockFace.Front]);
            }
        }

        private bool RayCastIntersect(Ray ray, out Vector3? hitPosition, out Vector3? hitNormal)
        {
            hitPosition = null;
            hitNormal = null;

            var internalHitPosition = Vector3.Zero;
            var boundingSphereHit = BoundingBox.Intersects(ref ray, out internalHitPosition);
            if (boundingSphereHit)
            {
                hitPosition = internalHitPosition;
                return VertexBuffer.RayCastIntersect(ray, _indexBuffer, out hitPosition, out hitNormal);
            }
            return false;
        }

        public bool RayCastIntersect(Ray ray, Matrix worldMatrix, out Vector3? hitPosition, out Vector3? hitNormal)
        {
            hitPosition = null;
            hitNormal = null;

            Matrix invertedWorldMatrix;
            Matrix.Invert(ref worldMatrix, out invertedWorldMatrix);

            var instanceRay = new Ray(Vector3.TransformCoordinate(ray.Position, invertedWorldMatrix),
                                      Vector3.TransformNormal(ray.Direction, invertedWorldMatrix));
            if (BoundingBox.Intersects(ref instanceRay))
            {
                return RayCastIntersect(instanceRay, out hitPosition, out hitNormal);
            }
            return false;
        }

    }
}