﻿namespace LessGravity.Quasar.Graphics.Blocks
{
    public struct Block
    {
        public static bool IsSolidBlock(BlockType blockType)
        {
            return blockType != BlockType.None;
        }

        public static bool IsTransparentBlock(BlockType blockType)
        {
            return blockType == BlockType.None;
        }

        public BlockType Type;

        public override string ToString()
        {
            return Type.ToString();
        }
    }
}