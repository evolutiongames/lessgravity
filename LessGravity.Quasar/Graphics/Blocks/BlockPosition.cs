﻿using System;

namespace LessGravity.Quasar.Graphics.Blocks
{
    public struct  BlockPosition : IEquatable<BlockPosition>
    {
        public static readonly BlockPosition One = new BlockPosition(1, 1, 1);
        public static readonly BlockPosition Zero = new BlockPosition(0, 0, 0);

        public static readonly BlockPosition BlockLeft = new BlockPosition(-1, 0, 0);
        public static readonly BlockPosition BlockRight = new BlockPosition(1, 0, 0);
        public static readonly BlockPosition BlockUp = new BlockPosition(0, 1, 0);
        public static readonly BlockPosition BlockDown = new BlockPosition(0, -1, 0);
        public static readonly BlockPosition BlockFront = new BlockPosition(0, 0, 1);
        public static readonly BlockPosition BlockBack = new BlockPosition(0, 0, -1);

        public int X;
        public int Y;
        public int Z;


        public BlockPosition(int x, int y, int z)
        {
            X = x;
            Y = y;
            Z = z;
        }

        public bool Equals(BlockPosition other)
        {
            return (X == other.X) && (Y == other.Y) && (Z == other.Z);
        }

        public override bool Equals(object obj)
        {
            return Equals((BlockPosition)obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                int hash = (int)2166136261;
                hash = (hash * 16777619) ^ X.GetHashCode();
                hash = (hash * 16777619) ^ Y.GetHashCode();
                hash = (hash * 16777619) ^ Z.GetHashCode();
                return hash;
            }
        }

        public override string ToString()
        {
            return $"X: {X} Y: {Y} Z: {Z}";
        }

        public static BlockPosition operator +(BlockPosition position1, BlockPosition position2)
        {
            var result = new BlockPosition
            {
                X = position1.X + position2.X,
                Y = position1.Y + position2.Y,
                Z = position1.Z + position2.Z,
            };
            return result;
        }

        public static BlockPosition operator -(BlockPosition position1, BlockPosition position2)
        {
            var result = new BlockPosition
            {
                X = position1.X - position2.X,
                Y = position1.Y - position2.Y,
                Z = position1.Z - position2.Z,
            };
            return result;
        }

        public static bool operator ==(BlockPosition position1, BlockPosition position2)
        {
            return (position1.X == position2.X) && (position1.Y == position2.Y) && (position1.Z == position2.Z);
        }

        public static bool operator !=(BlockPosition position1, BlockPosition position2)
        {
            return (position1.X != position2.X) || (position1.Y != position2.Y) || (position1.Z != position2.Z);
        }
    }
}