﻿namespace LessGravity.Quasar.Graphics.Blocks
{
    public enum BlockFace
    {
        Right = 0,
        Left = 1,
        Top = 2,
        Bottom = 3,
        Front = 4,
        Back = 5,
    }
}