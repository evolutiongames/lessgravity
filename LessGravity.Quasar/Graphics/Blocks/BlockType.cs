﻿namespace LessGravity.Quasar.Graphics.Blocks
{
    public enum BlockType : byte
    {
        None = 0,
        Solid,
        Maximum
    }
}