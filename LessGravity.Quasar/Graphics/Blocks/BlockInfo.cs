﻿namespace LessGravity.Quasar.Graphics.Blocks
{
    public struct BlockInfo
    {
        public Block Block;
        public BlockPosition Position;
        public string TextureName;
    }
}