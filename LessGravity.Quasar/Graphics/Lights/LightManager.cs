﻿using System;
using System.Collections.Generic;
using LessGravity.Quasar.Entities;

namespace LessGravity.Quasar.Graphics.Lights
{
    class LightManager : IDisposable
    {
        private readonly List<LightComponent> _lightComponents; 
        private readonly RenderQueue _renderQueue;

        public void AddRange(IEnumerable<LightComponent> lightComponents)
        {
            _lightComponents.AddRange(lightComponents);
        }

        public void Dispose()
        {
            lock (_lightComponents)
            {
                foreach (var lightComponent in _lightComponents)
                {
                    lightComponent.Dispose();
                }
            }
        }

        public LightManager(RenderQueue renderQueue)
        {
            _lightComponents = new List<LightComponent>();
            _renderQueue = renderQueue;
        }
    }
}