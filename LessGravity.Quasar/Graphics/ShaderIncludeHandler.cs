﻿using SharpDX.D3DCompiler;
using System;
using System.IO;
using System.Linq;
using System.Text;

namespace LessGravity.Quasar.Graphics
{
    internal sealed class ShaderIncludeHandler : Include
    {
        private readonly string _rootPath = string.Empty;
        private readonly VirtualShaderFile[] _virtualFiles;

        public IDisposable Shadow { get; set; }

        public ShaderIncludeHandler(string rootPath, VirtualShaderFile[] virtualFiles)
        {
            _rootPath = rootPath;
            _virtualFiles = virtualFiles;
        }

        public void Dispose() { }

        public void Close(Stream stream)
        {
            if (stream != null)
            {
                stream.Close();
                stream.Dispose();
            }
        }

        public Stream Open(IncludeType type, string fileName, Stream parentStream)
        {
            string path;
            if (parentStream != null && parentStream.GetType() == typeof(FileStream))
            {
                path = Path.GetDirectoryName(((FileStream)parentStream).Name);
            }
            else
            {
                path = _rootPath;
            }

            VirtualShaderFile virtualFile = null;
            if (_virtualFiles != null)
            {
                virtualFile = _virtualFiles.FirstOrDefault(vf => vf.Name.ToLower() == fileName.ToLower());
            }

            if (virtualFile == null)
            {
                return File.OpenRead(Path.Combine(path, fileName));
            }
            return new MemoryStream(Encoding.ASCII.GetBytes(virtualFile.Text));
        }
    }
}