﻿using SharpDX;

namespace LessGravity.Quasar.Graphics
{
    public class ModelInstance : ModelInstanceBase
    {
        private Matrix _worldMatrix;

        public override Matrix WorldMatrix
        {
            get => _worldMatrix;
            set => _worldMatrix = value;
        }

        public ModelInstance(Model model, Matrix initialMatrix)
            : base(model)
        {
            _worldMatrix = initialMatrix;

        }

        public void AddVisibleMeshes(RenderQueue renderQueue)
        {
            for (var meshIndex = 0; meshIndex < Model.Meshes.Count; ++meshIndex)
            {
                var mesh = Model.Meshes[meshIndex];
                //TODO(deccer): Implement proper Culling / Review Current Implementation
                {
                    renderQueue.AddMesh(this, mesh);
                }
            }
        }
    }
}