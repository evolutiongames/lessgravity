﻿using LessGravity.Logging;
using SharpDX.D3DCompiler;
using SharpDX.Direct3D11;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace LessGravity.Quasar.Graphics
{
    public class ConstantBufferManager : IEnumerable<ConstantBuffer>, IDisposable
    {
        private static readonly ConstantBufferParameter _dummyParameter = new ConstantBufferParameter();

        private readonly IDictionary<string, ConstantBufferParameter> _parameters = new Dictionary<string, ConstantBufferParameter>();
        private readonly ILogger _logger;
        private readonly Device _device;
        private readonly string _shaderFileName;


        private ConstantBuffer[] _constantBuffers;

        public ConstantBufferManager(Device device, Shader shader, ILogger logger)
        {
            _device = device;
            _shaderFileName = shader.Name;
            _logger = logger;
        }

#if DEBUG
        private readonly HashSet<string> _warnedOnNotFound = new HashSet<string>();
#endif

        public ConstantBufferParameter this[string paramName]
        {
            get
            {
                ConstantBufferParameter variable;

                if (!_parameters.TryGetValue(paramName, out variable))
                {
#if DEBUG
                    if (!_warnedOnNotFound.Contains(paramName))
                    {
                        _logger.Warn($"Trying to set variable '{paramName}' in shader {Path.GetFileName(_shaderFileName)} which is not defined", "Shader");
                        _warnedOnNotFound.Add(paramName);
                    }
#endif
                    return _dummyParameter;
                }
                return variable;
            }
        }

        public void Dispose()
        {
            _parameters.Clear();
            if (_constantBuffers == null)
            {
                return;
            }
            foreach (var constantBuffer in _constantBuffers)
            {
                constantBuffer.Dispose();
            }
            _constantBuffers = null;
        }

        public IEnumerator<ConstantBuffer> GetEnumerator()
        {
            return _constantBuffers.AsEnumerable().GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public bool HasParameter(string name)
        {
            return _parameters.Any(p => p.Key == name);
        }

        internal void LoadConstantBuffers(ShaderReflection reflection)
        {
            var buffers = new List<ConstantBuffer>();
            for (var i = 0; i < reflection.Description.ConstantBuffers; ++i)
            {
                var reflectedBuffer = reflection.GetConstantBuffer(i);
                if (reflectedBuffer.Description.Type != ConstantBufferType.ConstantBuffer)
                {
                    continue;
                }

                var constantBuffer = new ConstantBuffer(_device, reflectedBuffer);
                buffers.Add(constantBuffer);
                foreach (var variable in constantBuffer.Parameters)
                {
                    _parameters.Add(variable.Name, variable);
                }
            }
            _constantBuffers = buffers.ToArray();
        }
    }
}