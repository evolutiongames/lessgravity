﻿using SharpDX.DXGI;
using System;
using System.Collections.Generic;

namespace LessGravity.Quasar.Graphics
{
    public class UInt16IndexBuffer : IndexBuffer<UInt16>
    {
        public UInt16IndexBuffer(int capacity)
            : base(capacity)
        {

        }

        public UInt16IndexBuffer(IEnumerable<UInt16> data, int count = -1)
            : base(data, count)
        {
        }

        public override Format Format => Format.R16_UInt;

        public override int this[int index] => Data[index];
    }
}