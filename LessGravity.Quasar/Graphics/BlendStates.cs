﻿using LessGravity.Quasar.Extensions;
using SharpDX.Direct3D11;

namespace LessGravity.Quasar.Graphics
{
    public static class BlendStates
    {
        public static BlendState CreateAdditiveBlendState(Device device)
        {
            var additive = CreateBlendState(device, true, BlendOption.One, BlendOption.One, BlendOperation.Add,
                BlendOption.One, BlendOption.One, BlendOperation.Add,
                ColorWriteMaskFlags.All);
            additive.SetDebugName("BST Additive");
            return additive;
        }

        public static BlendState CreateAlphaBlendState(Device device)
        {
            var alphaBlend = CreateBlendState(device, true, BlendOption.SourceAlpha, BlendOption.InverseSourceAlpha, BlendOperation.Add,
                BlendOption.Zero, BlendOption.Zero, BlendOperation.Add,
                ColorWriteMaskFlags.All);
            alphaBlend.SetDebugName("BST Additive");
            return alphaBlend;
        }

        public static BlendState CreateOpaqueBlendState(Device device)
        {
            var opaque = CreateBlendState(device, false, BlendOption.Zero, BlendOption.Zero, BlendOperation.Add,
                BlendOption.Zero, BlendOption.Zero, BlendOperation.Add,
                ColorWriteMaskFlags.All);
            opaque.SetDebugName("BST Opaque");
            return opaque;
        }

        private static BlendState CreateBlendState(Device device, bool blendEnabled,
            BlendOption sourceColor,
            BlendOption destColor,
            BlendOperation colorOperation,
            BlendOption sourceAlpha, BlendOption destAlpha,
            BlendOperation alphaOperation,
            ColorWriteMaskFlags renderTargetWriteMask)
        {
            var blendStateDescriptor = new BlendStateDescription
            {
                AlphaToCoverageEnable = false,
                IndependentBlendEnable = false,
            };
            blendStateDescriptor.RenderTarget[0].IsBlendEnabled = blendEnabled;
            blendStateDescriptor.RenderTarget[0].BlendOperation = colorOperation;
            blendStateDescriptor.RenderTarget[0].SourceBlend = sourceColor;
            blendStateDescriptor.RenderTarget[0].SourceAlphaBlend = sourceAlpha;
            blendStateDescriptor.RenderTarget[0].AlphaBlendOperation = alphaOperation;
            blendStateDescriptor.RenderTarget[0].DestinationBlend = destColor;
            blendStateDescriptor.RenderTarget[0].DestinationAlphaBlend = destAlpha;
            blendStateDescriptor.RenderTarget[0].RenderTargetWriteMask = renderTargetWriteMask;

            var blendState = new BlendState(device, blendStateDescriptor);
            return blendState;
        }
    }
}