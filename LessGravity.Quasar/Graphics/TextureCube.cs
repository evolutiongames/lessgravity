﻿using LessGravity.Logging;
using SharpDX.Direct3D11;
using SharpDX.WIC;
using System;
using System.IO;
using System.Linq;

namespace LessGravity.Quasar.Graphics
{
    public class TextureCube : Texture
    {
        protected override Texture2D LoadTexture2D(ImagingFactory2 imagingFactory, string fileName)
        {
            if (imagingFactory == null)
            {
                throw new ArgumentNullException(nameof(imagingFactory));
            }

            if (string.IsNullOrEmpty(fileName))
            {
                throw new ArgumentNullException(nameof(fileName));
            }

            var fileInfo = new FileInfo(fileName);
            var files = Directory.EnumerateFiles(fileInfo.Directory.FullName, fileInfo.Name + "_*", SearchOption.TopDirectoryOnly);

            return Device.CreateTexture2DCubeFromBitmaps(LoadBitmaps(imagingFactory, files.ToArray()));
        }

        internal TextureCube(Device device, ILogger logger, ImagingFactory2 imagingFactory, string fileName, string debugName = "")
            : base(device, logger, imagingFactory, fileName, debugName)
        {

        }
    }
}