﻿using LessGravity.Logging;
using LessGravity.Quasar.Extensions;
using SharpDX;
using SharpDX.D3DCompiler;
using SharpDX.Direct3D;
using SharpDX.Direct3D11;
using System;
using System.Collections.Generic;
using System.IO;
using System.Windows.Forms;

namespace LessGravity.Quasar.Graphics
{
    public class VirtualShaderFile
    {
        public string Name { get; private set; }
        public string Text { get; private set; }

        public VirtualShaderFile(string name, string text)
        {
            Name = name;
            Text = text;
        }
    }

    public class Shader : IDisposable
    {
        private struct ResourceDescriptor
        {
            public int BindPoint;
            public string Name;
        }

        private readonly IDictionary<string, ResourceDescriptor> _resources = new Dictionary<string, ResourceDescriptor>();
#if DEBUG
#if !THROW_ON_NOT_FOUND
        private readonly HashSet<string> _warnedOnNotFound = new HashSet<string>();
#endif
#endif

        private readonly Device _device;
        private readonly DeviceContext _deviceContext;

        private DeviceChild _shaderObject;

        public string FileName { get; private set; }
        public string Name { get; private set; }
        public ConstantBufferManager Parameters { get; }
        public CompilationResult Bytecode { get; private set; }
        public ShaderType Type { get; private set; }

        private static Shader FromBinaryFile(Device device, DeviceContext deviceContext, ILogger logger, string fileName)
        {
            var shader = new Shader(device, deviceContext, logger);
            shader.Name = Path.GetFileName(fileName);
            shader.LoadFromBinary(fileName);
            return shader;
        }

        public static Shader FromFile(Device device, DeviceContext deviceContext, ILogger logger, string fileName)
        {
            var fileExtension = Path.GetExtension(fileName);
            if (".hlsl .cs .ds .hs .vs .ps .gs".Contains(fileExtension))
            {
                return FromTextFile(device, deviceContext, logger, fileName);
            }
            if (fileExtension == ".lgq")
            {
                return FromBinaryFile(device, deviceContext, logger, fileName);
            }
            throw new FileNotFoundException("Unsupported File Extension");
        }

        private static Shader FromTextFile(Device device, DeviceContext deviceContext, ILogger logger, string fileName)
        {
            var shader = new Shader(device, deviceContext, logger);
            shader.FileName = fileName;
            shader.Name = Path.GetFileName(fileName);
            shader.LoadFromText(fileName);
            return shader;
        }

        public void BindBuffers()
        {
            var i = 0;

            foreach (var constantBuffer in Parameters)
            {
                if (constantBuffer.Changed)
                {
                    var db = new DataBox(constantBuffer.Data.DataPointer, 0, 0);
                    _deviceContext.UpdateSubresource(db, constantBuffer.Buffer);
                    constantBuffer.Changed = false;
                }
                switch (Type)
                {
                    case ShaderType.Undefined:
                        throw new Exception("Invalid Shader");
                    case ShaderType.ComputeShader:
                        _deviceContext.ComputeShader.SetConstantBuffer(i, constantBuffer.Buffer);
                        break;
                    case ShaderType.DomainShader:
                        _deviceContext.DomainShader.SetConstantBuffer(i, constantBuffer.Buffer);
                        break;
                    case ShaderType.GeometryShader:
                        _deviceContext.GeometryShader.SetConstantBuffer(i, constantBuffer.Buffer);
                        break;
                    case ShaderType.HullShader:
                        _deviceContext.HullShader.SetConstantBuffer(i, constantBuffer.Buffer);
                        break;
                    case ShaderType.PixelShader:
                        _deviceContext.PixelShader.SetConstantBuffer(i, constantBuffer.Buffer);
                        break;
                    case ShaderType.VertexShader:
                        _deviceContext.VertexShader.SetConstantBuffer(i, constantBuffer.Buffer);
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
                i++;
            }
        }

        private void CreateShaderObject(ShaderType shaderType)
        {
            switch (shaderType)
            {
                case ShaderType.ComputeShader:
                    _shaderObject = new ComputeShader(_device, Bytecode);
                    break;
                case ShaderType.DomainShader:
                    _shaderObject = new DomainShader(_device, Bytecode);
                    break;
                case ShaderType.GeometryShader:
                    _shaderObject = new GeometryShader(_device, Bytecode);
                    break;
                case ShaderType.HullShader:
                    _shaderObject = new HullShader(_device, Bytecode);
                    break;
                case ShaderType.PixelShader:
                    _shaderObject = new PixelShader(_device, Bytecode);
                    break;
                case ShaderType.VertexShader:
                    _shaderObject = new VertexShader(_device, Bytecode);
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        public void Dispose()
        {
            Bytecode?.Dispose();
            _shaderObject.DisposeIfNotDisposed();
        }

        private void LoadFromBinary(string fileName)
        {
            //TODO(deccer): implement Shader.LoadFromBinary
        }
        
        private void LoadFromText(string fileName, ShaderMacro[] defines = null, params VirtualShaderFile[] virtualFiles)
        {
            var shaderFlags = ShaderFlags.PackMatrixRowMajor/* | ShaderFlags.WarningsAreErrors*/;
#if DEBUG
            shaderFlags |= ShaderFlags.Debug | ShaderFlags.SkipOptimization;
#else
            shaderFlags |= ShaderFlags.OptimizationLevel3;
#endif
            string shaderProfile;
            if (fileName.Contains(".cs"))
            {
                Type = ShaderType.ComputeShader;
                shaderProfile = "cs_5_0";
            }
            else if (fileName.Contains(".ds"))
            {
                Type = ShaderType.DomainShader;
                shaderProfile = "ds_5_0";
            }
            else if (fileName.Contains(".gs"))
            {
                Type = ShaderType.GeometryShader;
                shaderProfile = "gs_5_0";
            }
            else if (fileName.Contains(".hs"))
            {
                Type = ShaderType.HullShader;
                shaderProfile = "hs_5_0";
            }
            else if (fileName.Contains(".ps"))
            {
                Type = ShaderType.PixelShader;
                shaderProfile = "ps_5_0";
            }
            else if (fileName.Contains(".vs"))
            {
                Type = ShaderType.VertexShader;
                shaderProfile = "vs_5_0";
            }
            else
            {
                throw new Exception("Invalid ShaderFile, Check FileName extension (only cs, ds, gs, hs, ps and vs are allowed)");
            }
            try
            {
                Configuration.ThrowOnShaderCompileError = true;
                Configuration.EnableObjectTracking = true;
                Bytecode = ShaderBytecode.CompileFromFile(fileName, "Main", shaderProfile, shaderFlags, EffectFlags.None, defines, new ShaderIncludeHandler(Path.GetDirectoryName(fileName), virtualFiles));
                if (!string.IsNullOrEmpty(Bytecode.Message))
                {
                    throw new Exception(Bytecode.Message);
                }
            }
            catch (Exception ex)
            {
                if (MessageBox.Show($"Error while compiling {fileName}.\n\nDetails:\n\n{ex.Message}\n\n", "Compiling Shader", MessageBoxButtons.OK, MessageBoxIcon.Error) == DialogResult.OK)
                {
                    Application.Exit();
                }
            }
            LoadReflectionData(Bytecode);
            CreateShaderObject(Type);
        }

        private void LoadReflectionData(ShaderBytecode bytecode)
        {
            if (bytecode == null)
            {
                throw new ArgumentNullException(nameof(bytecode));
            }
            using (var shaderReflection = new ShaderReflection(bytecode))
            {
                Parameters.LoadConstantBuffers(shaderReflection);

                for (var i = 0; i < shaderReflection.Description.BoundResources; ++i)
                {
                    var boundResource = shaderReflection.GetResourceBindingDescription(i);
                    if (boundResource.Type != ShaderInputType.ConstantBuffer)
                    {
                        var resourceDescriptor = new ResourceDescriptor
                        {
                            Name = boundResource.Name,
                            BindPoint = boundResource.BindPoint
                        };
                        _resources.Add(resourceDescriptor.Name, resourceDescriptor);
                    }
                }
            }
        }

        public void SetResource(string resourceName, ShaderResourceView shaderResourceView)
        {
            ResourceDescriptor resourceDescriptor;
            if (!_resources.TryGetValue(resourceName, out resourceDescriptor))
            {
#if !THROW_ON_NOT_FOUND
#if DEBUG
                if (!_warnedOnNotFound.Contains(resourceName))
                {
                    //Logger.Warn($"Trying to set Resource {resourceName} in Shader {FileName} which is not defined", "Shader");
                    _warnedOnNotFound.Add(resourceName);
                }
#endif
                return;
#else
                throw new Exception($"Resource {resourceName} was not found in shader {Name}");
#endif
            }
            switch (Type)
            {
                case ShaderType.Undefined:
                    throw new Exception("Invalid Shader");
                case ShaderType.ComputeShader:
                    _deviceContext.ComputeShader.SetShaderResource(resourceDescriptor.BindPoint, shaderResourceView);
                    break;
                case ShaderType.DomainShader:
                    _deviceContext.DomainShader.SetShaderResource(resourceDescriptor.BindPoint, shaderResourceView);
                    break;
                case ShaderType.GeometryShader:
                    _deviceContext.GeometryShader.SetShaderResource(resourceDescriptor.BindPoint, shaderResourceView);
                    break;
                case ShaderType.HullShader:
                    _deviceContext.HullShader.SetShaderResource(resourceDescriptor.BindPoint, shaderResourceView);
                    break;
                case ShaderType.PixelShader:
                    _deviceContext.PixelShader.SetShaderResource(resourceDescriptor.BindPoint, shaderResourceView);
                    break;
                case ShaderType.VertexShader:
                    _deviceContext.VertexShader.SetShaderResource(resourceDescriptor.BindPoint, shaderResourceView);
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        public void SetSamplerState(string samplerName, SamplerState samplerState)
        {
            ResourceDescriptor resourceDescriptor;

            if (!_resources.TryGetValue(samplerName, out resourceDescriptor))
            {
#if THROW_ON_NOT_FOUND
                throw new Exception($"Sampler {samplerName} was not found in shader {Name}");
#else
#if DEBUG
                if (!_warnedOnNotFound.Contains(samplerName))
                {
                    //Logger.Warn($"Trying to set Sampler {samplerName} in Shader {Name} which is not defined", "Shader");
                    _warnedOnNotFound.Add(samplerName);
                }
                return;
#endif
#endif
            }
            switch (Type)
            {
                case ShaderType.Undefined:
                    throw new Exception("Invalid Shader");
                case ShaderType.ComputeShader:
                    _deviceContext.ComputeShader.SetSampler(resourceDescriptor.BindPoint, samplerState);
                    break;
                case ShaderType.DomainShader:
                    _deviceContext.DomainShader.SetSampler(resourceDescriptor.BindPoint, samplerState);
                    break;
                case ShaderType.GeometryShader:
                    _deviceContext.GeometryShader.SetSampler(resourceDescriptor.BindPoint, samplerState);
                    break;
                case ShaderType.HullShader:
                    _deviceContext.HullShader.SetSampler(resourceDescriptor.BindPoint, samplerState);
                    break;
                case ShaderType.PixelShader:
                    _deviceContext.PixelShader.SetSampler(resourceDescriptor.BindPoint, samplerState);
                    break;
                case ShaderType.VertexShader:
                    _deviceContext.VertexShader.SetSampler(resourceDescriptor.BindPoint, samplerState);
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        public Shader(Device device, DeviceContext deviceContext, ILogger logger)
        {
            _device = device;
            _deviceContext = deviceContext;
            Parameters = new ConstantBufferManager(_device, this, logger);
        }

        public override string ToString()
        {
            return $"{Type} {Name} {FileName}";
        }

        public static implicit operator VertexShader(Shader shader)
        {
            return (VertexShader)shader._shaderObject;
        }

        public static implicit operator PixelShader(Shader shader)
        {
            return (PixelShader)shader._shaderObject;
        }

        public static implicit operator GeometryShader(Shader shader)
        {
            return (GeometryShader)shader._shaderObject;
        }

        public static implicit operator ComputeShader(Shader shader)
        {
            return (ComputeShader)shader._shaderObject;
        }
    }
}