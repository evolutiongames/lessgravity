﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace LessGravity.Quasar.Graphics
{
    public abstract class IndexBuffer<T> : IndexBufferBase where T : struct, IComparable<T>
    {
        protected T[] Data;

        public void Add(T index)
        {
            if (Count >= Data.Length)
            {
                if (!CanGrow)
                {
                    throw new InvalidOperationException("IndexBuffer cannot Grow");
                }
                Grow();
            }
            if (Buffer != null)
            {
                throw new InvalidOperationException("Can not add indices after the buffer has been created.");
            }
            Data[Count++] = index;
        }

        public void AddRange(IEnumerable<T> indices)
        {
            foreach (var index in indices)
            {
                Add(index);
            }
        }

        protected override Array GetData()
        {
            return Data;
        }

        private void Grow(int minimumSize = -1)
        {
            if (minimumSize == -1)
            {
                if (Data.Length == 0)
                {
                    minimumSize = 1;
                }
                else if (Data.Length < 10)
                {
                    minimumSize = 16;
                }
                else
                {
                    minimumSize = Data.Length + 16;
                }
            }
            Array.Resize(ref Data, minimumSize);
            Invalidate();
        }

        static IndexBuffer()
        {
            if (typeof(T) != typeof(UInt16) && typeof(T) != typeof(UInt32))
            {
                throw new ArgumentException("IndexBuffer Generic Type Argument must be either UInt16 or UInt32");
            }
        }

        protected IndexBuffer(int capacity)
        {
            if (capacity < 0 || capacity > 1048576)
            {
                throw new ArgumentOutOfRangeException(nameof(capacity), capacity, "Capacity must be between 0 and 1MB bytes");
            }
            Data = new T[capacity];
        }

        protected IndexBuffer(IEnumerable<T> data, int count)
        {
            var items = count == -1 ? data : data.Take(count);
            Data = items.ToArray();
            Count = Data.Length;
        }
    }
}