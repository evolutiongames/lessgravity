﻿using LessGravity.Quasar.Extensions;
using LessGravity.Quasar.Graphics.VertexTypes;
using SharpDX;
using SharpDX.D3DCompiler;
using SharpDX.Direct3D;
using SharpDX.Direct3D11;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;

namespace LessGravity.Quasar.Graphics
{
    //TODO(deccer) make this class protected and create derivates for StructuredBuffer<T>, VertexBuffer<T>
    public class TypedBuffer<T> : TypedBufferBase 
        where T : struct
    {
        private static readonly int _vertexStride;

        private T[] _elements;
        private InputLayout _inputLayout;

        protected override int Capacity => _elements.Length;

        static TypedBuffer()
        {
            _vertexStride = Marshal.SizeOf(typeof(T));
        }

        public void Add(T element)
        {
            if (Count >= Capacity)
            {
                Grow();
            }
            _elements[Count++] = element;
        }

        public override void Dispose()
        {
            _inputLayout.DisposeIfNotDisposed();
            base.Dispose();
        }

        public TypedBuffer(int capacity, PrimitiveTopology topology, BindFlags bindFlags = BindFlags.VertexBuffer, bool isStructured = false)
            : base(topology, bindFlags, isStructured)
        {
            _elements = new T[capacity];
            Count = 0;
        }

        public TypedBuffer(IEnumerable<T> elements, PrimitiveTopology topology, BindFlags bindFlags = BindFlags.VertexBuffer, bool isStructured = false)
            : base(topology, bindFlags, isStructured)
        {
            _elements = elements.ToArray();
            Count = _elements.Length;
        }

        private static InputElement[] GetInputElements(Type type)
        {
            if (!type.IsValueType)
            {
                throw new TypedBufferException($"Vertex Type '{type.Name}' must be a value type");
            }
            if (type.StructLayoutAttribute.Pack != 1 && type.StructLayoutAttribute.Value != LayoutKind.Sequential)
            {
                throw new TypedBufferException("Vertex types must use StructLayout.Sequential with Packing set to 1");
            }

            var fields = type.GetFields(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance);
            var referenceTypeFields = fields.FirstOrDefault(field => !field.FieldType.IsValueType);
            if (referenceTypeFields != null)
            {
                throw new TypedBufferException($"Vertex Type '{type.Name}' contains an invalid field '{referenceTypeFields.Name}'. All fields must be of value type.");
            }

            var fieldWithoutAttrib = fields.FirstOrDefault(field => field.GetCustomAttribute<VertexMemberAttribute>(true) == null);
            if (fieldWithoutAttrib != null)
            {
                throw new TypedBufferException($"Vertex Type '{type.Name}' lacks a VertexMemberAttribute for field '{fieldWithoutAttrib.Name}'");
            }

            var inputElements = fields.Select(field =>
            {
                SharpDX.DXGI.Format format;
                var fieldType = field.FieldType;
                var attribute = field.GetCustomAttribute<VertexMemberAttribute>(true);

                if (attribute.Format != SharpDX.DXGI.Format.Unknown)
                {
                    format = attribute.Format;
                }
                else if (fieldType == typeof(float))
                {
                    format = SharpDX.DXGI.Format.R32_Float;
                }
                else if (fieldType == typeof(int))
                {
                    format = SharpDX.DXGI.Format.R32_SInt;
                }
                else if (fieldType == typeof(uint))
                {
                    format = SharpDX.DXGI.Format.R32_UInt;
                }
                else if (fieldType == typeof(Vector2))
                {
                    format = SharpDX.DXGI.Format.R32G32_Float;
                }
                else if (fieldType == typeof(Vector3) || fieldType == typeof(Color3))
                {
                    format = SharpDX.DXGI.Format.R32G32B32_Float;
                }
                else if (fieldType == typeof(Vector4) || fieldType == typeof(Color4))
                {
                    format = SharpDX.DXGI.Format.R32G32B32A32_Float;
                }
                else
                {
                    throw new TypedBufferException($"No VertexMemberAttribute found on Type '{fieldType.Name}'");
                }

                var offset = Marshal.OffsetOf(type, field.Name).ToInt32();
                var inputElement = new InputElement(attribute.SemanticName.ToUpper(), attribute.SemanticIndex, format, offset, attribute.Slot);
                return inputElement;
            });

            var groups = inputElements.GroupBy(inputElement => new
            {
                inputElement.SemanticName,
                inputElement.SemanticIndex
            });
            var duplicate = groups.FirstOrDefault(group => group.Count() > 1);
            if (duplicate != null)
            {
                throw new TypedBufferException($"Duplicate Semantic {duplicate.Key.SemanticName}{duplicate.Key.SemanticIndex}");
            }
            return inputElements.ToArray();
        }

        public override InputLayout GetInputLayout(Device device, ShaderBytecode bytecode)
        {
            if (_inputLayout != null)
            {
                return _inputLayout;
            }

            _inputLayout = new InputLayout(device, bytecode, GetInputElements(typeof(T)));
#if DEBUG
            _inputLayout.DebugName = $"LAY {typeof(T).Name}";
#endif
            return _inputLayout;
        }

        protected override Array GetElements()
        {
            return _elements;
        }

        protected override int GetVertexStride()
        {
            return _vertexStride;
        }

        private void Grow(int minimumSize = -1)
        {
            if (minimumSize == -1)
            {
                if (_elements.Length == 0)
                {
                    minimumSize = 1;
                }
                else if (_elements.Length < 10)
                {
                    minimumSize = 16;
                }
                else
                {
                    minimumSize = _elements.Length + 16;
                }
            }
            Array.Resize(ref _elements, minimumSize);
            Dispose();
        }

        public override bool RayCastIntersect(Ray ray, IndexBufferBase indexContainer, out Vector3? hitPosition, out Vector3? hitNormal)
        {
            hitPosition = null;
            hitNormal = null;

            var closest = float.MaxValue;
            var elements = _elements.Select(element => element as ISpatial).ToArray();
            for (var i = 0; i < indexContainer.Count - 2; i += 3)
            {
                var vertex1 = elements[indexContainer[i + 0]].Position;
                var vertex2 = elements[indexContainer[i + 1]].Position;
                var vertex3 = elements[indexContainer[i + 2]].Position;

                if (ray.Intersects(ref vertex2, ref vertex1, ref vertex3, out Vector3 hitPos))
                {
                    hitPosition = hitPos;

                    var dist = (hitPos - ray.Position).Length();
                    if (dist < closest)
                    {
                        closest = dist;

                        var hitN = Vector3.Cross(vertex2 - vertex1, vertex2 - vertex3);
                        hitN = Vector3.Normalize(new Vector3(hitN.X, hitN.Y, hitN.Z));
                        hitNormal = hitN;
                    }
                }
            }

            if (closest == float.MaxValue)
            {
                return false;
            }
            hitPosition = ray.Position + (ray.Direction * closest);
            return true;
        }
    }
}