﻿using LessGravity.Quasar.Types;
using SharpDX;
using SharpDX.D3DCompiler;
using System;
using LessGravity.Quasar.Interop;

namespace LessGravity.Quasar.Graphics
{
    public sealed class ConstantBufferParameter
    {
        internal ConstantBufferParameter() { }

        public ConstantBufferParameter(ConstantBuffer constantBuffer, ShaderReflectionVariable reflectionVariable)
        {
            StartOffset = reflectionVariable.Description.StartOffset;
            Size = reflectionVariable.Description.Size;
            ShaderReflectionType variableType = reflectionVariable.GetVariableType();
            ShaderVariableClass = variableType.Description.Class;
            ShaderVariableType = variableType.Description.Type;
            Rows = variableType.Description.RowCount;
            Columns = variableType.Description.ColumnCount;
            Name = reflectionVariable.Description.Name;
            ConstantBuffer = constantBuffer;
        }

        private int Columns { get; }
        public string Name { get; }
        private int Rows { get; }
        private ShaderVariableClass ShaderVariableClass { get; }
        private ShaderVariableType ShaderVariableType { get; }
        private int Size { get; }
        private int StartOffset { get; }
        private ConstantBuffer ConstantBuffer { get; }

        public unsafe void SetValue(float[] parameterValue)
        {
            fixed (void* dataPointer = parameterValue)
            {
                SetArray(parameterValue.Length, dataPointer, sizeof(float));
            }
        }

        public unsafe void SetValue(Vector2[] parameterValue)
        {
            fixed (void* dataPointer = parameterValue)
            {
                SetArray(parameterValue.Length, dataPointer, sizeof(Vector2));
            }
        }

        public unsafe void SetValue(Vector3[] parameterValue)
        {
            fixed (void* dataPointer = parameterValue)
            {
                SetArray(parameterValue.Length, dataPointer, sizeof(Vector3));
            }
        }

        public unsafe void SetValue(Vector4[] parameterValue)
        {
            fixed (void* dataPointer = parameterValue)
            {
                SetArray(parameterValue.Length, dataPointer, sizeof(Vector4));
            }
        }

        public unsafe void SetValue(Matrix[] parameterValue)
        {
            fixed (void* dataPointer = parameterValue)
            {
                SetArray(parameterValue.Length, dataPointer, sizeof(Matrix));
            }
        }

        public unsafe void SetValue(float parameterValue)
        {
            Set(&parameterValue, sizeof(float));
        }

        public unsafe void SetValue(Vector2 parameterValue)
        {
            Set(&parameterValue, sizeof(Vector2));
        }

        public unsafe void SetValue(Vector3 parameterValue)
        {
            Set(&parameterValue, sizeof(Vector3));
        }

        public unsafe void SetValue(Vector4 parameterValue)
        {
            Set(&parameterValue, sizeof(Vector4));
        }

        public unsafe void SetValue(Matrix parameterValue)
        {
            Set(&parameterValue, sizeof(Matrix));
        }

        public unsafe void SetValue(int parameterValue)
        {
            Set(&parameterValue, sizeof(int));
        }

        public unsafe void SetValue(PackedUInt2 parameterValue)
        {
            Set(&parameterValue, sizeof(PackedUInt2));
        }

        public void SetValue(bool parameterValue)
        {
            int value = parameterValue ? 1 : 0;
            SetValue(value);
        }

        public unsafe void SetValue(Matrix3x2 matrix)
        {
            Set(&matrix, sizeof(Matrix3x2));
        }

        public unsafe void SetValue(Color color)
        {
            Set(&color, sizeof(Color));
        }

        private unsafe void Set(void* parameterValue, int parameterSize)
        {
            if (Size == 0)
            {
                return;
            }

            if (parameterSize != Size)
            {
                throw new ArgumentException($"parameterSize does not match the size of variable '{Name}'", nameof(parameterSize));
            }

            var dataPtr = &((byte*)ConstantBuffer.Data.DataPointer.ToPointer())[StartOffset];
            Msvcrt.MemCopy(dataPtr, parameterValue, parameterSize);

            ConstantBuffer.Changed = true;
        }

        private unsafe void SetArray(int elementCount, void* parameterValue, int elementSize)
        {
            if (Size == 0)
            {
                return;
            }

            if (elementCount * elementSize > Size)
            {
                throw new ArgumentOutOfRangeException(nameof(elementCount), "Number of elements does not match the size");
            }

            var dataPtr = &((byte*)ConstantBuffer.Data.DataPointer.ToPointer())[StartOffset];
            if (elementSize % 16 == 0)
            {
                Msvcrt.MemCopy(dataPtr, parameterValue, elementCount * elementSize);
            }
            else
            {
                for (int i = 0; i < elementCount; ++i)
                {
                    Msvcrt.MemCopy(&dataPtr[i * 16], &((byte*)parameterValue)[i * elementSize], elementSize);
                }
            }
            ConstantBuffer.Changed = true;
        }
    }
}