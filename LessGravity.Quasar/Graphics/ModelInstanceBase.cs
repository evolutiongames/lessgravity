﻿using System;
using SharpDX;

namespace LessGravity.Quasar.Graphics
{
    public abstract class ModelInstanceBase : IDisposable
    {
        public abstract Matrix WorldMatrix { get; set; }

        public Model Model { get; }

        public virtual void Dispose()
        {

        }

        protected ModelInstanceBase(Model model)
        {
            Model = model ?? throw new ArgumentNullException(nameof(model));
        }
    }
}