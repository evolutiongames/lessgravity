﻿using LessGravity.Quasar.Extensions;
using SharpDX;
using SharpDX.Direct3D11;
using System;
using System.Collections.Generic;
using Buffer = SharpDX.Direct3D11.Buffer;

namespace LessGravity.Quasar.Graphics
{
    public class ConstantBuffer : IDisposable
    {
        private readonly ConstantBufferParameter[] _parameters;

        public ConstantBuffer(Device device, SharpDX.D3DCompiler.ConstantBuffer reflectedBuffer)
        {
            Name = reflectedBuffer.Description.Name;
            Size = reflectedBuffer.Description.Size;
            Data = new DataStream(Size, true, true);

            Buffer = CreateBuffer(device);

            _parameters = new ConstantBufferParameter[reflectedBuffer.Description.VariableCount];
            for (var i = 0; i < reflectedBuffer.Description.VariableCount; i++)
            {
                var reflectedVariable = reflectedBuffer.GetVariable(i);
                var variable = new ConstantBufferParameter(this, reflectedVariable);
                _parameters[i] = variable;
            }
        }

        public Buffer Buffer { get; }

        public bool Changed { get; set; }

        public string Name { get; }

        public int Size { get; }

        public DataStream Data { get; }

        public IEnumerable<ConstantBufferParameter> Parameters => _parameters;

        public void Dispose()
        {
            Buffer.DisposeIfNotDisposed();
        }

        private Buffer CreateBuffer(Device device)
        {
            var bufferDescriptor = new BufferDescription
            {
                BindFlags = BindFlags.ConstantBuffer,
                CpuAccessFlags = CpuAccessFlags.None,
                OptionFlags = ResourceOptionFlags.None,
                SizeInBytes = Size,
                Usage = ResourceUsage.Default
            };
            var buffer = new Buffer(device, bufferDescriptor);
            buffer.DebugName = $"CB {Name}";
            return buffer;
        }
    }
}