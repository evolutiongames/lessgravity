﻿using LessGravity.Logging;
using LessGravity.Quasar.Extensions;
using SharpDX;
using SharpDX.Direct3D;
using SharpDX.Direct3D11;
using SharpDX.WIC;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;

namespace LessGravity.Quasar.Graphics
{
    [DebuggerDisplay("{NativeTexture.DebugName}")]
    public class Texture : IDisposable
    {
        public int Depth { get; }
        public int Height { get; }
        public bool Is1D { get; }
        public bool Is2D { get; }
        public bool Is3D { get; }
        public bool IsCube { get; }
        public int MipLevels { get; }
        public Resource NativeTexture { get; }
        public int Width { get; }

        protected Device Device => _device;

        private BindFlags _bindFlags;
        private readonly Device _device;
        private readonly SharpDX.DXGI.Format _format;
        private readonly ILogger _logger;

        private DepthStencilView _dsv;
        private RenderTargetView _rtv;
        private ShaderResourceView _srv;
        private UnorderedAccessView _uav;

        public void Dispose()
        {
            _dsv.DisposeIfNotDisposed();
            _rtv.DisposeIfNotDisposed();
            _srv.DisposeIfNotDisposed();
            _uav.DisposeIfNotDisposed();
            NativeTexture.DisposeIfNotDisposed(); //TODO(deccer) dispose nativetexture right after RTV creation
        }

        public static BitmapSource LoadBitmap(ImagingFactory2 factory, string filename)
        {
            var bitmapDecoder = new BitmapDecoder(factory, filename, DecodeOptions.CacheOnDemand);
            var formatConverter = new FormatConverter(factory);
            formatConverter.Initialize(bitmapDecoder.GetFrame(0), PixelFormat.Format32bppPRGBA, BitmapDitherType.None, null, 0.0, BitmapPaletteType.Custom);
            return formatConverter;
        }

        public static IEnumerable<BitmapSource> LoadBitmaps(ImagingFactory2 factory, params string[] fileNames)
        {
            foreach (var fileName in fileNames)
            {
                yield return LoadBitmap(factory, fileName);
            }
        }

        //TODO(deccer): this is probably malarky :)
        public void SetData<T>(T[] textureData) where T : struct
        {
            Utilities.Pin(textureData, textureDataPtr => _device.ImmediateContext.UpdateSubresource(new DataBox(textureDataPtr), NativeTexture));
        }

        //
        // creates the backbuffer texture from a swapchain
        //
        public Texture(Device device, ILogger logger, SharpDX.DXGI.SwapChain swapChain)
        {
            if (swapChain == null)
            {
                throw new ArgumentNullException(nameof(swapChain));
            }

            _logger = logger;
            _device = device ?? throw new ArgumentNullException(nameof(device));

            NativeTexture = Resource.FromSwapChain<Texture2D>(swapChain, 0);
            if (NativeTexture != null)
            {
                NativeTexture.DebugName = "TEX BackBuffer";
                var texture = (Texture2D)NativeTexture;
                _bindFlags = texture.Description.BindFlags;
                _format = texture.Description.Format;
                Height = texture.Description.Height;
                MipLevels = texture.Description.MipLevels;
                Width = texture.Description.Width;
                Is2D = true;
            }
            _logger.Info($"Create 2D Texture ({NativeTexture.NativePointer.ToInt64():X16})({Width}x{Height}) from Swapchain", Strings.Logging.Categories.Textures);
        }

        //
        // generic 1d Texture constructor
        //
        public Texture(Device device, ILogger logger, int width, SharpDX.DXGI.Format format, BindFlags bindFlags, int mipLevels = 1, ResourceOptionFlags optionFlags = ResourceOptionFlags.None, string debugName = "")
        {
            if (width < 1)
            {
                throw new ArgumentOutOfRangeException("Width and/or Height must not be smaller than 1");
            }
            _logger = logger;
            _bindFlags = bindFlags;
            _device = device ?? throw new ArgumentNullException(nameof(device));
            _format = format;
            var textureDescription = new Texture1DDescription
            {
                ArraySize = 1,
                BindFlags = _bindFlags,
                CpuAccessFlags = CpuAccessFlags.None,
                Format = _format,
                MipLevels = mipLevels,
                OptionFlags = optionFlags,
                Usage = ResourceUsage.Default,
                Width = width,
            };
            MipLevels = mipLevels;
            NativeTexture = new Texture1D(device, textureDescription);
            NativeTexture.DebugName = debugName;
            Is1D = true;
            Is2D = false;
            Is3D = false;
            IsCube = false;
            Width = textureDescription.Width;
            Height = 0;
            Depth = 0;
            _logger.Info($"Create 1D Texture ({NativeTexture.NativePointer.ToInt64():X16})({Width})", Strings.Logging.Categories.Textures);
        }

        //
        // generic 2d Texture constructor
        //
        public Texture(Device device, ILogger logger, int width, int height, SharpDX.DXGI.Format format, BindFlags bindFlags, int mipLevels = 1, ResourceOptionFlags optionFlags = ResourceOptionFlags.None, string debugName = "")
        {
            if (width < 1 || height < 1)
            {
                throw new ArgumentOutOfRangeException("Width and/or Height must not be smaller than 1");
            }
            _logger = logger;
            _bindFlags = bindFlags;
            _device = device ?? throw new ArgumentNullException(nameof(device));
            _format = format;
            var textureDescription = new Texture2DDescription
            {
                ArraySize = 1,
                BindFlags = _bindFlags,
                CpuAccessFlags = CpuAccessFlags.None,
                Format = _format,
                Height = height,
                MipLevels = mipLevels,
                OptionFlags = optionFlags,
                SampleDescription = new SharpDX.DXGI.SampleDescription
                {
                    Count = 1,
                    Quality = 0
                },
                Usage = ResourceUsage.Default,
                Width = width,
            };
            MipLevels = mipLevels;
            NativeTexture = new Texture2D(device, textureDescription);
            NativeTexture.DebugName = debugName;

            Is1D = false;
            Is2D = true;
            Is3D = false;
            IsCube = (optionFlags & ResourceOptionFlags.TextureCube) == ResourceOptionFlags.TextureCube;
            Width = textureDescription.Width;
            Height = textureDescription.Height;
            Depth = 0;
            _logger.Info($"Create 2D Texture ({NativeTexture.NativePointer.ToInt64():X16})({Width}x{Height})", Strings.Logging.Categories.Textures);
        }

        //
        // generic 3d Texture constructor
        //
        public Texture(Device device, ILogger logger, int width, int height, int depth, SharpDX.DXGI.Format format, BindFlags bindFlags, int mipLevels = 1, ResourceOptionFlags optionFlags = ResourceOptionFlags.None, string debugName = "")
        {
            if (width < 1 || height < 1 || depth < 1)
            {
                throw new ArgumentOutOfRangeException("Width and/or Height and/or Depth must not be smaller than 1");
            }
            _logger = logger;
            _bindFlags = bindFlags;
            _device = device ?? throw new ArgumentNullException(nameof(device));
            _format = format;
            var textureDescription = new Texture3DDescription
            {
                BindFlags = _bindFlags,
                CpuAccessFlags = CpuAccessFlags.None,
                Depth = depth,
                Format = _format,
                Height = height,
                MipLevels = mipLevels,
                OptionFlags = optionFlags,
                Usage = ResourceUsage.Default,
                Width = width,
            };
            MipLevels = mipLevels;
            NativeTexture = new Texture3D(device, textureDescription);
            NativeTexture.DebugName = debugName;

            Is1D = false;
            Is2D = false;
            Is3D = true;
            IsCube = false;

            Width = textureDescription.Width;
            Height = textureDescription.Height;
            Depth = textureDescription.Depth;
            _logger.Info($"Create 3D Texture ({NativeTexture.NativePointer.ToInt64():X16})({Width}x{Height}x{Depth})", Strings.Logging.Categories.Textures);
        }

        protected virtual Texture2D LoadTexture2D(ImagingFactory2 imagingFactory, string fileName)
        {
            if (imagingFactory == null)
            {
                throw new ArgumentNullException(nameof(imagingFactory));
            }
            return _device.CreateTexture2DFromBitmap(LoadBitmap(imagingFactory, fileName));
        }

        //
        // creates a texture from file
        //
        internal Texture(Device device, ILogger logger, ImagingFactory2 imagingFactory, string fileName, string debugName = "")
        {
            _logger = logger;
            //TODO(deccer): remove the IServiceRegistry here and introduce some IDevice
            if (this is TextureCube)
            {
                if (string.IsNullOrEmpty(fileName))
                {
                    throw new FileNotFoundException($"File {fileName} not found.");
                }
            }
            else
            {
                if (string.IsNullOrEmpty(fileName) || !File.Exists(fileName))
                {
                    throw new FileNotFoundException($"File {fileName} not found.");
                }
            }
            _device = device ?? throw new ArgumentNullException(nameof(device));

            NativeTexture = LoadTexture2D(imagingFactory, fileName);
            if (NativeTexture != null)
            {
                NativeTexture.DebugName = $"TEX {fileName}";
                Is1D = NativeTexture.Dimension == ResourceDimension.Texture1D;
                Is2D = NativeTexture.Dimension == ResourceDimension.Texture2D;
                Is3D = NativeTexture.Dimension == ResourceDimension.Texture3D;

                if (Is1D)
                {
                    var texture = (Texture1D)NativeTexture;
                    _bindFlags = texture.Description.BindFlags;
                    _format = texture.Description.Format;
                    MipLevels = texture.Description.MipLevels;
                    Height = 1;
                    Width = texture.Description.Width;
                    logger.Info($"Loaded 1D Texture ({NativeTexture.NativePointer.ToInt64():X16})({Width}) from {fileName}", Strings.Logging.Categories.Textures);
                }
                else if (Is2D)
                {
                    var texture = (Texture2D)NativeTexture;
                    _bindFlags = texture.Description.BindFlags;
                    _format = texture.Description.Format;
                    Height = texture.Description.Height;
                    MipLevels = texture.Description.MipLevels;
                    Width = texture.Description.Width;
                    IsCube = ((texture.Description.OptionFlags & ResourceOptionFlags.TextureCube) == ResourceOptionFlags.TextureCube);
                    Is2D = !IsCube;
                    logger.Info($"Loaded 2D Texture ({NativeTexture.NativePointer.ToInt64():X16})({Width}x{Height}) from {fileName}", Strings.Logging.Categories.Textures);
                }
                else if (Is3D)
                {
                    var texture = (Texture3D)NativeTexture;
                    _bindFlags = texture.Description.BindFlags;
                    _format = texture.Description.Format;
                    Depth = texture.Description.Depth;
                    Height = texture.Description.Height;
                    MipLevels = texture.Description.MipLevels;
                    Width = texture.Description.Width;
                    logger.Info($"Loaded 3D Texture ({NativeTexture.NativePointer.ToInt64():X16})({Width}x{Height}x{Depth}) from {fileName}", Strings.Logging.Categories.Textures);
                }
            }
        }

        private DepthStencilView GetDsv()
        {
            if (_dsv != null)
            {
                return _dsv;
            }
            var dsvDescription = new DepthStencilViewDescription
            {
                Dimension = Is1D ? DepthStencilViewDimension.Texture1D :
                    Is2D ? DepthStencilViewDimension.Texture2D :
                        DepthStencilViewDimension.Unknown,
                Format = _format,
            };
            _dsv = new DepthStencilView(_device, NativeTexture, dsvDescription);
            _dsv.DebugName = $"DSV {NativeTexture.DebugName}";

            _logger.Debug($"Creating DSV for Texture ({NativeTexture.NativePointer.ToInt64():X16}) [{NativeTexture.DebugName}]", Strings.Logging.Categories.Textures);
            return _dsv;
        }

        private RenderTargetView GetRtv()
        {
            if (_rtv != null)
            {
                return _rtv;
            }
            var rtvDescription = new RenderTargetViewDescription
            {
                Dimension = Is1D ? RenderTargetViewDimension.Texture1D : 
                    Is2D ? RenderTargetViewDimension.Texture2D : 
                        Is3D ? RenderTargetViewDimension.Texture3D : 
                            RenderTargetViewDimension.Unknown,
                Format = _format,
            };
            _rtv = new RenderTargetView(_device, NativeTexture, rtvDescription);
            _rtv.DebugName = $"RTV {NativeTexture.DebugName}";

            _logger.Debug($"Creating RTV for Texture ({NativeTexture.NativePointer.ToInt64():X16}) [{NativeTexture.DebugName}]", Strings.Logging.Categories.Textures);
            return _rtv;
        }

        private ShaderResourceView GetSrv()
        {
            if (_srv != null)
            {
                return _srv;
            }
            var srvDescription = new ShaderResourceViewDescription
            {
                Dimension = Is1D ? ShaderResourceViewDimension.Texture1D :
                    Is2D ? ShaderResourceViewDimension.Texture2D :
                        Is3D ? ShaderResourceViewDimension.Texture3D :
                            IsCube ? ShaderResourceViewDimension.TextureCube :
                                ShaderResourceViewDimension.Unknown,
                Format = _format,
            };
            if (Is1D)
            {
                srvDescription.Texture1D.MipLevels = ((Texture1D)NativeTexture).Description.MipLevels;
                srvDescription.Texture1D.MostDetailedMip = 0;
            }
            else if (Is2D)
            {
                srvDescription.Texture2D.MipLevels = ((Texture2D)NativeTexture).Description.MipLevels;
                srvDescription.Texture2D.MostDetailedMip = 0;
            }
            else if (Is3D)
            {
                srvDescription.Texture3D.MipLevels = ((Texture3D)NativeTexture).Description.MipLevels;
                srvDescription.Texture3D.MostDetailedMip = 0;
            }
            else if (IsCube)
            {
                srvDescription.TextureCube.MipLevels = ((Texture2D)NativeTexture).Description.MipLevels;
                srvDescription.TextureCube.MostDetailedMip = 0;
            }
            _srv = new ShaderResourceView(_device, NativeTexture, srvDescription);
            _srv.DebugName = $"SRV {NativeTexture.DebugName}";

            _logger.Debug($"Creating SRV for Texture ({NativeTexture.NativePointer.ToInt64():X16}) [{NativeTexture.DebugName}]", Strings.Logging.Categories.Textures);
            return _srv;
        }

        private UnorderedAccessView GetUav()
        {
            if (_uav != null)
            {
                return _uav;
            }
            _uav = new UnorderedAccessView(_device, NativeTexture);
            _uav.DebugName = $"UAV {NativeTexture.DebugName}";

            _logger.Debug($"Creating UAV for Texture ({NativeTexture.NativePointer.ToInt64():X16}) [{NativeTexture.DebugName}]", Strings.Logging.Categories.Textures);
            return _uav;
        }

        public static implicit operator DepthStencilView(Texture texture)
        {
            return texture.GetDsv();
        }

        public static implicit operator RenderTargetView(Texture texture)
        {
            return texture.GetRtv();
        }

        public static implicit operator ShaderResourceView(Texture texture)
        {
            return texture.GetSrv();
        }

        public static implicit operator UnorderedAccessView(Texture texture)
        {
            return texture.GetUav();
        }
    }
}