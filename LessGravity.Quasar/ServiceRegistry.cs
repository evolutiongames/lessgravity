﻿using LessGravity.Logging;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace LessGravity.Quasar
{
    public class ServiceRegistry : IServiceRegistry
    {
        private readonly Dictionary<Type, object> _registeredService = new Dictionary<Type, object>();
        private readonly ILogger _logger;

        /// <summary>
        /// Occurs when a new service is added.
        /// </summary>
        public event EventHandler<ServiceEventArgs> ServiceAdded;

        /// <summary>
        /// Occurs when when a service is removed.
        /// </summary>
        public event EventHandler<ServiceEventArgs> ServiceRemoved;

        public ServiceRegistry(ILogger logger)
        {
            _logger = logger;
        }

        /// <summary>
        /// Gets the instance service providing a specified service.
        /// </summary>
        /// <param name="type">The type of service.</param>
        /// <returns>The registered instance of this service.</returns>
        /// <exception cref="System.ArgumentNullException">type</exception>
        public object GetService(Type type)
        {
            if (type == null)
            {
                throw new ArgumentNullException(nameof(type));
            }
            lock (_registeredService)
            {
                if (_registeredService.ContainsKey(type))
                {
                    return _registeredService[type];
                }
            }
            return null;
        }

        /// <summary>
        /// Gets the service object of specified type. The service must be registered with the <typeparamref name="T"/> type key.
        /// </summary>
        /// <remarks>This method will thrown an exception if the service is not registered, it null value can be accepted - use the <see cref="IServiceProvider.GetService"/> method.</remarks>
        /// <typeparam name="T">The type of the service to get.</typeparam>
        /// <returns>The service instance.</returns>
        /// <exception cref="ArgumentException">Is thrown when the corresponding service is not registered.</exception>
        public T GetService<T>()
        {
            var type = typeof(T);
            var service = GetService(type);
            if (service == null)
            {
                throw new ArgumentException($"Service of type {type.Name} is not registered.");
            }
            _logger.Info($"Retrieving Service {type.Name}", "ServiceRegistry");
            return (T)service;
        }

        /// <summary>
        /// Adds a service to this <see cref="ServiceRegistry"/>.
        /// </summary>
        /// <param name="type">The type of service to add.</param>
        /// <param name="provider">The service provider to add.</param>
        /// <exception cref="System.ArgumentNullException">type;Service type cannot be null</exception>
        /// <exception cref="System.ArgumentException">Service is already registered;type</exception>
        public void AddService(Type type, object provider)
        {
            if (type == null)
            {
                throw new ArgumentNullException(nameof(type));
            }
            if (provider == null)
            {
                throw new ArgumentNullException(nameof(provider));
            }
            if (!type.IsInstanceOfType(provider))
            {
                throw new ArgumentException($"Service [{provider.GetType().FullName}] must be assignable to [{type.FullName}]");
            }

            lock (_registeredService)
            {
                if (_registeredService.ContainsKey(type))
                {
                    throw new ArgumentException("Service is already registered", nameof(type));
                }
                _registeredService.Add(type, provider);
            }
            OnServiceAdded(new ServiceEventArgs(type, provider));
        }

        /// <summary>
        /// Adds a service to this service provider.
        /// </summary>
        /// <typeparam name="T">The type of the service to add.</typeparam>
        /// <param name="provider">The instance of the service provider to add.</param>
        /// <exception cref="System.ArgumentNullException">Service type cannot be null</exception>
        /// <exception cref="System.ArgumentException">Service is already registered</exception>
        public void AddService<T>(T provider)
        {
            AddService(typeof(T), provider);
        }

        /// <summary>Removes the object providing a specified service.</summary>
        /// <param name="type">The type of service.</param>
        public void RemoveService(Type type)
        {
            if (type == null)
            {
                throw new ArgumentNullException(nameof(type));
            }

            object oldService;
            lock (_registeredService)
            {
                if (_registeredService.TryGetValue(type, out oldService))
                {
                    _registeredService.Remove(type);
                }
            }
            if (oldService != null)
            {
                OnServiceRemoved(new ServiceEventArgs(type, oldService));
            }
        }

        private void OnServiceAdded(ServiceEventArgs e)
        {
            ServiceAdded?.Invoke(this, e);
        }

        private void OnServiceRemoved(ServiceEventArgs e)
        {
            ServiceRemoved?.Invoke(this, e);
        }
    }
}