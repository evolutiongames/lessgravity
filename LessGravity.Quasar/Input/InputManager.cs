﻿using LessGravity.Quasar.Extensions;
using SharpDX;
using SharpDX.DirectInput;
using System;
using System.Windows.Forms;

namespace LessGravity.Quasar.Input
{
    public class InputManager : IDisposable
    {
        private readonly Control _handleControl;
        private readonly DirectInput _input;
        private readonly Keyboard _keyboard;
        private readonly Mouse _mouse;
        private KeyboardState _currentKeyboardState;
        private MouseState _currentMouseState;

        private KeyboardState _lastKeyboardState;
        private MouseState _lastMouseState;

        private bool _acquired;
        private bool _disposing;
        private System.Drawing.Point _mousePos;

        public int MouseDeltaX => _currentMouseState?.X ?? 0;

        public int MouseDeltaY => _currentMouseState?.Y ?? 0;

        public int MousePositionX => _mousePos.X;

        public int MousePositionY => _mousePos.Y;

        public int MouseWheelDelta => _currentMouseState != null ? _currentMouseState.Z > 0 ? 1 : _currentMouseState.Z < 0 ? -1 : 0 : 0;

        public void Dispose()
        {
            if (_disposing)
            {
                return;
            }

            _disposing = true;
            if (!_mouse.IsDisposed)
            {
                _mouse.Unacquire();
                _mouse.Dispose();
            }
            if (!_keyboard.IsDisposed)
            {
                _keyboard.Unacquire();
                _keyboard.Dispose();
            }
            _input.DisposeIfNotDisposed();
        }

        public InputManager(IntPtr windowHandle)
        {
#if DEBUG
            const CooperativeLevel cooperativeLevel = CooperativeLevel.NonExclusive | CooperativeLevel.Background;
#else
            const CooperativeLevel cooperativeLevel = CooperativeLevel.NonExclusive | CooperativeLevel.Foreground;
#endif
            _disposing = false;
            _input = new DirectInput();
            _mouse = new Mouse(_input);
            _keyboard = new Keyboard(_input);
            _handleControl = Control.FromHandle(windowHandle);

            _mouse.SetCooperativeLevel(windowHandle, cooperativeLevel);
            _keyboard.SetCooperativeLevel(windowHandle, cooperativeLevel);
        }

        public bool KeyDown(Key key)
        {
            return _lastKeyboardState != null && !_lastKeyboardState.IsPressed(key) && _currentKeyboardState != null && _currentKeyboardState.IsPressed(key);
        }

        public bool KeyPressed(Key key)
        {
            return _currentKeyboardState != null && _currentKeyboardState.IsPressed(key);
        }

        public bool MouseButtonClicked(int index)
        {
            return _lastMouseState != null && !_lastMouseState.Buttons[index] && _currentMouseState != null && _currentMouseState.Buttons[index];
        }

        public bool MouseButtonPressed(int index)
        {
            return _currentMouseState != null && _currentMouseState.Buttons[index];
        }

        public void Update()
        {
            if (_disposing)
            {
                _lastKeyboardState = _currentKeyboardState = null;
                _lastMouseState = _currentMouseState = null;
                return;
            }

            try
            {
                if (!_acquired)
                {
                    _mouse.Acquire();
                    _keyboard.Acquire();
                    _acquired = true;
                }
                _mouse.Poll();
                _keyboard.Poll();
            }
            catch (SharpDXException)
            {
                try
                {
                    _mouse.Acquire();
                    _keyboard.Acquire();

                    _mouse.Poll();
                    _keyboard.Poll();
                }
                catch (SharpDXException)
                {
                    return;
                }
            }
            _lastKeyboardState = _currentKeyboardState;
            _lastMouseState = _currentMouseState;
            _currentMouseState = _mouse.GetCurrentState();
            _currentKeyboardState = _keyboard.GetCurrentState();
            _mousePos = _handleControl.PointToClient(Cursor.Position);
        }
    }
}