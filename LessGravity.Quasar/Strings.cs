﻿namespace LessGravity.Quasar
{
    public static class Strings
    {
        public static class Logging
        {
            public static class Categories
            {
                public const string Assets = "Assets";
                public const string Textures = "Textures";
                public const string Shaders = "Shaders";
                public const string States = "States";
                public const string Resources = "Resources";

                public const string SharpDX = "SharpDX";
            }
        }
    }
}