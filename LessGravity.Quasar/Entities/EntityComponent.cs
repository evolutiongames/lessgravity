﻿using System;
using System.Diagnostics;

namespace LessGravity.Quasar.Entities
{
    [DebuggerDisplay("Name: {Name}, Id: {Id}")]
    public class EntityComponent : IDisposable
    {
        public Guid Id { get; set; }
        public string Name { get; set; }

        public virtual void Dispose() { }
    }
}