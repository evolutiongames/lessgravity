﻿using LessGravity.Logging;
using LessGravity.Quasar.Assets;
using LessGravity.Quasar.Game;
using LessGravity.Quasar.Graphics;
using LessGravity.Quasar.Graphics.Lights;
using SharpDX.Direct3D11;
using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace LessGravity.Quasar.Entities
{
    public class EntityManager : IDisposable
    {
        private readonly IServiceRegistry _serviceRegistry;
        private readonly List<Entity> _entities;
        private readonly List<GraphicsComponent> _graphicsComponents;
        private readonly AssetManager _assetManager;
        private readonly LightManager _lightManager;
        private readonly RenderQueue _renderQueue;
        private bool _disposed;
        private readonly Device _device;

        private readonly DepthStencilState _defaultDepthStencilState;
        private readonly Shader _vsGeometry;
        private readonly Shader _psGeometry;
        
        public void AddEntity(Entity entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException(nameof(entity));
            }
            _entities.Add(entity);
            var graphicsComponents = entity.GetComponents<GraphicsComponent>();
            _graphicsComponents.AddRange(graphicsComponents);
            var lightComponents = entity.GetComponents<LightComponent>();
            _lightManager.AddRange(lightComponents);
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (_disposed)
            {
                return;
            }
            if (disposing)
            {
                //TODO(deccer): check if this is legit :)
                _graphicsComponents.Clear();
                _lightManager.Dispose();
                lock (_entities)
                {
                    foreach (var entity in _entities)
                    {
                        entity.Dispose();
                    }
                }
                _entities.Clear();
                //TODO(deccer): dispose graphics, lights, ...
            }
            _disposed = true;
        }

        public void Draw(Camera camera)
        {
            _renderQueue.Clear();
            foreach (var graphicsComponent in _graphicsComponents)
            {
                graphicsComponent.AddVisibileMeshes(_renderQueue);
            }
            _renderQueue.Sort();

            _vsGeometry.Parameters["M_View"].SetValue(camera.ViewMatrix);
            _vsGeometry.Parameters["M_Projection"].SetValue(camera.ProjectionMatrix);
            var useInstancing = false;
#if DEBUG
            if (useInstancing)
            {
                Debug.WriteLine("================================ !!!!!!!!!!!!!!!!!!!! ================================");
                Debug.WriteLine("= Instancing is Enabled. Make sure an InstancingBuffer is bound the the VertexShader =");
                Debug.WriteLine("================================ !!!!!!!!!!!!!!!!!!!! ================================");
            }
#endif
            //_vsGeometry.Parameters["UseInstancing"].SetValue(useInstancing); //TODO: set "UseInstancing" per mesh...
            _psGeometry.Parameters["CameraFarPlane"].SetValue(camera.FarPlane);

            var deviceContext = _device.ImmediateContext;
            deviceContext.OutputMerger.DepthStencilState = _defaultDepthStencilState;
            deviceContext.VertexShader.Set(_vsGeometry);
            deviceContext.PixelShader.Set(_psGeometry);

            _renderQueue.Draw(_device, _psGeometry, _vsGeometry, _assetManager);
        }
        
        public EntityManager(IServiceRegistry serviceRegistry, ILogger logger)
        {
            _serviceRegistry = serviceRegistry;
            _assetManager = _serviceRegistry.GetService<AssetManager>();
            _device = _serviceRegistry.GetService<Device>();

            _entities = new List<Entity>();
            _graphicsComponents = new List<GraphicsComponent>();
            _renderQueue = new RenderQueue(logger);
            _lightManager = new LightManager(_renderQueue);

            _vsGeometry = _assetManager.Load<Shader>("Shaders\\Geometry.vs");
            _psGeometry = _assetManager.Load<Shader>("Shaders\\Geometry.ps");

            _defaultDepthStencilState = DepthStencilStates.CreateDefaultDepthStencilState(_device);

        }

        ~EntityManager()
        {
            Dispose(false);
        }
    }
}