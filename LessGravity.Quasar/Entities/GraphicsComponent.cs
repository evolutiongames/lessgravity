﻿using System;
using LessGravity.Quasar.Graphics;
using SharpDX;

namespace LessGravity.Quasar.Entities
{
    public class GraphicsComponent : EntityComponent
    {
        public ModelInstance ModelInstance { get; }

        public Matrix Matrix
        {
            get { return ModelInstance.WorldMatrix; }
        }

        public void AddVisibileMeshes(RenderQueue renderQueue)
        {
            ModelInstance.AddVisibleMeshes(renderQueue);
        }

        public override void Dispose()
        {
            ModelInstance.Dispose();
        }

        public GraphicsComponent(ModelInstance modelInstance)
        {
            ModelInstance = modelInstance ?? throw new ArgumentNullException(nameof(modelInstance));
        }
    }
}