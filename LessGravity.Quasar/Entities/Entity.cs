﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace LessGravity.Quasar.Entities
{
    [DebuggerDisplay("Name: {Name}, Id: {Id}")]
    public class Entity : IDisposable
    {
        private IList<Entity> _children;
        private IList<EntityComponent> _components;

        private Entity _parent;

        public Guid Id { get; private set; }
        public string Name { get; set; }

        public Entity Parent
        {
            get => _parent;
            set
            {
                _parent?.Remove(this);
                _parent = value;
                _parent?.Add(this);
            }
        }

        public void Add(EntityComponent component)
        {
            if (component == null)
            {
                throw new ArgumentNullException(nameof(component));
            }
            if (_components == null)
            {
                _components = new List<EntityComponent>();
            }
            _components.Add(component);
        }

        public void Add(Entity entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException(nameof(entity));
            }
            if (_children != null)
            {
                _children = new List<Entity>();
            }
            _children.Add(entity);
        }

        public virtual void Dispose()
        {
            if (_components != null)
            {
                lock (_components)
                {
                    foreach (var component in _components)
                    {
                        component.Dispose();
                    }
                }
            }
            if (_children != null)
            {
                lock (_children)
                {
                    foreach (var child in _children)
                    {
                        child.Dispose();
                    }
                }
            }
        }

        public Entity(Entity parent)
        {
            Id = Guid.NewGuid();
            _parent = parent;
        }

        public T GetComponent<T>() where T : EntityComponent
        {
            return _components?.OfType<T>().FirstOrDefault();
        }

        public IEnumerable<T> GetComponents<T>()
        {
            return _components?.OfType<T>() ?? Enumerable.Empty<T>();
        }

        public void Remove(EntityComponent component)
        {
            if (component == null)
            {
                throw new ArgumentNullException(nameof(component));
            }
            _components?.Remove(component);
        }

        public void Remove(Entity entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException(nameof(entity));
            }
            _children?.Remove(entity);
        }
    }
}