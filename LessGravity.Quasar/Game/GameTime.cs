﻿using System.Diagnostics;

namespace LessGravity.Quasar.Game
{
    public sealed class GameTime
    {
        private Stopwatch _stopWatch;
        private double _lastUpdate;

        public float ElapsedMilliSeconds => _stopWatch.ElapsedMilliseconds;
        public float ElapsedSeconds => _stopWatch.ElapsedMilliseconds * 0.001f;

        public GameTime()
        {
            _stopWatch = new Stopwatch();
        }

        public void Start()
        {
            _stopWatch.Start();
            _lastUpdate = 0;
        }

        public void Stop()
        {
            _stopWatch.Stop();
        }

        public double Update()
        {
            var now = ElapsedSeconds;
            var updateTime = now - _lastUpdate;
            _lastUpdate = now;
            return updateTime;
        }
    }
}