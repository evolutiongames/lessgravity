﻿using System.Windows.Forms;

namespace LessGravity.Quasar.Game
{
    public interface IFormFactory
    {
        Form CreateForm(string title);
    }
}