﻿using SharpDX.Windows;
using System.Windows.Forms;

namespace LessGravity.Quasar.Game
{
    public class DefaultFormFactory : IFormFactory
    {
        public Form CreateForm(string title)
        {
            return new RenderForm(title);
        }
    }
}