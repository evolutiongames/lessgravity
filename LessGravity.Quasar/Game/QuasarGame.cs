﻿using LessGravity.Logging;
using LessGravity.Quasar.Entities;

namespace LessGravity.Quasar.Game
{
    public class QuasarGame : QuasarGameBase
    {
        protected EntityManager Entities { get; private set; }

        public QuasarGame(ILogger logger, IFormFactory formFactory)
            : base(logger, formFactory)
        {
            
        }

        protected override void Dispose(bool disposeManagedResources)
        {
            Entities.Dispose();
            base.Dispose(disposeManagedResources);
        }

        protected override void Initialize(GameSettings settings)
        {
            base.Initialize(settings);

            Entities = new EntityManager(Services, Logger);
        }
    }
}