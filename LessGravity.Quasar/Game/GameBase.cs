﻿using LessGravity.Logging;
using SharpDX.Windows;
using System;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Windows.Forms;

namespace LessGravity.Quasar.Game
{
    public abstract class GameBase : IDisposable
    {
        private readonly IFormFactory _formFactory;
        private readonly ILogger _logger;

        private bool _isDisposed;
        private Form _form;
        private readonly GameTime _gameTime = new GameTime();
        private float _frameAccumulator;
        private int _frameCount;
        private FormWindowState _windowState;
        private System.Drawing.Point _oldLocation;
        private System.Drawing.Size _oldRenderingSize;

        protected ILogger Logger => _logger;

        public IntPtr DisplayHandle => _form.Handle;
        public float FrameDelta { get; private set; }
        public float FramesPerSecond { get; private set; }
        public System.Drawing.Size RenderingSize => _form.ClientSize;
        public GameSettings Settings { get; private set; }
        public IServiceRegistry Services { get; }

        protected virtual void BeginDraw() { }

        protected virtual void BeginRun() { }

        private Form CreateRenderForm()
        {
            var renderForm = _formFactory.CreateForm(Settings.Title);
            renderForm.ClientSize = new System.Drawing.Size(Settings.Width, Settings.Height);
            if (Settings.FullScreen)
            {
                renderForm.FormBorderStyle = FormBorderStyle.None;
            }
            return renderForm;
        }

        public void Dispose()
        {
            if (!_isDisposed)
            {
                Dispose(true);
                _isDisposed = true;
            }
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposeManagedResources) { }
        protected virtual void Draw(GameTime gameTime) { }
        protected virtual void EndDraw() { }
        protected virtual void EndRun() { }

        public void Exit()
        {
            _form.Close();
        }

        protected GameBase(ILogger logger, IFormFactory formFactory)
        {
            _formFactory = formFactory;
            _logger = logger;
            Services = new ServiceRegistry(logger);
            Services.AddService(logger);
        }

        private void HandleKeyDown(object sender, KeyEventArgs e)
        {
            KeyDown(e);
        }

        private void HandleKeyPress(object sender, KeyPressEventArgs e)
        {
            KeyPress(e);
        }

        private void HandleKeyUp(object sender, KeyEventArgs e)
        {
            KeyUp(e);
        }

        private void HandleMouseClick(object sender, MouseEventArgs e)
        {
            MouseClick(e);
        }

        private void HandleMouseMove(object sender, MouseEventArgs e)
        {
            MouseMove(e);
        }

        private void HandleResize(object sender, EventArgs e)
        {
            if (_form.WindowState == FormWindowState.Minimized)
            {
                return;
            }
            Resize();
        }

        protected abstract void Initialize(GameSettings settings);

        protected virtual void KeyDown(KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                Exit();
            }
        }

        protected virtual void KeyPress(KeyPressEventArgs e) { }

        protected virtual void KeyUp(KeyEventArgs e) { }

        protected virtual void LoadContent() { }

        protected virtual void MouseClick(MouseEventArgs e) { }

        protected virtual void MouseMove(MouseEventArgs e) { }

        private void OnUpdate()
        {
            FrameDelta = (float)_gameTime.Update();
            Update(_gameTime);
        }

        private void Render()
        {
            _frameAccumulator += FrameDelta;
            ++_frameCount;
            if (_frameAccumulator >= 1.0f)
            {
                FramesPerSecond = _frameCount / _frameAccumulator;
                if (_form.WindowState == FormWindowState.Normal && Settings.ShowFps)
                {
                    _form.Text = $"{Settings.Title} - FPS: {(int)FramesPerSecond}";
                }
                _frameAccumulator = 0.0f;
                _frameCount = 0;
            }
            BeginDraw();
            Draw(_gameTime);
            EndDraw();
        }

        protected virtual void Resize() { }

        public void Run()
        {
            Run(new GameSettings());
        }

        public void Run(GameSettings settings)
        {
            Settings = settings ?? new GameSettings();

            //if (settings.EnableLogging)
            //{
            //    var loggerClientAlreadyRunning = Process.GetProcessesByName("LessGravity.Sun").Any();
            //    if (!loggerClientAlreadyRunning)
            //    {
            //        var logServerPath = ConfigurationManager.AppSettings["Path.LogServer"];
            //        if (File.Exists(Path.Combine(Application.StartupPath, logServerPath)))
            //        {
            //            var entryAssemblyName =
            //                Path.GetFileNameWithoutExtension(Assembly.GetEntryAssembly().ManifestModule.Name);
            //            if (_logger.RemoteRecipients.Any())
            //            {
            //                var randomPort = _logger.RemoteRecipients.FirstOrDefault()?.Port;
            //                var logServerArgument = $"{entryAssemblyName}:{randomPort}";

            //                var processStartInfo = new ProcessStartInfo
            //                {
            //                    FileName = logServerPath,
            //                    Arguments = logServerArgument
            //                };
            //                if (!Debugger.IsAttached)
            //                {
            //                    Process.Start(processStartInfo);
            //                }
            //            }
            //        }
            //        else
            //        {
            //            Debug.WriteLine("Logging Enabled, but no Path.LogServer set in app.config");
            //        }
            //    }
            //}

            _form = CreateRenderForm();

            _oldRenderingSize = _form.ClientSize;
            _oldLocation = _form.Location;

            Application.DoEvents();

            Initialize(Settings);

            var isFormClosing = false;
            var isFormResizing = false;

            _form.MouseClick += HandleMouseClick;
            _form.MouseMove += HandleMouseMove;
            _form.KeyDown += HandleKeyDown;
            _form.KeyPress += HandleKeyPress;
            _form.KeyUp += HandleKeyUp;
            _form.Load += (sender, e) =>
            {
                var primaryScreen = Screen.PrimaryScreen;
                _form.Location = new System.Drawing.Point(primaryScreen.Bounds.Width / 2 - Settings.Width / 2, primaryScreen.Bounds.Height / 2 - Settings.Height / 2);
            };
            _form.Resize += (sender, e) =>
            {
                if (_form.WindowState != _windowState)
                {
                    isFormResizing = true;
                    HandleResize(sender, e);
                    isFormResizing = false;
                }
                _windowState = _form.WindowState;
            };
            _form.ResizeBegin += (sender, e) =>
            {
                isFormResizing = true;
            };
            _form.ResizeEnd += (sender, e) =>
            {
                HandleResize(sender, e);
                isFormResizing = false;
            };
            _form.FormClosed += (sender, e) =>
            {
                isFormClosing = true;
            };

            LoadContent();
            _gameTime.Start();
            BeginRun();
            RenderLoop.Run(_form, () =>
            {
                if (isFormClosing)
                {
                    return;
                }
                OnUpdate();
                if (!isFormResizing)
                {
                    Render();
                }
            });
            EndRun();
            UnloadContent();
        }

        protected virtual void ToggleFullscreen()
        {
            ToggleFullscreen(Screen.PrimaryScreen);
        }

        protected virtual void ToggleFullscreen(Screen screen)
        {
            if (_windowState == FormWindowState.Normal)
            {
                _oldRenderingSize = RenderingSize;
                _oldLocation = _form.Location;
                _form.FormBorderStyle = FormBorderStyle.None;
                _form.WindowState = FormWindowState.Maximized;
                Settings.FullScreen = true;
            }
            else
            {
                _form.FormBorderStyle = FormBorderStyle.Sizable;
                _form.WindowState = FormWindowState.Normal;
                _form.Location = _oldLocation;
                _form.ClientSize = _oldRenderingSize;
                Settings.FullScreen = false;
            }
        }

        protected virtual void UnloadContent() { }

        protected virtual void Update(GameTime gameTime) { }
    }
}