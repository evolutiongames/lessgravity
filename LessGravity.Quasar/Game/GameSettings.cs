﻿using System.IO;
using Newtonsoft.Json;

namespace LessGravity.Quasar.Game
{
    //TODO(deccer): add INPC maybe
    public class GameSettings
    {
        public bool FullScreen { get; set; }
        public int Height { get; set; }
        public bool ShowFps { get; set; }
        public bool Support2d { get; set; }
        public string Title { get; set; }
        public int Width { get; set; }
        public bool VSync { get; set; }

        public bool EnableLogging { get; set; }

        [JsonIgnore]
        private string ConfigDir = Directory.GetCurrentDirectory() + @"\Assets\Config";

#if DEBUG
        public GameSettings()
        {
            ShowFps = true;
        }
#endif

        public static GameSettings ReadSettings(string settingsFileName)
        {
            GameSettings gameSettings;

            var settingsDirectory = Path.GetDirectoryName(settingsFileName);
            if (!Directory.Exists(settingsDirectory))
            {
                Directory.CreateDirectory(settingsDirectory);
            }

            if (!File.Exists(settingsFileName))
            {
                gameSettings = new GameSettings();
                gameSettings.SetDefaultSettings();
                File.WriteAllText(settingsFileName, JsonConvert.SerializeObject(gameSettings, Formatting.Indented));
            }
            else
            {
                var jsonConfig = File.ReadAllText(settingsFileName);
                gameSettings = JsonConvert.DeserializeObject<GameSettings>(jsonConfig);
            }
            return gameSettings;
        }

        public void WriteSettings(string settingsFileName)
        {
            File.WriteAllText(settingsFileName, JsonConvert.SerializeObject(this));
        }

        public virtual void SetDefaultSettings()
        {
            Title = "5D-DX11 Engine (LessGravity.Quasar)";
            FullScreen = false;
            Width = 1024;
            Height = 728;
            Support2d = true;
            VSync = false;
#if DEBUG
            EnableLogging = true;
#endif
        }
    }
}