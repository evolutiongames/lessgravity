﻿using SharpDX;

namespace LessGravity.Quasar.Game
{
    public class Camera
    {
        private Matrix _viewMatrix;
        private Matrix _viewMatrixInverted;
        private Vector3 _position;
        private Viewport _viewport;
        private Vector3 _direction;
        private Vector3 _right;
        private CameraProjectionMode _projectionMode;
        private float _scale; //HINT(deccer) in ortho mode, can be (ab)used to "zoom" in and out
        private float _nearPlane;
        private float _farPlane;
        private float _fieldOfView;

        public Vector3 Direction
        {
            get => _direction;
            set
            {
                _direction = value;
                UpdateViewMatrix();
            }
        }

        public float FarPlane
        {
            get => _farPlane;
            set
            {
                if (System.Math.Abs(_farPlane - value) < Constants.Epsilon)
                {
                    return;
                }
                _farPlane = value;
                UpdateProjectionMatrix();
            }
        }

        public float FieldOfView
        {
            get => _fieldOfView;
            set
            {
                if (System.Math.Abs(_fieldOfView - value) < Constants.Epsilon)
                {
                    return;
                }
                _fieldOfView = value;
                UpdateProjectionMatrix();
            }
        }

        public float NearPlane
        {
            get => _nearPlane;
            set
            {
                if (System.Math.Abs(_nearPlane - value) < Constants.Epsilon)
                {
                    return;
                }
                _nearPlane = value;
                UpdateProjectionMatrix();
            }
        }

        public Vector3 Position
        {
            get => _position;
            set
            {
                _position = value;
                UpdateViewMatrix();
            }
        }

        public Ray PickRay { get; private set; }

        public Matrix ProjectionMatrix { get; private set; }

        public CameraProjectionMode ProjectionMode
        {
            get => _projectionMode;
            set
            {
                if (_projectionMode == value)
                {
                    return;
                }
                _projectionMode = value;
                UpdateProjectionMatrix();
            }
        }

        public float Scale
        {
            get => _scale;
            set
            {
                if (System.Math.Abs(_scale - value) < Constants.Epsilon)
                {
                    return;
                }
                _scale = value;
                UpdateProjectionMatrix();
            }
        }

        public Matrix ViewMatrix => _viewMatrix;

        public Viewport Viewport
        {
            get => _viewport;
            set
            {
                if (_viewport == value)
                {
                    return;
                }
                _viewport = value;
                UpdateProjectionMatrix();
            }
        }

        public Camera(int windowWidth, int windowHeight)
        {
            _viewport = new Viewport(0, 0, windowWidth, windowHeight);
            _direction = -Vector3.UnitZ;
            _nearPlane = 0.1f;
            _farPlane = 16384f;
            _scale = 1.0f;
            _projectionMode = CameraProjectionMode.Perspective;
            _fieldOfView = MathUtil.PiOverFour;
            UpdateViewMatrix();
            UpdateProjectionMatrix();
        }

        public void MoveForward(float delta)
        {
            _position += _direction * delta;

            UpdateViewMatrix();
        }

        public void Lift(float delta)
        {
            var up = Vector3.Cross(_direction, _right);
            Vector3.Normalize(up);
            _position += up * delta;

            UpdateViewMatrix();
        }

        public void LookAt(Vector3 position)
        {
            var direction = position - Position;
            Vector3.Normalize(direction);

            Direction = direction;

            UpdateViewMatrix();
        }

        public void RotateMouseX(float delta)
        {
            var rotationMatrix = Matrix.RotationY(-delta);
            _direction = Vector3.TransformNormal(_direction, rotationMatrix);
            UpdateViewMatrix();
        }

        public void RotateMouseY(float delta)
        {
            var rotationMatrix = Matrix.RotationAxis(_right, delta);
            _direction = Vector3.TransformNormal(_direction, rotationMatrix);
            UpdateViewMatrix();
        }

        public void Strafe(float delta)
        {
            _position += _right * delta;

            UpdateViewMatrix();
        }

        private void UpdateProjectionMatrix()
        {
            switch (ProjectionMode)
            {
                case CameraProjectionMode.Perspective:
                    ProjectionMatrix = Matrix.PerspectiveFovLH(_fieldOfView, (float)_viewport.Width / _viewport.Height, _nearPlane, _farPlane);
                    break;
                case CameraProjectionMode.Orthogonal:
                    ProjectionMatrix = Matrix.Scaling(_scale) * Matrix.OrthoLH(_viewport.Width, _viewport.Height, _nearPlane, _farPlane);
                    break;
            }
        }

        private void UpdateViewMatrix()
        {
            _viewMatrix = Matrix.LookAtLH(_position, _position + _direction, Vector3.Up);
            Matrix.Invert(ref _viewMatrix, out _viewMatrixInverted);

            _right = _viewMatrixInverted.Right;

            var centerX = Viewport.Width / 2;
            var centerY = Viewport.Height / 2;

            PickRay = Ray.GetPickRay(centerX, centerY, Viewport, _viewMatrix * ProjectionMatrix);
        }

        public Vector3 WorldSpaceToScreenSpace(Vector3 worldspacePosition)
        {
            return Vector3.Project(worldspacePosition, Viewport.X, Viewport.Y, Viewport.Width, Viewport.Height, NearPlane, FarPlane, ViewMatrix * ProjectionMatrix);
        }
    }
}