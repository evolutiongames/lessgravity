﻿namespace LessGravity.Quasar.Game
{
    public enum CameraProjectionMode
    {
        Orthogonal,
        Perspective
    }
}