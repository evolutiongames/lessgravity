﻿using LessGravity.Quasar.Assets;
using LessGravity.Quasar.Extensions;
using LessGravity.Quasar.Graphics;
using LessGravity.Quasar.Input;
using SharpDX;
using SharpDX.Direct2D1;
using SharpDX.Direct3D;
using SharpDX.Direct3D11;
using System.Collections.Generic;
using System.Linq;
using LessGravity.Logging;
using Device = SharpDX.Direct3D11.Device;
using DeviceContext = SharpDX.Direct3D11.DeviceContext;

namespace LessGravity.Quasar.Game
{
    public abstract class QuasarGameBase : GameBase
    {
        private Device _device;
        private SharpDX.DXGI.SwapChain _swapChain;
        private SharpDX.DXGI.Factory1 _dxgiFactory;

        public AssetManager Assets { get; private set; }
        public Camera MainCamera { get; protected set; }
        public Ray MouseRay { get; private set; }

        public Texture BackBuffer { get; private set; }
        public RenderTarget BackBuffer2D { get; private set; }
        public Texture GBuffer1 { get; private set; }
        public Texture GBuffer2 { get; private set; }
        public Texture GBuffer3 { get; private set; }

        public Device Device { get { return _device; } }
        public DeviceContext DeviceContext { get; private set; }
        public Texture DepthBuffer { get; private set; }
        public Factory Direct2dFactory { get; private set; }
        public SharpDX.DirectWrite.Factory DirectWriteFactory { get; private set; }
        public Viewport Viewport { get; private set; }
        public SharpDX.WIC.ImagingFactory2 ImagingFactory { get; private set; }

        protected InputManager InputManager;

        public QuasarGameBase(ILogger logger, IFormFactory formFactory)
            : base(logger, formFactory)
        {
            
        }

        protected override void BeginDraw()
        {
            base.BeginDraw();

            //_device.ImmediateContext.Rasterizer.SetViewport(_viewport);
            //_device.ImmediateContext.OutputMerger.SetTargets(depthStencilView: _depthBuffer, renderTargetView: _backBuffer);

            //_backBuffer2d.BeginDraw();
        }

        private RenderTarget CreateRenderTarget2D()
        {
            using (var dxgiSurface = BackBuffer.NativeTexture.QueryInterface<SharpDX.DXGI.Surface>())
            {
                return new RenderTarget(Direct2dFactory, dxgiSurface, new RenderTargetProperties(new PixelFormat(_swapChain.Description.ModeDescription.Format, AlphaMode.Premultiplied)));
            }
        }

        private void CreateResources()
        {
            var backBufferWidth = RenderingSize.Width;
            var backBufferHeight = RenderingSize.Height;
            BackBuffer = new Texture(_device, Logger, _swapChain);
            DepthBuffer = new Texture(_device, Logger, backBufferWidth, backBufferHeight, SharpDX.DXGI.Format.D32_Float_S8X24_UInt, BindFlags.DepthStencil, 1, ResourceOptionFlags.None, "TEX DepthBuffer");

            BackBuffer2D = CreateRenderTarget2D();
            BackBuffer2D.AntialiasMode = AntialiasMode.PerPrimitive;

            GBuffer1 = new Texture(_device, Logger, backBufferWidth, backBufferHeight, SharpDX.DXGI.Format.R8G8B8A8_UNorm, BindFlags.RenderTarget | BindFlags.ShaderResource, 1, ResourceOptionFlags.None, "TEX GBuffer1");
            GBuffer2 = new Texture(_device, Logger, backBufferWidth, backBufferHeight, SharpDX.DXGI.Format.R32G32B32A32_Float, BindFlags.RenderTarget | BindFlags.ShaderResource, 1, ResourceOptionFlags.None, "TEX GBuffer2");
            GBuffer3 = new Texture(_device, Logger, backBufferWidth, backBufferHeight, SharpDX.DXGI.Format.R8G8B8A8_UNorm, BindFlags.RenderTarget | BindFlags.ShaderResource, 1, ResourceOptionFlags.None, "TEX GBuffer3");
        }

        private void DisposeResources()
        {
            GBuffer1.Dispose();
            GBuffer2.Dispose();
            GBuffer3.Dispose();
            BackBuffer2D.Dispose();
            BackBuffer.Dispose();
        }

        protected override void Dispose(bool disposeManagedResources)
        {
            var messages = GetInfoMessages();

            if (disposeManagedResources)
            {
                DisposeResources();
                InputManager.Dispose();

                Direct2dFactory.DisposeIfNotDisposed();
                DirectWriteFactory.DisposeIfNotDisposed();
                ImagingFactory.DisposeIfNotDisposed();
            }
            base.Dispose(disposeManagedResources);
        }

        public List<Message> GetInfoMessages()
        {
            var infoqueue = _device.QueryInterface<InfoQueue>();
            if (infoqueue != null)
            {
                var messages = new List<Message>();
                var messageCount = infoqueue.NumMessagesAllowedByStorageFilter;
                for (var i = 0; i < messageCount; ++i)
                {
                    messages.Add(infoqueue.GetMessage(i));
                }
                return messages;
            }
            return null;
        }

        public static SharpDX.DXGI.Adapter1 GetNVidiaAdapter(SharpDX.DXGI.Factory1 factory)
        {
            return factory.Adapters1.FirstOrDefault(adapter => adapter.Description.VendorId == 0x10DE);
        }

        public static SharpDX.DXGI.Adapter1 GetIntelAdapter(SharpDX.DXGI.Factory1 factory)
        {
            return factory.Adapters1.FirstOrDefault(adapter => adapter.Description.VendorId == 0x8086);
        }

        protected override void Initialize(GameSettings settings)
        {
            var swapChainDescription = new SharpDX.DXGI.SwapChainDescription
            {
                BufferCount = 1,
                ModeDescription = new SharpDX.DXGI.ModeDescription
                {
                    Format = SharpDX.DXGI.Format.R8G8B8A8_UNorm_SRgb,
                    Height = Settings.Height,
                    RefreshRate = new SharpDX.DXGI.Rational
                    {
                        Denominator = 1,
                        Numerator = 60
                    },
                    Width = Settings.Width,
                },
                IsWindowed = !settings.FullScreen,
                OutputHandle = DisplayHandle,
                Flags = SharpDX.DXGI.SwapChainFlags.AllowModeSwitch,
                SampleDescription = new SharpDX.DXGI.SampleDescription
                {
                    Count = 1,
                    Quality = 0
                },
                SwapEffect = SharpDX.DXGI.SwapEffect.Discard,
                Usage = SharpDX.DXGI.Usage.RenderTargetOutput
            };
            var deviceCreationFlags = DeviceCreationFlags.BgraSupport;
#if DEBUG
            deviceCreationFlags |= DeviceCreationFlags.Debug;
#endif
            Device.CreateWithSwapChain(DriverType.Hardware, deviceCreationFlags, swapChainDescription, out _device, out _swapChain);
            _device.DebugName = "DEV Main";
            DeviceContext = _device.ImmediateContext;
            DeviceContext.DebugName = "CTX Main";

            _dxgiFactory = _swapChain.GetParent<SharpDX.DXGI.Factory1>();
            _dxgiFactory.MakeWindowAssociation(DisplayHandle, SharpDX.DXGI.WindowAssociationFlags.IgnoreAll);

            Direct2dFactory = new Factory();
            DirectWriteFactory = new SharpDX.DirectWrite.Factory();
            ImagingFactory = new SharpDX.WIC.ImagingFactory2();

            Viewport = new Viewport(0, 0, Settings.Width, Settings.Height);

            InputManager = new InputManager(DisplayHandle);

            CreateResources();

            Services.AddService(_device);
            Services.AddService(DeviceContext);
            Services.AddService(_dxgiFactory);
            Services.AddService(Direct2dFactory);
            Services.AddService(DirectWriteFactory);
            Services.AddService(ImagingFactory);
            Services.AddService(InputManager);

            Assets = new AssetManager(_device, ImagingFactory, Logger, "Assets");
            Services.AddService(Assets);
        }

        protected override void EndDraw()
        {
            //_backBuffer2d.EndDraw();
            _swapChain.Present(Settings.VSync ? 1 : 0, SharpDX.DXGI.PresentFlags.None);
        }

        protected override void Resize()
        {
            base.Resize();

            if (RenderingSize.Width <= 1 || RenderingSize.Height <= 1)
            {
                return;
            }

            DisposeResources();

            _swapChain.ResizeBuffers(_swapChain.Description.BufferCount, RenderingSize.Width, RenderingSize.Height, SharpDX.DXGI.Format.Unknown, _swapChain.Description.Flags);
            Viewport = new Viewport(0, 0, RenderingSize.Width, RenderingSize.Height);

            CreateResources();
        }

        protected override void Update(GameTime gameTime)
        {
            base.Update(gameTime);

            //TODO(deccer) make this an option via some Configuration/Settings class
            var mouseX = InputManager.MousePositionX;
            var mouseY = InputManager.MousePositionY;

            if (MainCamera != null)
            {
                MouseRay = Ray.GetPickRay(mouseX, mouseY, Viewport, MainCamera.ViewMatrix * MainCamera.ProjectionMatrix);
            }
        }
    }
}