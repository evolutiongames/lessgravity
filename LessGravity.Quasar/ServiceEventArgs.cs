﻿using System;

namespace LessGravity.Quasar
{
    public class ServiceEventArgs : EventArgs
    {
        public Type Type { get; private set; }
        public object Service { get; private set; }

        public ServiceEventArgs(Type type, object service)
        {
            Type = type;
            Service = service;
        }
    }
}