﻿using System;
using System.IO;

namespace LessGravity.Quasar
{
    internal static class FileHelpers
    {
        public const char BackwardSlash = '\\';

        public const char ForwardSlash = '/';

        public static readonly string ForwardSlashString = new string(ForwardSlash, 1);

        public static readonly char NotSeparator = Path.DirectorySeparatorChar == BackwardSlash ? ForwardSlash : BackwardSlash;

        public static readonly char Separator = Path.DirectorySeparatorChar;

        public static string NormalizeFilePathSeparators(string name)
        {
            return name.Replace(NotSeparator, Separator);
        }

        public static string ResolveRelativePath(string filePath, string relativeFile)
        {
            filePath = filePath.Replace(BackwardSlash, ForwardSlash);
            var startsWithForwardSlash = filePath.StartsWith(ForwardSlashString, StringComparison.Ordinal);
            if (!startsWithForwardSlash)
            {
                filePath = ForwardSlashString + filePath;
            }
            var baseUri = new Uri("file://" + filePath);
            var uri = new Uri(baseUri, relativeFile);
            var text = uri.LocalPath;
            if (!startsWithForwardSlash && text.StartsWith("/", StringComparison.Ordinal))
            {
                text = text.Substring(1);
            }
            return NormalizeFilePathSeparators(text);
        }
    }
}