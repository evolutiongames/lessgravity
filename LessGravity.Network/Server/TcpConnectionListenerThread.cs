﻿using System;
using System.Diagnostics;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using LessGravity.Network.Server.Exceptions;
using LessGravity.Network.Shared;

namespace LessGravity.Network.Server
{
    public class TcpConnectionListenerThread
    {
        private Thread _listeningThread;
        private volatile Socket _socket;
        private int _backLog;
        private readonly IPEndPoint _localEp;
        public event Action<SocketException> Error;

        public event Action<Socket> NewConnection;

        public event Action<Socket> StartedListening;

        public event Action<Socket> StoppedListening;

        public int BackLog
        {
            get { return _backLog; }
            set
            {
                if (_socket != null)
                {
                    throw new InvalidOperationException("Can not set BackLog while listening");
                }
                _backLog = value;
            }
        }

        public TcpConnectionListenerThread(int port)
            : this(16, port) {}

        public TcpConnectionListenerThread(int backLog, int port)
        {
            _backLog = backLog;
            _localEp = new IPEndPoint(IPAddress.Any, port);
        }

        public TcpConnectionListenerThread(string address, int port)
        {
            _localEp = new IPEndPoint(IPAddress.Parse(address), port);
        }

        public void StartListening()
        {
            if (_socket != null)
            {
                throw new InvalidOperationException("Already Listening");
            }

            _socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.IP);
            _socket.ReceiveBufferSize = Constants.BufferSize;
            _socket.SendBufferSize = Constants.BufferSize;
            _socket.Bind(_localEp);
            _socket.Listen(BackLog);
            _listeningThread = new Thread(ListeningProc);
            _listeningThread.Start(_socket);
        }

        public void StopListening()
        {
            var socket = _socket;
            if (socket == null)
            {
                throw new InvalidOperationException("Not Listening");
            }
            _socket = null;

            OnStoppedListening(socket);
            socket.Dispose();
        }

        protected virtual void OnError(SocketException exception)
        {
            if (Error != null)
            {
                Error(exception);
            }
        }

        protected virtual void OnNewConnection(Socket socket)
        {
            if (NewConnection == null)
            {
                throw new ListenerException("NewConnection Event needs to be handled");
            }
            NewConnection(socket);
        }

        protected virtual void OnStartedListening(Socket socket)
        {
            if (StartedListening != null)
            {
                StartedListening(socket);
            }
        }

        protected virtual void OnStoppedListening(Socket socket)
        {
            if (StoppedListening != null)
            {
                StoppedListening(socket);
            }
        }

        private void ListeningProc(object state)
        {
            var socket = (Socket)state;

            OnStartedListening(socket);

            Debug.WriteLine("TcpConnectionListener: Listener Thread Started");

            while (true)
            {
                try
                {
                    var clientSocket = socket.Accept();
                    Debug.WriteLine("TcpConnectionListener: Incoming connection from " + clientSocket.RemoteEndPoint);
                    ThreadPool.QueueUserWorkItem(s => OnNewConnection((Socket)s), clientSocket);
                }
                catch (SocketException ex)
                {
                    //If Exception isnt caused by the socket beeing Disposed
                    if (_socket != null)
                    {
                        OnError(ex);
                    }
                    else
                        break;
                }
            }
            Debug.WriteLine("TcpConnectionListener: Listener Thread Terminated");
        }
    }
}