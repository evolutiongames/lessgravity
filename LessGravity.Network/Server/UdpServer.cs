﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using LessGravity.Network.Server.Helpers;
using LessGravity.Network.Shared;

namespace LessGravity.Network.Server
{
    public class UdpServer<T> where T : UdpConnection
    {
        private readonly byte[] _receiveBuffer;
        private readonly MemoryStream _receiveStream;
        private readonly IUdpConnectionAcceptor _connectionAcceptor;
        private readonly IUdpConnectionFactory<T> _connectionFactory;
        private readonly List<T> _connections = new List<T>();
        private readonly BinaryReader _receiveReader;
        private Socket _socket;
        private EndPoint _receiveEndPoint;

        public event Action<T, BinaryReader> Received;

        public int Port { get; private set; }

        public UdpServer(int port, IUdpConnectionAcceptor connectionAcceptor, IUdpConnectionFactory<T> connectionFactory)
        {
            if (connectionAcceptor == null)
            {
                throw new ArgumentNullException("connectionAcceptor");
            }
            if (connectionFactory == null)
            {
                throw new ArgumentNullException("connectionFactory");
            }
            if (port <= 0 || port >= ushort.MaxValue)
            {
                throw new ArgumentException("port must be between 1 and " + ushort.MaxValue);
            }

            Port = port;
            _connectionAcceptor = connectionAcceptor;
            _connectionFactory = connectionFactory;
            _receiveEndPoint = new IPEndPoint(IPAddress.Any, Port);

            _receiveBuffer = new byte[Constants.BufferSize];
            _receiveStream = new MemoryStream(_receiveBuffer);
            _receiveReader = new BinaryReader(_receiveStream);
        }

        public void StartListening()
        {
            if (_socket != null)
            {
                throw new InvalidOperationException("Already Working");
            }

            _socket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.IP);
            _socket.Bind(new IPEndPoint(IPAddress.Any, Port));
        }

        public void StopListening()
        {
            if (_socket == null)
                throw new InvalidOperationException("Not Working");

            _socket.Dispose();
            _socket = null;
        }

        public void Update()
        {
            if (_socket == null)
            {
                throw new InvalidOperationException("Not Working");
            }

            if (_socket.Available <= 0)
            {
                return;
            }
            var received = _socket.ReceiveFrom(_receiveBuffer, ref _receiveEndPoint);
            var endPoint = (IPEndPoint)_receiveEndPoint;

            if (received == -1)
            {
                return;
            }
            _receiveStream.Position = 0;
            Debug.WriteLine("UdpServer: Received {0}B from {1}", received, endPoint);

            var connection = _connections.FirstOrDefault(c => Equals(c.RemoteEndPoint, endPoint));
            if (connection == null)
            {
                var accept = _connectionAcceptor.Accept(endPoint, _receiveReader);

                Debug.WriteLine("UdpServer: New Connection from {0} {1}", endPoint, (accept ? " accepted" : " rejected"));

                if (accept)
                {
                    connection = _connectionFactory.CreateConnection(endPoint);
                    _connections.Add(connection);
                }
            }

            if (connection != null)
            {
                OnReceived(connection, _receiveReader);
            }
        }

        protected virtual void OnReceived(T udpConnection, BinaryReader reader)
        {
            var handler = Received;
            if (handler != null)
            {
                handler(udpConnection, reader);
            }
        }
    }

    public class UdpServer : UdpServer<UdpConnection>
    {
        public UdpServer(int port) 
            : base(port, UdpConnectionAcceptorAlways.Instance, DefaultUdpConnectionFactory.Instance) {}
    }
}