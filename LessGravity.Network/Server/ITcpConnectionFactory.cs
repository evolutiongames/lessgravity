﻿using System.Net.Sockets;
using LessGravity.Network.Shared;

namespace LessGravity.Network.Server
{
    public interface ITcpConnectionFactory<out T> where T : TcpConnection
    {
        T CreateConnection(Socket socket);
    }
}