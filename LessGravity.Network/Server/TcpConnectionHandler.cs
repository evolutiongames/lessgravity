﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using LessGravity.Network.Server.Helpers;
using LessGravity.Network.Shared;

namespace LessGravity.Network.Server
{
    public class TcpConnectionHandler<T> where T : TcpConnection
    {
        private readonly ITcpConnectionFactory<T> _tcpConnectionFactory;
        private readonly ITcpConnectionAcceptor _tcpConnectionAcceptor;
        private readonly TcpConnectionListenerThread _listenerThread;

        private readonly object _connectionHandlersLock = new object();
        private readonly List<TcpConnectionHandlerThread<T>> _connectionHandlerThreads = new List<TcpConnectionHandlerThread<T>>();
        private bool _isWorking;

        public event Action<T> Disconnected;
        public event Action<T> NewConnection;
        public event Action<T, BinaryReader> Received;

        public int WorkDelay { get; private set; }
        public int ConnectionsPerWorker { get; private set; }

        public TcpConnectionHandler(ITcpConnectionFactory<T> tcpConnectionFactory, ITcpConnectionAcceptor tcpConnectionAcceptor,
            int backLog,
            int port, int workDelay = 10, int connectionsPerWorker = 100)
        {
            if (tcpConnectionFactory == null)
            {
                throw new ArgumentNullException("tcpConnectionFactory");
            }
            if (tcpConnectionAcceptor == null)
            {
                throw new ArgumentNullException("tcpConnectionAcceptor");
            }
            if (backLog <= 0)
            {
                throw new ArgumentException("backLog must be bigger than 0", "backLog");
            }
            if (port <= 0 || port > ushort.MaxValue)
            {
                throw new ArgumentException("port must be between 1 and " + short.MaxValue);
            }
            if (workDelay < 0)
            {
                throw new ArgumentException("WorkDelay must be >= 0", "workDelay");
            }
            if (connectionsPerWorker <= 0)
            {
                throw new ArgumentException("ConnectionsPerWorker must be bigger than 0", "connectionsPerWorker");
            }

            _tcpConnectionFactory = tcpConnectionFactory;
            _tcpConnectionAcceptor = tcpConnectionAcceptor;

            ConnectionsPerWorker = connectionsPerWorker;
            WorkDelay = workDelay;

            _listenerThread = new TcpConnectionListenerThread(backLog, port);
            _listenerThread.NewConnection += ListenerThreadOnNewConnection;
        }

        public void StartListening()
        {
            if (_isWorking)
            {
                throw new InvalidOperationException("Already Listening");
            }

            _isWorking = true;
            _listenerThread.StartListening();
        }

        public void StopListening()
        {
            if (!_isWorking)
            {
                return;
                //throw new InvalidOperationException("Not Listening");
            }

            _listenerThread.StopListening();

            _isWorking = false;

            lock (_connectionHandlersLock)
            {
                foreach (var handlerThread in _connectionHandlerThreads)
                {
                    handlerThread.StopWorking();
                }
                _connectionHandlerThreads.Clear();
            }
        }

        protected virtual void OnDisconnected(T connection)
        {
            if (Disconnected != null)
            {
                Disconnected(connection);
            }
        }

        protected virtual void OnNewConnection(T connection)
        {
            if (NewConnection != null)
            {
                NewConnection(connection);
            }
        }

        protected virtual void OnReceived(T connection, BinaryReader reader)
        {
            if (Received != null)
            {
                Received(connection, reader);
            }
        }

        private void HandlerOnShuttingDown(TcpConnectionHandlerThread<T> handler)
        {
            lock (_connectionHandlersLock)
            {
                _connectionHandlerThreads.Remove(handler);
            }
        }

        private void ListenerThreadOnNewConnection(Socket socket)
        {
            if (!_tcpConnectionAcceptor.AcceptConnection(socket))
            {
                return;
            }

            var connection = _tcpConnectionFactory.CreateConnection(socket);

            OnNewConnection(connection);

            lock (_connectionHandlersLock)
            {
                var handler = _connectionHandlerThreads.FirstOrDefault(t => t.ConnectionCount < ConnectionsPerWorker);
                if (handler == null)
                {
                    Debug.WriteLine("TcpConnectionHandler: No free ConnectionHandlers found, creating new one");

                    handler = new TcpConnectionHandlerThread<T>(WorkDelay, 1000);
                    _connectionHandlerThreads.Add(handler);

                    handler.Received += OnReceived;
                    handler.Disconnected += OnDisconnected;
                    handler.ShuttingDown += HandlerOnShuttingDown;

                    handler.AddConnection(connection);
                    handler.StartWorking();
                }
                else
                {
                    Debug.WriteLine("TcpConnectionHandler: Free ConnectionHandlers found, adding connection");
                    handler.AddConnection(connection);
                }
            }
        }
    }

    public class TcpConnectionHandler : TcpConnectionHandler<TcpConnection>
    {
        public TcpConnectionHandler(int port, int workDelay = 10, int connectionsPerWorker = 100)
            : this(16, port, workDelay, connectionsPerWorker) {}

        public TcpConnectionHandler(int backLog, int port, int workDelay = 10, int connectionsPerWorker = 100)
            : base(
                DefaultTcpConnectionFactory.Instance, TcpConnectionAcceptorAlways.Instance, backLog, port, workDelay,
                connectionsPerWorker) {}
    }
}