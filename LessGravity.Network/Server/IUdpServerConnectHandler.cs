﻿using System.IO;
using System.Net;

namespace LessGravity.Network.Server
{
    public interface IUdpConnectionAcceptor
    {
        bool Accept(IPEndPoint endPoint, BinaryReader reader);
    }
}