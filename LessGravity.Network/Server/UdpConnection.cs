﻿using System.Net;

namespace LessGravity.Network.Server
{
    public class UdpConnection
    {
        internal IPEndPoint RemoteEndPoint { get; private set; }

        public UdpConnection(IPEndPoint remoteEndPoint)
        {
            RemoteEndPoint = remoteEndPoint;
        }

        public override string ToString()
        {
            return RemoteEndPoint.ToString();
        }
    }
}