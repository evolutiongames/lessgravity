﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Threading;
using LessGravity.Network.Shared;

namespace LessGravity.Network.Server
{
    internal class TcpConnectionHandlerThread<T> where T : TcpConnection
    {
        private static int _threadCount;

        private readonly object _connectionsLock = new object();
        private readonly object _newConnectionsLock = new object();

        private readonly List<T> _connections = new List<T>();
        private readonly List<T> _newConnections = new List<T>();

        private readonly ManualResetEventSlim _workerTerminatedEvent = new ManualResetEventSlim();
        private readonly int _maxNoConRepeats;

        private readonly byte[] _receiveBuffer;
        private readonly BinaryReader _receiveReader;
        private volatile bool _isWorking;
        private int _noConCount;
        private MemoryStream _receiveStream;

        public event Action<T> Disconnected;

        public event Action<T, BinaryReader> Received;
        public event Action<TcpConnectionHandlerThread<T>> ShuttingDown;

        public int ConnectionCount { get; private set; }
        public int WorkDelay { get; private set; }
        public string Name { get; private set; }

        public TcpConnectionHandlerThread(int workDelay, int maxIdleTime)
        {
            WorkDelay = workDelay;
            Name = string.Format("{0} {1}", GetType().Name, _threadCount++);
            _maxNoConRepeats = maxIdleTime / workDelay;

            _receiveBuffer = new byte[Constants.BufferSize];
            _receiveStream = new MemoryStream(_receiveBuffer);
            _receiveReader = new BinaryReader(_receiveStream);
        }

        public void AddConnection(T connection)
        {
            ConnectionCount++;
            if (Monitor.TryEnter(_connectionsLock))
            {
                try
                {
                    _connections.Add(connection);
                    Debug.WriteLine("{0}: New Connection from {1} added to _connections", Name, connection.RemoteEndPoint);
                }
                finally
                {
                    Monitor.Exit(_connectionsLock);
                }
            }
            else
            {
                lock (_newConnectionsLock)
                {
                    _newConnections.Add(connection);

                    Debug.WriteLine("{0}: New Connection from {1} added to _newConnections queue", Name, connection.RemoteEndPoint);
                }
            }
        }

        public void StartWorking()
        {
            if (_isWorking)
            {
                throw new InvalidOperationException("Already Working");
            }

            _isWorking = true;
            _workerTerminatedEvent.Reset();
            var thread = new Thread(WorkerProc)
            {
                Name = Name
            };
            thread.Start();
        }

        public void StopWorking()
        {
            if (!_isWorking)
            {
                throw new InvalidOperationException("Not Working");
            }

            _isWorking = false;
            _workerTerminatedEvent.Wait();
        }

        protected virtual void OnDisconnected(T connection)
        {
            var handler = Disconnected;
            if (handler != null)
            {
                handler(connection);
            }
        }

        protected virtual void OnReceived(T connection, BinaryReader reader)
        {
            var handler = Received;
            if (handler != null)
            {
                handler(connection, reader);
            }
        }

        protected virtual void OnShuttingDown(TcpConnectionHandlerThread<T> connectionHandlerThread)
        {
            var handler = ShuttingDown;
            if (handler != null)
            {
                handler(connectionHandlerThread);
            }
        }

        private void CloseConnections()
        {
            lock (_connectionsLock)
            {
                foreach (var connection in _connections)
                {
                    connection.Dispose();
                }
            }
        }

        private bool WorkConnections(byte[] buffer)
        {
            lock (_connectionsLock)
            {
                lock (_newConnectionsLock)
                {
                    if (_newConnections.Count > 0)
                    {
                        _connections.AddRange(_newConnections);
                        _newConnections.Clear();
                    }
                }

                for (var i = 0; i < _connections.Count; i++)
                {
                    var connection = _connections[i];
                    if (connection.TryReceive(buffer) > 0)
                    {
                        //TODO(deccer): check/test if resetting the position is necessary. its fixing the issue
                        // when a message only comes through once
                        _receiveReader.BaseStream.Position = 0;
                        OnReceived(connection, _receiveReader);
                    }
                    connection.SendData();

                    if (connection.Connected)
                    {
                        continue;
                    }
                    Debug.WriteLine("TcpConnectionHandlerThread: Connection to {0} lost", connection);
                    OnDisconnected(_connections[i]);
                    connection.Dispose();
                    _connections.RemoveAt(i);
                    ConnectionCount--;
                }
                return _connections.Count > 0;
            }
        }

        private void WorkerProc()
        {
            Debug.WriteLine("{0}: ConnectionWorker Started", Name);

            while (_isWorking)
            {
                var hasConnections = WorkConnections(_receiveBuffer);
                if (!hasConnections && _noConCount++ > _maxNoConRepeats)
                {
                    Debug.WriteLine("{0}: has no more Connections to Handle, shutting down", Name);
                    OnShuttingDown(this);
                    _isWorking = false;
                    _workerTerminatedEvent.Set();
                    return;
                }
                if (hasConnections)
                {
                    _noConCount = 0;
                }
                Thread.Sleep(WorkDelay);
            }

            WorkConnections(_receiveBuffer);

            Debug.WriteLine("{0}: Closing Connections", Name);
            CloseConnections();

            _workerTerminatedEvent.Set();
            Debug.WriteLine("{0}: ConnectionWorker Terminated", Name);
        }
    }
}