using System;
using System.Net.Sockets;

namespace LessGravity.Network.Server.Helpers
{
    public sealed class LambdaTcpConnectionAcceptor : ITcpConnectionAcceptor
    {
        private readonly Predicate<Socket> _acceptorFunc;

        public LambdaTcpConnectionAcceptor(Predicate<Socket> acceptorFunc)
        {
            if (acceptorFunc == null)
            {
                throw new ArgumentNullException("acceptorFunc");
            }
            _acceptorFunc = acceptorFunc;
        }

        public bool AcceptConnection(Socket socket)
        {
            return _acceptorFunc(socket);
        }
    }
}