﻿using System.Net;

namespace LessGravity.Network.Server.Helpers
{
    public class DefaultUdpConnectionFactory : IUdpConnectionFactory<UdpConnection>
    {
        public static readonly DefaultUdpConnectionFactory Instance = new DefaultUdpConnectionFactory();

        private DefaultUdpConnectionFactory() {}

        public UdpConnection CreateConnection(IPEndPoint endPoint)
        {
            return new UdpConnection(endPoint);
        }
    }
}