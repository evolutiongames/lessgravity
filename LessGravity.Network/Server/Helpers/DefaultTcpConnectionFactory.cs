﻿using System.Net.Sockets;
using LessGravity.Network.Shared;

namespace LessGravity.Network.Server.Helpers
{
    public sealed class DefaultTcpConnectionFactory : ITcpConnectionFactory<TcpConnection>
    {
        public static readonly DefaultTcpConnectionFactory Instance = new DefaultTcpConnectionFactory();

        private DefaultTcpConnectionFactory() {}

        public TcpConnection CreateConnection(Socket socket)
        {
            return new TcpConnection(socket);
        }
    }
}