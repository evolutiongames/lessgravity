﻿using System;
using System.Net.Sockets;
using LessGravity.Network.Shared;

namespace LessGravity.Network.Server.Helpers
{
    public sealed class LambdaTcpConnectionFactory<T> : ITcpConnectionFactory<T> where T : TcpConnection
    {
        private readonly Func<Socket, T> _factoryFunc;

        public LambdaTcpConnectionFactory(Func<Socket, T> factoryFunc)
        {
            if (factoryFunc == null)
            {
                throw new ArgumentNullException("factoryFunc");
            }
            _factoryFunc = factoryFunc;
        }

        public T CreateConnection(Socket socket)
        {
            return _factoryFunc(socket);
        }
    }
}