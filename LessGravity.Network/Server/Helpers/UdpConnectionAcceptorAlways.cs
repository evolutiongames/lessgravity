﻿using System.IO;
using System.Net;

namespace LessGravity.Network.Server.Helpers
{
    public class UdpConnectionAcceptorAlways : IUdpConnectionAcceptor
    {
        public static readonly UdpConnectionAcceptorAlways Instance = new UdpConnectionAcceptorAlways();

        private UdpConnectionAcceptorAlways() {}

        public bool Accept(IPEndPoint endPoint, BinaryReader reader)
        {
            return true;
        }
    }
}