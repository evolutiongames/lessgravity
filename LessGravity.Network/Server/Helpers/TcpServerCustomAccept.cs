﻿using LessGravity.Network.Shared;

namespace LessGravity.Network.Server.Helpers
{
    public sealed class TcpServerCustomAccept : TcpServer<TcpConnection>
    {
        public TcpServerCustomAccept(ITcpConnectionAcceptor tcpConnectionAcceptor, int backLog, int port)
            : base(DefaultTcpConnectionFactory.Instance, tcpConnectionAcceptor, backLog, port) {}
    }
}