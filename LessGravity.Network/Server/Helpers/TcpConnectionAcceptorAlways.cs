using System.Net.Sockets;

namespace LessGravity.Network.Server.Helpers
{
    public sealed class TcpConnectionAcceptorAlways : ITcpConnectionAcceptor
    {
        public static readonly TcpConnectionAcceptorAlways Instance = new TcpConnectionAcceptorAlways();

        private TcpConnectionAcceptorAlways() {}

        public bool AcceptConnection(Socket socket)
        {
            return true;
        }
    }
}