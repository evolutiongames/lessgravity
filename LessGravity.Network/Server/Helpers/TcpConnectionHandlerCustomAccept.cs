﻿using LessGravity.Network.Shared;

namespace LessGravity.Network.Server.Helpers
{
    public sealed class TcpConnectionHandlerCustomAccept : TcpConnectionHandler<TcpConnection>
    {
        public TcpConnectionHandlerCustomAccept(ITcpConnectionAcceptor tcpConnectionAcceptor, int backLog, int port)
            : base(DefaultTcpConnectionFactory.Instance, tcpConnectionAcceptor, backLog, port) {}
    }
}