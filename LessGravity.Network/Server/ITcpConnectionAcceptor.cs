﻿using System.Net.Sockets;

namespace LessGravity.Network.Server
{
    public interface ITcpConnectionAcceptor
    {
        bool AcceptConnection(Socket socket);
    }
}