﻿using System;

namespace LessGravity.Network.Server.Exceptions
{
    public class ListenerException : Exception
    {
        public ListenerException(string message) : base(message) {}
    }
}