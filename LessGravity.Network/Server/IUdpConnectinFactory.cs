﻿using System.Net;

namespace LessGravity.Network.Server
{
    public interface IUdpConnectionFactory<out T> where T : UdpConnection
    {
        T CreateConnection(IPEndPoint endPoint);
    }
}