﻿using System;
using System.IO;
using LessGravity.Network.Server.Helpers;
using LessGravity.Network.Shared;

namespace LessGravity.Network.Server
{
    public class TcpServer<T> where T : TcpConnection
    {
        private readonly TcpConnectionHandler<T> _connectionHandler;

        public event Action<T> Disconnected
        {
            add { _connectionHandler.Disconnected += value; }
            remove { _connectionHandler.Disconnected -= value; }
        }

        public event Action<T> NewConnection
        {
            add { _connectionHandler.NewConnection += value; }
            remove { _connectionHandler.NewConnection -= value; }
        }

        public event Action<T, BinaryReader> DataReceived
        {
            add { _connectionHandler.Received += value; }
            remove { _connectionHandler.Received -= value; }
        }

        public TcpServer(ITcpConnectionFactory<T> tcpConnectionFactory, ITcpConnectionAcceptor tcpConnectionAcceptor, int backLog,
            int port)
        {
            _connectionHandler = new TcpConnectionHandler<T>(tcpConnectionFactory, tcpConnectionAcceptor, backLog, port);
        }

        public void Start()
        {
            _connectionHandler.StartListening();
        }

        public void Stop()
        {
            _connectionHandler.StopListening();
        }
    }

    public class TcpServer : TcpServer<TcpConnection>
    {
        public TcpServer(int backLog, int port)
            : base(DefaultTcpConnectionFactory.Instance, TcpConnectionAcceptorAlways.Instance, backLog, port) {}
    }
}