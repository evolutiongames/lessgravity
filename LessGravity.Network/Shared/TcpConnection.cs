﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Reflection;
using System.Threading;

namespace LessGravity.Network.Shared
{
    public class TcpConnection : IDisposable
    {
        private static readonly byte[] SendBuffer = new byte[Constants.BufferSize];
        private static readonly FieldInfo ExposableField;

        private readonly List<Stream> _sendQueue = new List<Stream>();
        private readonly Socket _socket;

        public EndPoint RemoteEndPoint
        {
            get { return _socket.RemoteEndPoint; }
        }

        public bool Connected
        {
            get { return !(!_socket.Connected || (_socket.Poll(0, SelectMode.SelectRead) && (_socket.Available == 0))); }
        }

        static TcpConnection()
        {
            ExposableField = typeof(MemoryStream).GetField("_exposable", BindingFlags.Instance | BindingFlags.NonPublic);
        }

        public TcpConnection(Socket socket)
        {
            _socket = socket;
        }

        public void Dispose()
        {
            Debug.WriteLine("TcpConnection: {0} disconnected", _socket.RemoteEndPoint);
            GC.SuppressFinalize(this);
            _socket.Dispose();
        }

        public void Disconnect()
        {
            if (!Connected)
            {
                throw new InvalidOperationException("Not Connected");
            }

            _socket.Shutdown(SocketShutdown.Both);
            _socket.Disconnect(false);
            Thread.Sleep(20);
            _socket.Dispose();

            lock (_sendQueue)
            {
                _sendQueue.Clear();
            }
        }

        public void Send(Stream dataStream)
        {
            lock (_sendQueue)
            {
                _sendQueue.Add(dataStream);
            }
        }

        public void Send(byte[] bytes)
        {
            Send(new MemoryStream(bytes));
        }

        public void SendData()
        {
            lock (_sendQueue)
            {
                foreach (var stream in _sendQueue)
                {
                    byte[] buffer;
                    int count;
                    var memStream = stream as MemoryStream;
                    if (memStream != null && (bool)ExposableField.GetValue(memStream))
                    {
                        buffer = memStream.GetBuffer();
                        count = (int)memStream.Length;
                    }
                    else
                    {
                        buffer = SendBuffer; 
                        //
                        //TODO(deccer): add some smart way (strategy pattern?) to dynamically resize buffers, 
                        //or increase their size up to a certain size
                        //
                        stream.Read(SendBuffer, 0, (int)stream.Length);
                        count = (int)stream.Length;
                    }
                    _socket.Send(buffer, count, 0);

                    Debug.WriteLine("TcpConnection: {0} sent {1} Bytes of data", this, count);
                }
                _sendQueue.Clear();
                //TODO(deccer): check for memory leaks (streams)
            }
        }

        public override string ToString()
        {
            return string.Format("{0} -> {1}", _socket.LocalEndPoint, _socket.RemoteEndPoint);
        }

        public int TryReceive(byte[] buffer)
        {
            if (_socket.Available <= 0)
            {
                return 0;
            }

            var receivedBytes = _socket.Receive(buffer);
            Debug.WriteLine("TcpConnection: Received {0} Bytes from {1}", receivedBytes, _socket.RemoteEndPoint);

            return receivedBytes;
        }
    }
}