﻿namespace LessGravity.Network.Shared
{
    public interface IClientConnectHandler
    {
        bool Connect(string address, int port);
    }
}