﻿using System;
using System.Net.Sockets;
using LessGravity.Network.Shared;

namespace LessGravity.Network.Client
{
    public class UdpClient : IDisposable
    {
        private readonly IClientConnectHandler _connectHandler;
        protected readonly Socket Socket;
        public bool IsConnected { get; private set; }

        public void Dispose()
        {
            if (Socket != null)
            {
                Socket.Dispose();
            }
        }

        public UdpClient(Socket socket, IClientConnectHandler connectHandler)
        {
            if (socket == null)
            {
                throw new ArgumentNullException("socket");
            }
            if (connectHandler == null)
            {
                throw new ArgumentNullException("connectHandler");
            }

            _connectHandler = connectHandler;
            Socket = socket;
        }

        public bool Connect(string address, int port)
        {
            if (port <= 0 || port >= ushort.MaxValue)
            {
                throw new ArgumentException("port must be between 1 and " + ushort.MaxValue);
            }

            return IsConnected = _connectHandler.Connect(address, port);
        }
    }
}