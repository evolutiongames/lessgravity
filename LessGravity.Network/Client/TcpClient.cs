﻿using System;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using LessGravity.Network.Shared;

namespace LessGravity.Network.Client
{
    public class TcpClient : IDisposable
    {
        private readonly byte[] _receiveBuffer;
        private readonly ManualResetEventSlim _workerTerminatedEvent;
        private TcpConnection _connection;
        private bool _connected;
        private readonly MemoryStream _receiveStream;
        private readonly BinaryReader _receiveReader;

        public event Action<BinaryReader> DataReceived;

        public EndPoint ServerEndPoint
        {
            get { return _connection == null ? null : _connection.RemoteEndPoint; }
        }

        public TcpClient()
        {
            _workerTerminatedEvent = new ManualResetEventSlim();

            _receiveBuffer = new byte[Constants.BufferSize];
            _receiveStream = new MemoryStream(_receiveBuffer);
            _receiveReader = new BinaryReader(_receiveStream);
        }

        public void Dispose()
        {
            if (_connected)
            {
                Disconnect();
            }
            _receiveStream.Dispose();
        }

        public void Connect(string address, int port)
        {
            if (port <= 0 || port > ushort.MaxValue)
            {
                throw new ArgumentException("port must be between 1 and " + short.MaxValue);
            }

            if (_connection != null)
            {
                throw new InvalidOperationException("Already Connected");
            }

            var socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.IP);
            socket.ReceiveBufferSize = Constants.BufferSize;
            socket.SendBufferSize = Constants.BufferSize;
            socket.Connect(IPAddress.Parse(address), port);
            _connection = new TcpConnection(socket);
            _connected = true;

            _workerTerminatedEvent.Reset();

            var thread = new Thread(ThreadProc);
            thread.Start();
        }

        public void Disconnect()
        {
            if (_connection == null)
            {
                throw new InvalidOperationException("Not Connected");
            }

            _connected = false;
            _workerTerminatedEvent.Wait();

            Debug.WriteLine("TcpClient: Disconnected");
        }

        public void Send(byte[] data)
        {
            _connection.Send(new MemoryStream(data, 0, data.Length, false, true));
        }

        protected virtual void OnDataReceived(BinaryReader reader)
        {
            var handler = DataReceived;
            if (handler != null)
            {
                handler(reader);
            }
        }

        private void ThreadProc()
        {
            while (_connected)
            {
                _connection.SendData();

                var count = _connection.TryReceive(_receiveBuffer);
                if (count > 0)
                {
                    //TODO(deccer): check if this is really necessary, or can be done better
                    _receiveReader.BaseStream.Position = 0;
                    OnDataReceived(_receiveReader);
                }

                Thread.Sleep(10);
            }
            _connection.Dispose();
            _connection = null;
            _workerTerminatedEvent.Set();
        }
    }
}