﻿using Newtonsoft.Json;
using System;
using System.IO;
using System.Windows.Forms;

namespace LessGravity.Sun.Helpers
{
    [Serializable]
    public class Settings
    {
        private const string SettingsFile = "Settings.json";

        public int WindowTop { get; set; }

        public int WindowLeft { get; set; }

        public int WindowWidth { get; set; }

        public int WindowHeight { get; set; }

        public FormWindowState WindowState { get; set; }

        public int ListViewColumnType { get; set; }
        public int ListViewColumnTimestamp { get; set; }
        public int ListViewColumnProcessId { get; set; }
        public int ListViewColumnThreadId { get; set; }
        public int ListViewColumnApplication { get; set; }
        public int ListViewColumnHostname { get; set; }
        public int ListViewColumnCategory { get; set; }
        public int ListViewColumnMessage { get; set; }


        public Settings()
        {
            SizeToFit();
            //MoveIntoView();
        }

        public void Load()
        {
            if (!File.Exists(SettingsFile))
            {
                WindowTop = Properties.Settings.Default.WindowTop;
                WindowLeft = Properties.Settings.Default.WindowLeft;
                WindowHeight = Properties.Settings.Default.WindowHeight;
                WindowWidth = Properties.Settings.Default.WindowWidth;
                WindowState = Properties.Settings.Default.WindowState;
                ListViewColumnType = Properties.Settings.Default.ListViewColumnType;
                ListViewColumnTimestamp = Properties.Settings.Default.ListViewColumnTimestamp;
                ListViewColumnProcessId = Properties.Settings.Default.ListViewColumnProcessId;
                ListViewColumnThreadId = Properties.Settings.Default.ListViewColumnThreadId;
                ListViewColumnApplication = Properties.Settings.Default.ListViewColumnApplication;
                ListViewColumnHostname = Properties.Settings.Default.ListViewColumnHostname;
                ListViewColumnCategory = Properties.Settings.Default.ListViewColumnCategory;
                ListViewColumnMessage = Properties.Settings.Default.ListViewColumnMessage;
            }
            else
            {
                var settingsFile = File.ReadAllText(SettingsFile);
                var settings = JsonConvert.DeserializeObject<Settings>(settingsFile);
                WindowTop = settings.WindowTop;
                WindowLeft = settings.WindowLeft;
                WindowHeight = settings.WindowHeight;
                WindowWidth = settings.WindowWidth;
                WindowState = settings.WindowState;
                ListViewColumnType = settings.ListViewColumnType;
                ListViewColumnTimestamp = settings.ListViewColumnTimestamp;
                ListViewColumnProcessId = settings.ListViewColumnProcessId;
                ListViewColumnThreadId = settings.ListViewColumnThreadId;
                ListViewColumnApplication = settings.ListViewColumnApplication;
                ListViewColumnHostname = settings.ListViewColumnHostname;
                ListViewColumnCategory = settings.ListViewColumnCategory;
                ListViewColumnMessage = settings.ListViewColumnMessage;
            }
        }

        public void Save()
        {
            var settings = JsonConvert.SerializeObject(this, Formatting.Indented);
            File.WriteAllText(SettingsFile, settings);
        }

        private void SizeToFit()
        {
            if (WindowHeight > SystemInformation.VirtualScreen.Height)
            {
                WindowHeight = SystemInformation.VirtualScreen.Height;
            }
            if (WindowWidth > SystemInformation.VirtualScreen.Width)
            {
                WindowWidth = SystemInformation.VirtualScreen.Width;
            }
        }

        private void MoveIntoView()
        {
            if (WindowTop + WindowHeight / 2 > (SystemInformation.VirtualScreen.Height + SystemInformation.VirtualScreen.Top))
            {
                WindowTop = SystemInformation.VirtualScreen.Height + SystemInformation.VirtualScreen.Top - WindowHeight;
            }
            if (WindowLeft + WindowWidth / 2 > (SystemInformation.VirtualScreen.Width + SystemInformation.VirtualScreen.Left))
            {
                WindowLeft = SystemInformation.VirtualScreen.Width + SystemInformation.VirtualScreen.Left - WindowWidth;
            }
            if (WindowTop < SystemInformation.VirtualScreen.Top)
            {
                WindowTop = SystemInformation.VirtualScreen.Top;
            }
            if (WindowLeft < SystemInformation.VirtualScreen.Left)
            {
                WindowLeft = SystemInformation.VirtualScreen.Left;
            }
        }
    }
}