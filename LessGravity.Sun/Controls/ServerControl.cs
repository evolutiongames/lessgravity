﻿using BrightIdeasSoftware;
using LessGravity.Logging;
using LessGravity.Sun.Helpers;
using LessGravity.Sun.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace LessGravity.Sun.Controls
{
    public partial class ServerControl : UserControl
    {
        public const int ColumnType = 0;
        public const int ColumnTimeStamp = 1;
        public const int ColumnProcessId = 2;
        public const int ColumnThreadId = 3;
        public const int ColumnApplication = 4;
        public const int ColumnHostname = 5;
        public const int ColumnCategory = 6;
        public const int ColumnMessage = 7;

        private readonly Settings _settings;
        private Server _server;
        private readonly List<LogEntry> _logEntries;

        public Server Server
        {
            get => _server;
            set
            {
                if (value == null)
                {
                    _server?.Stop();
                }
                _server = value;
                if (_server == null)
                {
                    btnPause.Enabled = false;
                    btnResume.Enabled = false;
                }
                else
                {
                    btnPause.Enabled = true;
                    btnResume.Enabled = false;
                }
            }
        }

        public ServerControl()
        {
            InitializeComponent();
            _logEntries = new List<LogEntry>();

            var logTypeColumn = lvLog.Columns[ColumnType] as OLVColumn;
            logTypeColumn.AspectGetter = row => string.Empty;
            logTypeColumn.ImageGetter = row =>
            {
                switch ((row as LogEntry)?.LogType)
                {
                    case LogType.Debug: return 3;
                    case LogType.Error: return 4;
                    case LogType.Info: return 1;
                    case LogType.Warn: return 2;
                    case null: return 0;
                    default: return 0;
                }
            };

        }

        public ServerControl(Settings settings)
            : this()
        {
            _settings = settings;
        }

        public void AddLogEntry(LogEntry logEntry)
        {
            _logEntries.Add(logEntry);
            btnFilterMessages(null, EventArgs.Empty);
        }

        private void ServerControl_Load(object sender, EventArgs e)
        {
            lvLog.Columns[ColumnType].Width = _settings.ListViewColumnType;
            lvLog.Columns[ColumnTimeStamp].Width = _settings.ListViewColumnTimestamp;
            lvLog.Columns[ColumnProcessId].Width = _settings.ListViewColumnProcessId;
            lvLog.Columns[ColumnThreadId].Width = _settings.ListViewColumnThreadId;
            lvLog.Columns[ColumnApplication].Width = _settings.ListViewColumnApplication;
            lvLog.Columns[ColumnHostname].Width = _settings.ListViewColumnHostname;
            lvLog.Columns[ColumnCategory].Width = _settings.ListViewColumnCategory;
            lvLog.Columns[ColumnMessage].Width = _settings.ListViewColumnMessage;
        }

        private void btnFilterMessages(object sender, EventArgs e)
        {
            FilterMessages(btnLatestOnTop.Checked);
        }

        private void FilterMessages(bool latestOnTop)
        {
            IEnumerable<LogEntry> logEntries = _logEntries;
            if (!btnShowErrors.Checked)
            {
                logEntries = logEntries.Except(logEntries.Where(entry => entry.LogType == LogType.Error));
            }
            if (!btnShowDebug.Checked)
            {
                logEntries = logEntries.Except(logEntries.Where(entry => entry.LogType == LogType.Debug));
            }
            if (!btnShowWarnings.Checked)
            {
                logEntries = logEntries.Except(logEntries.Where(entry => entry.LogType == LogType.Warn));
            }
            if (!btnShowInfos.Checked)
            {
                logEntries = logEntries.Except(logEntries.Where(entry => entry.LogType == LogType.Info));
            }

            if (latestOnTop)
            {
                logEntries = logEntries.OrderByDescending(logEntry => logEntry.TimeStamp);
            }
            lvLog.SetObjects(logEntries);
        }

        private void btnPause_Click(object sender, EventArgs e)
        {
            _server.Stop();
            btnPause.Enabled = false;
            btnResume.Enabled = !btnPause.Enabled;
        }

        private void btnResume_Click(object sender, EventArgs e)
        {
            _server.Start();
            btnPause.Enabled = true;
            btnResume.Enabled = !btnPause.Enabled;
        }
    }
}