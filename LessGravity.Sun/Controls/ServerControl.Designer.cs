﻿using BrightIdeasSoftware;

namespace LessGravity.Sun.Controls
{
    partial class ServerControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ServerControl));
            this.tsControls = new System.Windows.Forms.ToolStrip();
            this.btnPause = new System.Windows.Forms.ToolStripButton();
            this.btnResume = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.btnShowErrors = new System.Windows.Forms.ToolStripButton();
            this.btnShowDebug = new System.Windows.Forms.ToolStripButton();
            this.btnShowWarnings = new System.Windows.Forms.ToolStripButton();
            this.btnShowInfos = new System.Windows.Forms.ToolStripButton();
            this.lvLog = new BrightIdeasSoftware.ObjectListView();
            this.colType = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.colTimestamp = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.colPID = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.colThreadId = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.colApplication = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.colHostName = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.colCategory = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.colMessage = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.ilType = new System.Windows.Forms.ImageList(this.components);
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.btnLatestOnTop = new System.Windows.Forms.ToolStripButton();
            this.tsControls.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lvLog)).BeginInit();
            this.SuspendLayout();
            // 
            // tsControls
            // 
            this.tsControls.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.tsControls.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnPause,
            this.btnResume,
            this.toolStripSeparator1,
            this.btnShowErrors,
            this.btnShowDebug,
            this.btnShowWarnings,
            this.btnShowInfos,
            this.toolStripSeparator2,
            this.btnLatestOnTop});
            this.tsControls.Location = new System.Drawing.Point(0, 0);
            this.tsControls.Name = "tsControls";
            this.tsControls.Size = new System.Drawing.Size(1035, 25);
            this.tsControls.TabIndex = 1;
            this.tsControls.Text = "toolStrip1";
            // 
            // btnPause
            // 
            this.btnPause.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnPause.Image = ((System.Drawing.Image)(resources.GetObject("btnPause.Image")));
            this.btnPause.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnPause.Name = "btnPause";
            this.btnPause.Size = new System.Drawing.Size(23, 22);
            this.btnPause.Text = "Pause Server";
            this.btnPause.Click += new System.EventHandler(this.btnPause_Click);
            // 
            // btnResume
            // 
            this.btnResume.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnResume.Enabled = false;
            this.btnResume.Image = ((System.Drawing.Image)(resources.GetObject("btnResume.Image")));
            this.btnResume.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnResume.Name = "btnResume";
            this.btnResume.Size = new System.Drawing.Size(23, 22);
            this.btnResume.Text = "Resume Server";
            this.btnResume.Click += new System.EventHandler(this.btnResume_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // btnShowErrors
            // 
            this.btnShowErrors.Checked = true;
            this.btnShowErrors.CheckOnClick = true;
            this.btnShowErrors.CheckState = System.Windows.Forms.CheckState.Checked;
            this.btnShowErrors.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnShowErrors.Image = global::LessGravity.Sun.Properties.Resources.bullet_red;
            this.btnShowErrors.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnShowErrors.Name = "btnShowErrors";
            this.btnShowErrors.Size = new System.Drawing.Size(23, 22);
            this.btnShowErrors.Text = "Show Errors";
            this.btnShowErrors.CheckedChanged += new System.EventHandler(this.btnFilterMessages);
            // 
            // btnShowDebug
            // 
            this.btnShowDebug.Checked = true;
            this.btnShowDebug.CheckOnClick = true;
            this.btnShowDebug.CheckState = System.Windows.Forms.CheckState.Checked;
            this.btnShowDebug.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnShowDebug.Image = global::LessGravity.Sun.Properties.Resources.bullet_orange;
            this.btnShowDebug.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnShowDebug.Name = "btnShowDebug";
            this.btnShowDebug.Size = new System.Drawing.Size(23, 22);
            this.btnShowDebug.Text = "Show Debug Messages";
            this.btnShowDebug.CheckedChanged += new System.EventHandler(this.btnFilterMessages);
            // 
            // btnShowWarnings
            // 
            this.btnShowWarnings.Checked = true;
            this.btnShowWarnings.CheckOnClick = true;
            this.btnShowWarnings.CheckState = System.Windows.Forms.CheckState.Checked;
            this.btnShowWarnings.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnShowWarnings.Image = global::LessGravity.Sun.Properties.Resources.bullet_green;
            this.btnShowWarnings.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnShowWarnings.Name = "btnShowWarnings";
            this.btnShowWarnings.Size = new System.Drawing.Size(23, 22);
            this.btnShowWarnings.Text = "Show Warnings";
            this.btnShowWarnings.CheckedChanged += new System.EventHandler(this.btnFilterMessages);
            // 
            // btnShowInfos
            // 
            this.btnShowInfos.Checked = true;
            this.btnShowInfos.CheckOnClick = true;
            this.btnShowInfos.CheckState = System.Windows.Forms.CheckState.Checked;
            this.btnShowInfos.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnShowInfos.Image = global::LessGravity.Sun.Properties.Resources.bullet_blue;
            this.btnShowInfos.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnShowInfos.Name = "btnShowInfos";
            this.btnShowInfos.Size = new System.Drawing.Size(23, 22);
            this.btnShowInfos.Text = "Show Information";
            this.btnShowInfos.CheckedChanged += new System.EventHandler(this.btnFilterMessages);
            // 
            // lvLog
            // 
            this.lvLog.AllColumns.Add(this.colType);
            this.lvLog.AllColumns.Add(this.colTimestamp);
            this.lvLog.AllColumns.Add(this.colPID);
            this.lvLog.AllColumns.Add(this.colThreadId);
            this.lvLog.AllColumns.Add(this.colApplication);
            this.lvLog.AllColumns.Add(this.colHostName);
            this.lvLog.AllColumns.Add(this.colCategory);
            this.lvLog.AllColumns.Add(this.colMessage);
            this.lvLog.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.lvLog.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.colType,
            this.colTimestamp,
            this.colPID,
            this.colThreadId,
            this.colApplication,
            this.colHostName,
            this.colCategory,
            this.colMessage});
            this.lvLog.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lvLog.FullRowSelect = true;
            this.lvLog.GridLines = true;
            this.lvLog.HasCollapsibleGroups = false;
            this.lvLog.Location = new System.Drawing.Point(0, 25);
            this.lvLog.Name = "lvLog";
            this.lvLog.ShowGroups = false;
            this.lvLog.Size = new System.Drawing.Size(1035, 677);
            this.lvLog.SmallImageList = this.ilType;
            this.lvLog.TabIndex = 2;
            this.lvLog.TintSortColumn = true;
            this.lvLog.UseCompatibleStateImageBehavior = false;
            this.lvLog.View = System.Windows.Forms.View.Details;
            // 
            // colType
            // 
            this.colType.AspectName = "LogType";
            this.colType.CellPadding = null;
            this.colType.Groupable = false;
            this.colType.HeaderImageKey = "(none)";
            this.colType.ImageAspectName = "LogType";
            this.colType.Text = "Type";
            // 
            // colTimestamp
            // 
            this.colTimestamp.AspectName = "TimeStamp";
            this.colTimestamp.AspectToStringFormat = "";
            this.colTimestamp.CellPadding = null;
            this.colTimestamp.Text = "Timestamp";
            this.colTimestamp.Width = 141;
            // 
            // colPID
            // 
            this.colPID.AspectName = "ProcessId";
            this.colPID.CellPadding = null;
            this.colPID.Text = "PID";
            // 
            // colThreadId
            // 
            this.colThreadId.AspectName = "ThreadId";
            this.colThreadId.CellPadding = null;
            this.colThreadId.Text = "ThreadId";
            // 
            // colApplication
            // 
            this.colApplication.AspectName = "Application";
            this.colApplication.CellPadding = null;
            this.colApplication.Text = "Application";
            this.colApplication.Width = 240;
            // 
            // colHostName
            // 
            this.colHostName.AspectName = "HostName";
            this.colHostName.CellPadding = null;
            this.colHostName.Text = "HostName";
            this.colHostName.Width = 75;
            // 
            // colCategory
            // 
            this.colCategory.AspectName = "Category";
            this.colCategory.CellPadding = null;
            this.colCategory.Text = "Category";
            this.colCategory.Width = 164;
            // 
            // colMessage
            // 
            this.colMessage.AspectName = "Message";
            this.colMessage.CellPadding = null;
            this.colMessage.FillsFreeSpace = true;
            this.colMessage.Text = "Message";
            this.colMessage.Width = 414;
            // 
            // ilType
            // 
            this.ilType.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("ilType.ImageStream")));
            this.ilType.TransparentColor = System.Drawing.Color.Transparent;
            this.ilType.Images.SetKeyName(0, "asterisk_orange.png");
            this.ilType.Images.SetKeyName(1, "bullet_blue");
            this.ilType.Images.SetKeyName(2, "bullet_green");
            this.ilType.Images.SetKeyName(3, "bullet_orange");
            this.ilType.Images.SetKeyName(4, "bullet_red");
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // btnLatestOnTop
            // 
            this.btnLatestOnTop.Checked = true;
            this.btnLatestOnTop.CheckState = System.Windows.Forms.CheckState.Checked;
            this.btnLatestOnTop.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnLatestOnTop.Image = ((System.Drawing.Image)(resources.GetObject("btnLatestOnTop.Image")));
            this.btnLatestOnTop.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnLatestOnTop.Name = "btnLatestOnTop";
            this.btnLatestOnTop.Size = new System.Drawing.Size(23, 22);
            this.btnLatestOnTop.Text = "Latest On Top";
            // 
            // ServerControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.lvLog);
            this.Controls.Add(this.tsControls);
            this.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "ServerControl";
            this.Size = new System.Drawing.Size(1035, 702);
            this.Load += new System.EventHandler(this.ServerControl_Load);
            this.tsControls.ResumeLayout(false);
            this.tsControls.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lvLog)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStrip tsControls;
        private System.Windows.Forms.ToolStripButton btnPause;
        private System.Windows.Forms.ToolStripButton btnResume;
        private ObjectListView lvLog;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ImageList ilType;
        private OLVColumn colType;
        private OLVColumn colTimestamp;
        private OLVColumn colPID;
        private OLVColumn colApplication;
        private OLVColumn colCategory;
        private OLVColumn colMessage;
        private OLVColumn colHostName;
        private System.Windows.Forms.ToolStripButton btnShowErrors;
        private System.Windows.Forms.ToolStripButton btnShowDebug;
        private System.Windows.Forms.ToolStripButton btnShowWarnings;
        private System.Windows.Forms.ToolStripButton btnShowInfos;
        private OLVColumn colThreadId;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripButton btnLatestOnTop;
    }
}
