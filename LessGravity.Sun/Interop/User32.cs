﻿using System;
using System.Runtime.InteropServices;

namespace LessGravity.Sun.Interop
{
    public static class User32
    {
        public const int HwndBroadcast = 0xffff;
        public const int WmCopydata = 0x4A;

        [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Ansi)]
        public struct CopyDataStruct
        {
            public IntPtr Data;
            public int DataSize;
            public IntPtr Payload;
        }

        public struct DataStruct
        {
            public int Port;
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 32)]
            public string ServerName;
        }

        [return: MarshalAs(UnmanagedType.Bool)]
        [DllImport("user32.dll", SetLastError = true)]
        public static extern bool SendMessage(IntPtr windowHandle, uint message, IntPtr wParam, ref CopyDataStruct lParam);
    }
}