﻿using LessGravity.Sun.Controls;
using LessGravity.Sun.Dialogs;
using LessGravity.Sun.Helpers;
using LessGravity.Sun.Interop;
using LessGravity.Sun.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows.Forms;

namespace LessGravity.Sun
{
    public partial class MainForm : Form
    {
        private readonly List<Server> _servers;
        private readonly Settings _settings;

        public MainForm()
        {
            InitializeComponent();

            _settings = new Settings();
            _settings.Load();
            _servers = new List<Server>();
        }

        public void AddServer(string name, int port)
        {
            var serverControl = new ServerControl(_settings);
            var server = new Server(port, serverControl)
            {
                Name = name
            };
            serverControl.Server = server;
            _servers.Add(server);
            var serverTabPage = new TabPage(name);
            serverTabPage.Controls.Add(serverControl);
            serverControl.Dock = DockStyle.Fill;
            tcMain.TabPages.Add(serverTabPage);

            serverTabPage.Tag = server;
            lblServerPort.Text = $"Server {server.Name} is listening on port {server.Port}";
        }

        private void SaveSettings()
        {
            var listViewColumnType = Properties.Settings.Default.ListViewColumnType;
            var listViewColumnTimestamp = Properties.Settings.Default.ListViewColumnTimestamp;
            var listViewColumnProcessId = Properties.Settings.Default.ListViewColumnProcessId;
            var listViewColumnThreadId = Properties.Settings.Default.ListViewColumnThreadId;
            var listViewColumnApplication = Properties.Settings.Default.ListViewColumnApplication;
            var listViewColumnHostname = Properties.Settings.Default.ListViewColumnHostname;
            var listViewColumnCategory = Properties.Settings.Default.ListViewColumnCategory;
            var listViewColumnMessage = Properties.Settings.Default.ListViewColumnMessage;

            if (tcMain.TabPages.Count > 0)
            {
                var firstOpenServerControl = tcMain.TabPages[0].Controls.Cast<ServerControl>().FirstOrDefault();
                var firstOpenListView = firstOpenServerControl?.Controls.Cast<ListView>().FirstOrDefault();
                if (firstOpenListView != null)
                {
                    listViewColumnType = firstOpenListView.Columns[ServerControl.ColumnType].Width;
                    listViewColumnTimestamp = firstOpenListView.Columns[ServerControl.ColumnTimeStamp].Width;
                    listViewColumnProcessId = firstOpenListView.Columns[ServerControl.ColumnProcessId].Width;
                    listViewColumnThreadId = firstOpenListView.Columns[ServerControl.ColumnThreadId].Width;
                    listViewColumnApplication = firstOpenListView.Columns[ServerControl.ColumnApplication].Width;
                    listViewColumnHostname = firstOpenListView.Columns[ServerControl.ColumnHostname].Width;
                    listViewColumnCategory = firstOpenListView.Columns[ServerControl.ColumnCategory].Width;
                    listViewColumnMessage = firstOpenListView.Columns[ServerControl.ColumnMessage].Width;
                }
            }

            _settings.WindowHeight = Height;
            _settings.WindowWidth = Width;
            _settings.WindowLeft = Left;
            _settings.WindowTop = Top;
            _settings.WindowState = WindowState;
            _settings.ListViewColumnType = listViewColumnType;
            _settings.ListViewColumnTimestamp = listViewColumnTimestamp;
            _settings.ListViewColumnProcessId = listViewColumnProcessId;
            _settings.ListViewColumnThreadId = listViewColumnThreadId;
            _settings.ListViewColumnApplication = listViewColumnApplication;
            _settings.ListViewColumnHostname = listViewColumnHostname;
            _settings.ListViewColumnCategory = listViewColumnCategory;
            _settings.ListViewColumnMessage = listViewColumnMessage;
            _settings.Save();
        }

        private void tcMain_TabIndexChanged(object sender, System.EventArgs e)
        {
            var server = tcMain.SelectedTab.Tag as Server;
            if (server != null)
            {
                lblServerPort.Text = $"Server {server.Name} is listening on port {server.Port}";
            }
        }

        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            foreach (var server in _servers)
            {
                server.Dispose();
            }

            SaveSettings();
        }

        protected override void WndProc(ref Message message)
        {
            base.WndProc(ref message);

            if (message.Msg == User32.WmCopydata)
            {
                var copyDataStruct = (User32.CopyDataStruct)Marshal.PtrToStructure(message.LParam, typeof(User32.CopyDataStruct));
                var dataBuffer = new byte[copyDataStruct.DataSize];
                Marshal.Copy(copyDataStruct.Payload, dataBuffer, 0, copyDataStruct.DataSize);

                var serverNameAndPort = Encoding.ASCII.GetString(dataBuffer, 0, copyDataStruct.DataSize);

                var serverName = serverNameAndPort.Split(':');
                var name = serverName[0];
                var port = serverName[1];
                int portValue;
                if (!int.TryParse(port, out portValue))
                {
                    message.Result = IntPtr.Zero;
                }
                var server = _servers.FirstOrDefault(s => s.Port == portValue && s.Name == name);
                if (server == null)
                {
                    AddServer(name, portValue);
                }
            }
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            Height = _settings.WindowHeight;
            Width = _settings.WindowWidth;
            Top = _settings.WindowTop;
            Left = _settings.WindowLeft;
            WindowState = _settings.WindowState;
        }

        private void btnAddServer_Click(object sender, EventArgs e)
        {
            using (var addServerDialog = new AddServerDialog())
            {
                if (addServerDialog.ShowDialog() != DialogResult.OK)
                {
                    return;
                }

                var serverName = addServerDialog.ServerName;
                var serverPort = addServerDialog.Port;

                AddServer(serverName, serverPort);
            }
        }

        private void btnRemoveServer_Click(object sender, EventArgs e)
        {
            var selectedTab = tcMain.SelectedTab;
            if (selectedTab == null)
            {
                return;
            }

            if (MessageBox.Show("Remove current server?", "Removing Log Server", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question) != DialogResult.Yes)
            {
                return;
            }

            var server = selectedTab.Tag as Server;
            if (server != null)
            {
                server.Stop();
                _servers.Remove(server);

                tcMain.TabPages.RemoveAt(selectedTab.TabIndex);
            }
        }
    }
}