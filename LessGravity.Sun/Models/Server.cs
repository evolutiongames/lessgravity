﻿using LessGravity.Logging;
using LessGravity.Sun.Controls;
using System.Collections.ObjectModel;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LessGravity.Sun.Models
{
    public class Server
    {
        private CancellationTokenSource _cancellationTokenSource;
        private readonly ServerControl _serverControl;

        public string Name { get; set; }

        public int Port { get; }

        public ObservableCollection<LogEntry> Entries { get; private set; }

        public void Dispose()
        {
            Stop();
        }

        public Server(int port, ServerControl serverControl)
        {
            _serverControl = serverControl;
            Port = port;

            Entries = new ObservableCollection<LogEntry>();
            Start();
        }

        public void Start()
        {
            _cancellationTokenSource = new CancellationTokenSource();

            Task.Run(async () =>
            {
                var dataStream = new MemoryStream(512);
                using (var udpClient = new UdpClient(Port))
                {
                    while (true)
                    {
                        var received = await udpClient.ReceiveAsync();
                        var bytesReceived = received.Buffer;
                        var clientEndPoint = received.RemoteEndPoint;

                        var hostName = Dns.GetHostEntry(clientEndPoint.Address).HostName;
                        if (string.IsNullOrEmpty(hostName))
                        {
                            hostName = clientEndPoint.Address.ToString();
                        }

                        dataStream.Position = 0;
                        dataStream.Write(bytesReceived, 0, bytesReceived.Length);

                        var logEntry = new LogEntry();
                        logEntry.ReadFromStream(dataStream);
                        logEntry.HostName = hostName;
                        _serverControl.Invoke((MethodInvoker)(() => _serverControl.AddLogEntry(logEntry)));
                    }
                }
            }, _cancellationTokenSource.Token);
        }

        public void Stop()
        {
            if (_cancellationTokenSource == null)
            {
                return;
            }
            _cancellationTokenSource.Cancel();
            _cancellationTokenSource.Dispose();
        }

    }
}