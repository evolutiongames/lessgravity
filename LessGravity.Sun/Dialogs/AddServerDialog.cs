﻿using System.Windows.Forms;

namespace LessGravity.Sun.Dialogs
{
    public partial class AddServerDialog : Form
    {
        public string ServerName
        {
            get => txtServerName.Text;
            set => txtServerName.Text = value;
        }

        public int Port
        {
            get => (int)txtPortNumber.Value;
            set => txtPortNumber.Value = value;
        }

        public AddServerDialog()
        {
            InitializeComponent();
        }
    }
}