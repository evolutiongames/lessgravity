﻿using System;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading;
using System.Windows.Forms;
using LessGravity.Sun.Interop;

namespace LessGravity.Sun
{
    internal static class Program
    {
        private static readonly Mutex _appMutex = new Mutex(true, "{2023BED4-C926-4635-A7D7-7BB534DC1E32}");

        [STAThread]
        private static void Main(string[] args)
        {
            var servers = args.Select(argument =>
            {
                var serverName = argument.Split(':');
                var name = serverName[0];
                var port = serverName[1];
                if (int.TryParse(port, out int portValue))
                {
                    return new { ServerName = name, Port = portValue };
                }
                throw new ArgumentException("Wrong Arguments. Allowed Values \"LogName:Port\"");
            });


            if (_appMutex.WaitOne(TimeSpan.Zero, true))
            {
                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);

                var mainForm = new MainForm();
                foreach (var server in servers)
                {
                    mainForm.AddServer(server.ServerName, server.Port);
                }

                Application.Run(mainForm);

                _appMutex.ReleaseMutex();
            }
            else
            {
                foreach (var server in servers)
                {
                    var payloadData = $"{server.ServerName}:{server.Port}";
                    var copyDataStruct = new User32.CopyDataStruct
                    {
                        Data = new IntPtr(0x0000BEEF),
                        DataSize = payloadData.Length,
                        Payload = Marshal.StringToHGlobalAnsi(payloadData)
                    };
                    User32.SendMessage(new IntPtr(User32.HwndBroadcast), User32.WmCopydata, IntPtr.Zero, ref copyDataStruct);
                    Marshal.FreeHGlobal(copyDataStruct.Payload);
                }
                Application.Exit();
            }
        }
    }
}