﻿using System;
using System.Runtime.CompilerServices;

namespace LessGravity.Pluto.Wrappers
{
    public class ModelWrapper<TModel>
    {
        public TModel Model { get; private set; }

        public ModelWrapper(TModel model)
        {
            if (model == null)
            {
                throw new ArgumentNullException(nameof(model));
            }
            Model = model;
        }

        protected TValue GetValue<TValue>([CallerMemberName] string propertyName = null)
        {
            var propertyInfo = Model.GetType().GetProperty(propertyName);
            return (TValue)propertyInfo.GetValue(Model);
        }

        protected void SetValue<TValue>(ref TValue value, [CallerMemberName]string propertyName = null)
        {
            var propertyInfo = Model.GetType().GetProperty(propertyName);
            var currentValue = propertyInfo.GetValue(Model);
            if (!Equals(currentValue, value))
            {
                propertyInfo.SetValue(Model, value);
            }
        }
    }
}