﻿using System;
using System.Threading.Tasks;
using System.Windows.Input;

namespace LessGravity.Pluto.Commands
{
    public abstract class AsyncCommandBase : IAsyncCommand
    {
        public abstract bool CanExecute(object parameter);
        public abstract Task ExecuteAsync(object parameter);

        public async void Execute(object parameter)
        {
            await ExecuteAsync(parameter).ConfigureAwait(false);
        }

        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        public void RaiseCanExecuteChanged()
        {
            CommandManager.InvalidateRequerySuggested();
        }
    }

    public class AsyncCommand<T> : AsyncCommandBase
    {
        private readonly Func<T, Task> _command;
        private readonly Func<T, bool> _canExecute;

        public AsyncCommand(Func<T, Task> command, Func<T, bool> canExecute = null)
        {
            if (command == null)
            {
                throw new ArgumentNullException(nameof(command));
            }
            _command = command;
            _canExecute = canExecute;
        }

        public override bool CanExecute(object parameter)
        {
            return _canExecute == null || _canExecute((T)parameter);
        }

        public override Task ExecuteAsync(object parameter)
        {
            return _command((T)parameter);
        }
    }

    public class AsyncCommand : AsyncCommandBase
    {
        private readonly Func<Task> _command;
        private readonly Func<bool> _canExecute;

        public AsyncCommand(Func<Task> command, Func<bool> canExecute = null)
        {
            if (command == null)
            {
                throw new ArgumentNullException(nameof(command));
            }
            _command = command;
            _canExecute = canExecute;
        }

        public override bool CanExecute(object parameter)
        {
            return _canExecute == null || _canExecute();
        }

        public override Task ExecuteAsync(object parameter)
        {
            return _command();
        }
    }
}
