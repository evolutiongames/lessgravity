﻿using System.Threading.Tasks;
using System.Windows.Input;

namespace LessGravity.Pluto.Commands
{
    public interface IAsyncCommand : ICommand
    {
        Task ExecuteAsync(object parameter);
        void RaiseCanExecuteChanged();
    }
}