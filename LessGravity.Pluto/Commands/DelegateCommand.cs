﻿using System;
using System.Windows.Input;

namespace LessGravity.Pluto.Commands
{
    public class DelegateCommand : ICommand
    {
        private readonly Action _action = null;
        private readonly Func<bool> _canExecute = null;

        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        public DelegateCommand(Action action)
        {
            if (action == null)
            {
                throw new ArgumentNullException(nameof(action));
            }
            _action = action;
        }

        public DelegateCommand(Action action, Func<bool> canExecuteEvaluator) : this(action)
        {
            if (canExecuteEvaluator == null)
            {
                throw new ArgumentNullException(nameof(canExecuteEvaluator));
            }
            _canExecute = canExecuteEvaluator;
        }

        public bool CanExecute(object parameter)
        {
            return _canExecute == null ? true : _canExecute.Invoke();
        }

        public void Execute(object parameter)
        {
            _action();
        }
    }

    public class DelegateCommand<T> : ICommand
    {
        private readonly Action<T> _action = null;
        private readonly Predicate<T> _canExecute = null;

        public DelegateCommand(Action<T> action) 
            : this(action, null)
        {
        }

        public DelegateCommand(Action<T> action, Predicate<T> canExecute)
        {
            if (action == null)
            {
                throw new ArgumentNullException("execute");
            }
            _action = action;
            _canExecute = canExecute;
        }

        public bool CanExecute(object parameter)
        {
            return _canExecute == null || _canExecute((T)parameter);
        }

        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        public void Execute(object parameter)
        {
            _action((T)parameter);
        }
    }
}