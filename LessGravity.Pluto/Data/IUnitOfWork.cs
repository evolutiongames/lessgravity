﻿using System;

namespace LessGravity.Pluto.Data
{
    public interface IUnitOfWork : IDisposable
    {
        int SaveChanges();
    }
}