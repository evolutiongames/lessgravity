﻿using System;

namespace LessGravity.Pluto.Extensions
{
    public static class EventExtensions
    {
        public static void Raise(this EventHandler handler, object sender)
        {
            handler?.Invoke(sender, EventArgs.Empty);
        }
    }
}