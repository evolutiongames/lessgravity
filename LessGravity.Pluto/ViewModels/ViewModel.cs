﻿using LessGravity.Pluto.Commands;
using LessGravity.Pluto.Extensions;
using LessGravity.Pluto.Observables;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Windows.Data;
using System.Windows.Input;

namespace LessGravity.Pluto.ViewModels
{
    public class ViewModel<T> : ObservableObject where T : ObservableObject
    {
        private object _itemsLock;

        public EventHandler SelectionChanged;
        public EventHandler SelectionChaging;

        private bool _isBusy;
        /// <summary>
        /// Indicates if there is an operation running.
        /// Modified by adding <see cref="BusyToken"/> to the <see cref="BusyStack"/> property
        /// </summary>
        public bool IsBusy
        {
            get { return _isBusy; }
            private set { SetValue(ref _isBusy, value); }
        }

        private BusyStack _busyStack;
        /// <summary>
        /// Provides IDisposable tokens for running async operations
        /// </summary>
        public BusyStack BusyStack
        {
            get { return _busyStack; }
            private set { SetValue(ref _busyStack, value); }
        }

        private T _selectedItem;
        public virtual T SelectedItem
        {
            get { return _selectedItem; }
            set
            {
                if (EqualityComparer<T>.Default.Equals(_selectedItem, value))
                {
                    return;
                }
                SelectionChaging?.Raise(this);
                _selectedItem = value;
                SelectionChanged?.Raise(this);

                OnPropertyChanged();
            }
        }

        private RangeObservableCollection<T> _items;
        /// <summary>
        /// Contains all the UI relevant Models and notifies about changes in the collection and inside the Models themself
        /// </summary>
        public RangeObservableCollection<T> Items
        {
            get { return _items; }
            private set { SetValue(ref _items, value); }
        }

        private ICollectionView _view;
        /// <summary>
        /// For grouping, sorting and filtering
        /// </summary>
        public ICollectionView View
        {
            get { return _view; }
            protected set { SetValue(ref _view, value); }
        }

        public int Count => Items.Count;

        public T this[int index] => Items[index];

        public ICommand RemoveRangeCommand { get; private set; }
        public ICommand RemoveCommand { get; private set; }
        public ICommand ClearCommand { get; private set; }

        public ViewModel()
        {
            InitializeProperties();
            InitializeCommands();

            BindingOperations.EnableCollectionSynchronization(Items, _itemsLock);
        }

        public ViewModel(IEnumerable<T> items) 
            : this()
        {
            Items.AddRange(items);
        }

        private void InitializeProperties()
        {
            _itemsLock = new object();

            Items = new RangeObservableCollection<T>();
            Items.CollectionChanged += ItemsCollectionChanged;

            BusyStack = new BusyStack
            {
                OnChanged = hasItems => { IsBusy = hasItems; }
            };
            View = CollectionViewSource.GetDefaultView(Items);
            OnPropertyChanged(nameof(Count));
        }

        private void InitializeCommands()
        {
            RemoveCommand = new DelegateCommand<T>(Remove, CanRemove);
            RemoveRangeCommand = new DelegateCommand<IList>(RemoveRange, CanRemoveRange);
            ClearCommand = new DelegateCommand(Clear, CanClear);
        }

        public virtual void ItemsCollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            OnPropertyChanged(nameof(Count));
        }

        public virtual void Add(T item)
        {
            if (item == null)
            {
                throw new ArgumentNullException(nameof(item));
            }
            using (BusyStack.GetToken())
            {
                Items.Add(item);
            }
        }

        public virtual void AddRange(IEnumerable<T> items)
        {
            if (items == null)
            {
                throw new ArgumentNullException(nameof(items));
            }
            using (BusyStack.GetToken())
            {
                Items.AddRange(items);
            }
        }

        protected virtual bool CanAdd()
        {
            return Items != null;
        }

        public void Remove(T item)
        {
            using (BusyStack.GetToken())
            {
                Items.Remove(item);
            }
        }

        public void RemoveRange(IEnumerable<T> items)
        {
            if (items == null)
            {
                throw new ArgumentNullException(nameof(items));
            }
            using (BusyStack.GetToken())
            {
                Items.RemoveRange(items);
            }
        }

        public void RemoveRange(IList items)
        {
            if (items == null)
            {
                throw new ArgumentNullException(nameof(items));
            }
            using (BusyStack.GetToken())
            {
                Items.RemoveRange(items);
            }
        }

        protected virtual bool CanRemove(T item)
        {
            return CanClear() && item != null && Items.Contains(item);
        }

        protected virtual bool CanRemoveRange(IEnumerable<T> items)
        {
            return CanClear() && items != null && items.Any(p => Items.Contains(p));
        }

        protected virtual bool CanRemoveRange(IList items)
        {
            return items != null && CanRemoveRange(items.Cast<T>());
        }

        public void Clear()
        {
            using (BusyStack.GetToken())
            {
                Items.Clear();
            }
        }

        protected virtual bool CanClear()
        {
            return Items?.Any() == true;
        }
    }
}