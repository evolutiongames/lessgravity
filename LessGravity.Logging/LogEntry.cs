﻿using System;
using System.IO;
using System.Text;

namespace LessGravity.Logging
{
    public class LogEntry
    {
        public LogType LogType { get; set; }
        public DateTime TimeStamp { get; set; }

        public string TimeStampAsString => TimeStamp.ToString("dd.MM.yyyy HH:mm:ss,fff");
        public string HostName { get; set; }
        public int ProcessId { get; set; }
        public int ThreadId { get; set; }
        public string Application { get; set; }
        public string Category { get; set; }
        public string Message { get; set; }

        public void ReadFromStream(Stream stream)
        {
            using (var binaryReader = new BinaryReader(stream, Encoding.UTF8, true))
            {
                stream.Position = 0;
                LogType = (LogType)binaryReader.ReadByte();
                var ticks = binaryReader.ReadInt64();
                TimeStamp = new DateTime(ticks);
                ProcessId = binaryReader.ReadInt32();
                ThreadId = binaryReader.ReadInt32();
                Application = binaryReader.ReadString();
                Category = binaryReader.ReadString();
                Message = binaryReader.ReadString();
            }
        }

        public void WriteToStream(Stream stream)
        {
            using (var binaryWriter = new BinaryWriter(stream, Encoding.UTF8, true))
            {
                binaryWriter.Write((byte)LogType);
                binaryWriter.Write(TimeStamp.Ticks);
                binaryWriter.Write(ProcessId);
                binaryWriter.Write(ThreadId);
                binaryWriter.Write(Application);
                binaryWriter.Write(Category);
                binaryWriter.Write(Message);
            }            
        }
    }
}