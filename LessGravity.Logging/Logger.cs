﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Threading;

namespace LessGravity.Logging
{
    public class Logger : ILogger
    {
        private readonly UdpClient _udpClient;
        private readonly MemoryStream _dataStream;
        private readonly string _entryAssemblyName;

        private readonly List<Tuple<LogType, string, Action<LogEntry>>> _subscribers;

        public List<IPEndPoint> RemoteRecipients { get; }

        /*
        private static StreamDisposer _streamDisposer = new StreamDisposer();

        class StreamDisposer
        {
            ~StreamDisposer()
            {
                _dataStream.Dispose();
            }
        }
         * */
        
        public Logger()
        {
            var entryAssembly = Assembly.GetEntryAssembly();
            _entryAssemblyName = Path.GetFileName(entryAssembly.Location);
            _udpClient = new UdpClient();
            _dataStream = new MemoryStream(512);
            _subscribers = new List<Tuple<LogType, string, Action<LogEntry>>>();
            var recipients = ConfigurationManager.GetSection("LoggerRecipients") as LogConfigSection;
            if (recipients != null)
            {
                RemoteRecipients = new List<IPEndPoint>();
                RemoteRecipients.AddRange(recipients.Recipients.Cast<LogRecipientElement>().Select(lre => lre.EndPoint));
            }
        }

        public void Subscribe(LogType logType, string category, Action<LogEntry> logAction)
        {
            _subscribers.Add(Tuple.Create(logType, category, logAction));
        }

        public void Debug(string message, string category, [CallerMemberName]string caller = null)
        {
            Log(LogType.Debug, DateTime.Now, message, category, caller);
        }

        public void Warn(string message, string category, [CallerMemberName]string caller = null)
        {
            Log(LogType.Warn, DateTime.Now, message, category, caller);
        }

        public void Info(string message, string category, [CallerMemberName]string caller = null)
        {
            Log(LogType.Info, DateTime.Now, message, category, caller);
        }

        public void Error(string message, string category, [CallerMemberName]string caller = null)
        {
            Log(LogType.Error, DateTime.Now, message, category, caller);
        }

        private void Log(LogType logType, DateTime timeStamp, string message, string category, string caller = null)
        {
            if (!_dataStream.CanWrite)
            {
                return;
            }
            _dataStream.Position = 0;
            var logEntry = new LogEntry
            {
                LogType = logType,
                Application = _entryAssemblyName,
                ProcessId = Process.GetCurrentProcess().Id,
                ThreadId = Thread.CurrentThread.ManagedThreadId,
                TimeStamp = timeStamp,
                Category = category,
                Message = $"{caller} {message}"
            };
            logEntry.WriteToStream(_dataStream);
            SendAsync();

            foreach (var subscriber in _subscribers.Where(s => (s.Item1 & logType) == logType && (s.Item2 == category || s.Item2 == "*")))
            {
                subscriber.Item3(logEntry);
            }
        }

        private void SendAsync()
        {
            var bytes = _dataStream.GetBuffer();
            var bytesLength = (int) _dataStream.Length;
            foreach (var remoteRecipient in RemoteRecipients)
            {
                _udpClient.SendAsync(bytes, bytesLength, remoteRecipient);
            }
        }
    }
}