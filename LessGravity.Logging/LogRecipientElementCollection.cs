﻿using System.Configuration;

namespace LessGravity.Logging
{
    [ConfigurationCollection(typeof(LogRecipientElement), CollectionType = ConfigurationElementCollectionType.AddRemoveClearMap)]
    public sealed class LogRecipientElementCollection : ConfigurationElementCollection
    {
        private static readonly ConfigurationPropertyCollection _properties;

        static LogRecipientElementCollection()
        {
            _properties = new ConfigurationPropertyCollection();
        }

        public LogRecipientElement this[int index]
        {
            get => (LogRecipientElement)BaseGet(index);
            set
            {
                if (BaseGet(index) != null)
                {
                    BaseRemoveAt(index);
                }
                BaseAdd(index, value);
            }
        }

        public new LogRecipientElement this[string name] => (LogRecipientElement)BaseGet(name);

        protected override ConfigurationPropertyCollection Properties => _properties;

        public override ConfigurationElementCollectionType CollectionType => ConfigurationElementCollectionType.AddRemoveClearMap;

        protected override ConfigurationElement CreateNewElement()
        {
            return new LogRecipientElement();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            return (element as LogRecipientElement).Name;
        }
    }
}