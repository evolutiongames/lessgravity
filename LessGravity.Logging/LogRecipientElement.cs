﻿using System;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Sockets;

namespace LessGravity.Logging
{
    public sealed class LogRecipientElement : ConfigurationElement
    {
        private const string StringName = "Name";
        private const string StringEndPoint = "EndPoint";

        private static readonly ConfigurationPropertyCollection _properties;
        private static readonly ConfigurationProperty _propertyName;
        private static readonly ConfigurationProperty _propertyEndPoint;

        static LogRecipientElement()
        {
            _propertyName = new ConfigurationProperty(StringName, typeof(string));
            _propertyEndPoint = new ConfigurationProperty(StringEndPoint, typeof(string));

            _properties = new ConfigurationPropertyCollection
            {
                _propertyName,
                _propertyEndPoint
            };
        }

        protected override ConfigurationPropertyCollection Properties => _properties;

        public string Name => (string)base[_propertyName];

        public IPEndPoint EndPoint
        {
            get
            {
                var endPointString = (string)base[_propertyEndPoint];
                var endPointStringSplit = endPointString.Split(':');
                return new IPEndPoint(Dns.GetHostEntry(endPointStringSplit[0]).AddressList.FirstOrDefault(a => a.AddressFamily == AddressFamily.InterNetwork), port: Int32.Parse(endPointStringSplit[1]));
            }
        }
    }
}