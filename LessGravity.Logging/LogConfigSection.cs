﻿using System.Configuration;

namespace LessGravity.Logging
{
    public sealed class LogConfigSection : ConfigurationSection
    {
        private const string StringRecipients = "Recipients";

        private static readonly ConfigurationPropertyCollection _properties;

        static LogConfigSection()
        {
            var configProperty = new ConfigurationProperty(StringRecipients, typeof(LogRecipientElementCollection));

            _properties = new ConfigurationPropertyCollection
            {
                configProperty
            };
        }

        protected override ConfigurationPropertyCollection Properties => _properties;

        public LogRecipientElementCollection Recipients => (LogRecipientElementCollection)base[StringRecipients];
    }
}