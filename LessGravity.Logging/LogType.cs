﻿using System;

namespace LessGravity.Logging
{
    [Flags]
    public enum LogType : byte
    {
        Debug,
        Info,
        Warn,
        Error
    }
}