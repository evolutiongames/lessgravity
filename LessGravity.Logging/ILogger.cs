﻿using System.Collections.Generic;
using System.Net;
using System.Runtime.CompilerServices;

namespace LessGravity.Logging
{
    public interface ILogger
    {
        List<IPEndPoint> RemoteRecipients { get; }

        void Debug(string message, string category, [CallerMemberName] string caller = null);
        void Error(string message, string category, [CallerMemberName] string caller = null);
        void Info(string message, string category, [CallerMemberName] string caller = null);
        void Warn(string message, string category, [CallerMemberName] string caller = null);
    }
}