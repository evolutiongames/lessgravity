﻿using LessGravity.Logging;
using LessGravity.Quasar.Game;
using System;
using System.IO;
using System.Windows.Forms;

namespace LessGravity.Quasar.BlocksAndPhysics
{
    internal static class Program
    {
        [STAThread]
        private static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            var settingsFileName = Path.Combine(Application.StartupPath, "Assets", "Config", "Game.settings");
            var gameSettings = GameSettings.ReadSettings(settingsFileName);

            using (var game = new MainGame(new Logger(), new DefaultFormFactory()))
            {
                game.Run(gameSettings);
            }
        }
    }
}