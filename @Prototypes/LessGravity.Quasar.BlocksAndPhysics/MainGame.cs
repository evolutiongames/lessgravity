﻿using LessGravity.Logging;
using LessGravity.Quasar.Game;

namespace LessGravity.Quasar.BlocksAndPhysics
{
    public sealed class MainGame : QuasarGame
    {
        public MainGame(ILogger logger, IFormFactory formFactory)
            : base(logger, formFactory)
        {
            
        }
    }
}