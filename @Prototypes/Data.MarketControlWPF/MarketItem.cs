﻿using System.Windows.Input;

namespace Data.MarketControlWPF
{
    public class MarketItem
    {
        public ICommand BuyCommand { get; set; } 
        public ICommand SellCommand { get; set; }

        public string Name { get; set; }

        public int Quantity { get; set; }

        public double Rate { get; set; }

        public decimal Price { get; set; }
    }
}