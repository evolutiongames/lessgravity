﻿using System.Collections.ObjectModel;
using System.Windows;

namespace Data.MarketControlWPF
{
    public partial class MainWindow : Window
    {
        public ObservableCollection<MarketItem> MarketItems { get; private set; } = new ObservableCollection<MarketItem>();

        public MainWindow()
        {
            InitializeComponent();

            DataContext = this;

            MarketItems.Add(new MarketItem { Name = "Iron", Quantity = 20, Price = 40, Rate = 0.25 });
            MarketItems.Add(new MarketItem { Name = "Aluminium", Quantity = 200, Price = 400.34m, Rate = 1.5 });
            MarketItems.Add(new MarketItem { Name = "Chemicals", Quantity = 7650, Price = 907.98m, Rate = -4.5 });
            MarketItems.Add(new MarketItem { Name = "Electronics", Quantity = 20, Price = 40, Rate = 1.5 });
        }
    }
}