﻿using LessGravity.OpenSpace.Data;
using System.Collections;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Reflection;
using System.Text;

namespace System.Data
{
    public static class DbConnectionExtensions
    {
        private static string MapDataType(Type dataType, out bool nullAllowed)
        {
            var nullableType = Nullable.GetUnderlyingType(dataType);
            var allowNull = nullableType != null;
            var isRequired = dataType.GetCustomAttribute<RequiredAttribute>() != null;
            if (isRequired)
            {
                nullAllowed = false;
            }
            else
            {
                nullAllowed = allowNull;
            }

            var innerType = nullableType ?? dataType;
            if (innerType == typeof(string))
            {
                var maxLengthAttribute = innerType.GetCustomAttribute<MaxLengthAttribute>();
                return "NVARCHAR(" + (maxLengthAttribute?.Length.ToString() ?? "MAX") + ")";
            }
            if (innerType == typeof(int) || innerType == typeof(uint))
            {
                return "INTEGER";
            }
            if (innerType == typeof(long) || innerType == typeof(ulong))
            {
                return "BIGINT";
            }
            if (innerType == typeof(bool))
            {
                return "BIT";
            }
            if (innerType == typeof(float))
            {
                return "FLOAT";
            }
            if (innerType == typeof(double))
            {
                return "FLOAT";
            }
            if (innerType == typeof(decimal))
            {
                return "DECIMAL(18,2)";
            }
            if (innerType.IsSubclassOf(typeof(ObjectBase)))
            {
                nullAllowed = true;
                return "BIGINT";
            }
            return "UNKNOWN";
        }

        public static void CreateTable(this IDbConnection connection, Type instance)
        {
            var properties = instance.GetProperties()
                                     .OrderBy(property => property.Name == "Id" ? property.Name.Replace("Id", "aaaa") : property.Name == "Name" ? property.Name.Replace("Name", "aaab") : property.Name);
            var typeName = instance.Name;
            var tableAttribute = instance.GetCustomAttribute<TableAttribute>();
            var tableName = tableAttribute?.Name ?? typeName;

            var ddl = new StringBuilder();
            ddl.AppendLine($"CREATE TABLE {tableName}");
            ddl.AppendLine("(");

            var columns = properties.Select(property =>
            {
                if (typeof(ICollection).IsAssignableFrom(property.PropertyType))
                {
                    return null;
                }
                if (property.PropertyType.GetCustomAttribute<NotMappedAttribute>() != null || property.GetSetMethod() == null)
                {
                    return null;
                }
                bool nullAllowed;
                var databaseType = MapDataType(property.PropertyType, out nullAllowed);
                if (nullAllowed)
                {
                    return $"    {property.Name} {databaseType}";
                }
                return $"    {property.Name} {databaseType} NOT NULL";
            });

            ddl.AppendLine(string.Join(",\n", columns.Where(column => column != null)));
            ddl.AppendLine(");");

            Console.WriteLine(ddl.ToString());
        }

        public static void CreateTable<T>(this IDbConnection connection)
        {
            var type = typeof(T);
            CreateTable(connection, type);
        }

        public static void CreateTables(this IDbConnection connection, params Type[] types)
        {
            foreach (var type in types)
            {
                CreateTable(connection, type);
            }
        }

        public static void CreateSchema(this IDbConnection connection, Assembly assembly)
        {

        }
    }
}