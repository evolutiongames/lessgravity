﻿using LessGravity.OpenSpace.Data;
using LessGravity.OpenSpace.Data.ItemTemplates;
using System;
using System.Data;
using System.Data.SqlClient;

namespace Data.CreateSchema
{
    class Program
    {
        static void Main(string[] args)
        {
            using (var dbConnection = new SqlConnection(@"Server=10.0.8.1\MSSQLDEV01;Initial Catalog=GameDb;Database=GameDb;User Id=gamedb;Password=gamedb;Integrated Security=False;"))
            {
                dbConnection.Open();

                dbConnection.CreateTable<ObjectBase>();
                dbConnection.CreateTable<Ore>();
                dbConnection.CreateTable<Wallet>();
                
            }

            Console.WriteLine("Press a key.");
            Console.ReadKey();
        }
    }
}