﻿namespace LessGravity.Network.TestServerLidgren
{
    public enum MessageType : int
    {
        Incoming = 0,
        Outgoing,
        Info,
        Error
    }
}