﻿namespace LessGravity.Network.TestServerLidgren
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.mnuMain = new System.Windows.Forms.MenuStrip();
            this.miFile = new System.Windows.Forms.ToolStripMenuItem();
            this.miServer = new System.Windows.Forms.ToolStripMenuItem();
            this.miServerStart = new System.Windows.Forms.ToolStripMenuItem();
            this.miServerStop = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripSeparator();
            this.miServerStatistics = new System.Windows.Forms.ToolStripMenuItem();
            this.statusMain = new System.Windows.Forms.StatusStrip();
            this.lblStatusServer = new System.Windows.Forms.ToolStripStatusLabel();
            this.pnlMessages = new System.Windows.Forms.Panel();
            this.tcMessages = new System.Windows.Forms.TabControl();
            this.tpServerMessage = new System.Windows.Forms.TabPage();
            this.lvServerMessages = new System.Windows.Forms.ListView();
            this.chTimestamp = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.chEndpoint = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.chText = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.ilIcons = new System.Windows.Forms.ImageList(this.components);
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.btnEnableMessages = new System.Windows.Forms.ToolStripButton();
            this.tpChatSystem = new System.Windows.Forms.TabPage();
            this.splitter1 = new System.Windows.Forms.Splitter();
            this.pnlMain = new System.Windows.Forms.Panel();
            this.pnlContent = new System.Windows.Forms.Panel();
            this.tcContent = new System.Windows.Forms.TabControl();
            this.tpCharacters = new System.Windows.Forms.TabPage();
            this.tpStations = new System.Windows.Forms.TabPage();
            this.splitter3 = new System.Windows.Forms.Splitter();
            this.pnlProperties = new System.Windows.Forms.Panel();
            this.tcProperties = new System.Windows.Forms.TabControl();
            this.tpProperties = new System.Windows.Forms.TabPage();
            this.splitter2 = new System.Windows.Forms.Splitter();
            this.pnlUsers = new System.Windows.Forms.Panel();
            this.tcUsers = new System.Windows.Forms.TabControl();
            this.tpUsers = new System.Windows.Forms.TabPage();
            this.lbUsers = new System.Windows.Forms.ListBox();
            this.tsAccounts = new System.Windows.Forms.ToolStrip();
            this.btnUserAdd = new System.Windows.Forms.ToolStripButton();
            this.btnUserRemove = new System.Windows.Forms.ToolStripButton();
            this.mnuMain.SuspendLayout();
            this.statusMain.SuspendLayout();
            this.pnlMessages.SuspendLayout();
            this.tcMessages.SuspendLayout();
            this.tpServerMessage.SuspendLayout();
            this.toolStrip1.SuspendLayout();
            this.pnlMain.SuspendLayout();
            this.pnlContent.SuspendLayout();
            this.tcContent.SuspendLayout();
            this.pnlProperties.SuspendLayout();
            this.tcProperties.SuspendLayout();
            this.pnlUsers.SuspendLayout();
            this.tcUsers.SuspendLayout();
            this.tpUsers.SuspendLayout();
            this.tsAccounts.SuspendLayout();
            this.SuspendLayout();
            // 
            // mnuMain
            // 
            this.mnuMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.miFile,
            this.miServer});
            this.mnuMain.Location = new System.Drawing.Point(0, 0);
            this.mnuMain.Name = "mnuMain";
            this.mnuMain.Size = new System.Drawing.Size(930, 24);
            this.mnuMain.TabIndex = 0;
            this.mnuMain.Text = "menuStrip1";
            // 
            // miFile
            // 
            this.miFile.Name = "miFile";
            this.miFile.Size = new System.Drawing.Size(37, 20);
            this.miFile.Text = "&File";
            // 
            // miServer
            // 
            this.miServer.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.miServerStart,
            this.miServerStop,
            this.toolStripMenuItem1,
            this.miServerStatistics});
            this.miServer.Name = "miServer";
            this.miServer.Size = new System.Drawing.Size(51, 20);
            this.miServer.Text = "&Server";
            // 
            // miServerStart
            // 
            this.miServerStart.Name = "miServerStart";
            this.miServerStart.Size = new System.Drawing.Size(152, 22);
            this.miServerStart.Text = "&Start";
            this.miServerStart.Click += new System.EventHandler(this.miServerStart_Click);
            // 
            // miServerStop
            // 
            this.miServerStop.Name = "miServerStop";
            this.miServerStop.Size = new System.Drawing.Size(152, 22);
            this.miServerStop.Text = "S&top";
            this.miServerStop.Click += new System.EventHandler(this.miServerStop_Click);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(149, 6);
            // 
            // miServerStatistics
            // 
            this.miServerStatistics.Name = "miServerStatistics";
            this.miServerStatistics.Size = new System.Drawing.Size(152, 22);
            this.miServerStatistics.Text = "Show Statisti&cs";
            this.miServerStatistics.Click += new System.EventHandler(this.miServerStatistics_Click);
            // 
            // statusMain
            // 
            this.statusMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.lblStatusServer});
            this.statusMain.Location = new System.Drawing.Point(0, 568);
            this.statusMain.Name = "statusMain";
            this.statusMain.Size = new System.Drawing.Size(930, 22);
            this.statusMain.TabIndex = 1;
            this.statusMain.Text = "statusStrip1";
            // 
            // lblStatusServer
            // 
            this.lblStatusServer.Name = "lblStatusServer";
            this.lblStatusServer.Size = new System.Drawing.Size(75, 17);
            this.lblStatusServer.Text = "Not Running";
            // 
            // pnlMessages
            // 
            this.pnlMessages.Controls.Add(this.tcMessages);
            this.pnlMessages.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnlMessages.Location = new System.Drawing.Point(0, 425);
            this.pnlMessages.Name = "pnlMessages";
            this.pnlMessages.Size = new System.Drawing.Size(930, 143);
            this.pnlMessages.TabIndex = 2;
            // 
            // tcMessages
            // 
            this.tcMessages.Controls.Add(this.tpServerMessage);
            this.tcMessages.Controls.Add(this.tpChatSystem);
            this.tcMessages.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tcMessages.Location = new System.Drawing.Point(0, 0);
            this.tcMessages.Name = "tcMessages";
            this.tcMessages.SelectedIndex = 0;
            this.tcMessages.Size = new System.Drawing.Size(930, 143);
            this.tcMessages.TabIndex = 0;
            // 
            // tpServerMessage
            // 
            this.tpServerMessage.Controls.Add(this.lvServerMessages);
            this.tpServerMessage.Controls.Add(this.toolStrip1);
            this.tpServerMessage.Location = new System.Drawing.Point(4, 24);
            this.tpServerMessage.Name = "tpServerMessage";
            this.tpServerMessage.Padding = new System.Windows.Forms.Padding(3);
            this.tpServerMessage.Size = new System.Drawing.Size(922, 115);
            this.tpServerMessage.TabIndex = 0;
            this.tpServerMessage.Text = "Server Messages";
            this.tpServerMessage.UseVisualStyleBackColor = true;
            // 
            // lvServerMessages
            // 
            this.lvServerMessages.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.chTimestamp,
            this.chEndpoint,
            this.chText});
            this.lvServerMessages.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lvServerMessages.Location = new System.Drawing.Point(3, 28);
            this.lvServerMessages.Name = "lvServerMessages";
            this.lvServerMessages.Size = new System.Drawing.Size(916, 84);
            this.lvServerMessages.SmallImageList = this.ilIcons;
            this.lvServerMessages.TabIndex = 1;
            this.lvServerMessages.UseCompatibleStateImageBehavior = false;
            this.lvServerMessages.View = System.Windows.Forms.View.Details;
            // 
            // chTimestamp
            // 
            this.chTimestamp.Text = "Timestamp";
            this.chTimestamp.Width = 170;
            // 
            // chEndpoint
            // 
            this.chEndpoint.Text = "Endpoint";
            this.chEndpoint.Width = 207;
            // 
            // chText
            // 
            this.chText.Text = "Text";
            this.chText.Width = 467;
            // 
            // ilIcons
            // 
            this.ilIcons.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("ilIcons.ImageStream")));
            this.ilIcons.TransparentColor = System.Drawing.Color.Transparent;
            this.ilIcons.Images.SetKeyName(0, "arrow_right.png");
            this.ilIcons.Images.SetKeyName(1, "arrow_left.png");
            this.ilIcons.Images.SetKeyName(2, "information.png");
            this.ilIcons.Images.SetKeyName(3, "error.png");
            // 
            // toolStrip1
            // 
            this.toolStrip1.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnEnableMessages});
            this.toolStrip1.Location = new System.Drawing.Point(3, 3);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(916, 25);
            this.toolStrip1.TabIndex = 0;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // btnEnableMessages
            // 
            this.btnEnableMessages.Checked = true;
            this.btnEnableMessages.CheckOnClick = true;
            this.btnEnableMessages.CheckState = System.Windows.Forms.CheckState.Checked;
            this.btnEnableMessages.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnEnableMessages.Image = ((System.Drawing.Image)(resources.GetObject("btnEnableMessages.Image")));
            this.btnEnableMessages.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnEnableMessages.Name = "btnEnableMessages";
            this.btnEnableMessages.Size = new System.Drawing.Size(23, 22);
            this.btnEnableMessages.Text = "Enable Messages";
            // 
            // tpChatSystem
            // 
            this.tpChatSystem.Location = new System.Drawing.Point(4, 24);
            this.tpChatSystem.Name = "tpChatSystem";
            this.tpChatSystem.Padding = new System.Windows.Forms.Padding(3);
            this.tpChatSystem.Size = new System.Drawing.Size(922, 115);
            this.tpChatSystem.TabIndex = 1;
            this.tpChatSystem.Text = "Chat [System]";
            this.tpChatSystem.UseVisualStyleBackColor = true;
            // 
            // splitter1
            // 
            this.splitter1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.splitter1.Location = new System.Drawing.Point(0, 422);
            this.splitter1.Name = "splitter1";
            this.splitter1.Size = new System.Drawing.Size(930, 3);
            this.splitter1.TabIndex = 3;
            this.splitter1.TabStop = false;
            // 
            // pnlMain
            // 
            this.pnlMain.Controls.Add(this.pnlContent);
            this.pnlMain.Controls.Add(this.splitter2);
            this.pnlMain.Controls.Add(this.pnlUsers);
            this.pnlMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlMain.Location = new System.Drawing.Point(0, 24);
            this.pnlMain.Name = "pnlMain";
            this.pnlMain.Size = new System.Drawing.Size(930, 398);
            this.pnlMain.TabIndex = 4;
            // 
            // pnlContent
            // 
            this.pnlContent.Controls.Add(this.tcContent);
            this.pnlContent.Controls.Add(this.splitter3);
            this.pnlContent.Controls.Add(this.pnlProperties);
            this.pnlContent.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlContent.Location = new System.Drawing.Point(149, 0);
            this.pnlContent.Name = "pnlContent";
            this.pnlContent.Size = new System.Drawing.Size(781, 398);
            this.pnlContent.TabIndex = 2;
            // 
            // tcContent
            // 
            this.tcContent.Controls.Add(this.tpCharacters);
            this.tcContent.Controls.Add(this.tpStations);
            this.tcContent.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tcContent.Location = new System.Drawing.Point(0, 0);
            this.tcContent.Name = "tcContent";
            this.tcContent.SelectedIndex = 0;
            this.tcContent.Size = new System.Drawing.Size(578, 398);
            this.tcContent.TabIndex = 2;
            // 
            // tpCharacters
            // 
            this.tpCharacters.Location = new System.Drawing.Point(4, 24);
            this.tpCharacters.Name = "tpCharacters";
            this.tpCharacters.Padding = new System.Windows.Forms.Padding(3);
            this.tpCharacters.Size = new System.Drawing.Size(570, 370);
            this.tpCharacters.TabIndex = 0;
            this.tpCharacters.Text = "Characters";
            this.tpCharacters.UseVisualStyleBackColor = true;
            // 
            // tpStations
            // 
            this.tpStations.Location = new System.Drawing.Point(4, 24);
            this.tpStations.Name = "tpStations";
            this.tpStations.Padding = new System.Windows.Forms.Padding(3);
            this.tpStations.Size = new System.Drawing.Size(570, 370);
            this.tpStations.TabIndex = 1;
            this.tpStations.Text = "Stations";
            this.tpStations.UseVisualStyleBackColor = true;
            // 
            // splitter3
            // 
            this.splitter3.Dock = System.Windows.Forms.DockStyle.Right;
            this.splitter3.Location = new System.Drawing.Point(578, 0);
            this.splitter3.Name = "splitter3";
            this.splitter3.Size = new System.Drawing.Size(3, 398);
            this.splitter3.TabIndex = 1;
            this.splitter3.TabStop = false;
            // 
            // pnlProperties
            // 
            this.pnlProperties.Controls.Add(this.tcProperties);
            this.pnlProperties.Dock = System.Windows.Forms.DockStyle.Right;
            this.pnlProperties.Location = new System.Drawing.Point(581, 0);
            this.pnlProperties.Name = "pnlProperties";
            this.pnlProperties.Size = new System.Drawing.Size(200, 398);
            this.pnlProperties.TabIndex = 0;
            // 
            // tcProperties
            // 
            this.tcProperties.Controls.Add(this.tpProperties);
            this.tcProperties.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tcProperties.Location = new System.Drawing.Point(0, 0);
            this.tcProperties.Name = "tcProperties";
            this.tcProperties.SelectedIndex = 0;
            this.tcProperties.Size = new System.Drawing.Size(200, 398);
            this.tcProperties.TabIndex = 0;
            // 
            // tpProperties
            // 
            this.tpProperties.Location = new System.Drawing.Point(4, 24);
            this.tpProperties.Name = "tpProperties";
            this.tpProperties.Padding = new System.Windows.Forms.Padding(3);
            this.tpProperties.Size = new System.Drawing.Size(192, 370);
            this.tpProperties.TabIndex = 0;
            this.tpProperties.Text = "Properties";
            this.tpProperties.UseVisualStyleBackColor = true;
            // 
            // splitter2
            // 
            this.splitter2.Location = new System.Drawing.Point(146, 0);
            this.splitter2.Name = "splitter2";
            this.splitter2.Size = new System.Drawing.Size(3, 398);
            this.splitter2.TabIndex = 1;
            this.splitter2.TabStop = false;
            // 
            // pnlUsers
            // 
            this.pnlUsers.Controls.Add(this.tcUsers);
            this.pnlUsers.Dock = System.Windows.Forms.DockStyle.Left;
            this.pnlUsers.Location = new System.Drawing.Point(0, 0);
            this.pnlUsers.Name = "pnlUsers";
            this.pnlUsers.Size = new System.Drawing.Size(146, 398);
            this.pnlUsers.TabIndex = 0;
            // 
            // tcUsers
            // 
            this.tcUsers.Controls.Add(this.tpUsers);
            this.tcUsers.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tcUsers.Location = new System.Drawing.Point(0, 0);
            this.tcUsers.Name = "tcUsers";
            this.tcUsers.SelectedIndex = 0;
            this.tcUsers.Size = new System.Drawing.Size(146, 398);
            this.tcUsers.TabIndex = 0;
            // 
            // tpUsers
            // 
            this.tpUsers.Controls.Add(this.lbUsers);
            this.tpUsers.Controls.Add(this.tsAccounts);
            this.tpUsers.Location = new System.Drawing.Point(4, 24);
            this.tpUsers.Name = "tpUsers";
            this.tpUsers.Padding = new System.Windows.Forms.Padding(3);
            this.tpUsers.Size = new System.Drawing.Size(138, 370);
            this.tpUsers.TabIndex = 0;
            this.tpUsers.Text = "Users";
            this.tpUsers.UseVisualStyleBackColor = true;
            // 
            // lbUsers
            // 
            this.lbUsers.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbUsers.FormattingEnabled = true;
            this.lbUsers.IntegralHeight = false;
            this.lbUsers.ItemHeight = 15;
            this.lbUsers.Location = new System.Drawing.Point(3, 28);
            this.lbUsers.Name = "lbUsers";
            this.lbUsers.Size = new System.Drawing.Size(132, 339);
            this.lbUsers.TabIndex = 1;
            // 
            // tsAccounts
            // 
            this.tsAccounts.CanOverflow = false;
            this.tsAccounts.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.tsAccounts.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnUserAdd,
            this.btnUserRemove});
            this.tsAccounts.Location = new System.Drawing.Point(3, 3);
            this.tsAccounts.Name = "tsAccounts";
            this.tsAccounts.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.tsAccounts.Size = new System.Drawing.Size(132, 25);
            this.tsAccounts.TabIndex = 0;
            this.tsAccounts.Text = "toolStrip1";
            // 
            // btnUserAdd
            // 
            this.btnUserAdd.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnUserAdd.Image = ((System.Drawing.Image)(resources.GetObject("btnUserAdd.Image")));
            this.btnUserAdd.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnUserAdd.Name = "btnUserAdd";
            this.btnUserAdd.Size = new System.Drawing.Size(23, 22);
            this.btnUserAdd.Text = "Add Account";
            // 
            // btnUserRemove
            // 
            this.btnUserRemove.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnUserRemove.Image = ((System.Drawing.Image)(resources.GetObject("btnUserRemove.Image")));
            this.btnUserRemove.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnUserRemove.Name = "btnUserRemove";
            this.btnUserRemove.Size = new System.Drawing.Size(23, 22);
            this.btnUserRemove.Text = "Remove Account";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(930, 590);
            this.Controls.Add(this.pnlMain);
            this.Controls.Add(this.splitter1);
            this.Controls.Add(this.pnlMessages);
            this.Controls.Add(this.statusMain);
            this.Controls.Add(this.mnuMain);
            this.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.mnuMain;
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "LessGravity.Network.Server";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.mnuMain.ResumeLayout(false);
            this.mnuMain.PerformLayout();
            this.statusMain.ResumeLayout(false);
            this.statusMain.PerformLayout();
            this.pnlMessages.ResumeLayout(false);
            this.tcMessages.ResumeLayout(false);
            this.tpServerMessage.ResumeLayout(false);
            this.tpServerMessage.PerformLayout();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.pnlMain.ResumeLayout(false);
            this.pnlContent.ResumeLayout(false);
            this.tcContent.ResumeLayout(false);
            this.pnlProperties.ResumeLayout(false);
            this.tcProperties.ResumeLayout(false);
            this.pnlUsers.ResumeLayout(false);
            this.tcUsers.ResumeLayout(false);
            this.tpUsers.ResumeLayout(false);
            this.tpUsers.PerformLayout();
            this.tsAccounts.ResumeLayout(false);
            this.tsAccounts.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip mnuMain;
        private System.Windows.Forms.ToolStripMenuItem miFile;
        private System.Windows.Forms.ToolStripMenuItem miServer;
        private System.Windows.Forms.ToolStripMenuItem miServerStart;
        private System.Windows.Forms.ToolStripMenuItem miServerStop;
        private System.Windows.Forms.StatusStrip statusMain;
        private System.Windows.Forms.ToolStripStatusLabel lblStatusServer;
        private System.Windows.Forms.Panel pnlMessages;
        private System.Windows.Forms.TabControl tcMessages;
        private System.Windows.Forms.TabPage tpServerMessage;
        private System.Windows.Forms.TabPage tpChatSystem;
        private System.Windows.Forms.Splitter splitter1;
        private System.Windows.Forms.Panel pnlMain;
        private System.Windows.Forms.Panel pnlContent;
        private System.Windows.Forms.Splitter splitter2;
        private System.Windows.Forms.Panel pnlUsers;
        private System.Windows.Forms.TabControl tcUsers;
        private System.Windows.Forms.TabPage tpUsers;
        private System.Windows.Forms.TabControl tcContent;
        private System.Windows.Forms.TabPage tpCharacters;
        private System.Windows.Forms.TabPage tpStations;
        private System.Windows.Forms.Splitter splitter3;
        private System.Windows.Forms.Panel pnlProperties;
        private System.Windows.Forms.TabControl tcProperties;
        private System.Windows.Forms.TabPage tpProperties;
        private System.Windows.Forms.ListBox lbUsers;
        private System.Windows.Forms.ToolStrip tsAccounts;
        private System.Windows.Forms.ToolStripButton btnUserAdd;
        private System.Windows.Forms.ToolStripButton btnUserRemove;
        private System.Windows.Forms.ImageList ilIcons;
        private System.Windows.Forms.ListView lvServerMessages;
        private System.Windows.Forms.ColumnHeader chTimestamp;
        private System.Windows.Forms.ColumnHeader chEndpoint;
        private System.Windows.Forms.ColumnHeader chText;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton btnEnableMessages;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem miServerStatistics;
    }
}

