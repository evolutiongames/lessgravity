﻿namespace LessGravity.Network.TestServerLidgren
{
    partial class StatisticsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(StatisticsForm));
            this.timerMain = new System.Windows.Forms.Timer(this.components);
            this.lblReceivedBytes = new System.Windows.Forms.Label();
            this.lblSent = new System.Windows.Forms.Label();
            this.lblReceivedPackets = new System.Windows.Forms.Label();
            this.lblReceived = new System.Windows.Forms.Label();
            this.lblReceivedMessages = new System.Windows.Forms.Label();
            this.lblSentBytes = new System.Windows.Forms.Label();
            this.lblSentPackets = new System.Windows.Forms.Label();
            this.lblSentMessages = new System.Windows.Forms.Label();
            this.lblStorageBytesAllocated = new System.Windows.Forms.Label();
            this.lblBytesInRecyclePool = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // timerMain
            // 
            this.timerMain.Enabled = true;
            this.timerMain.Interval = 1000;
            this.timerMain.Tick += new System.EventHandler(this.timerMain_Tick);
            // 
            // lblReceivedBytes
            // 
            this.lblReceivedBytes.AutoSize = true;
            this.lblReceivedBytes.Location = new System.Drawing.Point(57, 68);
            this.lblReceivedBytes.Name = "lblReceivedBytes";
            this.lblReceivedBytes.Size = new System.Drawing.Size(38, 15);
            this.lblReceivedBytes.TabIndex = 0;
            this.lblReceivedBytes.Text = "label1";
            // 
            // lblSent
            // 
            this.lblSent.AutoSize = true;
            this.lblSent.Location = new System.Drawing.Point(224, 53);
            this.lblSent.Name = "lblSent";
            this.lblSent.Size = new System.Drawing.Size(30, 15);
            this.lblSent.TabIndex = 1;
            this.lblSent.Text = "Sent";
            // 
            // lblReceivedPackets
            // 
            this.lblReceivedPackets.AutoSize = true;
            this.lblReceivedPackets.Location = new System.Drawing.Point(57, 83);
            this.lblReceivedPackets.Name = "lblReceivedPackets";
            this.lblReceivedPackets.Size = new System.Drawing.Size(38, 15);
            this.lblReceivedPackets.TabIndex = 2;
            this.lblReceivedPackets.Text = "label3";
            // 
            // lblReceived
            // 
            this.lblReceived.AutoSize = true;
            this.lblReceived.Location = new System.Drawing.Point(57, 53);
            this.lblReceived.Name = "lblReceived";
            this.lblReceived.Size = new System.Drawing.Size(54, 15);
            this.lblReceived.TabIndex = 3;
            this.lblReceived.Text = "Received";
            // 
            // lblReceivedMessages
            // 
            this.lblReceivedMessages.AutoSize = true;
            this.lblReceivedMessages.Location = new System.Drawing.Point(57, 98);
            this.lblReceivedMessages.Name = "lblReceivedMessages";
            this.lblReceivedMessages.Size = new System.Drawing.Size(38, 15);
            this.lblReceivedMessages.TabIndex = 4;
            this.lblReceivedMessages.Text = "label2";
            // 
            // lblSentBytes
            // 
            this.lblSentBytes.AutoSize = true;
            this.lblSentBytes.Location = new System.Drawing.Point(224, 68);
            this.lblSentBytes.Name = "lblSentBytes";
            this.lblSentBytes.Size = new System.Drawing.Size(38, 15);
            this.lblSentBytes.TabIndex = 5;
            this.lblSentBytes.Text = "label4";
            // 
            // lblSentPackets
            // 
            this.lblSentPackets.AutoSize = true;
            this.lblSentPackets.Location = new System.Drawing.Point(224, 83);
            this.lblSentPackets.Name = "lblSentPackets";
            this.lblSentPackets.Size = new System.Drawing.Size(38, 15);
            this.lblSentPackets.TabIndex = 6;
            this.lblSentPackets.Text = "label5";
            // 
            // lblSentMessages
            // 
            this.lblSentMessages.AutoSize = true;
            this.lblSentMessages.Location = new System.Drawing.Point(224, 98);
            this.lblSentMessages.Name = "lblSentMessages";
            this.lblSentMessages.Size = new System.Drawing.Size(38, 15);
            this.lblSentMessages.TabIndex = 7;
            this.lblSentMessages.Text = "label6";
            // 
            // lblStorageBytesAllocated
            // 
            this.lblStorageBytesAllocated.AutoSize = true;
            this.lblStorageBytesAllocated.Location = new System.Drawing.Point(57, 143);
            this.lblStorageBytesAllocated.Name = "lblStorageBytesAllocated";
            this.lblStorageBytesAllocated.Size = new System.Drawing.Size(38, 15);
            this.lblStorageBytesAllocated.TabIndex = 8;
            this.lblStorageBytesAllocated.Text = "label2";
            // 
            // lblBytesInRecyclePool
            // 
            this.lblBytesInRecyclePool.AutoSize = true;
            this.lblBytesInRecyclePool.Location = new System.Drawing.Point(57, 128);
            this.lblBytesInRecyclePool.Name = "lblBytesInRecyclePool";
            this.lblBytesInRecyclePool.Size = new System.Drawing.Size(38, 15);
            this.lblBytesInRecyclePool.TabIndex = 9;
            this.lblBytesInRecyclePool.Text = "label2";
            // 
            // StatisticsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(358, 189);
            this.Controls.Add(this.lblBytesInRecyclePool);
            this.Controls.Add(this.lblStorageBytesAllocated);
            this.Controls.Add(this.lblSentMessages);
            this.Controls.Add(this.lblSentPackets);
            this.Controls.Add(this.lblSentBytes);
            this.Controls.Add(this.lblReceivedMessages);
            this.Controls.Add(this.lblReceived);
            this.Controls.Add(this.lblReceivedPackets);
            this.Controls.Add(this.lblSent);
            this.Controls.Add(this.lblReceivedBytes);
            this.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "StatisticsForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "StatisticsForm";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Timer timerMain;
        private System.Windows.Forms.Label lblReceivedBytes;
        private System.Windows.Forms.Label lblSent;
        private System.Windows.Forms.Label lblReceivedPackets;
        private System.Windows.Forms.Label lblReceived;
        private System.Windows.Forms.Label lblReceivedMessages;
        private System.Windows.Forms.Label lblSentBytes;
        private System.Windows.Forms.Label lblSentPackets;
        private System.Windows.Forms.Label lblSentMessages;
        private System.Windows.Forms.Label lblStorageBytesAllocated;
        private System.Windows.Forms.Label lblBytesInRecyclePool;
    }
}