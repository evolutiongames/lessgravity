﻿using LessGravity.Network.TestServerLidgren.Properties;
using LessGravity.OpenSpace.Data.Context;
using System;
using System.Windows.Forms;

namespace LessGravity.Network.TestServerLidgren
{
    public partial class MainForm : Form
    {
        private readonly NetworkServer _server;
        private readonly StatisticsForm _statisticsForm;

        public MainForm()
        {
            InitializeComponent();

            var dbContext = new GameDbContext();
            _server = new NetworkServer(dbContext);
            _server.OnStatusMessage += ServerStatusMessage;

            _statisticsForm = new StatisticsForm(_server);
        }

        private void ServerStatusMessage(MessageType messageType, string endPoint, string message)
        {
            if (lvServerMessages.InvokeRequired)
            {
                lvServerMessages.Invoke(new MethodInvoker(() => ServerStatusMessage(messageType, endPoint, message)));
            }
            else
            {
                if (!btnEnableMessages.Checked)
                {
                    return;
                }
                var lvItem = lvServerMessages.Items.Add(DateTime.Now.ToString("G"));
                lvItem.SubItems.Add(endPoint ?? string.Empty);
                lvItem.SubItems.Add(message);
                lvItem.ImageIndex = (int)messageType;
                lvItem.StateImageIndex = (int)messageType;
            }
        }

        private void miServerStart_Click(object sender, EventArgs e)
        {
            _server.StartServer();
            lblStatusServer.Text = Resources.Strings_Running;
            ServerStatusMessage(MessageType.Info, null, "Server Started");
        }

        private void miServerStop_Click(object sender, EventArgs e)
        {
            _server.StopServer();
            lblStatusServer.Text = Resources.Strings_NotRunning;
            ServerStatusMessage(MessageType.Info, null, "Server Stopped");
        }

        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            _server.StopServer();
            _statisticsForm.Close();
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
#if DEBUG
            miServerStart.PerformClick();
#endif
            UpdateUserList();
        }

        private void miServerStatistics_Click(object sender, EventArgs e)
        {
            if (_statisticsForm.Visible)
            {
                _statisticsForm.Hide();
            }
            else
            {
                _statisticsForm.Show();
            }
        }

        private void UpdateUserList()
        {
            lbUsers.BeginUpdate();
            foreach (var user in _server.Users)
            {
                lbUsers.Items.Add(user);
            }
            lbUsers.EndUpdate();
        }
    }
}