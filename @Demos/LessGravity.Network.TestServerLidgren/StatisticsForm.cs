﻿using System;
using System.Windows.Forms;

namespace LessGravity.Network.TestServerLidgren
{
    public partial class StatisticsForm : Form
    {
        private readonly NetworkServer _networkServer;

        public StatisticsForm()
        {
            InitializeComponent();
        }

        public StatisticsForm(NetworkServer networkServer) 
            : this()
        {
            _networkServer = networkServer;
        }

        private void timerMain_Tick(object sender, EventArgs e)
        {
            var statistics = _networkServer.Statistics;

            lblReceivedBytes.Text = string.Format("Received Bytes: {0}", statistics.ReceivedBytes);
            lblReceivedPackets.Text = string.Format("Received Packets: {0}", statistics.ReceivedPackets);
            lblReceivedMessages.Text = string.Format("Received Messages: {0}", statistics.ReceivedMessages);

            lblSentBytes.Text = string.Format("Sent Bytes: {0}", statistics.SentBytes);
            lblSentPackets.Text = string.Format("Sent Packets: {0}", statistics.SentPackets);
            lblSentMessages.Text = string.Format("Sent Messages: {0}", statistics.SentMessages);

            lblBytesInRecyclePool.Text = string.Format("BytesInRecyclePool: {0}", statistics.BytesInRecyclePool);
            lblStorageBytesAllocated.Text = string.Format("StorageBytesAllocated: {0}", statistics.StorageBytesAllocated);
        }
    }
}