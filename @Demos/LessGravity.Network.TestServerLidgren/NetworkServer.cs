﻿using LessGravity.OpenSpace.Data;
using LessGravity.OpenSpace.Data.Context;
using LessGravity.OpenSpace.Shared;
using LessGravity.OpenSpace.Shared.Packets;
using Lidgren.Network;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Windows.Forms;

namespace LessGravity.Network.TestServerLidgren
{
    public class NetworkServer
    {
        private const int ServerPort = 14433;
        private const int ClientPort = 14434;

        private readonly NetPeerConfiguration _serverConfiguration;
        private readonly NetServer _server;
        private bool _isServerRunning;
        private Thread _serverThread;
        private readonly string _userListFileName = Path.Combine(Application.StartupPath, "Data", "UserList.xml");

        public List<Character> Characters { get; private set; }

        public event Action<MessageType, string, string> OnStatusMessage;

        public NetPeerStatistics Statistics => _server.Statistics;

        public List<User> Users { get; private set; }

        private void DoStatusMessage(MessageType messageType, NetIncomingMessage incomingMessage, string message)
        {
            OnStatusMessage?.Invoke(messageType, $"{incomingMessage.SenderEndPoint} {NetUtility.ToHexString(incomingMessage.SenderConnection.RemoteUniqueIdentifier)}", message);
        }

        public NetworkServer(GameDbContext dbContext)
        {
            Users = dbContext.Users.ToList();
            Characters = dbContext.Characters.ToList();

            _serverConfiguration = new NetPeerConfiguration(Strings.Session.ApplicationName)
            {
                Port = ServerPort,
                MaximumConnections = 100,
            };
            _serverConfiguration.SetMessageTypeEnabled(NetIncomingMessageType.DebugMessage, false);
            _serverConfiguration.SetMessageTypeEnabled(NetIncomingMessageType.VerboseDebugMessage, false);

            _server = new NetServer(_serverConfiguration);
        }

        private void ServerThreadProc(object state)
        {
            while (_server.Status == NetPeerStatus.Running)
            {
                NetIncomingMessage incomingMessage;
                while ((incomingMessage = _server.ReadMessage()) != null)
                {
                    switch (incomingMessage.MessageType)
                    {
                        case NetIncomingMessageType.DebugMessage:
                        case NetIncomingMessageType.ErrorMessage:
                        case NetIncomingMessageType.WarningMessage:
                        case NetIncomingMessageType.VerboseDebugMessage:
                            string text = incomingMessage.ReadString();
                            DoStatusMessage(MessageType.Info, incomingMessage, incomingMessage.MessageType + " : " + text);
                            break;

                        case NetIncomingMessageType.StatusChanged:
                            var networkStatus = (NetConnectionStatus)incomingMessage.ReadByte();
                            var reason = incomingMessage.ReadString();
                            DoStatusMessage(MessageType.Info, incomingMessage, "Status : " + NetUtility.ToHexString(incomingMessage.SenderConnection.RemoteUniqueIdentifier) + " " + networkStatus + ": " + reason);

                            if (networkStatus == NetConnectionStatus.Connected)
                            {
                                DoStatusMessage(MessageType.Info, incomingMessage, "Remote hail: " + incomingMessage.SenderConnection.RemoteHailMessage.ReadString());
                            }

                            UpdateConnectionsList();
                            break;
                        case NetIncomingMessageType.Data:
                            var packetType = (PacketType)incomingMessage.ReadByte();
                            var packet = PacketFactory.CreatePacket(packetType, incomingMessage);
                            var senderEndPoint = incomingMessage.SenderEndPoint;
                            var senderConnection = incomingMessage.SenderConnection;
                            DoStatusMessage(MessageType.Incoming, incomingMessage, "Packet Received: " + packet);
                            switch (packetType)
                            {
                                case PacketType.Chat:
                                    var chatPacket = packet as ChatPacket;
                                    // broadcast this to all connections, except sender
                                    var allConnections = _server.Connections; // get copy
                                    allConnections.Remove(senderConnection);
                                    if (allConnections.Count > 0)
                                    {
                                        var outgoingMessage = _server.CreateMessage();
                                        outgoingMessage.Write(NetUtility.ToHexString(senderConnection.RemoteUniqueIdentifier) + " said: " + chatPacket.Message);
                                        _server.SendMessage(outgoingMessage, allConnections, NetDeliveryMethod.ReliableOrdered, 0);
                                    }
                                    break;
                                case PacketType.DeleteCharacterRequest:
                                    var deleteCharacterRequest = packet as DeleteCharacterRequestPacket;
                                    var deleteCharacterResponse = new DeleteCharacterResponsePacket
                                    {
                                        UserId = deleteCharacterRequest.UserId,
                                        CharacterId = deleteCharacterRequest.CharacterId
                                    };
                                    var userCharacterToBeDeleted = Users.FirstOrDefault(u => u.Id == deleteCharacterRequest.UserId);
                                    var characterToBeDeleted = Characters.FirstOrDefault(c => c.Id == deleteCharacterRequest.CharacterId);
                                    if (userCharacterToBeDeleted != null && characterToBeDeleted != null && characterToBeDeleted.User == userCharacterToBeDeleted)
                                    {
                                        Characters.Remove(characterToBeDeleted);
                                    }
                                    _server.SendMessage(deleteCharacterResponse.GetMessage(_server), senderConnection, NetDeliveryMethod.ReliableOrdered);
                                    DoStatusMessage(MessageType.Outgoing, incomingMessage, "Packet Sent: " + deleteCharacterResponse);
                                    break;
                                case PacketType.LoginRequest:
                                    var loginRequest = packet as LoginRequestPacket;
                                    var loginResponse = new LoginResponsePacket();
                                    var user = Users.FirstOrDefault(a => a.Name == loginRequest.UserName);
                                    if (user != null)
                                    {
                                        if (user.PasswordHash == loginRequest.Password)
                                        {
                                            if (user.IsBanned)
                                            {
                                                loginResponse.Result = LoginResult.FailureUserBanned;
                                            }
                                            else if (user.IsTrialAccount) //TODO(deccer) no indication about trial period
                                            {
                                                loginResponse.Result = LoginResult.FailureUserTrialExpired;
                                            }
                                            else
                                            {
                                                loginResponse.Result = LoginResult.Success;
                                                senderConnection.Tag = user;
                                                //senderConnection.Approve();
                                            }
                                        }
                                        else
                                        {
                                            loginResponse.Result = LoginResult.FailureWrongPassword;
                                        }
                                        loginResponse.LoginTimeStamp = DateTime.Now;
                                        loginResponse.LastLoginTimeStamp = user.DateLastLogin.HasValue ? user.DateLastLogin.Value : DateTime.MinValue;
                                        loginResponse.UserId = user.Id;
                                    }
                                    else
                                    {
                                        loginResponse.UserId = int.MaxValue;
                                        loginResponse.LoginTimeStamp = DateTime.MinValue;
                                        loginResponse.Result = LoginResult.FailureWrongUserName;
                                    }
                                    _server.SendMessage(loginResponse.GetMessage(_server), senderConnection, NetDeliveryMethod.ReliableOrdered);
                                    DoStatusMessage(MessageType.Outgoing, incomingMessage, "Packet Sent: " + loginResponse);
                                    break;
                                case PacketType.LogoutRequest:
                                    var logoutRequest = packet as LogoutRequestPacket;
                                    var logoutResponse = new LogoutResponsePacket();
                                    var userToLogout = Users.FirstOrDefault(u => u.Id == logoutRequest.UserId);
                                    userToLogout.DateLastLogin = userToLogout.DateLogin;

                                    logoutResponse.UserId = logoutRequest.UserId;
                                    logoutResponse.Result = LogoutResult.Normal;
                                    _server.SendMessage(logoutResponse.GetMessage(_server), senderConnection, NetDeliveryMethod.ReliableOrdered);
                                    DoStatusMessage(MessageType.Outgoing, incomingMessage, "Packet Sent: " + logoutResponse);
                                    break;
                                case PacketType.RegisterRequest:
                                    var registerRequest = packet as RegisterRequestPacket;
                                    var registerResponse = new RegisterResponsePacket();

                                    var newUser = Users.FirstOrDefault(u => u.Name == registerRequest.UserName);
                                    if (newUser == null)
                                    {
                                        //
                                        // create new user
                                        //

                                        //
                                        // check if password is strong enough
                                        //

                                        newUser = new User
                                        {
                                            Name = registerRequest.UserName, 
                                            PasswordHash = registerRequest.Password,
                                            DateCreated = DateTime.Now,
                                            Role = UserRole.Normal,
                                            IsLoggedIn = true,
                                            DateLogin = DateTime.Now,
                                        };
                                        Users.Add(newUser);
                                        registerResponse.UserId = newUser.Id;
                                        registerResponse.Result = RegisterResult.Success;
                                    }
                                    else
                                    {
                                        //
                                        // user exists already, revoke registration
                                        //
                                        registerResponse.Result = RegisterResult.FailureUserAlreadyExists;
                                    }
                                    _server.SendMessage(registerResponse.GetMessage(_server), senderConnection, NetDeliveryMethod.ReliableOrdered);
                                    break;
                                case PacketType.GetCharactersRequest:
                                    var getCharactersRequest = packet as GetCharactersRequestPacket;
                                    var getCharactersResponse = new GetCharactersResponsePacket();
                                    var characters = Characters.Where(character => character.User.Id == getCharactersRequest.UserId);
                                    getCharactersResponse.UserId = getCharactersRequest.UserId;
                                    getCharactersResponse.Characters.AddRange(characters.Select(character =>
                                    {
                                        var characterImageFileName = Path.Combine(Application.StartupPath, "Data", "Characters", character.Id.ToString("0000") + ".png");
                                        var characterImageBytes = File.Exists(characterImageFileName) ? File.ReadAllBytes(characterImageFileName) : null;
                                        var characterInfo = new CharacterInfo
                                        {
                                            CharacterId = character.Id,
                                            CharacterName = character.Name,
                                            CharacterAppearance = character.Appearance
                                        };
                                        return characterInfo;
                                    }));
                                    _server.SendMessage(getCharactersResponse.GetMessage(_server), senderConnection, NetDeliveryMethod.ReliableOrdered);
                                    DoStatusMessage(MessageType.Outgoing, incomingMessage, "Packet Sent: " + getCharactersResponse);

                                    break;
                            }
                            break;
                        default:
                            DoStatusMessage(MessageType.Error, incomingMessage, $"Unhandled type: {incomingMessage.MessageType} {incomingMessage.LengthBytes} bytes {incomingMessage.DeliveryMethod} | {incomingMessage.SequenceChannel}");
                            break;
                    }
                    _server.Recycle(incomingMessage);
                }
                Thread.Sleep(1);
            }
        }

        public void StartServer()
        {
            try
            {
                _server.Start();
                _serverThread = new Thread(ServerThreadProc);
                _serverThread.Start();
            }
            finally
            {
                _isServerRunning = true;
            }
        }

        public void StopServer()
        {
            if (!_isServerRunning)
            {
                return;
            }
            try
            {
                _serverThread.Abort();
                Thread.Sleep(100);
                _serverThread = null;
                _server.Shutdown("Server Stopped");
            }
            finally
            {
                _isServerRunning = false;
            }
        }

        private void UpdateConnectionsList()
        {
            foreach (var connection in _server.Connections)
            {
                var str = NetUtility.ToHexString(connection.RemoteUniqueIdentifier) + " from " + connection.RemoteEndPoint + " [" + connection.Status + "]";
            }
        }
    }
}