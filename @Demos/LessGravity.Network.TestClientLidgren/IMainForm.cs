﻿using LessGravity.Network.TestClientLidgren.Controls;

namespace LessGravity.Network.TestClientLidgren
{
    public interface IMainForm
    {
        CharacterCreationControl CharacterCreationControl { get; }

        CharacterSelectionControl CharacterSelectionControl { get; }

        void Close();
    }
}