﻿using LessGravity.Quasar.Game;
using System.Windows.Forms;

namespace LessGravity.Network.TestClientLidgren
{
    internal sealed class TestClientFormFactory : IFormFactory
    {
        public Form CreateForm(string title)
        {
            return new MainForm { Text = title };
        }
    }
}