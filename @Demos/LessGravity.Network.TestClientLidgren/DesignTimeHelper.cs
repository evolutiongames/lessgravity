﻿using System.ComponentModel;
using System.Diagnostics;

namespace LessGravity.Network.TestClientLidgren
{
    public static class DesignTimeHelper
    {
        public static bool IsInDesignMode
        {
            get
            {
                var isInDesignMode = LicenseManager.UsageMode == LicenseUsageMode.Designtime || Debugger.IsAttached;

                if (!isInDesignMode)
                {
                    using (var process = Process.GetCurrentProcess())
                    {
                        return process.ProcessName.ToLowerInvariant().Contains("devenv");
                    }
                }
                return isInDesignMode;
            }
        }
    }
}