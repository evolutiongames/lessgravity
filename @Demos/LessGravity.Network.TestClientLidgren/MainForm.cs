﻿using LessGravity.OpenSpace.Data;
using LessGravity.OpenSpace.Shared;
using LessGravity.OpenSpace.Shared.Packets;
using SharpDX.Windows;
using System;
using System.Configuration;
using System.Windows.Forms;
using LessGravity.Network.TestClientLidgren.Controls;

namespace LessGravity.Network.TestClientLidgren
{
    public partial class MainForm : RenderForm, IMainForm
    {
        private const int DefaultServerPort = 14433;
        private readonly string _serverHost;
        private readonly int _serverPort;

        private readonly NetworkClient _networkClient;
        private readonly LoginControl _userLoginControl;

        public CharacterCreationControl CharacterCreationControl { get; }

        public CharacterSelectionControl CharacterSelectionControl { get; }

        private static void AddMessage(ListView control, string message)
        {
            if (control.InvokeRequired)
            {
                control.Invoke(new MethodInvoker(() => AddMessage(control, message)));
            }
            else
            {
                var lvItem = control.Items.Add(DateTime.Now.ToString("G"));
                lvItem.SubItems.Add(message);
            }
        }

        private void ClientConnected()
        {
            if (_networkClient.IsRegistering)
            {
                _networkClient.Register(_userLoginControl.UserName, _userLoginControl.PasswordHash);
            }
            else if (_networkClient.IsLoggedIn)
            {
                _networkClient.Logout();
            }
            else
            {
                _networkClient.Login(_userLoginControl.UserName, _userLoginControl.PasswordHash);
            }
        }

        private void ClientLoginFailure(LoginResult loginResult)
        {
            _userLoginControl.SetLoginResultOnError(loginResult);
        }

        private void ClientLoginSuccess(long userId, DateTime loginTimeStamp, DateTime lastLoginTimeStamp)
        {
            if (InvokeRequired)
            {
                Invoke(new MethodInvoker(() => ClientLoginSuccess(userId, loginTimeStamp, lastLoginTimeStamp)));
            }
            else
            {
                _networkClient.UserId = userId;

                _userLoginControl.Visible = false;
                LoadCharacters();
                CharacterSelectionControl.Visible = true;
                CharacterCreationControl.PopuplateImageLists();
                CharacterCreationControl.Visible = false;

                Text = "UserId: " + userId;
                SetStatusText("Last Login: " + lastLoginTimeStamp);
            }
        }

        private void ClientLogout(LogoutResult logoutResult)
        {
            CharacterCreationControl.Visible = false;
            CharacterSelectionControl.Visible = false;
            _userLoginControl.Visible = true;
        }

        private void ClientReceivePacket(PacketType packetType, IPacket packet)
        {
            switch (packetType)
            {
                case PacketType.DeleteCharacterResponse:
                    var deleteCharacterResponse = packet as DeleteCharacterResponsePacket;
                    var getCharactersRequest = new GetCharactersRequestPacket
                    {
                        UserId = _networkClient.UserId,
                    };
                    _networkClient.SendPacket(getCharactersRequest);
                    break;
                case PacketType.GetCharactersResponse:
                    var getCharactersResponse = packet as GetCharactersResponsePacket;
                    CharacterSelectionControl.SetCharacters(getCharactersResponse.Characters);
                    break;

            }
        }

        private void ClientRegisterSuccess(long userId, DateTime registeredTimeStamp)
        {
            ClientLoginSuccess(userId, DateTime.Now, DateTime.Now);
        }

        private void ClientRegisterFailure(RegisterResult registerResult)
        {

        }

        private void LoadCharacters()
        {
            var getCharactersRequest = new GetCharactersRequestPacket
            {
                UserId = _networkClient.UserId,
            };
            _networkClient.SendPacket(getCharactersRequest);
        }

        public MainForm()
        {
            InitializeComponent();

            _serverHost = ConfigurationManager.AppSettings[Strings.Server.Host];
            var serverPortAsString = ConfigurationManager.AppSettings.Get(Strings.Server.Port);
            _serverPort = string.IsNullOrEmpty(serverPortAsString)
                ? DefaultServerPort
                : (int.TryParse(serverPortAsString, out int serverPort) ? serverPort : DefaultServerPort);

            _networkClient = new NetworkClient();
            _networkClient.OnConnected += ClientConnected;
            _networkClient.OnLoginFailure += ClientLoginFailure;
            _networkClient.OnLoginSuccess += ClientLoginSuccess;
            _networkClient.OnLogout += ClientLogout;
            _networkClient.OnReceivedPacket += ClientReceivePacket;
            _networkClient.OnRegisterSuccess += ClientRegisterSuccess;
            _networkClient.OnRegisterFailure += ClientRegisterFailure;

            _userLoginControl =
                new LoginControl(this, _networkClient, _serverHost, _serverPort) { Dock = DockStyle.Fill };


            CharacterSelectionControl = new CharacterSelectionControl(this, _networkClient)
            {
                Visible = false,
                Dock = DockStyle.Fill
            };
            CharacterSelectionControl.OnCharacterCreate += CharacterSelection_CreateCharacter;
            CharacterSelectionControl.OnCharacterSelect += CharacterSelection_SelectCharacter;

            CharacterCreationControl = new CharacterCreationControl(this, _networkClient)
            {
                Visible = false,
                Dock = DockStyle.Fill
            };

            Controls.Add(_userLoginControl);
            Controls.Add(CharacterSelectionControl);
            Controls.Add(CharacterCreationControl);
        }

        private void CharacterSelection_CreateCharacter()
        {
            CharacterSelectionControl.Visible = false;
            CharacterCreationControl.Visible = true;
        }

        private void CharacterSelection_SelectCharacter(CharacterInfo characterInfo)
        {
            CharacterSelectionControl.Visible = false;
            CharacterCreationControl.Visible = false;
        }

        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (_networkClient == null)
            {
                return;
            }
            if (_networkClient.IsLoggedIn)
            {
                _networkClient.Logout();
            }
            _networkClient.Dispose();
        }

        public void SetStatusText(string message)
        {
            if (InvokeRequired)
            {
                Invoke(new MethodInvoker(() => SetStatusText(message)));
            }

            lblStatus.Text = message;
        }
    }
}