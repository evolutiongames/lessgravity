﻿using LessGravity.OpenSpace.Data;
using LessGravity.OpenSpace.Shared;
using LessGravity.OpenSpace.Shared.Packets;
using Lidgren.Network;
using System;
using System.Diagnostics;
using System.Threading;

namespace LessGravity.Network.TestClientLidgren
{
    public class NetworkClient : IDisposable
    {
        private readonly NetPeerConfiguration _clientConfiguration;
        private readonly NetClient _client;
        public long UserId { get; set; }
        public bool IsConnected { get; private set; }
        public bool IsLoggedIn { get; private set; }
        public bool IsRegistering { get; set; }

        public event Action OnConnected;
        public event Action<string, string, string, DateTime> OnChat;
        public event Action<long, DateTime, DateTime> OnLoginSuccess;
        public event Action<LoginResult> OnLoginFailure;
        public event Action<LogoutResult> OnLogout;
        public event Action<long, DateTime> OnRegisterSuccess;
        public event Action<RegisterResult> OnRegisterFailure;

        public event Action<PacketType, IPacket> OnReceivedPacket;

        private void HandlePackets(NetIncomingMessage incomingMessage)
        {
            var packetType = (PacketType)incomingMessage.ReadByte();
            var packet = PacketFactory.CreatePacket(packetType, incomingMessage);
            if (packet == null)
            {
                throw new InvalidOperationException("Invalid Packet!!!");
            }

            var senderEndPoint = incomingMessage.SenderEndPoint;
            var senderConnection = incomingMessage.SenderConnection;
            Debug.WriteLine($"Client - Packet Received: {packet}");

            switch (packetType)
            {
                case PacketType.Chat:
                    var chatPacket = packet as ChatPacket;
                    DoChat(chatPacket.UserName, chatPacket.Channel, chatPacket.Message, chatPacket.MessageTimestamp);
                    break;
                case PacketType.LoginResponse:
                    var loginResponse = packet as LoginResponsePacket;
                    if (loginResponse.Result == LoginResult.Success)
                    {
                        IsLoggedIn = true;
                        DoLoginSuccess(loginResponse.UserId, loginResponse.LoginTimeStamp, loginResponse.LastLoginTimeStamp);
                    }
                    else
                    {
                        IsLoggedIn = false;
                        DoLoginFailure(loginResponse.Result);
                    }
                    break;
                case PacketType.LogoutResponse:
                    var logoutResponse = packet as LogoutResponsePacket;
                    DoLogout(logoutResponse.Result);
                    IsLoggedIn = false;
                    break;
                case PacketType.RegisterResponse:
                    var registerResponse = packet as RegisterResponsePacket;
                    if (registerResponse.Result == RegisterResult.Success)
                    {
                        DoRegisterSuccess(registerResponse.UserId, registerResponse.RegisteredDate);
                        IsLoggedIn = true;
                        UserId = registerResponse.UserId;
                    }
                    else
                    {
                        DoRegisterFailure(registerResponse.Result);
                    }
                    IsRegistering = false;
                    break;
                default:
                    DoReceivePacket(packetType, packet);
                    break;
            }
        }

        public void Connect(string address, int port)
        {
            _client.Start();
            var hailMessage = _client.CreateMessage("This is the hail message");
            _client.Connect(address, port, hailMessage);
        }

        public void Disconnect()
        {
            _client.Disconnect("Bye");
            IsConnected = false;
            IsLoggedIn = false;
        }

        public void Dispose()
        {
            if (IsConnected && IsLoggedIn)
            {
                Logout();
            }
            if (IsConnected)
            {
                Disconnect();
            }
        }

        private void DoChat(string userName, string channel, string message, DateTime messateTimeStamp)
        {
            OnChat?.Invoke(userName, channel, message, messateTimeStamp);
        }

        private void DoLoginSuccess(long userId, DateTime loginTimeStamp, DateTime lastLoginTimeStamp)
        {
            OnLoginSuccess?.Invoke(userId, loginTimeStamp, lastLoginTimeStamp);
            IsLoggedIn = true;
        }

        private void DoLoginFailure(LoginResult loginResult)
        {
            OnLoginFailure?.Invoke(loginResult);
            IsLoggedIn = false;
        }

        private void DoLogout(LogoutResult logoutResult)
        {
            OnLogout?.Invoke(logoutResult);
            IsLoggedIn = false;
        }

        private void DoOnConnected()
        {
            OnConnected?.Invoke();
        }

        private void DoReceivePacket(PacketType packetType, IPacket packet)
        {
            OnReceivedPacket?.Invoke(packetType, packet);
        }

        private void DoRegisterSuccess(long userId, DateTime registerTimeStamp)
        {
            OnRegisterSuccess?.Invoke(userId, registerTimeStamp);
        }

        private void DoRegisterFailure(RegisterResult registerResult)
        {
            OnRegisterFailure?.Invoke(registerResult);
        }

        public void Login(string userName, string passwordHash)
        {
            if (!IsConnected && IsLoggedIn)
            {
                return;
            }
            var loginRequest = new LoginRequestPacket
            {
                UserName = userName,
                Password = passwordHash
            };
            SendPacket(loginRequest);
        }

        public void Logout()
        {
            if (!IsConnected || !IsLoggedIn)
            {
                return;
            }

            var logoutRequest = new LogoutRequestPacket
            {
                UserId = UserId,
            };
            SendPacket(logoutRequest);
            Thread.Sleep(50);
        }

        private void OnDataReceived(object state)
        {
            NetIncomingMessage incomingMessage;
            while ((incomingMessage = _client.ReadMessage()) != null)
            {
                switch (incomingMessage.MessageType)
                {
                    case NetIncomingMessageType.DebugMessage:
                    case NetIncomingMessageType.ErrorMessage:
                    case NetIncomingMessageType.WarningMessage:
                    case NetIncomingMessageType.VerboseDebugMessage:
                        string text = incomingMessage.ReadString();
                        break;
                    case NetIncomingMessageType.StatusChanged:
                        var status = (NetConnectionStatus)incomingMessage.ReadByte();
                        var statusReason = incomingMessage.ReadString();
                        Debug.WriteLine($"Client - {status}: {statusReason}");

                        IsConnected = status == NetConnectionStatus.Connected;
                        if (IsConnected)
                        {
                            DoOnConnected();
                        }
                        break;
                    case NetIncomingMessageType.Data:
                        HandlePackets(incomingMessage);
                        break;
                    default:
                        Debug.WriteLine($"Client - Unhandled type: {incomingMessage.MessageType} ({incomingMessage.LengthBytes} bytes)");
                        break;
                }
                _client.Recycle(incomingMessage);
            }
        }

        public void Register(string userName, string passwordHash)
        {
            if (!IsConnected)
            {
                return;
            }

            var registerRequest = new RegisterRequestPacket
            {
                UserName = userName,
                Password = passwordHash
            };
            SendPacket(registerRequest);
        }

        public void SendPacket(IPacket packet)
        {
            if (packet == null)
            {
                throw new ArgumentNullException(nameof(packet));
            }
            if (!IsConnected)
            {
                throw new InvalidOperationException("Not Connected");
            }
            Debug.WriteLine($"Client - Sent Packet: {packet}");
            _client.SendMessage(packet.GetMessage(_client), NetDeliveryMethod.ReliableOrdered);
            _client.FlushSendQueue();
        }

        public NetworkClient()
        {
            _clientConfiguration = new NetPeerConfiguration(Strings.Session.ApplicationName);
            _clientConfiguration.AutoFlushSendQueue = false;
            _client = new NetClient(_clientConfiguration);
            _client.RegisterReceivedCallback(new SendOrPostCallback(OnDataReceived));
        }
    }
}