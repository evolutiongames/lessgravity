﻿using LessGravity.Logging;
using LessGravity.Quasar.Game;

namespace LessGravity.Network.TestClientLidgren
{
    internal sealed class TestClientGame : QuasarGameBase
    {
        public TestClientGame()
            : base(new Logger(), new TestClientFormFactory())
        {
        }
    }
}