﻿namespace LessGravity.Network.TestClientLidgren.Controls
{
    partial class CharacterCreationControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.mainCharacterControl = new LessGravity.Network.TestClientLidgren.Controls.CharacterCreateAvatarControl();
            this.lblAvatar = new System.Windows.Forms.Label();
            this.lblCharacterName = new System.Windows.Forms.Label();
            this.txtCharacterName = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // mainCharacterControl
            // 
            this.mainCharacterControl.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.mainCharacterControl.Location = new System.Drawing.Point(6, 69);
            this.mainCharacterControl.Name = "mainCharacterControl";
            this.mainCharacterControl.Size = new System.Drawing.Size(384, 574);
            this.mainCharacterControl.TabIndex = 0;
            // 
            // lblAvatar
            // 
            this.lblAvatar.AutoSize = true;
            this.lblAvatar.Location = new System.Drawing.Point(3, 51);
            this.lblAvatar.Name = "lblAvatar";
            this.lblAvatar.Size = new System.Drawing.Size(41, 15);
            this.lblAvatar.TabIndex = 1;
            this.lblAvatar.Text = "Avatar";
            // 
            // lblCharacterName
            // 
            this.lblCharacterName.AutoSize = true;
            this.lblCharacterName.Location = new System.Drawing.Point(3, 0);
            this.lblCharacterName.Name = "lblCharacterName";
            this.lblCharacterName.Size = new System.Drawing.Size(93, 15);
            this.lblCharacterName.TabIndex = 2;
            this.lblCharacterName.Text = "Character Name";
            // 
            // txtCharacterName
            // 
            this.txtCharacterName.Location = new System.Drawing.Point(9, 18);
            this.txtCharacterName.Name = "txtCharacterName";
            this.txtCharacterName.Size = new System.Drawing.Size(272, 23);
            this.txtCharacterName.TabIndex = 3;
            // 
            // CharacterCreationControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.Controls.Add(this.txtCharacterName);
            this.Controls.Add(this.lblCharacterName);
            this.Controls.Add(this.lblAvatar);
            this.Controls.Add(this.mainCharacterControl);
            this.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "CharacterCreationControl";
            this.Size = new System.Drawing.Size(739, 656);
            this.Resize += new System.EventHandler(this.CharacterCreationControl_Resize);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private TestClientLidgren.Controls.CharacterCreateAvatarControl mainCharacterControl;
        private System.Windows.Forms.Label lblAvatar;
        private System.Windows.Forms.Label lblCharacterName;
        private System.Windows.Forms.TextBox txtCharacterName;
    }
}
