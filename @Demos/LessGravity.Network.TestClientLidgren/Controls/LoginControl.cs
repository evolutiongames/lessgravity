﻿using System;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using LessGravity.OpenSpace.Shared.Packets;

namespace LessGravity.Network.TestClientLidgren.Controls
{
    public partial class LoginControl : UserControl
    {
        private readonly IMainForm _mainForm;
        private readonly NetworkClient _networkClient;
        private readonly string _serverHost;
        private readonly int _serverPort;

        public string PasswordHash
        {
            get
            {
                var password = txtPassword.Text;
                if (string.IsNullOrEmpty(password))
                {
                    return string.Empty;
                }

                string passwordHash;
                using (var sha = SHA256.Create())
                {
                    passwordHash = BitConverter.ToString(sha.ComputeHash(Encoding.UTF8.GetBytes(password))).Replace("-", string.Empty);
                }
                return passwordHash;
            }
        }

        public string UserName => txtUserName.Text;

        public LoginControl(IMainForm mainForm, NetworkClient networkClient, string serverHost, int serverPort)
        {
            InitializeComponent();
            _mainForm = mainForm;
            _networkClient = networkClient;
            _serverHost = serverHost;
            _serverPort = serverPort;
        }

        private void ResizeControls()
        {
            lblCaption.Top = 64;
            lblCaption.Left = ClientSize.Width / 2 - lblCaption.Width / 2;
            pnlCredentials.Top = ClientSize.Height / 2;
            pnlCredentials.Left = ClientSize.Width / 2 - pnlCredentials.Width / 2;
            lblErrorMessage.Top = pnlCredentials.Top + pnlCredentials.Height + 8;
            lblErrorMessage.Left = ClientSize.Width / 2 - lblErrorMessage.Width / 2;
            btnLoginLogout.Top = lblErrorMessage.Top + lblErrorMessage.Height + 8;
            btnLoginLogout.Left = ClientSize.Width / 2 - btnLoginLogout.Width / 2;
            btnRegister.Top = btnLoginLogout.Top + btnLoginLogout.Height + 8;
            btnRegister.Left = ClientSize.Width / 2 - btnRegister.Width / 2;
            btnExit.Top = btnRegister.Top + btnRegister.Height + 8;
            btnExit.Left = ClientSize.Width / 2 - btnExit.Width / 2;
        }

        public void SetLoginResultOnError(LoginResult loginResult)
        {
            if (InvokeRequired)
            {
                Invoke(new MethodInvoker(() => SetLoginResultOnError(loginResult)));
            }
            else
            {
                switch (loginResult)
                {
                    case LoginResult.FailureUserBanned:
                        lblErrorMessage.Text = "User Banned";
                        break;
                    case LoginResult.FailureUserTrialExpired:
                        lblErrorMessage.Text = "Trial Period Expired";
                        break;
                    case LoginResult.FailureWrongPassword:
                        lblErrorMessage.Text = "Wrong Password";
                        break;
                    case LoginResult.FailureWrongUserName:
                        lblErrorMessage.Text = "Wrong UserName";
                        break;
                }
                lblErrorMessage.Visible = true;
                btnLoginLogout.Text = "Login";
            }
        }

        private void AccountControl_Load(object sender, EventArgs e)
        {
            ResizeControls();
        }

        private void AccountControl_Resize(object sender, EventArgs e)
        {
            ResizeControls();
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            _mainForm.Close();
        }

        private void btnLoginLogout_Click(object sender, EventArgs e)
        {
            lblErrorMessage.Text = string.Empty;

            if (!_networkClient.IsConnected)
            {
                _networkClient.Connect(_serverHost, _serverPort);
                Thread.Sleep(100);
            }
            else
            {
                if (_networkClient.IsLoggedIn)
                {
                    _networkClient.Logout();
                }
                else
                {
                    _networkClient.Login(txtUserName.Text, PasswordHash);
                }
            }
        }

        private void btnRegister_Click(object sender, EventArgs e)
        {
            _networkClient.IsRegistering = true;
            if (!_networkClient.IsConnected)
            {
                _networkClient.Connect(_serverHost, _serverPort);
                Thread.Sleep(100);
            }
            else
            {
                _networkClient.Register(txtUserName.Text, PasswordHash);
            }
        }
    }
}