﻿namespace LessGravity.Network.TestClientLidgren.Controls
{
    partial class CharacterCreateAvatarControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pbChest = new System.Windows.Forms.PictureBox();
            this.pbLegs = new System.Windows.Forms.PictureBox();
            this.pbFeet = new System.Windows.Forms.PictureBox();
            this.pbArmLeft = new System.Windows.Forms.PictureBox();
            this.pbArmRight = new System.Windows.Forms.PictureBox();
            this.btnFeetNext = new System.Windows.Forms.Button();
            this.btnFeetPrev = new System.Windows.Forms.Button();
            this.btnLegsNext = new System.Windows.Forms.Button();
            this.btnLegsPrev = new System.Windows.Forms.Button();
            this.btnChestNext = new System.Windows.Forms.Button();
            this.btnChestPrev = new System.Windows.Forms.Button();
            this.pbHead = new System.Windows.Forms.PictureBox();
            this.btnHeadNext = new System.Windows.Forms.Button();
            this.btnHeadPrev = new System.Windows.Forms.Button();
            this.btnArmsPrev = new System.Windows.Forms.Button();
            this.btnArmsNext = new System.Windows.Forms.Button();
            this.lblSerial = new System.Windows.Forms.Label();
            this.txtSerial = new System.Windows.Forms.TextBox();
            this.btnRandomise = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pbChest)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbLegs)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbFeet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbArmLeft)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbArmRight)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbHead)).BeginInit();
            this.SuspendLayout();
            // 
            // pbChest
            // 
            this.pbChest.Location = new System.Drawing.Point(127, 137);
            this.pbChest.Name = "pbChest";
            this.pbChest.Size = new System.Drawing.Size(128, 128);
            this.pbChest.TabIndex = 0;
            this.pbChest.TabStop = false;
            // 
            // pbLegs
            // 
            this.pbLegs.Location = new System.Drawing.Point(127, 271);
            this.pbLegs.Name = "pbLegs";
            this.pbLegs.Size = new System.Drawing.Size(128, 192);
            this.pbLegs.TabIndex = 1;
            this.pbLegs.TabStop = false;
            // 
            // pbFeet
            // 
            this.pbFeet.Location = new System.Drawing.Point(127, 469);
            this.pbFeet.Name = "pbFeet";
            this.pbFeet.Size = new System.Drawing.Size(128, 64);
            this.pbFeet.TabIndex = 2;
            this.pbFeet.TabStop = false;
            // 
            // pbArmLeft
            // 
            this.pbArmLeft.Location = new System.Drawing.Point(297, 137);
            this.pbArmLeft.Name = "pbArmLeft";
            this.pbArmLeft.Size = new System.Drawing.Size(65, 206);
            this.pbArmLeft.TabIndex = 3;
            this.pbArmLeft.TabStop = false;
            // 
            // pbArmRight
            // 
            this.pbArmRight.Location = new System.Drawing.Point(21, 137);
            this.pbArmRight.Name = "pbArmRight";
            this.pbArmRight.Size = new System.Drawing.Size(64, 206);
            this.pbArmRight.TabIndex = 4;
            this.pbArmRight.TabStop = false;
            // 
            // btnFeetNext
            // 
            this.btnFeetNext.FlatAppearance.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.btnFeetNext.FlatAppearance.MouseDownBackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.btnFeetNext.FlatAppearance.MouseOverBackColor = System.Drawing.SystemColors.ControlDark;
            this.btnFeetNext.Font = new System.Drawing.Font("Consolas", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFeetNext.Location = new System.Drawing.Point(261, 469);
            this.btnFeetNext.Name = "btnFeetNext";
            this.btnFeetNext.Size = new System.Drawing.Size(12, 64);
            this.btnFeetNext.TabIndex = 5;
            this.btnFeetNext.Tag = "41";
            this.btnFeetNext.Text = ">";
            this.btnFeetNext.UseVisualStyleBackColor = true;
            this.btnFeetNext.Click += new System.EventHandler(this.BrowseButtonClick);
            // 
            // btnFeetPrev
            // 
            this.btnFeetPrev.FlatAppearance.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.btnFeetPrev.FlatAppearance.MouseDownBackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.btnFeetPrev.FlatAppearance.MouseOverBackColor = System.Drawing.SystemColors.ControlDark;
            this.btnFeetPrev.Font = new System.Drawing.Font("Consolas", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFeetPrev.Location = new System.Drawing.Point(109, 469);
            this.btnFeetPrev.Name = "btnFeetPrev";
            this.btnFeetPrev.Size = new System.Drawing.Size(12, 64);
            this.btnFeetPrev.TabIndex = 6;
            this.btnFeetPrev.Tag = "40";
            this.btnFeetPrev.Text = "<";
            this.btnFeetPrev.UseVisualStyleBackColor = true;
            this.btnFeetPrev.Click += new System.EventHandler(this.BrowseButtonClick);
            // 
            // btnLegsNext
            // 
            this.btnLegsNext.FlatAppearance.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.btnLegsNext.FlatAppearance.MouseDownBackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.btnLegsNext.FlatAppearance.MouseOverBackColor = System.Drawing.SystemColors.ControlDark;
            this.btnLegsNext.Font = new System.Drawing.Font("Consolas", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLegsNext.Location = new System.Drawing.Point(261, 271);
            this.btnLegsNext.Name = "btnLegsNext";
            this.btnLegsNext.Size = new System.Drawing.Size(12, 192);
            this.btnLegsNext.TabIndex = 7;
            this.btnLegsNext.Tag = "31";
            this.btnLegsNext.Text = ">";
            this.btnLegsNext.UseVisualStyleBackColor = true;
            this.btnLegsNext.Click += new System.EventHandler(this.BrowseButtonClick);
            // 
            // btnLegsPrev
            // 
            this.btnLegsPrev.FlatAppearance.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.btnLegsPrev.FlatAppearance.MouseDownBackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.btnLegsPrev.FlatAppearance.MouseOverBackColor = System.Drawing.SystemColors.ControlDark;
            this.btnLegsPrev.Font = new System.Drawing.Font("Consolas", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLegsPrev.Location = new System.Drawing.Point(109, 271);
            this.btnLegsPrev.Name = "btnLegsPrev";
            this.btnLegsPrev.Size = new System.Drawing.Size(12, 192);
            this.btnLegsPrev.TabIndex = 8;
            this.btnLegsPrev.Tag = "30";
            this.btnLegsPrev.Text = "<";
            this.btnLegsPrev.UseVisualStyleBackColor = true;
            this.btnLegsPrev.Click += new System.EventHandler(this.BrowseButtonClick);
            // 
            // btnChestNext
            // 
            this.btnChestNext.FlatAppearance.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.btnChestNext.FlatAppearance.MouseDownBackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.btnChestNext.FlatAppearance.MouseOverBackColor = System.Drawing.SystemColors.ControlDark;
            this.btnChestNext.Font = new System.Drawing.Font("Consolas", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnChestNext.Location = new System.Drawing.Point(261, 137);
            this.btnChestNext.Name = "btnChestNext";
            this.btnChestNext.Size = new System.Drawing.Size(12, 128);
            this.btnChestNext.TabIndex = 9;
            this.btnChestNext.Tag = "21";
            this.btnChestNext.Text = ">";
            this.btnChestNext.UseVisualStyleBackColor = true;
            this.btnChestNext.Click += new System.EventHandler(this.BrowseButtonClick);
            // 
            // btnChestPrev
            // 
            this.btnChestPrev.FlatAppearance.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.btnChestPrev.FlatAppearance.MouseDownBackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.btnChestPrev.FlatAppearance.MouseOverBackColor = System.Drawing.SystemColors.ControlDark;
            this.btnChestPrev.Font = new System.Drawing.Font("Consolas", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnChestPrev.Location = new System.Drawing.Point(109, 137);
            this.btnChestPrev.Name = "btnChestPrev";
            this.btnChestPrev.Size = new System.Drawing.Size(12, 128);
            this.btnChestPrev.TabIndex = 10;
            this.btnChestPrev.Tag = "20";
            this.btnChestPrev.Text = "<";
            this.btnChestPrev.UseVisualStyleBackColor = true;
            this.btnChestPrev.Click += new System.EventHandler(this.BrowseButtonClick);
            // 
            // pbHead
            // 
            this.pbHead.Location = new System.Drawing.Point(127, 3);
            this.pbHead.Name = "pbHead";
            this.pbHead.Size = new System.Drawing.Size(128, 128);
            this.pbHead.TabIndex = 11;
            this.pbHead.TabStop = false;
            // 
            // btnHeadNext
            // 
            this.btnHeadNext.FlatAppearance.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.btnHeadNext.FlatAppearance.MouseDownBackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.btnHeadNext.FlatAppearance.MouseOverBackColor = System.Drawing.SystemColors.ControlDark;
            this.btnHeadNext.Font = new System.Drawing.Font("Consolas", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnHeadNext.Location = new System.Drawing.Point(261, 3);
            this.btnHeadNext.Name = "btnHeadNext";
            this.btnHeadNext.Size = new System.Drawing.Size(12, 128);
            this.btnHeadNext.TabIndex = 12;
            this.btnHeadNext.Tag = "11";
            this.btnHeadNext.Text = ">";
            this.btnHeadNext.UseVisualStyleBackColor = true;
            this.btnHeadNext.Click += new System.EventHandler(this.BrowseButtonClick);
            // 
            // btnHeadPrev
            // 
            this.btnHeadPrev.FlatAppearance.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.btnHeadPrev.FlatAppearance.MouseDownBackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.btnHeadPrev.FlatAppearance.MouseOverBackColor = System.Drawing.SystemColors.ControlDark;
            this.btnHeadPrev.Font = new System.Drawing.Font("Consolas", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnHeadPrev.Location = new System.Drawing.Point(109, 3);
            this.btnHeadPrev.Name = "btnHeadPrev";
            this.btnHeadPrev.Size = new System.Drawing.Size(12, 128);
            this.btnHeadPrev.TabIndex = 13;
            this.btnHeadPrev.Tag = "10";
            this.btnHeadPrev.Text = "<";
            this.btnHeadPrev.UseVisualStyleBackColor = true;
            this.btnHeadPrev.Click += new System.EventHandler(this.BrowseButtonClick);
            // 
            // btnArmsPrev
            // 
            this.btnArmsPrev.FlatAppearance.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.btnArmsPrev.FlatAppearance.MouseDownBackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.btnArmsPrev.FlatAppearance.MouseOverBackColor = System.Drawing.SystemColors.ControlDark;
            this.btnArmsPrev.Font = new System.Drawing.Font("Consolas", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnArmsPrev.Location = new System.Drawing.Point(3, 137);
            this.btnArmsPrev.Name = "btnArmsPrev";
            this.btnArmsPrev.Size = new System.Drawing.Size(12, 206);
            this.btnArmsPrev.TabIndex = 14;
            this.btnArmsPrev.Tag = "50";
            this.btnArmsPrev.Text = "<";
            this.btnArmsPrev.UseVisualStyleBackColor = true;
            this.btnArmsPrev.Click += new System.EventHandler(this.BrowseButtonClick);
            // 
            // btnArmsNext
            // 
            this.btnArmsNext.FlatAppearance.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.btnArmsNext.FlatAppearance.MouseDownBackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.btnArmsNext.FlatAppearance.MouseOverBackColor = System.Drawing.SystemColors.ControlDark;
            this.btnArmsNext.Font = new System.Drawing.Font("Consolas", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnArmsNext.Location = new System.Drawing.Point(368, 137);
            this.btnArmsNext.Name = "btnArmsNext";
            this.btnArmsNext.Size = new System.Drawing.Size(12, 206);
            this.btnArmsNext.TabIndex = 15;
            this.btnArmsNext.Tag = "61";
            this.btnArmsNext.Text = ">";
            this.btnArmsNext.UseVisualStyleBackColor = true;
            this.btnArmsNext.Click += new System.EventHandler(this.BrowseButtonClick);
            // 
            // lblSerial
            // 
            this.lblSerial.AutoSize = true;
            this.lblSerial.Location = new System.Drawing.Point(68, 544);
            this.lblSerial.Name = "lblSerial";
            this.lblSerial.Size = new System.Drawing.Size(35, 15);
            this.lblSerial.TabIndex = 16;
            this.lblSerial.Text = "Serial";
            // 
            // txtSerial
            // 
            this.txtSerial.Font = new System.Drawing.Font("Consolas", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSerial.Location = new System.Drawing.Point(109, 539);
            this.txtSerial.Name = "txtSerial";
            this.txtSerial.Size = new System.Drawing.Size(164, 25);
            this.txtSerial.TabIndex = 17;
            this.txtSerial.Text = "000000000000";
            this.txtSerial.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // btnRandomise
            // 
            this.btnRandomise.Location = new System.Drawing.Point(3, 3);
            this.btnRandomise.Name = "btnRandomise";
            this.btnRandomise.Size = new System.Drawing.Size(82, 23);
            this.btnRandomise.TabIndex = 18;
            this.btnRandomise.Text = "Randomise";
            this.btnRandomise.UseVisualStyleBackColor = true;
            this.btnRandomise.Click += new System.EventHandler(this.btnRandomise_Click);
            // 
            // button1
            // 
            this.button1.FlatAppearance.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.button1.FlatAppearance.MouseDownBackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.button1.FlatAppearance.MouseOverBackColor = System.Drawing.SystemColors.ControlDark;
            this.button1.Font = new System.Drawing.Font("Consolas", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Location = new System.Drawing.Point(91, 137);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(12, 206);
            this.button1.TabIndex = 19;
            this.button1.Tag = "51";
            this.button1.Text = ">";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.BrowseButtonClick);
            // 
            // button2
            // 
            this.button2.FlatAppearance.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.button2.FlatAppearance.MouseDownBackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.button2.FlatAppearance.MouseOverBackColor = System.Drawing.SystemColors.ControlDark;
            this.button2.Font = new System.Drawing.Font("Consolas", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.Location = new System.Drawing.Point(279, 137);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(12, 206);
            this.button2.TabIndex = 20;
            this.button2.Tag = "60";
            this.button2.Text = "<";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.BrowseButtonClick);
            // 
            // CharacterControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.btnRandomise);
            this.Controls.Add(this.txtSerial);
            this.Controls.Add(this.lblSerial);
            this.Controls.Add(this.btnArmsNext);
            this.Controls.Add(this.btnArmsPrev);
            this.Controls.Add(this.btnHeadPrev);
            this.Controls.Add(this.btnHeadNext);
            this.Controls.Add(this.pbHead);
            this.Controls.Add(this.btnChestPrev);
            this.Controls.Add(this.btnChestNext);
            this.Controls.Add(this.btnLegsPrev);
            this.Controls.Add(this.btnLegsNext);
            this.Controls.Add(this.btnFeetPrev);
            this.Controls.Add(this.btnFeetNext);
            this.Controls.Add(this.pbArmRight);
            this.Controls.Add(this.pbArmLeft);
            this.Controls.Add(this.pbFeet);
            this.Controls.Add(this.pbLegs);
            this.Controls.Add(this.pbChest);
            this.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "CharacterControl";
            this.Size = new System.Drawing.Size(409, 574);
            ((System.ComponentModel.ISupportInitialize)(this.pbChest)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbLegs)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbFeet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbArmLeft)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbArmRight)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbHead)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pbChest;
        private System.Windows.Forms.PictureBox pbLegs;
        private System.Windows.Forms.PictureBox pbFeet;
        private System.Windows.Forms.PictureBox pbArmLeft;
        private System.Windows.Forms.PictureBox pbArmRight;
        private System.Windows.Forms.Button btnFeetNext;
        private System.Windows.Forms.Button btnFeetPrev;
        private System.Windows.Forms.Button btnLegsNext;
        private System.Windows.Forms.Button btnLegsPrev;
        private System.Windows.Forms.Button btnChestNext;
        private System.Windows.Forms.Button btnChestPrev;
        private System.Windows.Forms.PictureBox pbHead;
        private System.Windows.Forms.Button btnHeadNext;
        private System.Windows.Forms.Button btnHeadPrev;
        private System.Windows.Forms.Button btnArmsPrev;
        private System.Windows.Forms.Button btnArmsNext;
        private System.Windows.Forms.Label lblSerial;
        private System.Windows.Forms.TextBox txtSerial;
        private System.Windows.Forms.Button btnRandomise;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
    }
}
