﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Windows.Forms;

namespace LessGravity.Network.TestClientLidgren.Controls
{
    public partial class CharacterCreateAvatarControl : UserControl
    {
        private static readonly List<string> _headImages = new List<string>();
        private readonly List<string> _chestImages = new List<string>();
        private readonly List<string> _legsImages = new List<string>();
        private readonly List<string> _feetImages = new List<string>();
        private readonly List<string> _armsLeftImages = new List<string>();
        private readonly List<string> _armsRightImages = new List<string>();
        private readonly string _dataPath;
        private readonly string _characterPath;

        private int _headIndex = 0;
        private int _chestIndex = 0;
        private int _legsIndex = 0;
        private int _feetIndex = 0;
        private int _armLeftIndex = 0;
        private int _armRightIndex = 0;

        public ulong CharacterAppearanceSerial => ulong.TryParse(txtSerial.Text, out ulong serial) ? serial : 0ul;

        private void DrawBodyPart(Graphics graphics, Point position, string bodyPartName)
        {
            using (var image = Image.FromFile(Path.Combine(_characterPath, $"{bodyPartName}.png")))
            {
                graphics.DrawImage(image, position);
            }
        }

        public Image CreateImageFromCharacterAppearance(ulong appearance)
        {
            var bitmap = new Bitmap(256, 512, PixelFormat.Format32bppArgb);

            var armRightIndex = (appearance >> 40) & 0xFF;
            var armLeftIndex = (appearance >> 32) & 0xFF;
            var headIndex = (appearance >> 24) & 0xFF;
            var chestIndex = (appearance >> 16) & 0xFF;
            var legsIndex = (appearance >> 8) & 0xFF;
            var feetIndex = (appearance >> 0) & 0xFF;

            using (var graphics = Graphics.FromImage(bitmap))
            {
                DrawBodyPart(graphics, new Point(64, 0), $"Head_{headIndex:000}");
                DrawBodyPart(graphics, new Point(64, 128), $"Chest_{chestIndex:000}");
                DrawBodyPart(graphics, new Point(64, 256), $"Legs_{legsIndex:000}");
                DrawBodyPart(graphics, new Point(64, 448), $"Feet_{feetIndex:000}");
                DrawBodyPart(graphics, new Point(0, 128), $"ArmRight_{armRightIndex:000}");
                DrawBodyPart(graphics, new Point(192, 128), $"ArmLeft_{armLeftIndex:000}");
            }
            return bitmap;
        }

        public CharacterCreateAvatarControl()
        {
            InitializeComponent();

            _dataPath = Path.Combine(Application.StartupPath, "Data");
            _characterPath = Path.Combine(_dataPath, "Character");
        }

        private void PopulateList(List<string> list, string searchPattern)
        {
            var files = Directory.EnumerateFiles(_characterPath, searchPattern);
            list.AddRange(files);
        }

        public void PopulateImageLists()
        {
            PopulateList(_headImages, "Head_*.png");
            PopulateList(_chestImages, "Chest_*.png");
            PopulateList(_legsImages, "Legs_*.png");
            PopulateList(_feetImages, "Feet_*.png");
            PopulateList(_armsLeftImages, "ArmLeft_*.png");
            PopulateList(_armsRightImages, "ArmRight_*.png");

            UpdatePictureBoxes();
        }

        private void UpdatePictureBoxes()
        {
            pbArmLeft.Image = Image.FromFile(_armsLeftImages[_armLeftIndex]);
            pbArmRight.Image = Image.FromFile(_armsRightImages[_armRightIndex]);
            pbHead.Image = Image.FromFile(_headImages[_headIndex]);
            pbChest.Image = Image.FromFile(_chestImages[_chestIndex]);
            pbLegs.Image = Image.FromFile(_legsImages[_legsIndex]);
            pbFeet.Image = Image.FromFile(_feetImages[_feetIndex]);

            UpdateSerial();
        }

        private void UpdateSerial()
        {
            ulong appearance = (ulong)(_feetIndex |
                             (_legsIndex << 8) | 
                             (_chestIndex << 16) |
                             (_headIndex << 24) |
                             (_armLeftIndex << 32) |
                             (_armRightIndex << 40));
            txtSerial.Text = appearance.ToString();
        }

        private void BrowseButtonClick(object sender, EventArgs e)
        {
            var control = (Control)sender;
            int tag;
            if (!int.TryParse(control.Tag.ToString(), out tag))
            {
                return;
            }

            switch (tag)
            {
                case 10:
                    _headIndex--;
                    if (_headIndex < 0)
                    {
                        _headIndex = _headImages.Count - 1;
                    }
                    break;
                case 11:
                    _headIndex++;
                    if (_headIndex >= _headImages.Count)
                    {
                        _headIndex = 0;
                    }
                    break;
                case 20:
                    _chestIndex--;
                    if (_chestIndex < 0)
                    {
                        _chestIndex = _chestImages.Count - 1;
                    }
                    break;
                case 21:
                    _chestIndex++;
                    if (_chestIndex >= _chestImages.Count)
                    {
                        _chestIndex = 0;
                    }
                    break;
                case 30:
                    _legsIndex--;
                    if (_legsIndex < 0)
                    {
                        _legsIndex = _legsImages.Count - 1;
                    }
                    break;
                case 31:
                    _legsIndex++;
                    if (_legsIndex >= _legsImages.Count)
                    {
                        _legsIndex = 0;
                    }
                    break;
                case 40:
                    _feetIndex--;
                    if (_feetIndex < 0)
                    {
                        _feetIndex = _feetImages.Count - 1;
                    }
                    break;
                case 41:
                    _feetIndex++;
                    if (_feetIndex >= _feetImages.Count)
                    {
                        _feetIndex = 0;
                    }
                    break;
                case 50:
                    _armRightIndex--;
                    if (_armRightIndex < 0)
                    {
                        _armRightIndex = _armsRightImages.Count - 1;
                    }
                    break;
                case 51:
                    _armRightIndex++;
                    if (_armRightIndex >= _armsRightImages.Count)
                    {
                        _armRightIndex = 0;
                    }
                    break;
                case 60:
                    _armLeftIndex--;
                    if (_armLeftIndex < 0)
                    {
                        _armLeftIndex = _armsLeftImages.Count - 1;
                    }
                    break;
                case 61:
                    _armLeftIndex++;
                    if (_armLeftIndex >= _armsLeftImages.Count)
                    {
                        _armLeftIndex = 0;
                    }
                    break;

            }
            UpdatePictureBoxes();
        }

        private void btnRandomise_Click(object sender, EventArgs e)
        {
            var random = new Random();
            _headIndex = random.Next(0, _headImages.Count);
            _chestIndex = random.Next(0, _chestImages.Count);
            _legsIndex = random.Next(0, _legsImages.Count);
            _feetIndex = random.Next(0, _feetImages.Count);
            _armLeftIndex = random.Next(0, _armsLeftImages.Count);
            _armRightIndex = random.Next(0, _armsRightImages.Count);
            UpdatePictureBoxes();
        }
    }
}