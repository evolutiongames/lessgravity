﻿namespace LessGravity.Network.TestClientLidgren.Controls
{
    partial class CharacterSelectionControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pbCharacterPicture = new System.Windows.Forms.PictureBox();
            this.btnSelectCharacter = new System.Windows.Forms.Button();
            this.btnCreateCharacter = new System.Windows.Forms.Button();
            this.btnDeleteCharacter = new System.Windows.Forms.Button();
            this.lbCharacters = new System.Windows.Forms.ListBox();
            this.lblCaption = new System.Windows.Forms.Label();
            this.btnLogout = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pbCharacterPicture)).BeginInit();
            this.SuspendLayout();
            // 
            // pbCharacterPicture
            // 
            this.pbCharacterPicture.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pbCharacterPicture.Location = new System.Drawing.Point(157, 132);
            this.pbCharacterPicture.Name = "pbCharacterPicture";
            this.pbCharacterPicture.Size = new System.Drawing.Size(169, 227);
            this.pbCharacterPicture.TabIndex = 0;
            this.pbCharacterPicture.TabStop = false;
            // 
            // btnSelectCharacter
            // 
            this.btnSelectCharacter.Location = new System.Drawing.Point(181, 462);
            this.btnSelectCharacter.Name = "btnSelectCharacter";
            this.btnSelectCharacter.Size = new System.Drawing.Size(64, 20);
            this.btnSelectCharacter.TabIndex = 1;
            this.btnSelectCharacter.Text = "Select";
            this.btnSelectCharacter.UseVisualStyleBackColor = true;
            this.btnSelectCharacter.Click += new System.EventHandler(this.btnSelectCharacter_Click);
            // 
            // btnCreateCharacter
            // 
            this.btnCreateCharacter.Location = new System.Drawing.Point(183, 498);
            this.btnCreateCharacter.Name = "btnCreateCharacter";
            this.btnCreateCharacter.Size = new System.Drawing.Size(64, 20);
            this.btnCreateCharacter.TabIndex = 2;
            this.btnCreateCharacter.Text = "Create New";
            this.btnCreateCharacter.UseVisualStyleBackColor = true;
            this.btnCreateCharacter.Click += new System.EventHandler(this.btnCreateCharacter_Click);
            // 
            // btnDeleteCharacter
            // 
            this.btnDeleteCharacter.Location = new System.Drawing.Point(180, 99);
            this.btnDeleteCharacter.Name = "btnDeleteCharacter";
            this.btnDeleteCharacter.Size = new System.Drawing.Size(64, 20);
            this.btnDeleteCharacter.TabIndex = 3;
            this.btnDeleteCharacter.Text = "Delete";
            this.btnDeleteCharacter.UseVisualStyleBackColor = true;
            this.btnDeleteCharacter.Click += new System.EventHandler(this.btnDeleteCharacter_Click);
            // 
            // lbCharacters
            // 
            this.lbCharacters.FormattingEnabled = true;
            this.lbCharacters.IntegralHeight = false;
            this.lbCharacters.Location = new System.Drawing.Point(166, 378);
            this.lbCharacters.Name = "lbCharacters";
            this.lbCharacters.Size = new System.Drawing.Size(101, 60);
            this.lbCharacters.TabIndex = 4;
            this.lbCharacters.SelectedValueChanged += new System.EventHandler(this.lbCharacters_SelectedValueChanged);
            // 
            // lblCaption
            // 
            this.lblCaption.AutoSize = true;
            this.lblCaption.Font = new System.Drawing.Font("Segoe UI", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCaption.Location = new System.Drawing.Point(102, 46);
            this.lblCaption.Name = "lblCaption";
            this.lblCaption.Size = new System.Drawing.Size(294, 45);
            this.lblCaption.TabIndex = 5;
            this.lblCaption.Text = "Character Selection";
            // 
            // btnLogout
            // 
            this.btnLogout.Location = new System.Drawing.Point(181, 524);
            this.btnLogout.Name = "btnLogout";
            this.btnLogout.Size = new System.Drawing.Size(64, 20);
            this.btnLogout.TabIndex = 6;
            this.btnLogout.Text = "Logout";
            this.btnLogout.UseVisualStyleBackColor = true;
            this.btnLogout.Click += new System.EventHandler(this.btnLogout_Click);
            // 
            // CharacterSelectionControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.btnLogout);
            this.Controls.Add(this.lblCaption);
            this.Controls.Add(this.lbCharacters);
            this.Controls.Add(this.btnDeleteCharacter);
            this.Controls.Add(this.btnCreateCharacter);
            this.Controls.Add(this.btnSelectCharacter);
            this.Controls.Add(this.pbCharacterPicture);
            this.Name = "CharacterSelectionControl";
            this.Size = new System.Drawing.Size(563, 634);
            this.Load += new System.EventHandler(this.CharacterSelectionControl_Load);
            this.Resize += new System.EventHandler(this.CharacterSelectionControl_Resize);
            ((System.ComponentModel.ISupportInitialize)(this.pbCharacterPicture)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pbCharacterPicture;
        private System.Windows.Forms.Button btnSelectCharacter;
        private System.Windows.Forms.Button btnCreateCharacter;
        private System.Windows.Forms.Button btnDeleteCharacter;
        private System.Windows.Forms.ListBox lbCharacters;
        private System.Windows.Forms.Label lblCaption;
        private System.Windows.Forms.Button btnLogout;
    }
}
