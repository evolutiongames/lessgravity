﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace LessGravity.Network.TestClientLidgren.Controls
{
    public partial class CharacterCreationControl : UserControl
    {
        private readonly IMainForm _mainForm;
        private readonly NetworkClient _networkClient;

        public CharacterCreationControl(IMainForm mainForm, NetworkClient networkClient)
        {
            _mainForm = mainForm ?? throw new ArgumentNullException(nameof(mainForm));
            _networkClient = networkClient ?? throw new ArgumentNullException(nameof(networkClient));

            InitializeComponent();
        }

        private void CharacterCreationControl_Resize(object sender, System.EventArgs e)
        {
//            mainCharacterControl.Left = ClientSize.Width / 2 - mainCharacterControl.Width / 2;
//            mainCharacterControl.Top = ClientSize.Height / 2 - mainCharacterControl.Height / 2;
        }

        public Image CreateImageFromCharacterAppearance(ulong appearance)
        {
            return mainCharacterControl.CreateImageFromCharacterAppearance(appearance);
        }

        public void PopuplateImageLists()
        {
            mainCharacterControl.PopulateImageLists();
        }
    }
}