﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using LessGravity.OpenSpace.Shared.Packets;

namespace LessGravity.Network.TestClientLidgren.Controls
{
    public partial class CharacterSelectionControl : UserControl
    {
        private readonly IMainForm _mainForm;
        private readonly NetworkClient _networkClient;

        public CharacterInfo SelectedCharacter { get; private set; }

        public event Action<CharacterInfo> OnCharacterDelete;
        public event Action<CharacterInfo> OnCharacterSelect;
        public event Action OnCharacterCreate;

        public CharacterSelectionControl(IMainForm mainForm, NetworkClient networkClient)
        {
            if (mainForm == null)
            {
                throw new ArgumentNullException(nameof(mainForm));
            }
            if (networkClient == null)
            {
                throw new ArgumentNullException(nameof(networkClient));
            }

            InitializeComponent();
            _mainForm = mainForm;
            _networkClient = networkClient;
        }

        private void ResizeControls()
        {
            lblCaption.Top = 64;
            lblCaption.Left = ClientSize.Width / 2 - lblCaption.Width / 2;

            pbCharacterPicture.Top = ClientSize.Height / 2 - pbCharacterPicture.Height / 2;
            pbCharacterPicture.Left = ClientSize.Width / 2 - pbCharacterPicture.Width / 2;
            btnDeleteCharacter.Top = pbCharacterPicture.Top + pbCharacterPicture.Height + 8;
            btnDeleteCharacter.Left = ClientSize.Width / 2 - btnDeleteCharacter.Width / 2;

            lbCharacters.Top = btnDeleteCharacter.Top + btnDeleteCharacter.Height + 8;
            lbCharacters.Left = ClientSize.Width / 2 - lbCharacters.Width / 2;

            btnSelectCharacter.Top = lbCharacters.Top + lbCharacters.Height + 8;
            btnSelectCharacter.Left = ClientSize.Width / 2 - btnSelectCharacter.Width / 2;
            btnCreateCharacter.Top = btnSelectCharacter.Top + btnSelectCharacter.Height + 8;
            btnCreateCharacter.Left = ClientSize.Width / 2 - btnCreateCharacter.Width / 2;

            btnLogout.Top = ClientSize.Height - btnLogout.Height - 32;
            btnLogout.Left = ClientSize.Width / 2 - btnLogout.Width / 2;
        }

        private void CharacterSelectionControl_Load(object sender, EventArgs e)
        {
            ResizeControls();
        }

        private void CharacterSelectionControl_Resize(object sender, EventArgs e)
        {
            ResizeControls();
        }

        private void lbCharacters_SelectedValueChanged(object sender, EventArgs e)
        {
            SelectedCharacter = (CharacterInfo)lbCharacters.SelectedItem;
            if (SelectedCharacter != null)
            {
                var image = _mainForm.CharacterCreationControl.CreateImageFromCharacterAppearance(SelectedCharacter.CharacterAppearance);
                pbCharacterPicture.BackgroundImage = image;
            }
        }

        public void SetCharacters(IEnumerable<CharacterInfo> characters)
        {
            if (!characters.Any())
            {
                pbCharacterPicture.Visible = false;
                btnDeleteCharacter.Visible = false;
                btnSelectCharacter.Visible = false;
                lbCharacters.Visible = false;
            }
            else
            {
                lbCharacters.SelectedValueChanged -= lbCharacters_SelectedValueChanged;
                lbCharacters.Invoke(new MethodInvoker(() =>
                {
                    lbCharacters.ValueMember = "CHARACTERID";
                    lbCharacters.DisplayMember = "CHARACTERNAME";
                    lbCharacters.DataSource = characters;
                }));
                lbCharacters.SelectedValueChanged += lbCharacters_SelectedValueChanged;
                lbCharacters.SetSelected(0, true);

                pbCharacterPicture.Visible = true;
                btnDeleteCharacter.Visible = true;
                btnSelectCharacter.Visible = true;
                lbCharacters.Visible = true;
            }
        }

        private void btnDeleteCharacter_Click(object sender, EventArgs e)
        {
            var deleteCharactersRequest = new DeleteCharacterRequestPacket
            {
                UserId = _networkClient.UserId,
                CharacterId = SelectedCharacter.CharacterId
            };
            _networkClient.SendPacket(deleteCharactersRequest);

            OnCharacterDelete?.Invoke(SelectedCharacter);
        }

        private void btnSelectCharacter_Click(object sender, EventArgs e)
        {
            OnCharacterSelect?.Invoke(SelectedCharacter);
        }

        private void btnCreateCharacter_Click(object sender, EventArgs e)
        {
            OnCharacterCreate?.Invoke();
        }

        private void btnLogout_Click(object sender, EventArgs e)
        {
            _networkClient.Logout();
        }
    }
}