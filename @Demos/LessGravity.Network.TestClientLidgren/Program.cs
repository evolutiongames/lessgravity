﻿using LessGravity.Quasar.Game;
using System;
using System.Windows.Forms;

namespace LessGravity.Network.TestClientLidgren
{
    internal static class Program
    {
        [STAThread]
        private static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            using (var game = new TestClientGame())
            {
                var settings = new GameSettings
                {
                    FullScreen = false,
                    Height = 768,
                    Width = 1024,
                    Support2d = true,
                    Title = "DX11 Engine from scratch - Test :)",
                    VSync = false,
                };

                game.Run(settings);
            }
        }
    }
}