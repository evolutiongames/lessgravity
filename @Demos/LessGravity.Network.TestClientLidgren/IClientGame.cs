﻿using LessGravity.OpenSpace.Data;

namespace LessGravity.Network.TestClientLidgren
{
    public interface IClientGame
    {
        Character Character { get; }
    }
}