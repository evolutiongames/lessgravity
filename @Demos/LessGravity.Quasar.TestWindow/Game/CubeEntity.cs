﻿using LessGravity.Quasar.Entities;
using LessGravity.Quasar.Graphics;
using LessGravity.Quasar.Graphics.VertexTypes;
using SharpDX;
using SharpDX.Direct3D;
using System;
using System.Collections.Generic;
using System.Linq;

namespace LessGravity.Quasar.TestWindow.Game
{
    class CubeEntity : Entity
    {
        private readonly TypedBuffer<PositionNormalTangentTexture> _vertices;
        private readonly List<ModelMesh> _cubeMeshes;
        private Model _model;
        private ModelInstance _modelInstance;

        public void AddCube(Vector3 position, float width, float height, float depth)
        {
            AddCube(position.X, position.Y, position.Z, width, height, depth);
        }

        public void AddCube(Vector3 position, Vector3 size)
        {
            AddCube(position.X, position.Y, position.Z, size.X, size.Y, size.Z);
        }

        public void AddCube(float x, float y, float z, float width, float height, float depth)
        {
            var VectorU0V0 = new Vector2(0, 0);
            var VectorU1V0 = new Vector2(1, 0);
            var VectorU0V1 = new Vector2(0, 1);
            var VectorU1V1 = new Vector2(1, 1);

            var pos = new Vector3(x, y, z);
            var size = new Vector3(width, height, depth);

            var VectorTFL = pos + new Vector3(-1, +1, +1); // top front left
            var VectorTFR = pos + new Vector3(+1, +1, +1); // top front right
            var VectorTBL = pos + new Vector3(-1, +1, -1); // top back left
            var VectorTBR = pos + new Vector3(+1, +1, -1); // top back right
            var VectorBFL = pos + new Vector3(-1, -1, +1); // bottom front left
            var VectorBFR = pos + new Vector3(+1, -1, +1); // bottom front right
            var VectorBBL = pos + new Vector3(-1, -1, -1); // bottom back left
            var VectorBBR = pos + new Vector3(+1, -1, -1); // bottom back right

            var hw = (width / 2);
            var hh = (height / 2);
            var hd = (depth / 2);

            var indices = new UInt16IndexBuffer(36);

            Action<ushort[]> addIndices = idxs =>
            {
                var vertexOffset = (ushort)(_vertices.Count - 4);
                indices.AddRange(idxs.Select(i => (ushort)(i + vertexOffset)));
            };

            Action<Vector3, Vector3, Vector3, Vector2> addVertex = (p, n, t, uv) =>
            {
                var vertex = new PositionNormalTangentTexture
                {
                    Position = p + size,
                    Normal = n,
                    TextureCoordinate = uv,
                    Tangent = t
                };
                _vertices.Add(vertex);
            };

            //
            // front
            //
            var color = Color.Blue.ToColor4();
            var normal = Vector3.UnitZ;
            var tangent = -Vector3.UnitX;

            _vertices.Add(new PositionNormalTangentTexture(pos + new Vector3(-hw, +hh, +hd), normal, tangent, VectorU0V0));
            _vertices.Add(new PositionNormalTangentTexture(pos + new Vector3(+hw, +hh, +hd), normal, tangent, VectorU1V0));
            _vertices.Add(new PositionNormalTangentTexture(pos + new Vector3(-hw, -hh, +hd), normal, tangent, VectorU0V1));
            _vertices.Add(new PositionNormalTangentTexture(pos + new Vector3(+hw, -hh, +hd), normal, tangent, VectorU1V1));

            addIndices(new ushort[] { 1, 3, 0, 3, 2, 0 });

            //
            // back
            //
            color = Color.DarkBlue.ToColor4();
            normal = -Vector3.UnitZ;
            tangent = Vector3.UnitX;

            _vertices.Add(new PositionNormalTangentTexture(pos + new Vector3(-hw, +hh, -hd), normal, tangent, VectorU0V0));
            _vertices.Add(new PositionNormalTangentTexture(pos + new Vector3(+hw, +hh, -hd), normal, tangent, VectorU1V0));
            _vertices.Add(new PositionNormalTangentTexture(pos + new Vector3(-hw, -hh, -hd), normal, tangent, VectorU0V1));
            _vertices.Add(new PositionNormalTangentTexture(pos + new Vector3(+hw, -hh, -hd), normal, tangent, VectorU1V1));

            const ushort backOffset = 4;
            addIndices(new ushort[] { backOffset + 0, backOffset + 2, backOffset + 1, backOffset + 1, backOffset + 2, backOffset + 3 });

            //
            // left
            //
            color = Color.DarkRed.ToColor4();
            normal = -Vector3.UnitX;
            tangent = Vector3.UnitZ;

            _vertices.Add(new PositionNormalTangentTexture(pos + new Vector3(+hw, +hh, -hd), normal, tangent, VectorU1V0));
            _vertices.Add(new PositionNormalTangentTexture(pos + new Vector3(+hw, +hh, +hd), normal, tangent, VectorU0V0));
            _vertices.Add(new PositionNormalTangentTexture(pos + new Vector3(+hw, -hh, -hd), normal, tangent, VectorU1V1));
            _vertices.Add(new PositionNormalTangentTexture(pos + new Vector3(+hw, -hh, +hd), normal, tangent, VectorU0V1));
            const ushort leftOffset = 8;
            addIndices(new ushort[] { leftOffset + 0, leftOffset + 1, leftOffset + 3, leftOffset + 0, leftOffset + 3, leftOffset + 2 });

            //
            // right
            //
            color = Color.Red.ToColor4();
            normal = Vector3.UnitX;
            tangent = Vector3.UnitZ;

            _vertices.Add(new PositionNormalTangentTexture(pos + new Vector3(-hw, +hh, +hd), normal, tangent, VectorU0V0));
            _vertices.Add(new PositionNormalTangentTexture(pos + new Vector3(-hw, +hh, -hd), normal, tangent, VectorU1V0));
            _vertices.Add(new PositionNormalTangentTexture(pos + new Vector3(-hw, -hh, +hd), normal, tangent, VectorU0V1));
            _vertices.Add(new PositionNormalTangentTexture(pos + new Vector3(-hw, -hh, -hd), normal, tangent, VectorU1V1));
            const ushort rightOffset = 12;
            addIndices(new ushort[] { rightOffset + 0, rightOffset + 1, rightOffset + 2, rightOffset + 2, rightOffset + 1, rightOffset + 3 });

            //
            // top
            //
            color = Color.Green.ToColor4();
            normal = Vector3.UnitY;
            tangent = Vector3.UnitX;

            _vertices.Add(new PositionNormalTangentTexture(pos + new Vector3(-hw, +hh, +hd), normal, tangent, VectorU0V1));
            _vertices.Add(new PositionNormalTangentTexture(pos + new Vector3(+hw, +hh, +hd), normal, tangent, VectorU1V1));
            _vertices.Add(new PositionNormalTangentTexture(pos + new Vector3(-hw, +hh, -hd), normal, tangent, VectorU0V0));
            _vertices.Add(new PositionNormalTangentTexture(pos + new Vector3(+hw, +hh, -hd), normal, tangent, VectorU1V0));
            const ushort topOffset = 16;
            addIndices(new ushort[] { topOffset + 2, topOffset + 1, topOffset + 0, topOffset + 2, topOffset + 3, topOffset + 1 });

            //
            // bottom
            //
            color = Color.DarkGreen.ToColor4();
            normal = -Vector3.UnitY;
            tangent = Vector3.UnitX;

            _vertices.Add(new PositionNormalTangentTexture(pos + new Vector3(+hw, -hh, +hd), normal, tangent, VectorU1V0));
            _vertices.Add(new PositionNormalTangentTexture(pos + new Vector3(-hw, -hh, +hd), normal, tangent, VectorU0V0));
            _vertices.Add(new PositionNormalTangentTexture(pos + new Vector3(+hw, -hh, -hd), normal, tangent, VectorU1V1));
            _vertices.Add(new PositionNormalTangentTexture(pos + new Vector3(-hw, -hh, -hd), normal, tangent, VectorU0V1));

            const ushort bottomOffset = 20;
            addIndices(new ushort[] { bottomOffset + 0, bottomOffset + 2, bottomOffset + 1, bottomOffset + 1, bottomOffset + 2, bottomOffset + 3 });

            var cubeMaterial = new Material
            {
                AlphaTextureName = string.Empty,
                DiffuseTextureName = "Textures\\T_Default_D",
                NormalTextureName = "Textures\\T_Default_N",
                DiffuseColor = Color.Green,
                AmbientColor = Color.DarkGreen,
            };

            var cubeMesh = new ModelMesh(_model, cubeMaterial, indices);
            _cubeMeshes.Add(cubeMesh);
        }


        public void Begin()
        {
            var graphicsComponent = GetComponent<GraphicsComponent>();
            if (graphicsComponent != null)
            {
                graphicsComponent.Dispose();
            }
        }

        public CubeEntity(Entity parent)
            : base(parent)
        {
            _vertices = new TypedBuffer<PositionNormalTangentTexture>(32, PrimitiveTopology.TriangleList);
            _cubeMeshes = new List<ModelMesh>(8);
        }

        public void End()
        {
            if (_model != null)
            {
                _model.Dispose();
            }
            _model = new Model("", _vertices);
            foreach (var cubeMesh in _cubeMeshes)
            {
                cubeMesh.Model = _model;
            }
            _model.Meshes.AddRange(_cubeMeshes);

            _modelInstance = new ModelInstance(_model, Matrix.Identity);

           Add(new GraphicsComponent(_modelInstance));
        }

        /*
        public static Model CreateCube(string name, float width, float height, float depth)
        {

            float hw = width / 2, hh = height / 2, hd = depth / 2;

            var normal = Vector3.UnitZ;
            var tangent = -Vector3.UnitX;

            //front top left
            vertices.Add(new VertexPositionNormalTextureTangent(new Vector3(-hw, +hh, hd), normal,
                                                                new Vector2(0, 0), tangent));
            //front top right
            vertices.Add(new VertexPositionNormalTextureTangent(new Vector3(+hw, +hh, hd), normal,
                                                                new Vector2(1, 0), tangent));
            //front bottom left
            vertices.Add(new VertexPositionNormalTextureTangent(new Vector3(-hw, -hh, hd), normal,
                                                                new Vector2(0, 1), tangent));
            //front bottom right
            vertices.Add(new VertexPositionNormalTextureTangent(new Vector3(+hw, -hh, hd), normal,
                                                                new Vector2(1, 1), tangent));

            indices.Add(0, 1, 2, 1, 3, 2);

            normal = -Vector3.UnitZ;
            tangent = Vector3.UnitX;
            //back top left
            vertices.Add(new VertexPositionNormalTextureTangent(new Vector3(-hw, +hh, -hd), normal,
                                                                new Vector2(0, 0), tangent));
            //back top right
            vertices.Add(new VertexPositionNormalTextureTangent(new Vector3(+hw, +hh, -hd), normal,
                                                                new Vector2(1, 0), tangent));
            //back bottom left
            vertices.Add(new VertexPositionNormalTextureTangent(new Vector3(-hw, -hh, -hd), normal,
                                                                new Vector2(0, 1), tangent));
            //back bottom right
            vertices.Add(new VertexPositionNormalTextureTangent(new Vector3(+hw, -hh, -hd), normal,
                                                                new Vector2(1, 1), tangent));
            const int backOffs = 4;
            indices.Add(backOffs + 0, backOffs + 2, backOffs + 1, backOffs + 1, backOffs + 2, backOffs + 3);

            normal = -Vector3.UnitX;
            tangent = Vector3.UnitZ;
            //left top back
            vertices.Add(new VertexPositionNormalTextureTangent(new Vector3(-hw, +hh, -hd), normal,
                                                                new Vector2(0, 0), tangent));
            //left top front
            vertices.Add(new VertexPositionNormalTextureTangent(new Vector3(-hw, +hh, +hd), normal,
                                                                new Vector2(1, 0), tangent));
            //left bottom back
            vertices.Add(new VertexPositionNormalTextureTangent(new Vector3(-hw, -hh, -hd), normal,
                                                                new Vector2(0, 1), tangent));
            //left bottom front
            vertices.Add(new VertexPositionNormalTextureTangent(new Vector3(-hw, -hh, +hd), normal,
                                                                new Vector2(1, 1), tangent));
            const int leftOffs = 8;
            indices.Add(leftOffs + 0, leftOffs + 1, leftOffs + 2, leftOffs + 1, leftOffs + 3, leftOffs + 2);

            normal = Vector3.UnitX;
            tangent = -Vector3.UnitZ;
            //right top back
            vertices.Add(new VertexPositionNormalTextureTangent(new Vector3(hw, +hh, -hd), normal,
                                                                new Vector2(0, 0), tangent));
            //right top front
            vertices.Add(new VertexPositionNormalTextureTangent(new Vector3(hw, +hh, +hd), normal,
                                                                new Vector2(1, 0), tangent));
            //right bottom back
            vertices.Add(new VertexPositionNormalTextureTangent(new Vector3(hw, -hh, -hd), normal,
                                                                new Vector2(0, 1), tangent));
            //right bottom front
            vertices.Add(new VertexPositionNormalTextureTangent(new Vector3(hw, -hh, +hd), normal,
                                                                new Vector2(1, 1), tangent));
            const int rightOffs = 12;
            indices.Add(rightOffs + 0, rightOffs + 2, rightOffs + 1, rightOffs + 1, rightOffs + 2, rightOffs + 3);

            normal = Vector3.UnitY;
            tangent = Vector3.UnitX;
            //top front left
            vertices.Add(new VertexPositionNormalTextureTangent(new Vector3(-hw, +hh, +hd), normal,
                                                                new Vector2(0, 0), tangent));
            //top front right
            vertices.Add(new VertexPositionNormalTextureTangent(new Vector3(+hw, +hh, +hd), normal,
                                                                new Vector2(1, 0), tangent));
            //top back left
            vertices.Add(new VertexPositionNormalTextureTangent(new Vector3(-hw, +hh, -hd), normal,
                                                                new Vector2(0, 1), tangent));
            //top back right
            vertices.Add(new VertexPositionNormalTextureTangent(new Vector3(+hw, +hh, -hd), normal,
                                                                new Vector2(1, 1), tangent));
            const int topOffs = 16;
            indices.Add(topOffs + 0, topOffs + 2, topOffs + 1, topOffs + 1, topOffs + 2, topOffs + 3);

            normal = -Vector3.UnitY;
            tangent = Vector3.UnitX;
            //bottom front left
            vertices.Add(new VertexPositionNormalTextureTangent(new Vector3(-hw, -hh, +hd), normal,
                                                                new Vector2(0, 0), tangent));
            //bottom front right
            vertices.Add(new VertexPositionNormalTextureTangent(new Vector3(+hw, -hh, +hd), normal,
                                                                new Vector2(1, 0), tangent));
            //bottom back left
            vertices.Add(new VertexPositionNormalTextureTangent(new Vector3(-hw, -hh, -hd), normal,
                                                                new Vector2(0, 1), tangent));
            //bottom back right
            vertices.Add(new VertexPositionNormalTextureTangent(new Vector3(+hw, -hh, -hd), normal,
                                                                new Vector2(1, 1), tangent));

            const int bottomOffs = 20;
            indices.Add(bottomOffs + 0, bottomOffs + 1, bottomOffs + 2, bottomOffs + 1, bottomOffs + 3, bottomOffs + 2);

            var model = new Model(name) { Vertices = vertices };
            model.Meshes.Add(new ModelMesh(model, null) { IndexContainer = indices });
            return model;
        }
        */
    }
}
