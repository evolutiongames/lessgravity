﻿using LessGravity.Quasar.Assets;
using LessGravity.Quasar.Entities;
using LessGravity.Quasar.Graphics;
using LessGravity.Quasar.Graphics.Blocks;
using SharpDX;
using SharpDX.Direct3D11;
using System;
using System.Collections.Generic;

namespace LessGravity.Quasar.TestWindow.Game
{
    class BlockMeshEntity : Entity
    {
        private Dictionary<string, Dictionary<BlockFace, Point>> _blockMeshTextures;
        private BlockMesh _blockMesh;
        private ModelInstance _blockMeshInstance;

        public Matrix WorldMatrix
        {
            get => _blockMesh.WorldMatrix;
            set => _blockMesh.WorldMatrix = value;
        }

        public void AddBlock(BlockPosition location, string material)
        {
            _blockMesh.AddBlock(location, material);
            _blockMesh.BuildMesh(_blockMeshTextures);
        }

        public BlockMeshEntity(Entity parent, Device device, AssetManager assets)
            : base(parent)
        {
            var random = new Random();
            _blockMesh = new BlockMesh(device, assets, "Textures\\T_Default", 512);
            /*
            for (var i = 0; i < 20; ++i)
            {
                var randomVector = random.NextVector3(-10 * Vector3.One, 10 * Vector3.One).ToBlockPosition();
                _blockMesh.AddBlock(randomVector, "MAT_BlockMesh");
            }
            */
            _blockMesh.AddBlock(2, 0, 0, "MAT_BlockMesh");
            _blockMesh.AddBlock(2, 0, 0, "MAT_BlockMesh");
            _blockMesh.AddBlock(4, 2, 0, "MAT_BlockMesh");
            _blockMesh.AddBlock(5, 3, 0, "MAT_BlockMesh");

            _blockMeshTextures = new Dictionary<string, Dictionary<BlockFace, Point>>
            {
                {
                    "MAT_BlockMesh", new Dictionary<BlockFace, Point>
                    {
                        { BlockFace.Top, new Point(0, 0) },
                        { BlockFace.Bottom, new Point(0, 0) },
                        { BlockFace.Left, new Point(0, 0) },
                        { BlockFace.Right, new Point(0, 0) },
                        { BlockFace.Front, new Point(0, 0) },
                        { BlockFace.Back, new Point(0, 0) },
                    }
                }
            };
            _blockMesh.BuildMesh(_blockMeshTextures);

            _blockMeshInstance = new ModelInstance(_blockMesh, Matrix.Identity);

            Add(new GraphicsComponent(_blockMeshInstance));
        }

        public override void Dispose()
        {
            base.Dispose();

            _blockMesh.Dispose();
        }

        public bool RayCastIntersect(Ray ray, out Vector3? hitPosition, out Vector3? hitNormal)
        {
            return _blockMesh.RayCastIntersect(ray, _blockMesh.WorldMatrix, out hitPosition, out hitNormal);
        }

        public void RemoveBlock(BlockPosition location)
        {
            _blockMesh.RemoveBlock(location);
            _blockMesh.BuildMesh(_blockMeshTextures);
        }
    }
}