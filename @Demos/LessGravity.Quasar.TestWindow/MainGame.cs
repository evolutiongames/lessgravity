﻿using LessGravity.Quasar.Extensions;
using LessGravity.Quasar.Game;
using LessGravity.Quasar.Graphics;
using LessGravity.Quasar.TestWindow.Game;
using LessGravity.Quasar.Types;
using SharpDX;
using SharpDX.Direct2D1;
using SharpDX.Direct3D;
using SharpDX.Direct3D11;
using SharpDX.DirectInput;
using SharpDX.DirectWrite;
using System.Windows.Forms;
using LessGravity.Logging;

namespace LessGravity.Quasar.TestWindow
{
    internal sealed class MainGame : QuasarGame
    {
        private SolidColorBrush _rectangleColor; // BGRA
        private RoundedRectangleGeometry _rectangleGeometry;

        private SolidColorBrush _debugFontColor;
        private RectangleF _debugFontRectangle;

        private SolidColorBrush _fontColor; // BGRA
        private TextFormat _font;

        private Vector3 _blockPositionToAdd;
        private Vector3 _blockPosition; // to remove any block
        private Matrix _cubeWorldMatrix;
        private Model _cubeModel;
        private ModelInstance _cubeModelInstance;

        private BlockMeshEntity _blockMeshEntity;
        private CubeEntity _cubeEntity;

        private Color4 _clearColor; // RGBA

        private Shader _vsResolveGBuffer;
        private Shader _psResolveGBuffer;

        private SamplerState _linearSampler;
        private Texture _textureCubeSky;

        private RasterizerState _cullNoCullWireframeRasterizerState;
        private RasterizerState _cullBackCCWRasterizerState;
        private RasterizerState _cullNoneRasterizerState;
        private BlendState _opaqueBlendState;

        private readonly Vector3[] _farFrustumCorners = new Vector3[4];
        private Vector3[] _farFrustumCornersTransformed = new Vector3[4];
        private PackedUInt2 _screenSize;

        private Camera _camera;

        public MainGame(ILogger logger, IFormFactory formFactory)
            : base(logger, formFactory)
        {
            
        }

        protected override void BeginDraw()
        {
            DeviceContext.ClearRenderTargetView(GBuffer1, _clearColor);
            DeviceContext.ClearRenderTargetView(GBuffer2, _clearColor);
            DeviceContext.ClearRenderTargetView(GBuffer3, _clearColor);
            DeviceContext.ClearDepthStencilView(DepthBuffer, DepthStencilClearFlags.Depth, 1.0f, 0);
            DeviceContext.Rasterizer.SetViewport(_camera.Viewport);
        }

        protected override void Dispose(bool disposeManagedResources)
        {
            if (disposeManagedResources)
            {
                _rectangleColor.DisposeIfNotDisposed();
                _rectangleGeometry.DisposeIfNotDisposed();
                _fontColor.DisposeIfNotDisposed();
                _font.DisposeIfNotDisposed();
                _debugFontColor.DisposeIfNotDisposed();

                _cubeModel?.Dispose();
                _vsResolveGBuffer.Dispose();
                _psResolveGBuffer.Dispose();

                _textureCubeSky.Dispose();
                _linearSampler.DisposeIfNotDisposed();
                _opaqueBlendState.DisposeIfNotDisposed();
                _cullNoneRasterizerState.DisposeIfNotDisposed();
                _cullBackCCWRasterizerState.DisposeIfNotDisposed();
                _cullNoCullWireframeRasterizerState.DisposeIfNotDisposed();
            }
            base.Dispose(disposeManagedResources);
        }

        private void RenderPass_1_Geometry()
        {
            //
            // Geometry Pass
            //
            DeviceContext.ClearDepthStencilView(DepthBuffer, DepthStencilClearFlags.Depth, 1.0f, 0);
            DeviceContext.OutputMerger.SetTargets((DepthStencilView)DepthBuffer, GBuffer1, GBuffer2, GBuffer3);
            DeviceContext.Rasterizer.SetViewport(Viewport);
            DeviceContext.Rasterizer.State = _cullBackCCWRasterizerState;
            //DeviceContext.Rasterizer.State = _cullNoCullWireframeRasterizerState;
            Entities.Draw(_camera);
            //DeviceContext.Rasterizer.State = _cullBackCCWRasterizerState;
        }

        private void RenderPass_2_Lights()
        {
            // 
            // Lights Pass
            //
        }

        private void RenderPass_3_ResolveGBuffer()
        {
            //
            // Resolve GBuffer
            //
            DeviceContext.OutputMerger.SetTargets(renderTargetView: BackBuffer);
            //DeviceContext.ClearRenderTargetView(BackBuffer, _clearColor);
            DeviceContext.OutputMerger.BlendState = _opaqueBlendState;
            DeviceContext.InputAssembler.PrimitiveTopology = PrimitiveTopology.TriangleStrip;
            DeviceContext.InputAssembler.InputLayout = null;

            var viewFrustum = new BoundingFrustum(_camera.ViewMatrix * _camera.ProjectionMatrix);
            var corners = viewFrustum.GetCorners();
            _farFrustumCorners[0] = corners[7];
            _farFrustumCorners[1] = corners[6];
            _farFrustumCorners[2] = corners[4];
            _farFrustumCorners[3] = corners[5];
            var viewMatrix = _camera.ViewMatrix;
            Vector3.TransformCoordinate(_farFrustumCorners, ref viewMatrix, _farFrustumCornersTransformed);
            _farFrustumCornersTransformed = _farFrustumCorners;

            _vsResolveGBuffer.Parameters["FrustumCorners"].SetValue(_farFrustumCornersTransformed);
            _vsResolveGBuffer.BindBuffers();

            _psResolveGBuffer.Parameters["ScreenSize"].SetValue(_screenSize);
            _psResolveGBuffer.Parameters["FrustumCorners"].SetValue(_farFrustumCornersTransformed);
            _psResolveGBuffer.Parameters["InvertView"].SetValue(Matrix.Invert(viewMatrix));
            _psResolveGBuffer.Parameters["CameraPosition"].SetValue(_camera.Position);
            _psResolveGBuffer.BindBuffers();
            _psResolveGBuffer.SetSamplerState("SMP_Linear", _linearSampler);
            _psResolveGBuffer.SetResource("TEX_GBuffer1", GBuffer1);
            _psResolveGBuffer.SetResource("TEX_GBuffer2", GBuffer2);
            _psResolveGBuffer.SetResource("TEX_GBuffer3", GBuffer3);
            _psResolveGBuffer.SetResource("TXC_Sky", _textureCubeSky);

            //
            // Draw Fullscreen-Quad
            //
            DeviceContext.VertexShader.Set(_vsResolveGBuffer);
            DeviceContext.PixelShader.Set(_psResolveGBuffer);
            DeviceContext.Rasterizer.State = _cullNoneRasterizerState;
            DeviceContext.Draw(4, 0);
            _psResolveGBuffer.SetResource("TEX_GBuffer1", null);
            _psResolveGBuffer.SetResource("TEX_GBuffer2", null);
            _psResolveGBuffer.SetResource("TEX_GBuffer3", null);
        }

        private void RenderPass_4_PostFx()
        {
            //
            // PostFX Pass
            //
        }

        private void RenderPass_Z_UI(GameTime gameTime)
        {
            /*
            var red = (float)Math.Abs(Math.Cos(gameTime.ElapsedSeconds));
            var green = (float)Math.Abs(Math.Sin(gameTime.ElapsedSeconds * 3));
            var blue = (float)Math.Abs(Math.Cos(gameTime.ElapsedSeconds * 0.5f));
            _rectangleColor.Color = new Color4(red, green, red, red);
            BackBuffer2D.FillGeometry(_rectangleGeometry, _rectangleColor);

            var fontRectangle = new RectangleF(100, RenderingSize.Height - 460, 200, 50);
            _fontColor.Color = new Color4(green, blue, red, 1.0f);
            BackBuffer2D.DrawText($"{_blockPosition}", _font, fontRectangle, _fontColor);
            */
        }

        private void RenderPass_Z_UI_Debug(GameTime gameTime)
        {
#if DEBUG
            BackBuffer2D.DrawText($"Development {FramesPerSecond:0.00} FPS", _font, _debugFontRectangle, _debugFontColor);
#else
            BackBuffer2D.DrawText($"{FramesPerSecond:0.00} FPS", _font, _debugFontRectangle, _debugFontColor);
#endif
        }

        protected override void Draw(GameTime gameTime)
        {
            var time = gameTime.ElapsedSeconds;

            RenderPass_1_Geometry();
            RenderPass_2_Lights();
            RenderPass_3_ResolveGBuffer();
            RenderPass_4_PostFx();

            if (Settings.Support2d)
            {
                Draw2d(gameTime);
            }
        }

        private void Draw2d(GameTime gameTime)
        {
            //
            // Draw 2D
            //
            BackBuffer2D.BeginDraw();
            RenderPass_Z_UI_Debug(gameTime);
            RenderPass_Z_UI(gameTime);
            BackBuffer2D.EndDraw();
        }

        protected override void Initialize(GameSettings settings)
        {
            base.Initialize(settings);
            _camera = new Camera(settings.Width, settings.Height)
            {
                Position = new Vector3(0, 0, 5)
            };
            MainCamera = _camera;

            _screenSize = new PackedUInt2
            {
                X = (uint)settings.Width,
                Y = (uint)settings.Height
            };
        }

        protected override void KeyUp(KeyEventArgs e)
        {
            base.KeyUp(e);

            if (e.KeyCode == Keys.T)
            {
                ToggleFullscreen();
            }
        }

        protected override void LoadContent()
        {
            base.LoadContent();

            _rectangleColor = new SolidColorBrush(BackBuffer2D, Color.Firebrick);

            var rectangle = new RoundedRectangle
            {
                RadiusX = 64,
                RadiusY = 64,
                Rect = new RectangleF(32, RenderingSize.Height - 160, Settings.Width - 64, 100)
            };
            _rectangleGeometry = new RoundedRectangleGeometry(Direct2dFactory, rectangle);

            _fontColor = new SolidColorBrush(BackBuffer2D, Color.Fuchsia);
            _font = new TextFormat(DirectWriteFactory, "Source Code Pro Light", FontWeight.Light, FontStyle.Normal, 16.0f);

            _debugFontRectangle = new RectangleF(8, 8, 256, 64);
            _debugFontColor = new SolidColorBrush(BackBuffer2D, Color.White);

            _clearColor = new Color4(0.25f, 0.0f, 0.0f, 1.0f);

            _textureCubeSky = Assets.Load<TextureCube>($"{Assets.RootDirectory}\\Textures\\TC_AboveClouds");
            _linearSampler = SamplerStates.CreateLinearClampSamplerState(Device);
            _cullBackCCWRasterizerState = RasterizerStates.CreateCullCCWRasterizerState(Device);
            _cullNoneRasterizerState = RasterizerStates.CreateNoCullRasterizerState(Device);
            _cullNoCullWireframeRasterizerState = RasterizerStates.CreateNoCullWireFrameRasterizerState(Device);
            _opaqueBlendState = BlendStates.CreateOpaqueBlendState(Device);

            _vsResolveGBuffer = Assets.Load<Shader>("Shaders\\ResolveGBuffer.vs");
            _psResolveGBuffer = Assets.Load<Shader>("Shaders\\ResolveGBuffer.ps");

            /*
            _cubeModel = Model.CreateCube(0.5f, 0.5f, 0.5f);
            _cubeModelInstance = new ModelInstance(_cubeModel, Matrix.Identity * Matrix.Translation(0, 0, -5));
            var cubeEntity = new Entity(null);
            cubeEntity.Add(new GraphicsComponent(_cubeModelInstance));
            Entities.AddEntity(cubeEntity);
            */

            _blockMeshEntity = new BlockMeshEntity(null, Device, Assets);
            Entities.AddEntity(_blockMeshEntity);
            /*
            _cubeEntity = new CubeEntity(null);
            _cubeEntity.Begin();
            _cubeEntity.AddCube(0, 0, 0, 2.0f, 0.5f, 0.5f);
            _cubeEntity.AddCube(0, 0, 3, 1.0f, 4.0f, 0.5f);
            _cubeEntity.End();
            Entities.AddEntity(_cubeEntity);
            */
        }

        protected override void Resize()
        {
            base.Resize();

            _rectangleGeometry.Dispose();

            var rectangle = new RoundedRectangle
            {
                RadiusX = 64,
                RadiusY = 64,
                Rect = new RectangleF(32, RenderingSize.Height - 60, RenderingSize.Width - 64, 50)
            };

            _camera.Viewport = new Viewport(0, 0, RenderingSize.Width, RenderingSize.Height);
            _rectangleGeometry = new RoundedRectangleGeometry(Direct2dFactory, rectangle);
            var screenSize = new PackedUInt2
            {
                X = (uint)RenderingSize.Width,
                Y = (uint)RenderingSize.Height
            };
        }

        protected override void Update(GameTime gameTime)
        {
            base.Update(gameTime);

            InputManager.Update();

            const float modifier = 0.001f;
            Vector3? hitPos;
            Vector3? hitNormal;

            const float gridSize = 1.0f;
            if (_blockMeshEntity != null)
            {
                if (_blockMeshEntity.RayCastIntersect(MouseRay, out hitPos, out hitNormal))
                {
                    var hit = hitPos.Value;

                    //
                    // move inside the cube
                    //
                    hit = hit - 0.5f * gridSize * hitNormal.Value;
                    _blockPosition = hit.Floor(gridSize);
                    _blockPositionToAdd = _blockPosition + Vector3.One * 0.5f * gridSize + hitNormal.Value * gridSize;
                }
                else
                {
                    _blockPosition = Vector3.Zero;
                    _blockPositionToAdd = Vector3.Zero;
                }
            }
            _cubeWorldMatrix = Matrix.Translation(_blockPositionToAdd);
            if (_cubeModelInstance != null)
            {
                _cubeModelInstance.WorldMatrix = _cubeWorldMatrix;
            }

            if (InputManager.MouseButtonPressed(2)) // middle mouse button
            {
                _camera.RotateMouseX(-InputManager.MouseDeltaX * modifier);
                _camera.RotateMouseY(InputManager.MouseDeltaY * modifier);
            }
            if (InputManager.MouseButtonClicked(0)) // add block
            {
                if (_blockPositionToAdd != Vector3.Zero)
                {
                    _blockMeshEntity?.AddBlock(_blockPositionToAdd.ToBlockPosition(), "MAT_BlockMesh");
                }
            }
            if (InputManager.MouseButtonClicked(1)) // remove block
            {
                if (_blockPosition != Vector3.Zero)
                {
                    _blockMeshEntity?.RemoveBlock(_blockPosition.ToBlockPosition());
                }
            }
            if (InputManager.KeyPressed(Key.W))
            {
                _camera.MoveForward(+gameTime.ElapsedSeconds * modifier);
            }
            if (InputManager.KeyPressed(Key.S))
            {
                _camera.MoveForward(-gameTime.ElapsedSeconds * modifier);
            }
            if (InputManager.KeyPressed(Key.A))
            {
                _camera.Strafe(-gameTime.ElapsedSeconds * modifier);
            }
            if (InputManager.KeyPressed(Key.D))
            {
                _camera.Strafe(+gameTime.ElapsedSeconds * modifier);
            }
            if (InputManager.KeyPressed(Key.Q))
            {
                _camera.Lift(+gameTime.ElapsedSeconds * modifier);
            }
            if (InputManager.KeyPressed(Key.E))
            {
                _camera.Lift(-gameTime.ElapsedSeconds * modifier);
            }

        }
    }
}