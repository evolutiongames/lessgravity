#LessGravity

Build status
------------
[![Build Status](https://travis-ci.org/deccer/LessGravity.svg)](https://github.com/deccer/LessGravity)

This solution contains yet another dx11 engine built from scratch. 2D and 3D is supported.

##Contents of the Solution

###LessGravity.Quasar
the actual engine

###LessGravity.Network (discontinued)
simple networking code

#@Demos

##Graphics
###LessGravity.Quasar.TestWindow
the first application making use of Quasar

##Networking
###LessGravity.Network.TestClient
networking testclient, based on packages

###LessGravity.Network.TestServer
networking testserver, based on packages

#@Games

##OpenSpace

###LessGravity.OpenSpace
the actual space game

###LessGravity.OpenSpace.Data

###LessGravity.OpenSpace.Data.Editor

###LessGravity.OpenSpace.Server

###LessGravity.OpenSpace.Shared

