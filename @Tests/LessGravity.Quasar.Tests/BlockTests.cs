﻿using LessGravity.Quasar.Graphics.Blocks;
using SharpDX.Direct3D;
using SharpDX.Direct3D11;
using Xunit;

namespace LessGravity.Quasar.Tests
{
    class BlockTests
    {
        [Fact]
        public void AddAndRemoveBlock()
        {
            using (var device = new Device(DriverType.Hardware))
            {
                var blockMesh = new BlockMesh(device, null, string.Empty, 32);
                blockMesh.AddBlock(BlockPosition.Zero, string.Empty);
                blockMesh.RemoveBlock(BlockPosition.Zero);

                Assert.True(0 == blockMesh.BlockCount);
            }
        }
    }
}