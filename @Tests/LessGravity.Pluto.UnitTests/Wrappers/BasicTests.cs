﻿using System;
using LessGravity.OpenSpace.Data.Celestials;
using LessGravity.OpenSpace.Data.Editor.Wrappers;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace LessGravity.Pluto.UnitTests.Wrappers
{
    [TestClass]
    public class BasicTests
    {
        private Universe _universe;

        [TestInitialize]
        public void Initialize()
        {
            _universe = new Universe
            {
                Id = 0xDEADBEEF,
                Name = "Universe 1"
            };
        }

        [TestMethod]
        public void ShouldContainModelInModelProperty()
        {
            var wrapper = new UniverseWrapper(_universe);
            Assert.AreEqual(_universe, wrapper.Model);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void ShouldThrowArgumentNullExceptionIfModelIsNull()
        {
            try
            {
                var wrapper = new UniverseWrapper(null);
            }
            catch (ArgumentNullException ex)
            {
                Assert.AreEqual("model", ex.ParamName);
                throw;
            }
        }
    }
}